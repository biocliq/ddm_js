/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/layout/FieldSet",
			[ "dojo/_base/declare",
			"ddm/zui/page/base/Loader",
			"dijit/Fieldset",
	        'dojo/text!./templates/FieldSet.html'],
function(declare, loader,FieldSet,template) {

    var base = [ FieldSet,loader];

	return declare("ddm.zui.layout.FieldSet", base, {
		baseClass: "mchartConentPane",
		templateString:template,
		
		resize:function(newSize){}
	});

});
