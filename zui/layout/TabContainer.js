/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/layout/TabContainer", [ 'dojo/_base/declare',"dojo/_base/lang",
	"dijit/layout/TabController","dijit/layout/utils" ,"dojo/dom-geometry",
	"dijit/layout/TabContainer"], 
		function(declare,lang, TabController,layoutUtils,domGeometry,TabContainer) {

	var base = [TabContainer];

	return declare("ddm.zui.layout.TabContainer", base, {
		
	});

});