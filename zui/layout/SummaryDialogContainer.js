define("ddm/zui/layout/SummaryDialogContainer", 
	[   'dojo/_base/declare',
		'dojo/_base/lang',	"dojo/topic",
		"ddm/wire/store/base/JsonddmStore", "ddm/zui/widget/SummaryDialog"
	],
	function( declare,lang, topic,ddmStore, SummaryDialog) {
	var base = [];
    
    return declare('ddm.zui.layout.SummaryDialogContainer', base, {
            constructor:function(menuCode,callback, options){                
                var _options = options || {};
                
                var citStore = new ddmStore({
                    page : menuCode
                    });
                    citStore.getSummaryTab().then(function(item){
                    if(item.center && item.center[0].containers){
                        var container = item.center[0].containers[0];
                        var options = {
                                pageMenu : menuCode,
                                style : "height:100%",
                                autoHeight : true,
                                data : container,
                                callback:callback,
                                permission:container.permission};
                            
                        if(_options.source){
                            options.parentSource = _options.source;
                            if(_options.source.itemId)
                            options.defaultFilter = {__itemId : _options.source.itemId};
                        }
                        var dialog = new SummaryDialog(options);
                        dialog.show();
                    }
                });
            }

		});
	}
);
