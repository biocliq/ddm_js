/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/layout/StackContainer", [ 'dojo/_base/declare',"dojo/_base/lang",
	"dijit/layout/TabController","dijit/layout/utils" ,"dojo/dom-geometry",
	"dijit/layout/StackContainer"], 
		function(declare,lang, TabController,layoutUtils,domGeometry,StackContainer) {

	var base = [StackContainer];

	return declare("ddm.zui.layout.StackContainer", base, {
        // layout: function(){
		// 	var child = this.selectedChildWidget;
            
		// 	if(child && child.resize){
		// 		if(this.doLayout){
        //                 if(child.calcHeight){
        //                     console.log("Calculating height");
        //                     child.calcHeight();
        //                     child.resize();
        //                 }else
		// 			        child.resize(this._containerContentBox || this._contentBox);
		// 		}else{
		// 			child.resize();
		// 		}
		// 	}
		// }
	});

});