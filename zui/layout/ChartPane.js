/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/layout/ChartPane",[
	"dojo/_base/array", // array.forEach
	"dojo/on","dojo/_base/lang",
	"dojo/_base/declare", // declare
		'dijit/_TemplatedMixin',
	    'dijit/_WidgetsInTemplateMixin',
    "dijit/TitlePane", "dijit/layout/ContentPane",
	"dojo/text!./templates/ChartPane.html", 
	"ddm/zui/form/RangeSelector"
    
], function(array, on,lang,declare,_TemplatedMixin,_WidgetsInTemplateMixin, TitlePane,ContentPane, template ){

    var base = [TitlePane, _TemplatedMixin,_WidgetsInTemplateMixin, ContentPane]

	return declare("ddm.zui.layout.ChartPane",base, {
		templateString:template,
		widgetsInTemplate: true,
		postCreate:function(){
			this.own(on(this.rangeSelector, 'change', lang.hitch(this,function(data){
                    this.onRangeChange(data);
				})));
		},
		onRangeChange:function(){},
		resize:function(){
			// this.containerNode.resize(arguments);
		},
		_layoutChildren: function(){
			var cb = this._contentBox || domGeometry.getContentBox(this.containerNode);
			var children = this.getChildren(),
				widget,
				i = 0;
			while(widget = children[i++]){
				if(widget.resize){
					widget.resize({w: cb.w});
				}
			}			
		}
	});
});
