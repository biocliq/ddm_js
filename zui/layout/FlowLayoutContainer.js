/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/layout/FlowLayoutContainer", [ "dojo/_base/array",
	"dojo/_base/declare", // declare
    "dojo/dom-class",
    "dojo/dom-geometry",
	"dojo/dom-style",
    "dojo/_base/lang",
    "dijit/Fieldset",
	"dijit/_WidgetBase",
	"dijit/layout/_LayoutWidget",], 
		function(array, declare, domClass,domGeometry, domStyle, lang, Fieldset,_WidgetBase, _LayoutWidget) {

	var base = [_LayoutWidget];


	return declare("ddm.zui.layout.FlowLayoutContainer", base, {
        baseClass: "dijitFlowLayoutContainer",
        startup: function(){			
			if(this._started){
				return;
			}
			array.forEach(this.getChildren(), this._setupChild, this);
			this.inherited(arguments);			
		},
        _setupChild: function(/*dijit/_WidgetBase*/ child){
		  this.inherited(arguments);
        	domClass.add(child.domNode, this.baseClass + "-child");
        },
        
        addChild: function(/*dijit/_WidgetBase*/ child, /*Integer?*/ insertIndex){
			this.inherited(arguments);
			domClass.add(child.domNode, this.baseClass + "-child");
			if(this._started){
				this.layout();
			}
		},

        layout: function(){			
          this.layoutChildren(this.containerNode, this._contentBox, this.getChildren());
		},

		removeChild: function(/*dijit/_WidgetBase*/ child){
			this.inherited(arguments);
			if(this._started){
				this.layout();
			}

            domClass.remove(child.domNode, this.baseClass + "-child");
			domStyle.set(child.domNode, {
				top: "auto",
				bottom: "auto",
				left: "auto",
				right: "auto",
				position: "static"
			});
			domStyle.set(child.domNode, /top|bottom/.test(child.region) ? "width" : "height", "auto");
        },
		
		getContentBoxDimension:function(){
			if(this.dim)
				return this.dim;
			else{
				var container = this.containerNode;
				this.dim = this.marginBox2contentBox(container);
			}
			return this.dim;	
		},

        marginBox2contentBox: function(/*DomNode*/ node){
			var cs = domStyle.getComputedStyle(node);
			var me = domGeometry.getMarginExtents(node, cs);
			var pb = domGeometry.getPadBorderExtents(node, cs);
			var mb = domGeometry.getContentBox(node, cs);
			return {
				l: domStyle.toPixelValue(node, cs.paddingLeft),
				t: domStyle.toPixelValue(node, cs.paddingTop),
				w: mb.w - (me.w + pb.w),
				h: mb.h - (me.h + pb.h)
			};
		},

		size : function(widget, dim){
			// size the child
			var newSize = widget.resize ? widget.resize(dim) : domGeometry.setMarginBox(widget.domNode, dim);
			// record child's size
			if(newSize){
				// if the child returned it's new size then use that
				lang.mixin(widget, newSize);
			}else{
				// otherwise, call getMarginBox(), but favor our own numbers when we have them.
				// the browser lies sometimes
				lang.mixin(widget, domGeometry.getMarginBox(widget.domNode));
				lang.mixin(widget, dim);
			}
		},

        layoutChildren: function(/*DomNode*/ container, /*Object*/ dim, /*Widget[]*/ children){
			var sdim = this.marginBox2contentBox(container);			
			var _this = this;				
			array.forEach(children, function(child){				
				var width = child.widthPercent ? child.widthPercent : 100;
				var height = child.heightPercent;
				
				width = (sdim.w) * width / 100;

				if(height){
					if(isNaN(height)){
						height = height.replace(/\D+/g, "");						
					}else{
						height = sdim.h * height / 100;						
					}
					_this.size(child, {w: (width - 3), h:height});
				}else{					
					if(child.autoHeight)
						_this.size(child, {w: (width - 3), h:sdim.h});	
					else
						_this.size(child, {w: (width-3)});
				}
			});
        }
	});

});