/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/layout/HeaderContent", [ 'dojo/_base/declare',"dojo/_base/lang",
	 'dojo/request/xhr',"dojo/router","dojo/hash",
	"dijit/form/DropDownButton","dijit/TooltipDialog","ddm/util/config",
	"dijit/layout/ContentPane","dijit/layout/TabContainer",
	"dijit/layout/TabController" ,"../page/base/Base",  
	'../page/user/ChangepasswordDialog','../page/user/ChangeFacilityDialog',"ddm/wire/store/base/JsonUserStore",
	"ddm/wire/store/widget/FieldLookupStore","dojo/store/Memory",
	'dojo/text!./templates/HeaderContent.html',  
	"dijit/DropDownMenu", "ddm/fluwiz/widget/AccordionMenu", "dijit/form/FilteringSelect"
		], 
		function(declare,lang, xhr,router, hash,
				DropDownButton,TooltipDialog,config,
				ContentPane,TabContainer,TabController,
				 widgetBase, ChangepasswordDialog,ChangeFacilityDialog,				
				UserStore, FieldLookupStore,Memory, template) {

	var base = [ContentPane, widgetBase];
	

	return declare("ddm.zui.layout.HeaderContent", base, {
		templateString : template, 
				
		logout : function(event){
			dojo.stopEvent(event);
			xhr(config.url.logout, {handleAs: "json"})
				.then(function(data){
					location.href = config.url.loginPage;				  
				}, function(err){
					console.error(err);
				}, function(evt){
					console.error(evt);
				});
		},
		postCreate:function(){
			if (!this._started) {				
				this.populateUserDetails();
			}
			this.dialog = new ChangepasswordDialog();
			this.dialog.hide();
			//this.facilitydialog.hide();
			
		},
		populateUserDetails:function(){
			var userItem = new UserStore({});
				userItem.getUserItem().then(lang.hitch(this, function(item){
				if(null != item){
					this.userName.innerHTML=item.name;
				}
				if(0 == hash().length){
					if(item.homePage){
						router.go(item.homePage);
					}else
						router.go(config.url.defPage);
				}
			}));
		},
		recentpageViews:function(event){
			if (tabcontroller.innerHTML==""){
				this.recentview.innerHTML="No Recent Views";				
			}
			else{
				this.recentview.innerHTML="";			
			}
		},
		showDialog:function(){
			this.dialog.reset();
			this.dialog.show();
		},
		showFacilityDialog : function(){
		    this.facilitydialog = new ChangeFacilityDialog();
			this.facilitydialog.reset();
			this.facilitydialog.show();
		},
	});
});
