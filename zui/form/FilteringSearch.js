/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/form/FilteringSearch", ["dojo/_base/declare", "dojo/topic", "dojo/_base/lang", 'dojo/dom-class',
  "dijit/form/FilteringSelect", "ddm/util/util", "dojo/text!./templates/FilteringSearch.html",
  "ddm/wire/store/base/JsonddmStore", "ddm/zui/widget/SummaryDialog",
], function (declare, topic, lang, domClass, FilteringSelect, util, template, ddmStore, SummaryDialog) {

  var base = [FilteringSelect];

  return declare("ddm.zui.form.FilteringSearch", base, {
    templateString: template,
    targetPage: null,
    constructor: function (fieldvalue) {
      this.targetPage = fieldvalue.summaryPage;
    },
    openSummary: function () {
      var menuCode = this.targetPage;
      var itemId = this.itemId;
      if (this.targetPage != null)
        menuCode = this.targetPage;
      else
        return;
      var _this = this;
      var citStore = new ddmStore({
        page: menuCode
      });

      var callback = function (value, status) {
        var searchAttr = _this.refAttributeName;
        var item = this._grid.model.store.get(value);
        if (item.then) {
          item.then(function (data) {
            if (data[searchAttr])
              data[searchAttr] = data[searchAttr] + "";
            _this.set('item', data);
          });
        } else {
          if (item[searchAttr])
            item[searchAttr] = item[searchAttr] + "";
          _this.set('item', item);
        }
      }
      citStore.getSummaryTab().then(function (item) {
        if (item.center && item.center[0].containers) {
          var container = item.center[0].containers[0];
          var att = _this.refAttributeName;
          var value = _this.get('displayedValue');

          var options = {
            pageMenu: menuCode,
            style: "height:90%; width:75%;",
            autoHeight: true,
            data: container,
            callback: callback,
            permission: container.permission
          };

          options.defaultFilter = {};
          options.defaultFilter[att] = value + '*';

          if (itemId)
            options.itemId = itemId;
          var dialog = new SummaryDialog(options);
          dialog.show();
        }
      });

    },
    _setDisabledAttr: function (flag) {
      this.inherited(arguments);
      if (flag == 1)
        domClass.add(this.searchIcon, 'dijitSearchIcon');
      else
        domClass.remove(this.searchIcon, 'dijitSearchIcon');
    },


  });
});

