/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 define("ddm/zui/form/BooleanFilteringSelect", [ "dojo/_base/declare",
 "dojo/_base/array",
 "dijit/form/FilteringSelect","dojo/store/Memory" ], 
		function(declare,array, FilteringSelect, MemoryStore) {

	var base = FilteringSelect;

	return declare("ddm.zui.form.BooleanFilteringSelect", base, {
		postMixInProperties:function(){			
			var data = [];
			var displayPattern = this.displayPattern;
			if(displayPattern){						
				try {
					if(typeof displayPattern === String)
						this.constraints = JSON.parse(displayPattern);
					else
						this.constraints = displayPattern;
				}
				catch(err) {
					console.error("Error while processing " + this.label + " patter " + displayPattern) ;
					console.error(err);
				}					
			}

			var pattern = this.constraints;			

			if(pattern){
				if(pattern.true){
					var _yes = pattern.true ;
					var _No =  pattern.false ;            
					data = [
							{name:_yes, id:true},
							{name:_No, id:false}
						];
				}else{					
					if(Array.isArray(pattern)){						
						array.forEach(pattern, function(item){
							for (var prop in item){						
								data.push({id:prop, name :item[prop]});						
							}
						});	
					}
				}			
			}
						
			if(data.length <= 0){				
				if(this.value === 1 || this.value === 0){
					data = [
						{name:"Yes", id:1},
						{name:"No", id:0}
					]
				}else {
					data = [
						{name:"Yes", id:true},
						{name:"No", id:false}
					]
				}		
			}
			
            this.store = new MemoryStore({
				data: data
			});
			this.searchAttr = "name";
			this.inherited(arguments);
		}
	});
});