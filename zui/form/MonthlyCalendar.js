define("ddm/zui/form/MonthlyCalendar", [
	"dojo/_base/declare",
	"dojox/widget/_CalendarBase",
	"dojox/widget/_CalendarMonth"
], function(declare, _CalendarBase, _CalendarMonth){
	return declare("ddm.zui.form.MonthlyCalendar", [_CalendarBase, _CalendarMonth], {
		_makeDate: function(value){
			var now = new Date(value);
			return now;
		}
	});
});
