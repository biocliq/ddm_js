/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/form/ServerValidator", [ "dojo/_base/xhr", "dojo/_base/declare","dojo/_base/lang", 
		"dojo/on", "ddm/wire/store/base/JsonddmStore" ], 
		function(xhr, declare,lang,on, JsonddmStore) {

	var citStore = new JsonddmStore({});

	var ServerValidator = function(/*Form Input*/ input, page){
		input._serverValid = undefined;
		var _lastValidated = undefined;
		console.log(input);
		var _originalValidationMessage = undefined;
		
		on(input, "blur", function(){
			doServerValidation(input, page);
		});

		var originalValidation = input.isValid;
		input.isValid=function(){
			if(input._serverValid == true)
				return originalValidation.apply(input, arguments);
			else if(input._serverValid == false){
				return false;
			}else{				
				doServerValidation(input, page);				
				return originalValidation.apply(input, arguments);
			}
		};

		function doServerValidation(input, page){
			if(input._lastValidated != input.value){
				citStore.validateField(page, input.itemId, input.appId, input.get("value"))
					.then(lang.hitch(this, function(item){
					input._serverValid = item.result;
					input._lastValidated = input.value;
				}));
				//input._serverValid = (input._lastValidated == undefined || input._lastValidated == input.value);
				
			}
		}

		return input;
	};

	lang.setObject("ddm.zui.form.ServerValidator", ServerValidator);

	return ServerValidator;

});