define("ddm/zui/form/ParamTrack", [
	"dojo/_base/declare",
    "dijit/form/TextBox",
    "dijit/_HasDropDown",
    "dojo/text!./templates/ParamTrack.html"
	], function(declare, TextBox,_HasDropDown, template){
	
	var base = [TextBox,_HasDropDown];

	return declare( "ddm.zui.form.ParamTrack", base,{
        templateString:template, 
        dropDown:new TextBox(), 
        
	});
});
