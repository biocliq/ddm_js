/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/form/TimeTextBox", [
	"dojo/_base/declare", "dojo/_base/lang", "dojo/on", 'dijit/_WidgetBase', "dijit/_TemplatedMixin", 'dijit/_AttachMixin',
	'dijit/_WidgetsInTemplateMixin', "dojo/date/locale",
	"dijit/form/TimeTextBox",
	"ddm/util/DateParser",
	"dojo/text!./templates/TimeTextBox.html"
], function (declare, lang, on, _WidgetBase, _TemplatedMixin,
	_AttachMixin, _WidgetsInTemplateMixin, locale, TimeTextBox, DateParser,
	template) {

	var base = [_WidgetBase, _TemplatedMixin,
		_AttachMixin, _WidgetsInTemplateMixin];

	return declare("ddm.zui.form.TimeTextBox", base, {
		templateString: template,
		widgetsInTemplate: true,
		timePattern: "HH:mm:ss",
		constructor: function (options) {
			if (options)
				lang.mixin(this, options);
		},

		postCreate: function () {
			this.set("value", this.value);
			this.own(on(this.timeTextBox, 'change', lang.hitch(this, function (data) {
				this.onChange();
			})));
		},

		onChange: function () { },

		isValid: function () {
			return this.timeTextBox.isValid();
		},
		_setDisabledAttr: function (/*Boolean*/ value) {
			this.timeTextBox.set("disabled", value)
		},
		_getDisabledAttr: function (/*Boolean*/ value) {
			return this.timeTextBox.get("disabled");
		},
		_setReadOnlyAttr: function (/*Boolean*/ value) {
			this.timeTextBox.set("readOnly", value);
		},
		_getReadOnlyAttr: function (/*Boolean*/ value) {
			return this.timeTextBox.get("readOnly");
		},
		_getValueAttr: function () {
			var d = new Date();
			var t = this.timeTextBox.value;
			try {
				var value;

				if (t) {
					if (!isNaN(t.getTime())) {
						value = new Date(
							d.getFullYear(),
							d.getMonth(),
							d.getDate(),
							t.getHours(),
							t.getMinutes(),
							t.getSeconds(),
							t.getMilliseconds()
						);
						return locale.format(t, { selector: "time", timePattern: this.getServerFormat() });
					} else {
						return null;
					}
				} else {
					return null;
				}
			} catch (error) {
				console.log(error);
			}
		},

		getServerFormat: function () {
			return 'HH:mm:ss';
		},

		_format: function (date) {
			return locale.format(date, { selector: "time", timePattern: this.timePattern });
		},

		_getRawValueAttr: function () {
			var d = new Date();
			var t = this.timeTextBox.value;

			var value = new Date(
				d.getFullYear(),
				d.getMonth(),
				d.getDate(),
				t.getHours(),
				t.getMinutes(),
				t.getSeconds(),
				t.getMilliseconds()
			);
			return value;
		},

		_setValueAttr: function (value) {
			if (null != value && undefined != value && '' != value) {
				if (!DateParser.isValidDate(value)) {
					value = DateParser.formatServerTime(value);
				}
			}
			this.oldValue = value;
			// if(null != value){
			// 	if( !isNaN(value)){
			// 		value = new Date(value);
			// 	}else{                        
			// 		value = moment(value, 'HH:mm:ss').toDate();
			// 	}
			// }			
			this.timeTextBox.set("value", value);
		},

		_setRequiredAttr: function (required) {
			this.timeTextBox.set("required", required);
		},

		_setLabelAttr: function (value) {
			this.timeTextBox.set("label", value);
		},
		_getLabelAttr: function () {
			return this.timeTextBox.get("label");
		}
	});
});
