/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/form/TriStateCheckBox", [ 'dojo/_base/declare',"dojo/_base/lang",
	"dojox/form/TriStateCheckBox", "dojo/dom-attr"], 
		function(declare,lang, TriStateCheckBox, domAttr) {

	var base = [TriStateCheckBox];

	return declare("ddm.zui.form.TriStateCheckBox", base, {
		click:function(){		
			this._currentState = (0 == this._currentState) ? 2 : 0;
			var oldState = this._currentState;
			this.set("checked", this.states[this._currentState]);
			this._currentState = oldState;
			domAttr.set(this.stateLabelNode, 'innerHTML', this._stateLabels[this._stateType]);
		}
	});
});