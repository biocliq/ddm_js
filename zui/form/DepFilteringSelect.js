/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/form/DepFilteringSelect", [ "dojo/_base/xhr", "dojo/_base/declare","dojo/_base/lang", "dojo/on",
		"dojo/_base/array","dojo/Deferred", "dojo/topic", "dijit/form/FilteringSelect" ], 
		function(xhr, declare,lang,on, array, Deferred, topic, FilteringSelect) {

	var base = FilteringSelect;

	return declare("ddm.form.DepFilteringSelect", base, {
		startup:function(){
			this.inherited(arguments);
			// console.log("field.dependField")
			// console.log(field.dependField)
			console.log("this")
			    console.log(this.dependField)
			if(undefined != this.dependField){
				console.log("this")
			    console.log(this.dependField)
				var relatedField = dijit.byId(this.dependField + this.widgetIndex);
				console.log("relatedField")
				console.log(relatedField)
				var _this = this;
				console.log("_this")
				console.log(_this)
				if(null != relatedField){
					var connect = on(relatedField, 'change', function(){
						var relValue = relatedField.get('value');
						_this.query[relatedField.appId] = relValue;				
					});
					
					// @TODO disconnect the handle during destroy;
				}else{
					console.log('relatedField not found');
				}
			}
		}
	});
});