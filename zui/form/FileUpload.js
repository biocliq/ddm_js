/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/form/FileUpload",[
    "dojo/_base/declare",
    'dojo/topic',
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_OnDijitClickMixin",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",    
    "dijit/form/Button",
    "dijit/layout/ContentPane",
    "ddm/zui/form/TusUploader",
    "ddm/util/config",
    "dojox/form/uploader/FileList",
    "dijit/Fieldset",
    "tus/tus",
    "dojo/text!./templates/FileUpload.html"
    ], function(declare,topic, on, _WidgetBase, _OnDijitClickMixin, _TemplatedMixin,
    _WidgetsInTemplateMixin,Button,contentPane, Uploader, config, UploaderFileList,FieldSet,tus,template) {
    
        var base = [_WidgetBase, _OnDijitClickMixin,_TemplatedMixin, _WidgetsInTemplateMixin];

        return declare("ddm.zui.form.FileUpload", base , {
            templateString: template,
            ts:'',
            gridval:'',
            AttachmentStr:null,
            page:null, 
            itemId: -1,
            postCreate:function(){
                this.fileUploadUrl = config.url.fileUploadURL(this.pageMenu, this.itemId);
                this.uploaderzone.pageMenu = this.pageMenu;
                this.uploaderzone.itemId = this.itemId;
            },
            refreshTable: function() {
                this.gridval.model.setStore(this.AttachmentStr);
                this.gridval.body.refresh();
            },
            uploadFile: function() {
                this.uploaderzone.url= this.fileUploadUrl;
                this.uploaderzone.upload();
            } ,
            _setDisabledAttr:function(disabled){
                console.log(disabled);                
			    this.uploaderzone.set("disabled", disabled);
            }  
        });
    });
    