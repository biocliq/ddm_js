/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
 define("ddm/zui/form/AadhaarTextBox", [
	"dojo/_base/declare",
	"dijit/form/MappedTextBox"
	], function(declare, MappedTextBox){
	
	var base = [MappedTextBox];

        return declare( "ddm.zui.form.AadhaarTextBox", base,{
            placeHolder : '0000-0000-0000',
        
            intermediateChanges:true,
            onFocus:function(){
                this.isValid(false);
            },
            isValid:function(focused){
                if(this.inherited(arguments)){                    
                    var value = this.get("value");                    
                    
                    if(!this.required && this._isEmpty(value))
                        return true;

                    var res = value.length == 12 || focused;
                    if(! res){
                        message = this.getPromptMessage(focused);
                        this.set("message", message);
                    }
                    return res;
                }
                return false;
            },
            postscript :function(){
                this.inherited(arguments);
                this.constraints = {maxLength : 20};
                delete this.regExp;
            },
            postMixInProperties :function(){
                this.inherited(arguments);
                this.constraints = {maxLength : 20};                
                delete this.regExp;
                delete this.validMessage;
            },
            postCreate :function(){
                this.inherited(arguments);
                this.textbox.maxLength = 15;
            },
            format: function(value){                
                return this._formatAadhaar(value);;
            },
            parse: function(displayedValue){                
                var res =  displayedValue.split("-").join("");                
                return res;
            },
            onChange: function(/*anything*/ newValue, /*Boolean?*/ priorityChange){            
                this.inherited(arguments);                
                if(newValue.length){
                    var value = this._formatAadhaar(newValue);
                    if(value.length > 15)
                        value = value.substring(0,15);
                    this.set('displayedValue', value);
                }
            },
            _formatAadhaar:function(value){
                if(value && value.length)
                    return value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s => s.length > 0).join("-");
                else
                    return value;
            }
	});
});
