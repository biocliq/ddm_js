/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


 define("ddm/zui/form/DateWTimestampTextBox", [
	"dojo/_base/declare","dojo/_base/lang",'dijit/_WidgetBase',"dijit/_TemplatedMixin",'dijit/_AttachMixin',
    'dijit/_WidgetsInTemplateMixin',
	"dijit/form/DateTextBox","dijit/form/TimeTextBox",
	"dojo/topic","dojo/date/locale",
	"dojo/text!./templates/DateWTimestampTextBox.html"
	], function(declare,lang,_WidgetBase,_TemplatedMixin,
 _AttachMixin,_WidgetsInTemplateMixin, DateTextBox, TimeTextBox,topic,locale,
		template){

	var base = [_WidgetBase,_TemplatedMixin,_AttachMixin,_WidgetsInTemplateMixin];

	return declare( "ddm.zui.form.DateWTimestampTextBox", base,{
		templateString:template,
		widgetsInTemplate: true,
		datePattern:"yyyy-MM-dd'T'HH:mm:ss.SSSZ",
		_getValueAttr: function(){
			var d = this.dateFromTextBox.dateTextBox.value;
			var t = this.timeFromTextBox.value;
			var td = this.dateToTextBox.dateTextBox.value;
			var et = this.timeToTextBox.value;
			var fromTimestamp = undefined;			
			var toTimestamp = undefined;
			
			if(null !=d  && undefined != d && !isNaN(d) && d!= '')
				fromTimestamp =  this._extractDate(d,t);

			if(null !=td  && undefined != td && !isNaN(td) && td!= '')
				toTimestamp = this._extractDate(td,et);

			if(fromTimestamp == undefined)	{
				return (toTimestamp) ? '<='+toTimestamp : null;
			}else if(toTimestamp == undefined)	{
					return (fromTimestamp) ? '>='+fromTimestamp : null;
			}
			else{
				if(fromTimestamp <= toTimestamp){	 
					return fromTimestamp + '...'+ toTimestamp;
				}
				else {
					topic.publish("toaster","End date should be greater than Start date");
					return null;
				}
			}
		
			return null;		
		},
		_extractDate:function(x,y)
		{
			var dValue = new Date(x.getFullYear(),x.getMonth(),x.getDate(),y.getHours(),
							y.getMinutes(),y.getSeconds(),y.getMilliseconds());
			
			var serverFormat =  'yyyy-MM-ddTHH:mm:ss' //this.timeFromTextBox.getServerFormat();

			return locale.format(dValue, { selector: "date",datePattern : this.datePattern});
		},

		_setValueAttr: function(value){
			this.oldValue = value;
			if(null != value && undefined != value){
				if(!isNaN(value)){
					var _date = new Date(value);
					this.dateFromTextBox.set("value",_date);
					this.dateToTextBox.set("value",_date);
				}else{
					this.dateFromTextBox.set("value",value);
					this.timeFromTextBox.set("value",value);
					this.dateToTextBox.set("value",value);
					this.timeToTextBox.set("value",value);
				}
			}
		}

	});
});
