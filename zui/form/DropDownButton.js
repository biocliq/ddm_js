/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/form/DropDownButton", [
	"dojo/_base/declare",
    "dijit/form/DropDownButton",
    "dojo/text!./templates/DropDownButton.html",
	], function(declare, DropDownButton, template){
	
	var base = [DropDownButton];

	return declare( "ddm.zui.form.DropDownButton", base,{
		templateString:template
	});
});
