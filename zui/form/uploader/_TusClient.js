/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/form/uploader/_TusClient", [
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/_base/array",
	"dojo", 
	"dojo/topic",
	"tus/tus"
],function(declare, lang, arrayUtil, dojo, topic, tus){

	return declare("ddm.zui.form.uploader._TusClient", [], {
		// summary:
		//		A mixin for dojox/form/Uploader that adds Tus upload capabilities and
		//		progress events.
		//		
		//
		
		// debug message:
		errMsg:"Error uploading files. Try checking permissions",	
		uploadType:"tus",
		
		postMixInProperties: function(){
			this.inherited(arguments);
			if(this.uploadType === "tus"){ }
		},
	
		postCreate: function(){
			this.connectForm();
			this.inherited(arguments);
			if(this.uploadOnSelect){
				this.connect(this, "onChange", function(data){
					this.upload(data[0]);
				});
			}
		},
	
		_drop: function(e){
			dojo.stopEvent(e);
			var dt = e.dataTransfer;
			this._files = dt.files;
			this.onChange(this.getFileList());
		},
		/*************************
		 *	   Public Methods	 *
		 *************************/
	
		upload: function(/*Object ? */ formData){
			// summary:
			//		See: dojox.form.Uploader.upload
				
			this.onBegin(this.getFileList());
			this.uploadWithFormData(formData);
		},
	
		addDropTarget: function(node, /*Boolean?*/ onlyConnectDrop){
			// summary:
			//		Add a dom node which will act as the drop target area so user
			//		can drop files to this node.
			// description:
			//		If onlyConnectDrop is true, dragenter/dragover/dragleave events
			//		won't be connected to dojo.stopEvent, and they need to be
			//		canceled by user code to allow DnD files to happen.
			//		This API is only available in HTML5 plugin (only HTML5 allows
			//		DnD files).
			if(!onlyConnectDrop){
				this.connect(node, 'dragenter', dojo.stopEvent);
				this.connect(node, 'dragover', dojo.stopEvent);
				this.connect(node, 'dragleave', dojo.stopEvent);
			}
			this.connect(node, 'drop', '_drop');
		},
		
		uploadWithFormData: function(/*Object*/ data){
			// summary:
			//		Used with WebKit and Firefox 4+
			//		Upload files using the much friendlier FormData browser object.
			// tags:
			//		private
	
			if(!this.getUrl()){
				console.error("No upload url found.", this); return;
			}
			var fd = new FormData(), fieldName=this._getFileFieldName();
			arrayUtil.forEach(this._files, function(f, i){
				fd.append(fieldName, f);
				this.tusUpload(f);
			}, this);	
		}, 

		tusUpload:function(/*file */ file){
			var uploadURL = this.getUrl();
			var self = this;
			var upload = new tus.Upload(file, {
				endpoint: uploadURL,
				retryDelays: [0, 3000, 6000, 9000, 12000, 15000, 15000, 15000, 15000],
				removeFingerprintOnSuccess:true,
				chunkSize:1*1024*1024,
				metadata: {
					filename: file.name,
					filetype: file.type
				},
				onError: function(error) {
					var req = error.originalRequest;
					if(403 == req.status){
						topic.publish("/server/error/403");
					}
					else
						topic.publish("toaster", "Error while Uploading " + req.status + " " + req.statusText);
				}, 
				onProgress: function(bytesUploaded, bytesTotal) {
					var percentage = (bytesUploaded / bytesTotal * 100).toFixed(2)
					console.log(bytesUploaded, bytesTotal, percentage + "%")
					self._tusProgress(bytesUploaded, bytesTotal);
				},
				onSuccess: function() {	
					console.log("completed");
					self.onFileComplete(this.metadata.filename, upload.url);
				}
			})

			upload.start();
		},

		_tusProgress: function(bytesUploaded, bytesTotal){			
			var o = {
				bytesLoaded:bytesUploaded,
				bytesTotal:bytesTotal
			};
			o.decimal = bytesUploaded / bytesTotal;
			o.percent = Math.ceil((o.decimal)*100)+"%";			
			this.onProgress(o);			
		}
	});

});
