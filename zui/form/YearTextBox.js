define("ddm/zui/form/YearTextBox", [
	"dojo/_base/kernel",
	"dojo/_base/lang",	
	"./YearlyCalendar",
	"dijit/form/TextBox",
	"./DateTextBox",
	"dojo/_base/declare",
	"dojo/text!./templates/YearTextBox.html"
	], function(kernel, lang, YearlyCalendar, TextBox, DateTextBox, declare, template){
		return declare( "ddm.zui.form.YearTextBox", DateTextBox,
		{			
			templateString:template,			
			postCreate:function(){
				this.dateTextBox.popupClass = YearlyCalendar;
				this.inherited(arguments);
			}
		});
});