/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/form/DateTextBox", [
	"dojo/_base/declare", "dojo/_base/lang", "dojo/on", 'dijit/_WidgetBase', "dijit/_TemplatedMixin", 'dijit/_AttachMixin',
	'dijit/_WidgetsInTemplateMixin', "dojo/date/locale",
	"dijit/form/DateTextBox",
	"ddm/util/DateParser",
	"dojo/text!./templates/DateTextBox.html"
], function (declare, lang, on, _WidgetBase, _TemplatedMixin,
	_AttachMixin, _WidgetsInTemplateMixin, locale, DateTextBox, DateParser,
	template) {

	var base = [_WidgetBase, _TemplatedMixin,
		_AttachMixin, _WidgetsInTemplateMixin];

	return declare("ddm.zui.form.DateTextBox", base, {
		templateString: template,
		widgetsInTemplate: true,
		constructor: function (options) {
			if (options)
				lang.mixin(this, options);
		},

		postCreate: function () {
			this.set("value", this.value);
			this.own(on(this.dateTextBox, 'change', lang.hitch(this, function (data) {
				this.onChange();
			})));
		},

		onChange: function () { },

		isValid: function () {
			return this.dateTextBox.isValid();
		},
		_setDisabledAttr: function (/*Boolean*/ value) {
			this.dateTextBox.set("disabled", value)
		},
		_getDisabledAttr: function (/*Boolean*/ value) {
			return this.dateTextBox.get("disabled");
		},
		_setReadOnlyAttr: function (/*Boolean*/ value) {
			this.dateTextBox.set("readOnly", value);
		},
		_getReadOnlyAttr: function (/*Boolean*/ value) {
			return this.dateTextBox.get("readOnly");
		},
		_getValueAttr: function () {
			var t = this.dateTextBox.get('value');
			if (t){
				return locale.format(t, { selector: "date", datePattern: this.getServerFormat() });
			}
			return null;
		},

		_format: function (date) {
			return locale.format(date, { selector: "date", datePattern: this.datePattern });
		},

		getServerFormat: function () {
			return 'yyyy-MM-dd';
		},

		_setValueAttr: function (value) {
			if (null != value && undefined != value && '' != value) {
				if (!DateParser.isValidDate(value)) {
					value = DateParser.formatServerDate(value);
				}
			}
			this.oldValue = value;
			this.dateTextBox.set("value", value);
		},

		_setRequiredAttr: function (required) {
			this.dateTextBox.set("required", required);
		},

		_setLabelAttr: function (value) {
			this.dateTextBox.set("label", value);
		},
		_getLabelAttr: function () {
			return this.dateTextBox.get("label");
		}
	});
});
