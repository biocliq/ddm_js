/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/form/TimestampTextBox", [
	"dojo/_base/declare","dojo/_base/lang","dojo/on", 'dijit/_WidgetBase',"dijit/_TemplatedMixin",'dijit/_AttachMixin',
    'dijit/_WidgetsInTemplateMixin',"dojo/date/locale",
	"dijit/form/DateTextBox","dijit/form/TimeTextBox",
	"ddm/util/DateParser",
	"dojo/text!./templates/TimestampTextBox.html"
	], function(declare,lang,on, _WidgetBase,_TemplatedMixin,
 _AttachMixin,_WidgetsInTemplateMixin, locale, DateTextBox, TimeTextBox, 
		DateParser,
		template){
	
	var base = [_WidgetBase,_TemplatedMixin,
 _AttachMixin,_WidgetsInTemplateMixin];

	return declare( "ddm.zui.form.TimestampTextBox", base,{
		templateString:template,
		widgetsInTemplate: true,
		datePattern:"yyyy-MM-dd'T'HH:mm:ss.SSSZ",
		constructor:function(options){
			if(options)
				lang.mixin(this, options);
		},

		postCreate:function(){
			this.set("value", this.value);
			this.own(on(this.dateTextBox, 'change', lang.hitch(this,function(data){
					this.onChange();
				})));
				this.own(on(this.timeTextBox, 'change', lang.hitch(this,function(data){
					this.onChange();
				})));			
		},

		onChange:function(){},

		isValid:function(){
			return this.dateTextBox.isValid();
		},		
		_setDisabledAttr: function(/*Boolean*/ value){
			this.dateTextBox.set("disabled", value);
			this.timeTextBox.set("disabled", value)
		},
		_getDisabledAttr: function(/*Boolean*/ value){
			return this.dateTextBox.get("disabled");
		},
		_setReadOnlyAttr: function(/*Boolean*/ value){
			this.dateTextBox.set("readOnly", value);
			this.timeTextBox.set("readOnly", value)
		},
		_getReadOnlyAttr: function(/*Boolean*/ value){
			return this.dateTextBox.get("readOnly");
		},
		_getValueAttr: function(){
			var d = this.dateTextBox.value;
			var t = this.timeTextBox.value;
			if(isNaN(d)){
				return null;
			}
			try {
				var value;

				if(t){
					if(!isNaN(t.getTime())){					
						value = new Date(
							d.getFullYear(),
							d.getMonth(),
							d.getDate(),
							t.getHours(),
							t.getMinutes(),
							t.getSeconds(),
							t.getMilliseconds()
						);
					}else{
						value = new Date(
							d.getFullYear(),
							d.getMonth(),
							d.getDate(),
							8,0,0,0
						);
					}
				}else{
					value = new Date(
						d.getFullYear(),
						d.getMonth(),
						d.getDate(),
						0,0,0,0
					);
				}
				var result = DateParser.formatServerTimeStamp(value);
				console.log(result);
				return result;
				//return value.getTime();	
			} catch (error) {
				console.log(error);
			}
		},

		// _format:function(date){
		// 	return locale.format(date, { selector: "date",datePattern : this.datePattern} );
		// },

		_getRawValueAttr: function(){
			var d = this.dateTextBox.value;
			var t = this.timeTextBox.value;

			var value = new Date(
			    d.getFullYear(),
			    d.getMonth(),
			    d.getDate(),
			    t.getHours(),
			    t.getMinutes(),
			    t.getSeconds(),
			    t.getMilliseconds()
			);
			return value;
		},

		_setValueAttr: function(value){
			this.oldValue = value;
			if(null != value && undefined != value && '' != value){
				if(!DateParser.isValidDate(value)){
					value = DateParser.parseServerTimeStamp(value);
				}
				var _date = new Date(value);
				this.dateTextBox.set("value",_date);
				this.timeTextBox.set("value",_date);
			}
			else{
				this.reset();
			}
			
		}, 

		reset:function(){
			this.dateTextBox.reset();
			this.timeTextBox.reset();
		},
		_setRequiredAttr:function(required){
			this.dateTextBox.set("required",required);
		},

		_setLabelAttr:function(value){
			this.dateTextBox.set("label", value);
		}, 
		_getLabelAttr:function(){
			return this.dateTextBox.get("label");
		}
	});
});
