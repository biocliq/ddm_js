/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/form/DateWTextBox",[
"dojo/_base/declare","dijit/_WidgetBase","dijit/_OnDijitClickMixin",
"dijit/_TemplatedMixin",'dijit/_AttachMixin',"dijit/_WidgetsInTemplateMixin",
"ddm/zui/form/DateTextBox","dijit/form/TextBox","dojo/topic",
"dojo/text!./templates/DateWTextBox.html",], 

function(declare, _WidgetBase, _OnDijitClickMixin, 
	_TemplatedMixin,_AttachMixin,_WidgetsInTemplateMixin,
	DateTextBox,TextBox,topic,
	template) {
	var base = [_WidgetBase,_TemplatedMixin,_AttachMixin,_WidgetsInTemplateMixin];

	return declare("ddm/zui/form/DateWTextBox",base,{
		templateString: template,
		widgetsInTemplate: true,
		
		_getValueAttr: function(){
			this.inherited(arguments);
			var d = this.dateFromTextBox.get('value');
			var t = this.dateToTextBox.get('value');

			var fromDate = undefined;
			var toDate = undefined;
			var result = undefined;

			if(null != d && undefined != d && d!= ''){			
				fromDate =  d;
			}
			if(null != t && undefined != t && t!= ''){			
				toDate = t;
			}

			if(fromDate == undefined)	{
				result = (toDate) ? '<='+toDate : null;
			}else if(toDate == undefined)	{
					result = (fromDate) ? '>='+fromDate : null;
			}
			else{
				if(fromDate <= toDate){	 
					result = fromDate + '...'+ toDate;
				}
				else {
					topic.publish("toaster","End date should be greater than Start date");
					result = null;
				}
			}
			console.log(result);
			return result;
		},
		_extractDate:function(x)
		{
			var dValue = new Date(
					x.getFullYear(),
					x.getMonth(),
					x.getDate()
			);
			return dValue.getTime();
		},
		_setValueAttr: function(value){
			if(null != value && undefined != value){
				if(!isNaN(value)){
					var _date = new Date(value);
					this.dateFromTextBox.set("value",_date);
					this.dateToTextBox.set("value",_date);
				}else{
					this.dateFromTextBox.set("value",value);
					this.dateToTextBox.set("value",value);
				}
			}
		}
	});
});
