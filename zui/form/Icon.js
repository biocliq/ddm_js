/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 define("ddm/zui/form/Icon", [
	"dojo/_base/declare",
    "dijit/form/Button",
    "dojo/text!./templates/Icon.html"
	], function(declare, Button, template){
	
	var base = [Button];

	return declare( "ddm.zui.form.Icon", base,{
        templateString:template, 
        baseClass:'zuiIcon'        
	});
});
