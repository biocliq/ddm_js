define("ddm/zui/form/MonthTextBox", [
	"dojo/_base/kernel",
	"dojo/_base/lang",	
	"./MonthlyCalendar",
	"dijit/form/TextBox",
	"./DateTextBox",
	"dojo/_base/declare",
	"dojo/text!./templates/MonthTextBox.html"
	], function(kernel, lang, MonthlyCalendar, TextBox, DateTextBox, declare, template){
		return declare( "ddm.zui.form.MonthTextBox", DateTextBox,
		{			
			templateString:template,			
			postCreate:function(){
				this.dateTextBox.popupClass = MonthlyCalendar;
				this.inherited(arguments);				
			}
		});
});