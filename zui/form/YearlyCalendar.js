define("ddm/zui/form/YearlyCalendar", [
	"dojo/_base/declare",
	"dojox/widget/_CalendarBase",
	"dojox/widget/_CalendarYear"
], function(declare, _CalendarBase, _CalendarYear){
	return declare("ddm.zui.form.YearlyCalendar", [_CalendarBase, _CalendarYear], {
		_makeDate: function(value){
			var now = new Date(value);
			return now;
		}
	});
});
