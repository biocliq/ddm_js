define("ddm/zui/form/RangeSelector", [ 'dojo/_base/declare',"dojo/_base/lang",
	"dojo/dom",	'dojo/dom-style', 
	"dijit/form/DropDownButton","dijit/TooltipDialog",
	"dijit/layout/ContentPane","dijit/layout/TabContainer",
	"dijit/layout/TabController" ,"../page/base/Base",  
    'dojo/text!./templates/RangeSelector.html',
	"dijit/form/TextBox","dijit/form/Select",
		"dijit/TitlePane","dijit/form/CheckBox","dijit/form/DateTextBox",
		"dijit/form/TimeTextBox","ddm/zui/form/TimestampTextBox"
		], 
		function(declare,lang, dom,domStyle,
				DropDownButton,TooltipDialog,
				ContentPane,TabContainer,TabController,
				 widgetBase, template) {

	var base = [ContentPane, widgetBase];    
	return declare("ddm.zui.form.RangeSelector", base, {
        templateString : template, 
        postCreate:function(){
            this.dwTodayChange(true);
        },    
        dwTodayChange:function(selected){
            if(selected){
                var now = new Date();
                var todayDate = new Date(new Date().setHours(0, 0, 0, 0));
                this._update("Today", todayDate, now);
            }
        },
        dwYesterdayChange:function(selected){
             if(selected){
                var sd = this._calcDate(new Date(), 1);
                this._update("Yesterday", sd.start, sd.end);
            }
        },
        dwLweekChange:function(selected){
             if(selected){
                 var sd = this._calcDate(new Date(), 7);
                this._update("7 days", sd.start, sd.end);
            }
        },
        dwLmonthChange:function(selected){
             if(selected){
                 var sd = this._calcDate(new Date(), 30);
                this._update("30 days", sd.start, sd.end);
            }
        },
        updateCustomDate: function(selected){
            var disabled = !selected;
            this.dwFromDate.set('disabled', disabled);
            this.dwToDate.set('disabled', disabled);
            if(selected)
                this._update("Custom");
        },
        updateDate:function(){
            if(this.dwcustom.get('checked')){
                this._update("Custom", this.dwFromDate.get('rawValue'),  this.dwToDate.get('rawValue'));
            }            
        },
        _update:function(label, from, to){
            if(label)
                this.label.innerHTML = label;
            else return;
            if(from)
                this.dwFromDate.set('value', from);
            if(to)
                this.dwToDate.set('value', to);
            if(from && to)
                this.onChange({from:from, to:to});
        },
        _calcDate:function(date, day){
            var end = new Date(date);
                    end.setHours(0, 0, 0, 0);
                var start = new Date(end);
                start.setDate(start.getDate()-day);                
                end.setSeconds(end.getSeconds() - 1);
            return {end:end, start:start};
        }, 
        onChange:function(){}
		
	});
});