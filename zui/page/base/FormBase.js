/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/FormBase",	[
	'dojo/_base/declare',
	"dojo/_base/array"
],
function( declare, array) {
var base = null;

return declare('ddm.zui.page.base.FormBase', base, {
		// wChildren: [],
		isValid:function(){
			if(this.dChildren == null || this.dChildren == undefined)
				return true;				
			return array.every(this.dChildren, function(widget){
				return widget.isValid();
			});
		},
		reset:function(){
			if(this.dChildren == null || this.dChildren == undefined)
				return;
			var data = array.forEach(this.dChildren, function(widget){
				widget.reset();
			});
		},
		_setModeAttr(flag){
			this.mode=flag;
			if(this.dChildren == null || this.dChildren == undefined)
				return;
			return array.forEach(this.dChildren, function(widget){
				return widget.set('mode', flag);
			});				
		},
		_getValueAttr:function(){
			if(this.dChildren == null || this.dChildren == undefined)
				return;
			var data = {id : this.appId,				
						fields : array.map(this.dChildren, function(widget){
								try{
									return widget.get('value');
								}catch(err){
									console.log(err);
									return ;
								}
							})
			};
			return data;
		},
		resume:function(){
			if(this.wChildren == null || this.wChildren == undefined)
				return;				
			array.forEach(this.wChildren, function(widget){
				try{
					if(widget.resume)
						widget.resume();
				}catch(err){console.log(err);}
			});
			return;
		},
		pause:function(){
			if(this.wChildren == null || this.wChildren == undefined)
				return;				
			array.forEach(this.wChildren, function(widget){
				try{
				if(widget.pause)
					widget.pause();
				}catch(err){console.log(err);}
			});
			return ;
		},
		destroy:function(){
			// this.destroyArray(this.dChildren);
			delete this.dChildren;
			delete this.wChildren;				
			this.inherited(arguments);
		}, 
		destroyArray:function(children){
			try{
			if(children){
				var data = array.forEach(children, function(widget){
					delete widget;
				});
			}}catch(err){console.error(err);}
		}
	});
}
);