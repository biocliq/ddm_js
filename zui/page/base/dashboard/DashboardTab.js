/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/dashboard/DashboardTab", [
	"dojo/_base/declare","dojo/_base/array",
	"dojo/dom-geometry",'dojo/dom-style',"dojo/dom-class",
	"dijit/layout/ContentPane","ddm/zui/layout/FlowLayoutContainer",
	"ddm/zui/page/base/Base"
	],
        function(declare,array,
			domGeom, domStyle,domClass, 
            ContentPane, FlowLayoutContainer,widgetBase
	) {

	var base = [widgetBase,FlowLayoutContainer];

	return declare("ddm.zui.page.base.dashboard.DashboardTab", base, {				
		postCreate: function(){
			this.dChildren = [];
			this.wChildren = [];			
			this.inherited(arguments);
			domClass.add(this.domNode, "section group")
			// if(0 == this.idx)
				this.populate();
		},
		populate : function(){
			if(this.loaded)
				return;
			if(this.tabData){
				if(this.tabData.containers){
					this._populate(this.tabData.containers);
					this.loaded = true;
				}
				else{
					var _t = this;
					this.citStore.getDashboardTab(this.itemId, this.tabData.id)
					.then(function(item) {
						if(item.containers){
							_t._populate(item.containers);
							_t.calcHeight(true);
							_t.loaded = true;
						}
					});
				}
			}
		},
		createWidgetInContainer:function(widgetName, options, _childList){			
			var cntOptions  = {baseClass:"tileContainer"};
			if(options.data){
				cntOptions.widthPercent = options.data.width;
				cntOptions.heightPercent = options.data.height;
				cntOptions.itemId = this.itemId;
			}
			var cont = new ContentPane(cntOptions);
			this.addChild(cont);			
			this.createWidget(widgetName, options,
				function(widget){
					_childList.push(widget);
					cont.addChild(widget);
					widget.startup();					
				});
		},
		_populate : function(containers){
		    for(var _cont in containers){				
				var container = containers[_cont];				
				if(3 == container.contentType){
					var	options = {itemId: this.itemId, page :  this.page, appId : container.name,
						containerId : container.id,title : container.label , 
						data : container, args:this.args,
						widgetId : this.widgetId, mode:this.mode, rootPage:this.rootPage};							
					var renderer = container.reference || this.chartWidgetName;
					this.createWidgetInContainer(renderer, options, this.wChildren);
				}
		    }
		},
		getWidth:function(){
			var node = this.domNode;
			var computedStyle = domStyle.getComputedStyle(node);
			var output = domGeom.getMarginBox(node, computedStyle);
			return output.w;
		},
		refresh:function(){
			var children = this.wChildren;
			array.map(children, function(child){
				if(child.refresh){
					child.refresh();
				}
			});
		}, 
		destroy:function(){
			this.inherited(arguments);
			delete this.wChildren;
			delete this.dChildren;
		}
	});

});
