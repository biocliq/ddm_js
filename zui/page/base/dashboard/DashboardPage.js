/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/page/base/dashboard/DashboardPage", [ 
	'dojo/_base/declare',"../Base",  
	"dijit/layout/ContentPane","dojo/_base/array",
	"ddm/util/config", 'dojo/text!./templates/DashboardPage.html'], 
		function(declare, widgetBase,  
				ContentPane,array, 
				config,template) {

	var base = [ContentPane,widgetBase ];

	return declare("ddm.zui.page.base.dashboard.DashboardPage", base, {
		page : null,
		itemId : null,
		citStore : null,
		autoRefresh:true,
		_refreshHandle:undefined,
		templateString : template,
		tabWidgetName:"ddm/zui/page/base/dashboard/DashboardTab",
		tableWidgetName : config.page.expTableWidget,
		chartWidgetName: config.page.expChartContainer,
		topicHandle:null,
		setTitle:function(item){},	
		postCreate : function(){				
			this.dChildren=[];
			this.inherited(arguments);
			if (!this._started) {				
				this.populateContainers();
			}
			this.start();
		}, 
		populateContainers:function(){				
			var item = this.item;
			this.set('title', item.label);
			this.setTitle(item);		
			this.populateTab(item.center, this.centerContainer);
		},
		populateTab:function(tabs, container){
			var idx = 0;
			if (tabs) {
					var uq = this.id;
					for ( var _tab in tabs) {
						var tab = tabs[_tab];
						var options = {	title : tab.label,
										closable : false,appId: tab.id,
										id : uq + tab.name + tab.id,
										tabData : tab,
										mode : this.mode,
										citStore : this.citStore,
										itemId : this.itemId,
										page : this.page,
										widgetId : uq,
										idx:idx,
										tableWidgetName : this.tableWidgetName,
										chartWidgetName: this.chartWidgetName,
										rootPage: this,
										doLayout:false, args:this.args
									};
						this.createWidget(this.tabWidgetName, options, 
								function(widget){
							if(widget){
								container.addChild(widget);	
								this.dChildren.push(widget);
							}								
						});	
						idx++;				
					}						
				}
		}, 
		refresh:function(){
			var children = this.centerContainer.getChildren();
			array.map(children, function(child){
				if(child.refresh){
					child.refresh();
				}
			});
		}, 
		start:function(){
			if(undefined == this._refreshHandle){
				var _this = this;
				this._refreshHandle = setInterval(function(){
					_this.refresh();
				}, 300*1000);
			}
		},
		resume:function(){
			this.refresh();
			//console.log("Resuming Auto-Refresh of the dashboard " + this.name);
			this.start();
		},
		pause:function(){
			//console.log("Pausing Auto-Refresh of the dashboard " + this.name);
			clearInterval(this._refreshHandle);
			this._refreshHandle = undefined;
		},
		destroy:function(){
			this.topicHandle.remove();
			this.inherited(arguments);
			delete this.itemId;
			delete this.page;
			delete this.tabWidgetName;
			if(this.citStore && this.citStore.destroy)
				this.citStore.destroy();
			delete this.citStore;	
			delete this.topicHandle;		
		}
	});

});