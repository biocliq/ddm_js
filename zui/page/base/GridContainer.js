/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


 define("ddm/zui/page/base/GridContainer", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"./Loader"], 
	function(declare,array, Loader) {

	var base = [Loader];

	return declare("ddm.zui.page.base.GridContainer", base, {
		grid: "ddm/zui/page/base/summary/SummaryGrid",
       		
		// resize : function(dimension){
		// 	// this.inherited(arguments);
		// },
				
		postCreate:function(){
			this.inherited(arguments);
			var container = this.data;
			var options =  this.getGridOptions();
			this.createWidget(this.grid, options,
				function(widget){
					this.postGridCreate(widget);
				}
			);			
			if (!this._started) {					

			}
		}, 
        postGridCreate:function(widget){
			this.addChild(widget);
			this._grid = widget;
        },
        getGridOptions:function(){
            var container = this.data;
			var tableData;
			var grid ;
			var headerGroups;

			if(container.tableLayout){
				tableData = container.tableLayout.tableData;
				grid = container.tableLayout.gridLayout;
				headerGroups = container.tableLayout.headerGroups;
			}
			var options = {fields : grid,widgetId : this.widgetId,
				layout:container.tableLayout,
                pageMenu : this.pageMenu,
				data : tableData, headerGroups:headerGroups,
				title : container.label,	
                autoHeight:false,containerId : container.id,
				containerName : container.name,
				options:{}
			};			
			declare.safeMixin(options.options, container.options);			
            return options;
        },
		refresh:function(){
			var children = this.getChildren();
			array.map(children, function(child){
				if(child.refresh){
					child.refresh();
				}
			});
		},
		isLiveEditable:function(){
			if(this._grid)
				return this._grid.editable == true;
			else
				return false;
		}		
	});

});