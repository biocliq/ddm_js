/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/summary/SummaryReportGrid", [ 
	"dojo/_base/declare",
	"dojo/store/Memory","dojo/store/Cache",
	'ddm/wire/store/grid/JsonGridStore', 
	'ddm/fluwiz/grid/ddmGrid', 
	'gridx/modules/SingleSort', 
	'gridx/modules/ColumnResizer',
	'gridx/modules/Filter', 
	"ddm/zui/page/base/summary/SummaryReportFilterBar",
	"gridx/modules/Pagination", 
	"gridx/modules/pagination/PaginationBar",
	"gridx/modules/Persist", 	
	"gridx/modules/select/Row", 
	"gridx/modules/CellWidget" ,
	"dojo/topic"], function(
		declare, Memory, Cache, JsonGridStore, Grid, 
		Sort, ColumnResizer, Filter,SummaryFilterBar,
		Pagination, PaginationBar,Persist, 
		SelectRow, CellWidget, topic) {

	var base = Grid;

	return declare("ddm.zui.page.base.summary.SummaryReportGrid", base, {		
		filterBarClass:"ddm/zui/page/base/summary/SummaryReportFilterBar",		
		postMixInProperties : function() {
			var gridStore = this.getGridStore();
			this.store = new Cache(gridStore, new Memory());
			this.inherited(arguments);
		},
		postCreate:function(){
			this.inherited(arguments);
			var options = this.options;
			if(options && options.columnLock){
				if(this.columnLock){
					this.columnLock.lock(options.columnLock);
				}
			}
		},
		getGridStore:function(){
			var storeOptions = {
				pageMenu : this.pageMenu,
				pageType : "summary",
				idProperty:"_id",
				containerName : this.containerName
			};

			if(this.defaultFilter){
				storeOptions.defaultFilter = this.defaultFilter;
			}
			
			var gridStore = new JsonGridStore(storeOptions);

			return gridStore;
		},

		gridModulesCustomize : function(){
			this.inherited(arguments);			
			if(undefined == this.moduleOptions){
				this.modules.push("gridx/modules/Pagination", "gridx/modules/pagination/PaginationBar");
			}
		},

		/**
		 * onCell Double click or Enter key event  publishes the event message 
		 * to trigger opening the Explorer page of the selected CI Id.
		 */
		evtCellDblClick:function(e){
			dojo.stopEvent(e);			
			topic.publish("/form/view", e.rowId, this.pageMenu);
		}, 
		evtRowKeyUp:function(e){
			if("Enter" == e.key){
				dojo.stopEvent(e);
				topic.publish("/form/view", e.rowId, this.pageMenu);
			}
		}
	});

});
