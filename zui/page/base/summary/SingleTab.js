/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/summary/SingleTab", [ "dojo/_base/declare","dojo/_base/array",'dojo/_base/lang',
	'require', "ddm/zui/page/base/Base", "dijit/form/Form",  "dijit/layout/ContentPane", 
	"ddm/wire/store/base/JsonddmStore", ], 
		function(declare,array, lang, require, widgetBase, Form,ContentPane,  ddmStore) {

	var base = [ContentPane, widgetBase];

	return declare("ddm.zui.page.base.summary.SingleTab", base, {
		_getValueAttr(){
			var data = this.inherited(arguments);
			return {id:data.id, name:this.title, containers:data.fields};
		},
		postCreate: function(){
			this.dChildren = [];
			this.inherited(arguments);
			this.populate();
		},
		populate : function(){
			if(this.loaded)
				return;
			if(this.tabData){
				if(this.tabData.containers){
					this._populate(this.tabData.containers);
					this.loaded = true;
				}
				else{
					var _t = this;
					this.citStore.getExplorerTab(this.itemId, this.tabData.id)
					.then(function(item) { 
						if(item.containers){
							_t._populate(item.containers);
							_t.loaded = true;
						}
					});
				}
			}
		},
		setData(){
			
		},
		_populate : function(containers){
		    for(var _cont in containers){
				var container = containers[_cont];
				var cntWidget;
				switch(container.contentType){
				case 1:{
					options = {itemId: this.itemId, page :  this.page, appId : container.name, 				
									title : container.label ,  data : container,containerId : container.id,
									widgetId : this.widgetId, mode:this.mode, rootPage:this.rootPage};
					this.createWidget(this.formWidgetName, options, 
							function(widget){
						this.addChild(widget);
						this.dChildren.push(widget);
					});	
					
					break;
				}
				case 2: {
					options = {itemId: this.itemId, 
						page :  this.page, appId: container.name,
						title : container.label,  containerId : container.id,
						tabId:this.tabData.id, citStore : this.citStore, 
						data:container, mode:this.mode};
					this.createWidget(this.tableWidgetName, options, 
							function(widget){
						this.addChild(widget);
					});	
					break;}
				}
		    }
		}
	});

});