/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/summary/NavigatGrid", [ 
	"dojo/_base/declare","dojo/_base/array",
	'ddm/wire/store/grid/JsonGridStore', 
	'ddm/fluwiz/grid/ddmGrid', 
	'gridx/modules/SingleSort', 
	'gridx/modules/ColumnResizer',
	'gridx/modules/Filter', 
	"ddm/zui/page/base/summary/SummaryFilterBar",
	"gridx/modules/Pagination", 
	"gridx/modules/pagination/PaginationBar",
	"gridx/modules/Persist", 	
	"gridx/modules/extendedSelect/Row",
	"gridx/modules/CellWidget" ,
	"dojo/topic", 
	"ddm/zui/page/base/explorer/NavigatablePage"], function(
		declare, array,JsonGridStore, Grid, 
		Sort, ColumnResizer, Filter,SummaryFilterBar,
		Pagination, PaginationBar,Persist, 
		SelectRow, CellWidget, topic) {

	var base = Grid;

	return declare("ddm.zui.page.base.summary.NavigatGrid", base, {		
		filterBarClass:"ddm/zui/page/base/summary/SummaryFilterBar",		
		postMixInProperties : function() {
			// Creates the summary store and attach with summary Grid	
			this.store = new JsonGridStore({
				pageMenu : this.pageMenu,
				pageType : "summary",
				idProperty:"_id",
				containerName : this.containerName
			});				      
			this.inherited(arguments);
		},
		gridModulesCustomize : function(){
			this.inherited(arguments);			
			if(undefined == this.moduleOptions){
				this.modules.push("gridx/modules/Pagination", "gridx/modules/pagination/PaginationBar");
			}
		},
		filterSetupQuery : function(expr){
			if(undefined == expr || undefined == expr.data)
					return;
				var data = expr.data
				var query = {};
				array.forEach(data, function(_expr){
					query[_expr.data[0].data] = _expr.data[1].data;
				});	
			this.queryString = query;
			return query;
		},
		selectRow:function(index){
			this.select.row.selectByIndex(index);
		},
		deselectRow:function(index){			
			this.select.row.deselectByIndex(index);
		},
		/**
		 * onCell Double click event  publishes the event message 
		 * to trigger opening the Explorer page of the selected CI Id.
		 */
		evtCellDblClick:function(e){			
			dojo.stopEvent(e);			
			var options = {rowIndex : e.rowIndex, parentGrid : this};			
			topic.publish("/popup/form/view/", this.pageMenu, e.rowId, options);
		}
	});

});