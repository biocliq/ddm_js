/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/summary/SGridContainer", [
	"dojo/_base/declare", "dojo/_base/lang",
	"dijit/layout/ContentPane",
	"../GridContainer", 
	"ddm/zui/page/base/summary/SummaryGrid"], 
		function(declare,  lang,
				ContentPane,  GridContainer) {

	var base = [ContentPane, GridContainer];

	return declare("ddm.zui.page.base.summary.SGridContainer", base, {
		grid: "ddm/zui/page/base/summary/SummaryGrid",
		padding: "0px",
        doLayout :"false",
		permission:{},
		startup:function()
		{
			this.inherited(arguments);
		},
		postCreate:function(){
			this.inherited(arguments);
		},
		getGridOptions:function(){
			var options = this.inherited(arguments);			
			options.autoHeight = false;
            options.style="height:100%";
			options.permission=this.permission;	
			options = lang.mixin(options, this.grid_options);		
			return options;
		}
	});

});