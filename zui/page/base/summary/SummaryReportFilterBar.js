/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define([
        'dojo/_base/declare',
        'dojo/_base/lang',        
		'dojo/topic',
		"dijit/TitlePane",
		"dijit/layout/AccordionContainer",
		'gridx/modules/Filter',
		'dojo/text!./templates/SummaryReportFilterBar.html',
		'ddm/gridx/modules/filter/_FilterBar',
		"ddm/zui/form/DropDownButton"
        ], function(declare, lang, topic,TitlePane,AccordionContainer,
        		F, template, _FilterBar){

	return declare([ _FilterBar], {
		templateString: template,
		filterPlaceHolder:"",
		postMixInProperties:function(){
			this.setQuickFilterPlaceHolder();
			this.inherited(arguments);
		},
		postCreate: function(){			
			this.inherited(arguments);
			var grid = this.grid;
			var _this = this;

			grid.connect(grid.select.row,'onSelected', function(){
				
			});

			grid.select.row.onSelected = function(){
				console.log(arguments);
			}

			if(grid.permission.create)
			{
				this.disableBtnAdd(false);
			}
			else{
				this.disableBtnAdd(true);
			}
			if(this.titleBar)
				this.titleBar.innerHTML = grid.title;
		},

		setQuickFilterPlaceHolder:function(){
			var fields = this.grid.fields;
			var i = 0; 
			var max = 2;
			var labels = [];			
			for(var field of fields){				
				if(field.quickFilter){
					labels.push(field.label);
				}
			}
			
			this.filterPlaceHolder = labels.join(" or ");		
		},

		downloadXLS:function(){			
			this.grid.store.getXlsReport();
		},

		disableBtnAdd:function(flag){
			if(flag)
				this.btnAdd.setDisabled(true);
			else
				this.btnAdd.setDisabled(false);
		},		
		addRecord:function(){
			var t = this;
			var page = t.grid.pageMenu;
			topic.publish("/form/new",page);
		},
		removeRecord:function(){
				var t = this;
				var selRowIdArray = t.grid.select.row.getSelected();
				console.log(selRowIdArray);
				var gridStore = t.grid.model.store;
			for(var i=0; i<selRowIdArray.length; i++){
				gridStore.remove(selRowIdArray[i]);
			}
		}	
	});
});
