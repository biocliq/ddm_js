/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/page/base/summary/SummaryPage", [ 
	'dojo/_base/declare',
	"dojo/_base/lang", 
	"../Base",  
	"dijit/layout/ContentPane",
	"ddm/wire/store/base/JsonddmStore",
	"dojo/topic",
	"dojo/_base/array",
	'dojo/text!./templates/SummaryPage.html'], 
		function(declare,lang, widgetBase,  
				ContentPane,ddmStore, topic,array, template) {

	var base = [ContentPane,widgetBase ];

	return declare("ddm.zui.page.base.summary.SummaryPage", base, {
		page : null,
		itemId : null,
		citStore : null,
		templateString : template,
		tabWidgetName:"ddm/zui/page/base/summary/SummaryTab",
		topicHandle:null,
		
		onToolBarClick:function(e, sd){
			if(this.tgtPage){
				topic.publish("/form/new", this.tgtPage);
			}else
				topic.publish("/form/new", this.page);
		},
		
		setupEvents: function(){
			dojo.connect(this.centerContainer,"_transition",function(newPage, oldPage){
			    if(!newPage.loaded){
			    	newPage.populate();
			    }
				});
		},
		resume:function(){
			this.refresh();
		},
		postCreate:function(){
			this.inherited(arguments);
			if (!this._started) {				
				this.topicHandle = topic.subscribe("/summary/"+ this.page + "/refresh", 
					lang.hitch(this, function(args){
						try {
							this.refresh();
						}catch(e){
							console.error(e);
						}
					})
				);

				//this.citStore.getSummaryTab().then(lang.hitch(this, function(item) {
					var item = this.data;					
					if (item.center) {
						var tabs = item.center;
						var uq = this.page + item.ciType;
						var tab = tabs[0];
						document.title = tab.label;
						this.title = tab.label;
						this.createWidget(this.tabWidgetName, {
							title : tab.label,
							style:"height:100%",
							closable : false,
							id : uq + tab.name + tab.id,
							tabData : tab,
							citStore : this.citStore,
							page : this.page
						}, function(pageTab){
							pageTab.populate();
							this.centerContainer.addChild(pageTab);
						});	
					}	
				//}));
				this.setupEvents();
			}
		}, 
		refresh:function(){
			var children = this.centerContainer.getChildren();
			array.map(children, function(child){
				if(child.refresh){
					child.refresh();
				}
			});
		}, 

		destroy:function(){
			this.topicHandle.remove();
			this.inherited(arguments);
			delete this.itemId;
			delete this.page;
			delete this.tabWidgetName;
			if(this.citStore && this.citStore.destroy)
				this.citStore.destroy();
			delete this.citStore;	
			delete this.topicHandle;		
		}
	});

});