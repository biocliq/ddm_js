/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/page/base/page/Page", [ 
	'dojo/_base/declare',
	"dojo/_base/lang",  
	"dijit/layout/ContentPane",
	"dijit/layout/TabContainer",
	"ddm/wire/store/base/JsonddmStore",
	"dojo/topic",
	"dojo/_base/array", "../Loader"], 
		function(declare,lang, 
				ContentPane,TabContainer, ddmStore, topic,array, Loader) {

	var base = [ContentPane, Loader ];

	return declare("ddm.zui.page.base.page.Page", base, {
		page : null,
		itemId : null,
		citStore : null,
		tabWidgetName:"ddm/zui/page/base/page/Tab",
		topicHandle:null,
		
		onToolBarClick:function(e, sd){
			topic.publish("/form/new", this.page);
		},
		
		setupEvents: function(){
			dojo.connect(this.centerContainer,"_transition",function(newPage, oldPage){
			    if(!newPage.loaded){
			    	newPage.populate();
			    }
				});
		},		
		postCreate:function(){
			this.inherited(arguments);
			if (!this._started) {				
				this.topicHandle = topic.subscribe("/summary/"+ this.page + "/refresh", 
					lang.hitch(this, function(args){
						try {
							this.refresh();
						}catch(e){
							console.error(e);
						}
					})
				);

				//this.citStore.getSummaryTab().then(lang.hitch(this, function(item) {
					var item = this.data;
					if (item.center) {
						var tabs = item.center;
						if(1 == tabs.length)
							this.centerContainer = new ContentPane();
						else
							this.centerContainer = new TabContainer();
						this.addChild(this.centerContainer);
						var uq = this.page + item.ciType;
						for ( var _tab in tabs) {
							var tab = tabs[_tab];
							this.createWidget(this.tabWidgetName, {
								title : tab.label,
								style:"height:100%",
								closable : false,
								id : uq + tab.name + tab.id,
								tabData : tab,
								citStore : this.citStore,
								page : this.page
							}, function(pageTab){
								pageTab.populate();
								console.log("Adding tab");
								this.centerContainer.addChild(pageTab);
							});	
						}
					}	
				//}));
				this.setupEvents();
			}
		}, 
		refresh:function(){

			var children = this.centerContainer.getChildren();
			array.map(children, function(child){
				if(child.refresh){
					child.refresh();
				}
			});
		}, 

		destroy:function(){
			this.inherited(arguments);
			delete this.itemId;
			delete this.page;
			delete this.tabWidgetName;
			if(this.citStore && this.citStore.destroy)
				this.citStore.destroy();
			delete this.citStore;			
		}
	});

});