/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/ExplorerTab", [
	"dojo/_base/lang",
	"dojo/_base/declare",
	"dojo/dom-construct",
	"dojo/dom-geometry",	
	'dojo/dom-style',"dojo/dom-class",
	"ddm/zui/page/base/Base",
	"dijit/layout/ContentPane",
	"ddm/zui/page/base/explorer/FormContainer"
	,"ddm/zui/layout/FlowLayoutContainer"
	],
		function(lang, declare,
		domConstruct, domGeom, domStyle,domClass, widgetBase, ContentPane,
		FormContainer,FlowLayoutContainer
	) {

	var base = [widgetBase,FlowLayoutContainer];

	return declare("ddm.zui.page.base.explorer.ExplorerTab", base, {
		_getValueAttr(){
			var data = this.inherited(arguments);
			return {id:data.id, name:this.title, containers:data.fields};
		},
		postCreate: function(){
			this.dChildren = [];
			this.wChildren = [];			
			this.inherited(arguments);
			domClass.add(this.domNode, "section group")
			// Populate the tab, only if tabData is already available.
			if(0 == this.idx)
				this.populate();
		},
		focus:function(){
			var widget = this.dChildren[0];
			if(widget && widget.focus)
				widget.focus();
		},
		populate : function(){
			if(this.loaded)
				return;
			if(this.tabData){
				if(this.tabData.containers){
					this._populate(this.tabData.containers);
					//this.calcHeight(true);
					this.loaded = true;
				}
				else{
					var _t = this;
					this.citStore.getExplorerTab(this.itemId, this.tabData.id)
					.then(function(item) {
						if(item.containers){
							_t._populate(item.containers);
					//		_t.calcHeight(true);
							_t.loaded = true;
						}
					});
				}
			}
		},
		createTableInContainer:function(formWidget, options, _editableList, _readOnlyList){
			// if(! cntOptions)
			var cntOptions  = {baseClass:"tileContainer"};
			if(options.data){
				cntOptions.widthPercent = options.data.width;
				cntOptions.heightPercent = options.data.height;
			}
			
			
			var cont = new ContentPane(cntOptions);
			this.addChild(cont);
			this.createWidget(formWidget, options,
				function(widget){					
					if(widget.isLiveEditable())
						_editableList.push(widget._grid);
					
					_readOnlyList.push(widget);
					cont.addChild(widget);
					widget.startup();
					//this.calcHeight();					
				});
		},
		createWidgetInContainer:function(formWidget, options, _childList){
			// if(! cntOptions)
			var cntOptions  = {baseClass:"tileContainer"};
			if(options.data){				
				cntOptions.widthPercent = options.data.width;
				cntOptions.heightPercent = options.data.height;
			}
			
			
			var cont = new FlowLayoutContainer(cntOptions);
			this.addChild(cont);
			this.createWidget(formWidget, options,
				function(widget){					
					if(!widget.hasInitialized || widget.hasInitialized()){
						_childList.push(widget);
						cont.addChild(widget);
						widget.startup();						
						this.calcHeight();
					}else
						widget.destroy();
				});
		},
		calcHeight:function(){

		},
		getField:function(/*String*/ name){
			for(var idx in this.dChildren){
				var child = this.dChildren[idx];				
				if(child.getField){
					var result = child.getField(name);
					if(result)
						return result;
				}
			}
		},
		_populate : function(containers){			
			
		    for(var _cont in containers){
				var formWidget ;
				if(1 == containers.length){
					formWidget = this.formWidgetName;
				}else{
					formWidget = this.formsWidgetName;
				}
				var container = containers[_cont];				
				switch(container.contentType){
					// Cfg form
					case 1:
					// Special form
					case 5:{
						var	options = {itemId: this.itemId, page :  this.page, appId : container.name,
										containerId : container.id,title : container.label , 
										data : container, args:this.args,parentId:this.parentId,
										widgetId : this.widgetId, mode:this.mode, rootPage:this.rootPage};
											var reference = container.reference;										
											if(reference)
												formWidget = reference;
											if(container.options)
												options.options = container.options;
						this.createWidgetInContainer(formWidget, options, this.dChildren);
						break;
					}
					//table//
					case 2:{						
						if(!this.itemId)
							return;								
						var	options = {itemId: this.itemId,widgetId : this.widgetId,
							pageMenu :  this.page, appId: container.name,
							title : container.label,containerId : container.id,
							tabId:this.tabData.id,citStore : this.citStore,
							data:container, mode:this.mode,parentId:this.parentId,
							parent:this};
						var reference = container.reference;
						var tblWidget = this.tableWidgetName;
						if(reference){
							options.grid = reference;
						}
						if(container.options){
							var _opt = JSON.parse(container.options);
							if(_opt.toggleable != undefined)
								tblWidget = "ddm/zui/page/base/explorer/RGridContainer";
							if(_opt.open == false)
								options.open = _opt.open;
							options.grid_options = _opt;
						}
						this.createTableInContainer(tblWidget, options, this.dChildren, this.wChildren);
						break;
					}
					case 3:{
						var	options = {itemId: this.itemId, page :  this.page, appId : container.name,
							containerId : container.id,title : container.label , 
							data : container, args:this.args,parentId:this.parentId,
							widgetId : this.widgetId, mode:this.mode, rootPage:this.rootPage};							
						
						var renderer = container.reference || this.chartWidgetName;						
						this.createWidgetInContainer(renderer, options, this.wChildren);
						
						break;
					}
					case 4:{						
						if(!this.itemId)
							return;								
						var	options = {itemId: this.itemId,widgetId : this.widgetId,
							pageMenu :  this.page, containerName: container.name,
							title : container.label,citStore : this.citStore,
							data:container, mode:this.mode,parentId:this.parentId,
							parent:this};
						var reference = container.reference;
						var tblWidget = this.fileWidgetName;
						if(reference){
							options.grid = reference;
						}
						if(container.options){
							var _opt = JSON.parse(container.options);
							if(_opt.toggleable != undefined)
								tblWidget = "ddm/zui/page/base/explorer/RGridContainer";
							if(_opt.open == false)
								options.open = _opt.open;
							options.grid_options = _opt;
						}
						this.createWidgetInContainer(tblWidget, options, this.wChildren);
						break;
					}
				}
		    }
		},
		getWidth:function(){
			var node = this.domNode;
			var computedStyle = domStyle.getComputedStyle(node);
			var output = domGeom.getMarginBox(node, computedStyle);
			return output.w;
		},
		destroy:function(){
			this.inherited(arguments);
			delete this.wChildren;
			delete this.dChildren;
		}
	});

});
