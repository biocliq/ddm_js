/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/page/base/explorer/NavigatablePage", 
	[	'dojo/_base/declare',	'dojo/topic',	
		"ddm/zui/page/base/explorer/_ExplorerPage",	
		'dijit/layout/ContentPane',		
		'dojo/text!./templates/NavigatablePage.html',		
		"ddm/zui/layout/TabContainer"
	],
	function( declare, topic, widgetBase, ContentPane,template) {
	var base = [ContentPane, widgetBase ];	
	return declare('ddm.zui.page.base.explorer.NavigatablePage', base, {
			templateString : template,
			buildRendering:function(){
				this.inherited(arguments);
				var rowIndex = this.args.rowIndex;
				if(0 == rowIndex){
					this.previousBtn.set('disabled', true);
				}else
					this.previousBtn.set('disabled', false);


					
				var grid = this.args.parentGrid;							
				var model = grid.model;
				
				
				if( rowIndex+1 >= model.size()){
					this.nextBtn.set('disabled', true);
				}else
					this.nextBtn.set('disabled', false);
				
				
			},
			editForm: function(){
				this.inherited(arguments);
				this.saveBtn.set('disabled', false);
				this.editBtn.set('disabled', true);
			},		
			onSave:function(){
				var self = this;
				var message = "/summary/"+ self.page + "/refresh";
				topic.publish(message);
				self.editBtn.set('disabled', false);
				self.saveBtn.set('disabled', true);
			},
			previousRecord:function(){		
				var rowIndex = this.args.rowIndex;
				var grid = this.args.parentGrid;
				var pageMenu = this.page;				
				var pagination = grid.pagination;
				var paginationSize = pagination.pageSize();
				
				if(0 == rowIndex)
					return;				
				
				rowIndex = rowIndex -1;
				var remainder = rowIndex % paginationSize;
				
				if(1 > remainder){						
					var curPage = pagination.currentPage();
					pagination.gotoPage(curPage-1);	
				}

				var data = grid.model.byIndex(rowIndex);
				var item = null;

				if(data){				
					this.previousBtn.set('disabled', true);	
					item = data.item;	
					var options = {rowIndex : rowIndex, parentGrid : grid};			
					topic.publish("/popup/form/view/", pageMenu, item._id, options);
					var parent = this.getParent();
					setTimeout(function(){ 
						parent.hide();
					}, 500); 
				}else{
					if(!this.previousBtn.get('disabled')){
						var _this = this;
						setTimeout(function(){ 
							_this.prevRecord();
						}, 500); 
					}
				}
				
			},
			nextRecord:function(){
				var rowIndex = this.args.rowIndex;
				var grid = this.args.parentGrid;
				var pageMenu = this.page;				
				var pagination = grid.pagination;
				var model = grid.model;
				var paginationSize = pagination.pageSize();
				
				if( rowIndex >= model.size())
					return;				

				var remainder = rowIndex % paginationSize;
				
				rowIndex = rowIndex +1;				
				var data = grid.model.byIndex(rowIndex);
				var item = null;

				if(data){	
					this.nextBtn.set('disabled', true);				
					item = data.item;	
					var options = {rowIndex : rowIndex, parentGrid : grid};			
					topic.publish("/popup/form/view/", pageMenu, item._id, options);
					var parent = this.getParent();
					setTimeout(function(){ 
						parent.hide();
					}, 500);  					
				}else{
					console.log("no data found  - reloading");
					if((remainder + 1) == paginationSize){
						var curPage = pagination.currentPage();
						pagination.gotoPage(curPage+1);	
					}
					if(!this.nextBtn.get('disabled')){
						var _this = this;
						setTimeout(function(){ 
							_this.nextRecord();
						}, 500); 
					}
				}
			},
			onReset:function(){
				this.editBtn.set('disabled', false);
				this.saveBtn.set('disabled', true);
			},
			destroy:function(){
				this.inherited(arguments);				
			}
		});
	}
);