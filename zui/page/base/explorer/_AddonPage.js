/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/page/base/explorer/_AddonPage", 
	[	'dojo/_base/declare',"dijit/registry",		
		'dijit/layout/ContentPane',	
		"ddm/wire/store/base/JsonddmStore",
		"ddm/zui/page/base/explorer/InlineExplorerPage",
	],
	function( declare, registry, ContentPane, JsonddmStore,InlineExplorerPage,) {
	var base = [];
	return declare('ddm.zui.page.base.explorer._AddonPage', base, {
            createAddonTab:function(pageMenu, itemId, title, order, selectChild){
                var uq = "intern_" + pageMenu + itemId;
                var citStore = new JsonddmStore({page:pageMenu});
                var _this = this;
                var pane = new ContentPane({title:title, baseClass:"dijitReset", doLayout:false, class:"section"});
                _this.centerContainer.addChild(pane,order);
                citStore.get(itemId).then(function(item){
                    var options = {page : pageMenu, itemId : itemId, item:item,
                        pageType:"explorer", citStore:citStore,style:"height:97%;overflow:auto;",
                    id : uq,closable : true, widgetId : uq};
                    var page = new InlineExplorerPage(options);
                    page.startup();
                    pane.addChild(page);
                    if(selectChild){
                        _this.centerContainer.selectChild(pane);
                    }
                    page.resize();
                });
                return pane;
            }, 
            replaceAddonTab:function(pageMenu, itemId, selectChild, pane){
                var uq = "intern_" + pageMenu + itemId;
                var citStore = new JsonddmStore({page:pageMenu});
                var _this = this;
                var widget = registry.byId(uq);

                console.log(_this.centerContainer);

                if(widget){
                    if(selectChild){
                        _this.centerContainer.selectChild(pane);
                    }
                    return;
                }
                
                citStore.get(itemId).then(function(item){
                    var options = {page : pageMenu, itemId : itemId, item:item,
                        pageType:"explorer", citStore:citStore,style:"height:97%;overflow:auto;",
                    id : uq,closable : true, widgetId : uq};
                    
                    var page = new InlineExplorerPage(options);                    
                    page.startup();
                    var children = pane.getChildren();                

                    for(var i=0; i < children.length; i++){
                        var child = children[i];
                        pane.removeChild(child);
                        child.destroy();
                        delete child;
                    }

                    pane.addChild(page);
                    if(selectChild){
                        _this.centerContainer.selectChild(pane);
                    }
                    page.resize();
                });
                return pane;
            }
		});
	}
);