/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/_FormContainer", [
	"dojo/_base/declare","dojo/_base/array","dojo/dom-class","dojo/dom-construct","dojo/dom-style",
	"dojo/store/Memory","dojo/store/Cache",
	"../FormBase","ddm/zui/layout/FlowLayoutContainer", 
	"dijit/form/CheckBox", "dijit/form/ComboBox",
	"dijit/form/TextBox", "dijit/form/NumberTextBox",
	// "ddm/zui/form/ParamTrack",
	"dijit/form/Textarea", "dijit/form/ValidationTextBox", 
	"dijit/form/FilteringSelect","ddm/zui/form/FilteringSearch",
	"ddm/util/DateParser","ddm/util/util",
	"ddm/zui/form/BooleanFilteringSelect",
    "ddm/zui/form/DepFilteringSelect","ddm/zui/form/ServerValidator",
	"ddm/zui/form/DateTextBox","ddm/zui/form/TimestampTextBox",
	"ddm/zui/form/TimeTextBox","ddm/zui/form/AadhaarTextBox",
	"ddm/zui/form/MonthTextBox","ddm/zui/form/YearTextBox",
    "ddm/wire/store/widget/FieldLookupStore","ddm/wire/store/widget/FieldUniqueStore",
	"dojo/store/Memory", "dijit/layout/ContentPane", 
	 "dojo/text!ddm/zui/form/templates/FilteringSelect.html" ],
		function(declare, array, domClass,domConstruct, domStyle,
				Memory, Cache,
				formBase, FlowLayoutContainer,
				CheckBox, ComboBox, TextBox,NumberTextBox, Textarea, ValidationTextBox, FilteringSelect,FilteringSearch,
				DateParser, util, BooleanFilteringSelect,DepFilteringSelect,ServerValidator,
				DateTextBox,TimestampTextBox, TimeTextBox, AadhaarTextBox,
				MonthTextBox, YearTextBox, FieldLookupStore, FieldUniqueStore, MemoryStore,
				ContentPane, FilteringSelectTemplate) {

	var base = [formBase];

	return declare("ddm.zui.page.base.explorer._FormContainer", base, {
		//templateString: '<div><div data-dojo-attach-point="fieldSet" data-dojo-props="title : \'${title}\'"  data-dojo-type="dijit.Fieldset" ></div>',
		baseClass: "dijitBorderTop",
		// fields:{},
		postCreate: function(){
			this.inherited(arguments);
			this.widgets = {};
			var container = this.data;
			if(container && container.formLayout){
				var formContainer = new FlowLayoutContainer();
				if( !(this.data.colspan == 2 || this.data.colspan == 1 || this.data.colspan == 4)){
					this.data.colspan = 2;
				}				
				this.setSpanClass();
				// domClass.add(formContainer.domNode, "section group")
				var _widgetId = this.widgetId;				
				var fields = container.formLayout.fieldLayout;				
				fields = this.preProcess(fields);
				for(var _idx in fields){
					var field = fields[_idx];
					this.field = field;
					if(1 == this.mode)
					{  //view Record 1
						field.disabled = true;						
					}else if(3 == this.mode){						
						//new Record 3
						field.disabled = !((field.editable & 1) > 0);
					}else{
						//edit Record 2
						field.disabled = !((field.editable & 2) > 0);
					}

					if(field.validRegExp != null && field.validRegExp != undefined)
						field.regExp = field.validRegExp;

					field.itemId = this.itemId;
					field.style = "background-color : transparent;";
					field.appId = field.code;
										
					field.id = field.code + '_' + _widgetId;
					field.required = field.mandatory;
					if(field.mandatory)
						field.label = field.label + '*';
					if(field.displayPattern){						
						try {
							if(typeof field.displayPattern === String)
								field.constraints = JSON.parse(field.displayPattern);
							else
								field.constraints = field.displayPattern;
						}
						catch(err) {
							console.error("Error while processing " + field.label + " patter " + field.displayPattern) ;
							console.error(err);
						}
						
					}
					this.addField(field, formContainer, fields);
				}
				this.addChild(formContainer);
			}
		},
		preProcess:function(fields){
			return fields;
		},
		setSpanClass:function(){
			var spanClass = "span_" + this.data.colspan*6 + "_12";
			domClass.add(this.domNode, "tileLayout " + spanClass);
		},
		getWidgetValue(){
			var data = util.map(this.widgets, function(widget){
				var _val = widget.get('value');
				if (_val === ''){
					_val = null;
				}
				if(widget.ignore)
					return;
				return {code: widget.appId, name : widget.name, value : _val};
			});
			return data;
		},
		reset:function(){
			var data = util.forEach(this.widgets, function(widget){
				if(widget.reset){
					widget.reset();
				}
			});
		},
		isValid:function(){
			return util.every(this.widgets, function(widget){
				if(widget['isValid']){
					try{
						var status = widget.isValid();
						if(!status){
							widget.set("state", "Error");
						}
					}catch(err){
						widget.set("state", "Error");
					}
					return status;
				}
				return true;
			});
		},
		focus:function(){
			util.some(this.widgets, function(widget){
				if(widget.focus){
					widget.focus();
					return true;
				}
				return false;
			});
		},
		_setModeAttr(flag){
			return util.forEach(this.widgets, function(widget){				
				if(2 == flag)
					widget.set('disabled', (!widget.editable || widget.editable ==1));
				else if(3 == flag)
					widget.set('disabled', (!widget.editable || widget.editable == 2));
				else
					widget.set('disabled', true);
			});
		},
		_getValueAttr(){
			return {id:this.containerId, name:this.appId, formLayout : {fieldLayout:this.getWidgetValue()}};
		},
		createField:function(field, fields){
			var format = field.displayFormat;			
			var addMethod = this['add'+field.displayFormat];
			if(undefined != addMethod && typeof addMethod == "function"){
				return addMethod.call(this, field, fields);
			}else{
				console.error("add Method for format '" + field.displayFormat + "' is not found" );
			}
		},
		getField:function(name){			
			for(idx in this.widgets){
				if(idx == name){
					return this.widgets[idx];
				}
			}			
		},
		addField: function(field, container, fields){
			var djfield = this.createField(field, fields);
			
			if(undefined != djfield){
				var label = domConstruct.create("div");
				label.innerHTML = field.label;
				// container.domNode.appendChild(label);
				if(djfield != undefined){
					if(field.unique){
						djfield = new ServerValidator(djfield, this.page);
					}
					if( !(field.colspan == 2 || field.colspan == 1) 
							|| this.data.colspan == 1){
						field.colspan = 1;
					}
					var labelClass = "";
					var fieldClass = "";
					var width = "50%";					
					if(this.data.colspan == 1){
						// width = "100%";
						labelClass = "rspan_2_4";
						fieldClass = "span_2_4";
					}
					else if(this.data.colspan ==4){
						labelClass = "rspan_2_4";
						fieldClass = "span_2_4";
						width = '100%';
					}
					else{
						if(field.colspan == 1){							
							if(field.displayFormat != 'TEXTAREA'){
								labelClass = "rspan_2_4";
								fieldClass = "span_2_4";								
							}
							else{
								labelClass = "rspan_1_4";
								fieldClass = "span_3_4";
								width="100%";
							}
						}else{
							width = "100%";
							labelClass = "rspan_1_4";
							fieldClass = "span_3_4";
						}
					}
					
					// labelClass = "rspan_2_4";
					// fieldClass = "span_2_4";

					var fieldContainer = new ContentPane();
					domStyle.set(fieldContainer.domNode, {
						width:width
					});
					fieldContainer.domNode.appendChild(label);
					

					domClass.add(label, "formLayout formLabel " + labelClass);
					// container.addChild(djfield);
					domClass.add(djfield.domNode, "formLayout " + fieldClass);
					fieldContainer.addChild(djfield);
					container.addChild(fieldContainer);
					this.widgets[djfield.name] = djfield;
					// this.dChildren.push(djfield);
				}
			}
		},
		addDROPDOWN: function(field){
			field = this.prepareSelect(field);
			field.templateString = FilteringSelectTemplate;			
			return new FilteringSelect(field);						
		},
		addHIDDEN:function(field){

		},
		prepareSelect:function(field){
			var memoryStore;	
			var pattern = field.displayPattern;		
			var searchAttr = field.refAttributeName;
			var attribute = field.attributeName;
			var idAttr = this.getIdAttr(attribute);

			if(pattern == undefined){
				memoryStore = new Memory({idProperty : idAttr});
				if(field.value == undefined){				
					if(field.defaultValue){			
						if(':current_user' == field.defaultValue)
							field.displayedValue = userId;
						else if (field.defaultValue)
							field.value = field.defaultValue;
					}
				}else{					
					var item = {};
					item[idAttr] = field.value;	
					if(field.displayValue){
						item[searchAttr] = field.displayValue + "";	 // Hack - add an empty string to display Number value in drop down.						
						memoryStore.put(item);
					}else
						field.value = undefined;
				}			
			
				var options = {page:this.page,fieldId : field.appId, 
					idProperty : idAttr,
					itemId:this.itemId, parentId: this.parentId};
				
				field.store = new Cache(new FieldLookupStore (options), memoryStore);				
			}else{
				var data = [];

				if(Array.isArray(pattern)){						
					array.forEach(pattern, function(item){
						for (var prop in item){						
							data.push({id:prop, name :item[prop]});						
						}
					});	
				}
				field.store = new MemoryStore({
					data: data
				});
				searchAttr = "name";
			}

			field.searchAttr = searchAttr;
			field.queryExpr="*${0}*";
			field.pageSize = 15;
			field.autoComplete= false;
			return field;
		},		

		getIdAttr:function(attribute){
			var atts = attribute.split(".");
			if(atts.length > 1)
				return atts[1];
			else
				return atts[0];
		},
		addCOMBOBOX:function(field){
			field = this.prepareSelect(field);
			var djfield = new ComboBox(field);
			return djfield;
		},
		addSEARCHDROPDOWN: function(field){	
			field = this.prepareSelect(field);		
			var djfield = new FilteringSearch(field);			
			return djfield;
		},
		addUNIQUECOMBO:function(field){			
			var searchAttr = field.attributeName;
			var idAttr = field.attributeName;
		
			var options = {page:this.page,fieldId : field.appId, 
				idProperty : idAttr,
				itemId:this.itemId, parentId: this.parentId};
			
			field.store = new FieldUniqueStore(options);
			
			field.searchAttr = searchAttr;
			field.queryExpr="*${0}*";
			field.pageSize = 15;
			field.autoComplete= false;
			var djfield = new ComboBox(field);
			return djfield;
		},
		addOPTION_SELECT: function(field){
			field = this.prepareSelect(field);
			if(undefined == field.dependField || null == field.dependField)
			{
				djfield = new FilteringSelect(field);
			}
			else{
				var refField = field.dependField;
				field.widgetIndex = '_' + this.widgetId;
				field.query = {};
				field.query[refField]= '*';
				djfield = new DepFilteringSelect(field);
			}
			return djfield;
		},
		addMONTH: function(field){
			if(! field.value && 'now' == field.defaultValue)
				field.value = new Date();
			else{
				var fvalue = field.value;
				field.value = DateParser.parseServerDate(fvalue);
			}
			var displayPattern = field.displayPattern;
			if(displayPattern){
				field.placeHolder = displayPattern;
				field.constraints = { datePattern : displayPattern };
			}			
			return new MonthTextBox(field);
		},
		addYEAR: function(field){
			if(! field.value && 'now' == field.defaultValue)
				field.value = new Date();
			else{
				var fvalue = field.value;
				field.value = DateParser.parseServerDate(fvalue);
			}
			var displayPattern = field.displayPattern;
			if(displayPattern){
				field.placeHolder = displayPattern;
				field.constraints = { datePattern : displayPattern };
			}			
			return new YearTextBox(field);
		},
		addDATE: function(field){			
	
			if(! field.value && 'now' == field.defaultValue)
				field.value = new Date();
			// else{
			// 	var fvalue = field.value;
			// 	field.value = DateParser.parseServerDate(fvalue);
			// }

			var displayPattern = field.displayPattern;
			if(displayPattern){
				field.placeHolder = displayPattern;
				field.constraints = { datePattern : displayPattern };
			}
				//field.datePattern = "MM-dd-yyyy";
			return new DateTextBox(field);
		},
		addTIME: function(field){
			if(! field.value && 'now' == field.defaultValue)
				field.value = new Date();			
			return new TimeTextBox(field);
		},
		addTIMESTAMP: function(field){
			return this.addDATETIME(field)
		},
		addDATETIME: function(field){
			if(! field.value && 'now' == field.defaultValue)
				field.value = new Date();
			return new TimestampTextBox(field);
		},
		addAADHAAR : function(field){			
			return new AadhaarTextBox(field);
		},
		addTEXTFIELD: function(field){
			if(typeof field.value === 'string' && field.value.length > 30)
				field.title = field.value;

			return new ValidationTextBox(field);
		},
		addVARCHAR: function(field){
			return this.addTEXTFIELD(field);
		},
		addBIGINT: function(field){
			return new NumberTextBox(field);
		},
		addINT: function(field){
			if(undefined == field.value)
				field.value = field.defaultValue;
			return new NumberTextBox(field);
		},
		addDOUBLE: function(field){
			return new NumberTextBox(field);
		},
		addDECIMAL: function(field){			
			return new NumberTextBox(field);
		},
		addTEXTAREA: function(field){
			field.style="min-height:60px; max-height: 160px;";			
			return new Textarea(field);
		},
		addBIT: function(field){			
			if(undefined == field.value){			
					field.value = field.defaultValue ;				
			}
			return new BooleanFilteringSelect(field);

			// var field = new CheckBox(field);
			// field.startup();
			// field.domNode.appendChild(
			// 	domConstruct.create("label", {"for" : field.id, innerHTML: " IdChannel"}));
			// var pane = new ContentPane();
			// return field;
			// pane.addChild(new CheckBox(field));
			// return pane;
		},
		destroy:function(){
			delete this.widgets;

			this.inherited(arguments);
			
			delete this.widgets;
		}
	});
});
