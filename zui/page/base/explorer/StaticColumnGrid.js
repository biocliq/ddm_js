
define("ddm/zui/page/base/explorer/StaticColumnGrid", [ "dojo/_base/declare","dojo/_base/array",
		'ddm/wire/store/grid/JsonGridStore', 'dojo/store/Cache',"dojo/date/locale",
		'ddm/fluwiz/grid/ddmGrid', 'gridx/core/model/cache/Async',
		'gridx/modules/SingleSort', 'gridx/modules/ColumnResizer',
		'gridx/modules/Filter', "gridx/modules/CellWidget","ddm/gridx/modules/Edit",
		"gridx/modules/Pagination", "gridx/modules/pagination/PaginationBar",
		"gridx/modules/HiddenColumns", "gridx/modules/select/Row", 		
		"gridx/modules/IndirectSelect","gridx/modules/extendedSelect/Row",
		"gridx/modules/RowHeader","gridx/modules/move/Column",
		"gridx/modules/dnd/Column","gridx/modules/extendedSelect/Column",
		"dojo/store/Memory", "gridx/modules/Sort"
		], function(
		declare, array, JsonGridStore, StoreCache, locale, Grid, AsyncCache, Sort, ColumnResizer, Filter, CellWidget, Edit,
		Pagination, PaginationBar, HiddenColumns, SelectRow,
		IndirectSelect, ExSelectRow, RowHeader, moveColumn, dndColumn,selectColumn,Memory,Sort
	//	,Modify
		) {

	var base = Grid;

	return declare("ddm.zui.page.base.explorer.StaticColumnGrid", base, {
		root_id : null,
		sys_page : "aclrole",
		paginationInitialPageSize:10,
		paginationBarSizes: [10, 25, 50],
		autoHeight:true,
		cacheClass: AsyncCache,
		selectRowTriggerOnCell: true,
		editLazySave: true,
		modules : [
				Sort, ColumnResizer,
					 Filter, Pagination,
					PaginationBar, RowHeader
					,SelectRow,CellWidget
					,Edit,HiddenColumns,Sort
					]
	});

});