/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 /**
  * The simplegrid is mostly used in Explorerpage to display 
  * records with parent / child relationship in grid format
  * (foriegn key relation)
  */
 
define("ddm/zui/page/base/explorer/SimpleGrid", [ 
	"dojo/_base/declare",
	"dojo/topic",
	"dojo/_base/lang",
	'ddm/wire/store/grid/JsonGridStore', 
	'dojo/store/Cache',
	'ddm/fluwiz/grid/ddmGrid', 
	'gridx/core/model/cache/Async',
	'gridx/modules/SingleSort', 
	'gridx/modules/ColumnResizer',
	'gridx/modules/ColumnWidth',
	'gridx/modules/Filter', 
	"ddm/gridx/modules/toolbar/ExplorerFilterBar",
	"gridx/modules/Pagination", 
	"gridx/modules/pagination/PaginationBar",
	"gridx/modules/select/Row", 		
	"gridx/modules/IndirectSelect",
	"gridx/modules/extendedSelect/Row",
	"gridx/modules/RowHeader",
	"dojo/store/Memory",
	"gridx/modules/Edit",
	"gridx/modules/Bar",
	"gridx/modules/CellWidget", 
	"gridx/modules/Filter",
	"gridx/modules/filter/QuickFilter"], function(
		declare, topic, lang, JsonGridStore, StoreCache, Grid, 
		AsyncCache, Sort, ColumnResizer, ColumnWidth,Filter, FilterBar,
		Pagination, PaginationBar, SelectRow,
		IndirectSelect, ExSelectRow, RowHeader, Memory,Edit,Bar,CellWidget) {

	var base = Grid;

	return declare("ddm.zui.page.base.explorer.SimpleGrid", base, {
		root_id : null,
		sys_page : "null",
		filterBarClass:"ddm/gridx/modules/toolbar/ExplorerFilterBar",
		paginationInitialPageSize:15,
		paginationBarSizes: [15, 25, 50],		
		autoHeight:true,
		selectRowTriggerOnCell: true,
		postMixInProperties : function() {					
			var memoryStore = new Memory({});			
			var data = this.data;			
			if(null != data && undefined != data)
				isPagination = data.totalCount > 10;
			
			this.cacheClass = AsyncCache;			
			var restStore = new JsonGridStore({
				pageMenu : this.pageMenu,
				ciId : this.itemId,
				idProperty:"_id",
				containerName : this.containerName
			});
			this.inherited(arguments);
			this.store = restStore;
		},
		gridModulesCustomize : function(){
			this.inherited(arguments);			
			if(undefined == this.moduleOptions){
				this.modules.push("gridx/modules/Pagination", "gridx/modules/pagination/PaginationBar");
			}
		},
		evtCellDblClick : function(e){
			dojo.stopEvent(e);
			var self = this;
			if(self.tgtPageMenu){
				var callback = function(item, status){				
					topic.publish("/popup/summary/refresh/"+ self.itemId + "/" + self.tgtPageMenu);
				}

				var options = {parentId : this.itemId, parentMenu : this.pageMenu, 
						containerName: this.containerName};

				topic.publish("/popup/page/edit/",this.tgtPageMenu, e.rowId, options, callback);
			}
		}, 
		postCreate:function(){
			this.inherited(arguments);	
			
			if(this.tgtPageMenu){
				this.topicHandle = topic.subscribe("/popup/summary/refresh/"+ this.itemId + "/" + this.tgtPageMenu, 
					lang.hitch(this, function(args){
						try {
							this.refresh();
						}catch(e){
							console.error(e);
						}
					})
				);
			}
		},
		destroy:function(){
			if(this.topicHandle){
				this.topicHandle.remove();
			}
			this.inherited(arguments);
		}
	});

});