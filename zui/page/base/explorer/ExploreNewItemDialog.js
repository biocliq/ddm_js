/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 define("ddm/zui/page/base/explorer/ExploreNewItemDialog",
	[ 'dojo/_base/declare',	"dojo/_base/lang",	"dojo/topic",  	"dojo/on",
	"dojo/dom-style",	"dijit/layout/ContentPane",
	"ddm/zui/widget/Dialog","dijit/form/Button", ], 
function(declare,lang, topic,on,
		domStyle,ContentPane,
		Dialog,Button) {

	var base = [Dialog];

	return declare("ddm.zui.page.base.explorer.ExploreNewItemDialog", base, {
		resizeAxis:'xy',
		destroy:function(){
			this.inherited(arguments);
			delete this.popupGrid;
			delete this.store;
		}
	});
});