/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/PopupSummaryGrid", [ 
	"dojo/_base/declare",
	'ddm/wire/store/grid/JsonGridStore', 
	'ddm/fluwiz/grid/ddmGrid', 
	'gridx/modules/SingleSort', 
	'gridx/modules/ColumnResizer',
	'gridx/modules/Filter', 
	"ddm/zui/page/base/summary/SummaryFilterBar",
	"gridx/modules/Pagination", 
	"gridx/modules/pagination/PaginationBar",
	"gridx/modules/Persist", 
	"gridx/modules/select/Row", 
	"gridx/modules/CellWidget" ,
	"dojo/topic"], function(
		declare, JsonGridStore, Grid, 
		Sort, ColumnResizer, Filter,SummaryFilterBar,
		Pagination, PaginationBar,Persist, 
		SelectRow, CellWidget, topic) {

	var base = Grid;

	return declare("ddm.zui.page.base.explorer.PopupSummaryGrid", base, {
		root_id : null,
		sys_page : "null",
		pageMenu : null,
		// modules: [Sort, ColumnResizer, Filter, Pagination,PaginationBar, SelectRow],
	
		modules: [Sort, ColumnResizer, Filter, Pagination,PaginationBar, SelectRow, CellWidget],
		barTop:  [{pluginClass: SummaryFilterBar, style: 'text-align: right;'}],
		postMixInProperties : function() {
			/**
			 * Creates the summary store and attach with summary Grid
			 */	
			this.store = new JsonGridStore({
				pageMenu : this.pageMenu,
				pageType : "summary",
				containerId : this.containerId
			});				      
			this.inherited(arguments);
		},
		/**
		 * onRow selection event handler. 
		 * Will be called whenever a Row is clicked/selected
		 */
		// evtRowSelect:function(){
		// 	console.log(this);
		// 	console.log(this.bar.top[0]);
		// 	//this.advFilter.quickFilter.disableBtnDelete(false);
		// },

		/**
		 * onCell Double click event  publishes the event message 
		 * to trigger opening the Explorer page of the selected CI Id.
		 */
		// evtCellDblClick:function(e){
		// 	dojo.stopEvent(e);
		// 	alert("called")
		// }
	});

});