/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/page/base/explorer/_ExplorerPage", 
	[
		'dojo/_base/declare',
		"dojo/topic",
		"../Base", 
		"dijit/form/Button",
		'dijit/layout/ContentPane',
		"ddm/util/config", 
		'ddm/zui/form/Icon',
	],
	function( declare, topic, widgetBase, Button,
			ContentPane, config) {
	var base = [widgetBase ];	
	return declare('ddm.zui.page.base.explorer._ExplorerPage', base, {
			
			mode : 1,  //view Record 1, edit Record 2, new Record 3
			
			baseClass:'dijitTabContainer',

			tabWidgetName : config.page.expTabWidget,
			formWidgetName : config.page.expFormWidget,
			formsWidgetName: config.page.expFormsWidget,
			tableWidgetName : config.page.expTableWidget,
			singleTabWidgetName : config.page.expSingleTabWidget,
			chartWidgetName: config.page.expChartContainer,
			fileWidgetName:config.page.expFileContainer,

			_getValueAttr: function(){
				var data = this.inherited(arguments);
				if(null == this.itemId || undefined == this.itemId)
					return {center : data.fields};
				else					
					return {ciId:this.itemId, name:this.title, center : data.fields};
			},
			setTitle:function(item){},
			deleteRecord:function(){
				var proceed  = confirm("Are you sure you want to delete this record \n\n This action cannot be reverted");
				if(proceed == true){
					var data = this.get('value');
					var options = {};
					var _this = this;					
					if(data.ciId){
						options.id = data.ciId;					
						this.citStore.remove(data.ciId).then(function(result){
							_this.onDelete(data.ciId);
						}, 
						function(err){
							topic.publish("toaster", err.responseText);
							console.error(err.responseText);
						});					
					}				
				}
			},
			focus:function(){
				var _center = this.centerContainer;
				var widgets = _center.getChildren();
				var widget = widgets[0];
				if(widget && widget.focus){
					//console.log(widget.get('disabled'));
					widget.focus();
				}else{
					widget = widgets[1];
					if(widget && widget.focus)
						widget.focus();
				}
			},
			setupEvents: function(){
				var _this = this;
				var _center = this.centerContainer;
				this.own(dojo.connect(this.centerContainer,"_transition", function(newPage, oldPage){
				   if(!newPage.loaded && newPage.populate){
				    	newPage.populate();
				    }else if(newPage.resume)
						newPage.resume();
					
					if(oldPage && oldPage.pause){
						oldPage.pause();
					}
					if(newPage.calcHeight)
  						newPage.calcHeight();				    
			  	}));
			},
			canEdit:function(){
				if(this.editBtn && this.editBtn.get){
					var disabled = this.editBtn.get('disabled');
					return false == disabled;
				}
				return false;
			},
			canSave:function(){
				if(this.saveBtn && this.saveBtn.get){
					var disabled = this.saveBtn.get('disabled');
					return false == disabled;
				}
				return false;
			},
			editForm: function(){
				this.set('mode',2);
			},
			saveForm: function(options){
				var mode = this.get("mode");
				if(!(mode == 2 || mode ==3 )){
					return ;
				}
				this._saveForm(options);
			},
			_saveForm:function(options){
				var flag = this.isValid();
				if(flag){
					this.preSave();
					var data = this.get('value');
					var _this = this;					
					if(data.ciId)
						options.id = data.ciId;
					//this.citStore.put(data, {headers:{"X-Action":"test"}}).then(function(result){
					this.citStore.put(data, options).then(function(result){
						_this.set('mode',1);
						_this.onSave(result);						
					}, 
					function(err){						
						topic.publish("toaster", err.responseText);
						console.error(err.responseText);
						_this.onSaveError(err);
					});					
				}else{
					topic.publish("/client/script", "Please validate the fields highlighted");
				}
			},
			saveFormNew: function(options){
				var mode = this.get("mode");
				if(!(mode == 2 || mode ==3 )){
					return ;
				}
				var flag = this.isValid();
				if(flag){
					this.preSave();
					var data = this.get('value');
					var _this = this;					
					if(data.ciId)
						options.id = data.ciId;
					//this.citStore.put(data, {headers:{"X-Action":"test"}}).then(function(result){
					this.citStore.put(data, options).then(function(result){
						_this.set('mode',1);
						_this.onSaveNew(result);
					}, 
					function(err){
						topic.publish("toaster", err.responseText);
						console.error(err.responseText);
						_this.onSaveError(err);
					});					
				}
			},
			saveAction:function(action, callback){
				var flag = this.isValid();
				if(flag){
					var data = this.get('value');
					var _this = this;
					this.citStore.put(data, {headers:{"XAction":action}}).then(function(result){
						_this.set('mode',1);
						_this.onSave(result);
						callback(result);
					}, 
					function(err){
						topic.publish("toaster", err.responseText);
						console.error(err.responseText);
					});					
				}
			},
			onSave:function(){
				
			},
			onSaveNew:function(){

			},
			preSave:function(){
			},
			onSaveError:function(){
			},
			onDelete:function(){
			},
			onReset:function(){				
			},
			onClose:function(){
			},
			closeForm: function(){
				this.onClose();
				try{
					var self = this;
					setTimeout(function(){ 
						self.getParent().removeChild(self); 
						self.destroy();
					}, 1000);
					//this.getParent().removeChild(this);
				}catch(err){
					console.error(err);
				}				
				//this.destroy();
				//history.back();
			},
			resetForm: function(){
				this.reset();
				this.set('mode',1);				
				this.onReset();
			},
			postCreate : function(){				
				this.dChildren=[];
				this.inherited(arguments);
				if (!this._started) {				
					this.populateContainers();
					this.loaded=true;
				}
				this.setupEvents();
				this._setModeAttr(this.mode);
				// this.layout();
				this.resize();
				var _this  = this;
				setTimeout(function(){ _this.focus(); }, 100);
				this.setPermission(this.item.permission);
			},
			_setModeAttr:function(mode){
				this.inherited(arguments);
				this.mode = mode;
				if(3 == this.mode)
				{
					if(this.editBtn)
						this.editBtn.set('disabled', true);
					if(this.saveBtn)
						this.saveBtn.set('disabled', false);
				}
			},
			startup: function(){
				this.inherited(arguments);
				if(this.centerContainer.layout)
					this.centerContainer.layout();
			},
			populateContainers:function(){				
				var item = this.item;
				this.set('title', item.label);
				this.setTitle(item);
				
				this.populateTop(item.top, this.topContainer);
				this.populateTab(item.center, this.centerContainer);
				this.populateLeft(item.left);
				this.populateRight(item.right);
			},			

			populateTop:function(component){
				if(this.topContainer)
					this.populateTab(component, this.topContainer);
			},

			populateLeft:function(component){
				if(this.leftContainer && component){
					this.populateTab(component, this.leftContainer);
				}
			},

			populateRight:function(component){
				if(this.rightContainer && component)
					this.populateTab(component, this.rightContainer);
			},

			getField:function(/*String*/ name){
				for(var idx in this.dChildren){
					var child = this.dChildren[idx];					
					if(child.getField){
						var result = child.getField(name);
						if(result)
							return result;
					}
				}
			},

			populateTab:function(tabs, container){
				var idx = 0;

				if (tabs) {
						// var tabs = comp.tabs;
						var uq = this.id;
						for ( var _tab in tabs) {
							var tab = tabs[_tab];
							var options = {	title : tab.label,
											closable : false,appId: tab.id,
											id : uq + tab.name + tab.code,
											tabData : tab,
											mode : this.mode,
											citStore : this.citStore,
											itemId : this.itemId,
											parentId : this.parentId,
											page : this.page,
											widgetId : uq,
											idx:idx,
											formsWidgetName: this.formsWidgetName,
											formWidgetName: this.formWidgetName,
											tableWidgetName : this.tableWidgetName,
											chartWidgetName: this.chartWidgetName,
											fileWidgetName:this.fileWidgetName,
											rootPage: this,
											doLayout:false, args:this.args
										};
							
							this.createWidget(this.tabWidgetName, options, 
									function(widget){
								if(widget){
									container.addChild(widget);	
								//	widget.startup();
									this.dChildren.push(widget);
								}								
							});	
							idx++;				
						}						
					}
			},
			
			getContainer:function(/*String*/ name){
				for(var idx in this.dChildren){
					var child = this.dChildren[idx];

					for(var jdx in child.wChildren){
						var container = child.wChildren[jdx];
						if(container.appId){
							if(container.appId == name)
								return container;
						}
					}
				}
			},
			getItemField:function(/*String*/ name){
				var item = this.item;
				for(var i in item.center){
					var tab = item.center[i];
					for(var j in tab.containers){
						var container = tab.containers[j];
						if(container.formLayout){
							var fields = container.formLayout.fieldLayout;
							for (var k in fields){
								var field = fields[k];
								if(field.name === name)
									return field;
							}
						}
					}					
				}				
			},
			setPermission:function(permission){				
				if(permission != undefined){	
					if(this.editBtn)				
						this.editBtn.set('disabled', !permission.update);
					if(this.deleteBtn)
						this.deleteBtn.set('disabled', !permission.delete);
				}
			},

			destroy:function(){
				this.inherited(arguments);
				delete this.itemId;
				delete this.page;
				delete this.tabWidgetName;
				if(this.citStore && this.citStore.destroy)
					this.citStore.destroy();
				delete this.citStore;			
			}
		});
	}
);