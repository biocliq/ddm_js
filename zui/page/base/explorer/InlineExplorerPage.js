/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/page/base/explorer/InlineExplorerPage", 
	[	'dojo/_base/declare',		
		"ddm/zui/page/base/explorer/_ExplorerPage",	
		'dijit/layout/ContentPane',		
		'dojo/text!./templates/InlineExplorerPage.html',		
		"ddm/zui/layout/TabContainer"
	],
	function( declare, widgetBase, ContentPane,template) {
	var base = [ContentPane, widgetBase ];	
	return declare('ddm.zui.page.base.explorer.InlineExplorerPage', base, {
			templateString : template,
			populate:function(){this.loaded = true;},
			resume:function(){},
			pause:function(){},
			calcHeight:function(){},
			editForm: function(){
				this.inherited(arguments);
				this.saveBtn.set('disabled', false);
				this.editBtn.set('disabled', true);
			},		
			onSave:function(result){
				var self = this;
				var message = "/summary/"+ self.page + "/refresh";
				// topic.publish(message);
				self.editBtn.set('disabled', false);
				self.saveBtn.set('disabled', true);
				if(null == this.itemId || undefined == this.itemId){
					self.newItemId = result.ciId;
				}
			},
			onReset:function(){
				this.editBtn.set('disabled', false);
				this.saveBtn.set('disabled', true);
			}
			// ,
			// destroy:function(){
			// 	this.inherited(arguments);
			// }
		});
	}
);