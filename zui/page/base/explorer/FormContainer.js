/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */
 
define("ddm/zui/page/base/explorer/FormContainer", [
	"dojo/_base/declare", "dijit/layout/ContentPane","ddm/zui/layout/FieldSet",
	"ddm/zui/page/base/explorer/_FormContainer"],
		function(declare, ContentPane,FieldSet, _FormContainer) {

	var base = [FieldSet, _FormContainer];

	return declare("ddm.zui.page.base.explorer.FormContainer", base, {	
		baseClass: "dijitBorderTop"
	});
});
