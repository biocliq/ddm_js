/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/ExplorerGridDialog",
 	[ 'dojo/_base/declare', "dojo/_base/lang", "dojo/topic",  "dojo/on",
 	"dojo/dom-style", "dijit/layout/ContentPane", "ddm/zui/widget/Dialog",
 	"dijit/form/Button", "ddm/gridx/modules/toolbar/ExplorerFilterBar",
	"ddm/wire/store/grid/PopupGridStore"
	], 
function(declare,lang, topic,on,
	domStyle,ContentPane,Dialog,
	Button,FilterBar,
	PopupGridStore) {

	var base = [Dialog];

	return declare("ddm.zui.page.base.explorer.ExplorerGridDialog", base, {	
		resizeAxis:'xy',
		postCreate:function(){
			this.inherited(arguments);
			var t = this.grid;
			
			var popupGridStore = new PopupGridStore({
				itemId : t.itemId,
				pageMenu : t.pageMenu,
				containerId : t.containerId
			});
					
			var options = {	containerId : t.containerId,
							itemId : t.itemId,
							structure : t.structure,
							store : popupGridStore,
							fields : t.fields,
							style: "width: 800px"
						};
			var toolPane = new ContentPane();
			this.popupGrid = new FilterBar(options);
			var btn = new Button({label : 'Add', style:'align:right'});
			this.addChild(this.popupGrid);
			domStyle.set(toolPane.containerNode, 'float', 'right');
			this.addChild(toolPane);
			toolPane.addChild(btn);
			this.own(on(btn, "click", lang.hitch(this, 
						function(){this.addRecord(arguments)})));
		}, 
		addRecord:function(/* Click Event */ event){
			var pgrid = this.popupGrid;
			var store = pgrid.store;
			var selectedRow = pgrid.select.row.getSelected();
			var _this = this;
			if(selectedRow.length>0)
			{
				store.put(selectedRow).then(
					/** Success callback */
					function(result){
						_this.grid.refresh();
						_this.hide();
						//Show the success message and close the dialog
					},
					/** Error Callback */
					function(err){
						topic.publish("toaster", err.responseText);
						console.error(err.responseText);
					}
				); 
			}else{
				topic.publish("toaster", "Select atleast one record");
			}
		}, 
		destroy:function(){
			this.inherited(arguments);
			delete this.popupGrid;
			delete this.store;
		}
	});
});