/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/FieldSetContainer", [
	"dojo/_base/declare", "ddm/zui/layout/FieldSet",
	"ddm/zui/page/base/explorer/_FormContainer"],
		function(declare, FieldSet, _FormContainer) {

	var base = [FieldSet, _FormContainer];

	return declare("ddm.zui.page.base.explorer.FieldSetContainer", base, {	
		baseClass: "dijitBorderTop"
		//  postCreate:  function(){
		// 	 this.inherited(arguments);
		// // 	// console.log("this.field.")
		// 	//  console.log(this.field)
		// 	//  console.log()
		//  }
	});
});
