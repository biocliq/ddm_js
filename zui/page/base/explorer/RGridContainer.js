/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/RGridContainer", [
	"dojo/_base/declare",
	"dojo/topic",
	"dojo/_base/lang",
	"dijit/layout/ContentPane",
	"ddm/zui/layout/FieldSet",
	"../GridContainer", "ddm/zui/page/base/explorer/SimpleGrid"],
		function(declare, topic, lang,ContentPane,
			FieldSet, 
			GridContainer) {

	var base = [FieldSet, GridContainer];

return declare("ddm.zui.page.base.explorer.RGridContainer", base, {
	grid:"ddm/zui/page/base/explorer/SimpleGrid",
	style:"margin-top:20px; margin-left:10px; width:90%;",
	// permission:{},
	postMixInProperties:function(){
		this.permission = {};
		var tableData = this.data.tableLayout.tableData;
		if(tableData){
			this.toggleable = true;
			var count = tableData.totalCount;
			if(count > 0)
				this.title = this.title + ' (' + count + ')';
		}else{
			this.toggleable = false;
			this.disabled = true;
			this.open = true;
		}
		this.inherited(arguments);
	},
	resize:function(sd){
		if(this._grid)
			this._grid.resize(sd);
	},
	getGridOptions:function(){
		var options = this.inherited(arguments);
		options.autoHeight = true;
		options.itemId = this.itemId;	
		options.permission=this.data.permission;
		options.tgtPageMenu = this.data.tgtPage;
		options.title = this.title;
		// console.log(this.grid_options);
		options = lang.mixin(options, this.grid_options);
		return options;
	}
	});

});
