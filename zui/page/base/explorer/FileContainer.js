/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/FileContainer",[
    "dojo/_base/declare",
    "dojo/topic",
    "dijit/_WidgetBase",
    "dijit/_OnDijitClickMixin",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dijit/layout/ContentPane", 
    "ddm/zui/form/FileUpload",
    "ddm/wire/store/grid/AttachmentGridStore",
    "ddm/zui/widget/FileAttachGrid",
    "dojo/text!./templates/FileContainer.html",
    "dijit/form/Button",
    "ddm/zui/layout/FieldSet"
    ], function(declare, topic, _WidgetBase, _OnDijitClickMixin, _TemplatedMixin,
                _WidgetsInTemplateMixin, ContentPane, FileUpload, AttachmentGridStore,FileAttachGrid,
                template, Button, FieldSet) {
        
        var base = [ContentPane, _WidgetBase, _OnDijitClickMixin, _TemplatedMixin,_WidgetsInTemplateMixin];

        return declare("ddm.zui.page.base.explorer.FileContainer", base, {
            templateString: template, 
            baseClass:"dijitBorderTop", 
            postCreate:function(){
                this.inherited(arguments);
                var t = this;                
                var options = {
                    itemId : t.itemId,
                    pageMenu : t.pageMenu,
                    idProperty:"_id",
                    container : t.containerName
                };
               
                this.fileUpload = new FileUpload(options, this.tusUploader);                
                var fileStore = new AttachmentGridStore(options);
                this.fileStore = fileStore;

                var options = this.getGridOptions();
                options.store = fileStore;

                var grid = FileAttachGrid(options,this.fileGrid);            
                
                this.own(
                    topic.subscribe("fileUpload/" + this.pageMenu + "/" + this.itemId,
                        function(/*String file */ file ){
                            grid.refresh();
                        }
                    )
                )
                
            },

            getGridOptions:function(){
                var container = this.data;
                var tableData;
                var grid ;
                if(container.tableLayout){
                    tableData = container.tableLayout.tableData;
                    grid = container.tableLayout.gridLayout;
                }
                var options = {fields : grid,widgetId : this.widgetId,
                    layout:container.tableLayout,
                    pageMenu : this.pageMenu,
                    data : tableData,
                    title : container.label,                  
                    containerName : container.name,
                    options:{}
                };			

                declare.safeMixin(options.options, container.options);			
                

                options.autoHeight = true;
                options.itemId = this.itemId;	
                options.permission=this.data.permission;                
                options.mode = this.mode;	
                options.grid_options=this.grid_options;
                return options;
            },
            _setModeAttr(flag){  
                if(this.fileUpload){
                    if(flag != 1)
                        this.fileUpload.set('disabled', (!widget.editable || widget.editable ==2));
                    else
                        this.fileUpload.set('disabled', true);                
                }
            }
        });
});
    