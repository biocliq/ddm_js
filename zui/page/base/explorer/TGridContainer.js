/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/TGridContainer", [
	"dojo/_base/declare",
	"dojo/topic",
	"dojo/_base/lang",
	"dijit/layout/ContentPane",
	"../GridContainer", "ddm/zui/page/base/explorer/SimpleGrid"],
		function(declare, topic, lang,			
			ContentPane,GridContainer) {

	var base = [ContentPane, GridContainer];

	return declare("ddm.zui.page.base.explorer.TGridContainer", base, {
		grid:"ddm/zui/page/base/explorer/SimpleGrid",
		toggleable:false,	
		permission:{},
		resize:function(sd){
			if(this._grid)
				this._grid.resize(sd);
		},
		getGridOptions:function(){
			var options = this.inherited(arguments);
			options.autoHeight = true;
			options.itemId = this.itemId;	
			options.permission=this.data.permission;
			options.tgtPageMenu = this.data.tgtPage;			
			options.mode = this.mode;	
			options.grid_options=this.grid_options;
			options = lang.mixin(options, this.grid_options);
			// console.log(options);
			return options;
		}	
	});

});
