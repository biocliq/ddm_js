/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/page/base/explorer/PopupExplorerPage", 
	[	'dojo/_base/declare',		
		"ddm/zui/page/base/explorer/_ExplorerPage",	
		'dijit/layout/ContentPane',		
		'dojo/text!./templates/PopupExplorerPage.html',		
		"ddm/zui/layout/TabContainer"
	],
	function( declare, widgetBase, ContentPane,template) {
	var base = [ContentPane, widgetBase ];	
	return declare('ddm.zui.page.base.explorer.PopupExplorerPage', base, {
			templateString : template,
	
			editForm: function(){
				this.inherited(arguments);
				this.saveBtn.set('disabled', false);
				this.editBtn.set('disabled', true);
			},		
			onSave:function(){
				// this.getParent().hide();
			},
			onReset:function(){
				this.editBtn.set('disabled', false);
				this.saveBtn.set('disabled', true);
			}
		});
	}
);