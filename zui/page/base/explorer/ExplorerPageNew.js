/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/ExplorerPageNew", 
	[	'dojo/_base/declare',	
		'dojo/topic',	"dojo/dom-style",
		"ddm/zui/page/base/explorer/ExplorerPage",	
		'dijit/layout/ContentPane',	
		'dojo/text!./templates/ExplorerPageNew.html',
		'ddm/zui/page/base/explorer/PopupExplorerPage',
		"ddm/zui/page/base/explorer/ExplorerTab",
		"ddm/zui/page/base/explorer/FormContainer",
		"ddm/zui/layout/FlowLayoutContainer",
		"ddm/zui/page/base/explorer/TGridContainer",
		"ddm/fluwiz/chart/ChartContainer"
	],
	function( declare, topic,domStyle,widgetBase, ContentPane, template) {
	var base = [ ContentPane,widgetBase ];	
	return declare('ddm.zui.page.base.explorer.ExplorerPage', base, {
			templateString : template,
			setTitle:function(item){
				if(item.label){
					if(item && this.titleBar)
						this.titleBar.innerHTML = item.label;
				}else{
					domStyle.set(this.titleBar, "display", "none");
				}
			},			
			preSave:function(){
				this.saveBtn.set('disabled', true);
			},
			onSaveError:function(){
				this.saveBtn.set('disabled', false);
			},
			onSave:function(result){
				var self = this;
				//var message = "/summary/"+ self.page + "/refresh";
				//topic.publish(message);				
				self.saveBtn.set('disabled', true);
				if(null == this.itemId || undefined == this.itemId){
					self.newItemId = result.ciId;
					setTimeout(function(){ self.closeForm(); }, 500);
				}else{
					self.newItemId = undefined;
				}
			},
			onSaveNew:function(result){
				var self = this;
				var message = "/form/new/"+ self.page;
				topic.publish(message);				
				self.saveBtn.set('disabled', true);
				if(null == this.itemId || undefined == this.itemId){
					self.newItemId = result.ciId;
					setTimeout(function(){ self.closeForm(); }, 500);
				}else{
					self.newItemId = undefined;
				}
			},
			onReset:function(){			
				this.saveBtn.set('disabled', true);
			}
		});
	}
);