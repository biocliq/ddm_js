/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/PopupGrid", [ "dojo/_base/declare",
	"dojo/_base/array",
	'ddm/wire/store/grid/JsonGridStore',
	'dojo/store/Cache',
	"dojo/date/locale",
		'ddm/fluwiz/grid/ddmGrid', 'gridx/core/model/cache/Async',
		'gridx/modules/SingleSort', 'gridx/modules/ColumnResizer',
		'gridx/modules/Filter', "ddm/gridx/modules/toolbar/PopupFilterBar",
		"gridx/modules/Pagination", "gridx/modules/pagination/PaginationBar",
		"gridx/modules/HiddenColumns", "gridx/modules/select/Row",
		"gridx/modules/IndirectSelect","gridx/modules/extendedSelect/Row",
		"gridx/modules/RowHeader","gridx/modules/move/Column",
		"gridx/modules/dnd/Column","gridx/modules/extendedSelect/Column",
		"dojo/store/Memory"], function(
		declare, array, JsonGridStore, StoreCache, locale, Grid, AsyncCache, Sort, ColumnResizer, Filter, PopupFilterBar,
		Pagination, PaginationBar, HiddenColumns, SelectRow,
		IndirectSelect, ExSelectRow, RowHeader, moveColumn, dndColumn,selectColumn,  Memory) {

	var base = Grid;

	return declare("ddm.zui.page.base.explorer.PopupGrid", base, {
		root_id : null,
		sys_page : "null",
		paginationInitialPageSize:10,
		paginationBarSizes: [10, 25, 50],
		autoHeight:false,
		cacheClass: AsyncCache,
		selectRowTriggerOnCell: true,
		modules : [ Sort, ColumnResizer, Filter,Pagination,
					PaginationBar, RowHeader,SelectRow, IndirectSelect, ExSelectRow],
		barTop : [{pluginClass: PopupFilterBar, style: 'text-align: right;'}]
	});

});
