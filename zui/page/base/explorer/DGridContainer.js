/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 define("ddm/zui/page/base/explorer/DGridContainer", [
	"dojo/_base/declare",
	"dojo/topic",
	"dojo/_base/lang",
	"ddm/zui/layout/FieldSet",
	"dijit/layout/ContentPane",
	"../GridContainer", "ddm/zui/page/base/explorer/SimpleGrid"],
		function(declare, topic, lang,FieldSet, ContentPane,GridContainer) {

	var base = [ContentPane, GridContainer];

	return declare("ddm.zui.page.base.explorer.DGridContainer", base, {
		grid:"ddm/zui/page/base/explorer/SimpleGrid",
		toggleable:false,
		style:"margin-top:20px; margin-left:10px",
		permission:{},
		resize:function(sd){
			if(this._grid)
				this._grid.resize(sd);
		},
		getGridOptions:function(){
			var options = this.inherited(arguments);
			options.autoHeight = true;
			options.itemId = this.itemId;
			options.permission=this.permission;
			options.title = this.title;
			return options;
		},
		postGridCreate:function(widget){
			this.addChild(widget);
			this._grid = widget;
			var parent = this.parent;
			
			if(this.tgtPageMenu){
				this.topicHandle = topic.subscribe("/popup/summary/refresh/"+ this.itemId + "/" + this.tgtPageMenu, 
					lang.hitch(this, function(args){
						try {
							this.refresh();
						}catch(e){
							console.error(e);
						}
					})
				);
			}
		},
		destroy:function(){
			if(this.topicHandle){
				this.topicHandle.remove();
			}
			this.inherited(arguments);
		}
	});

});
