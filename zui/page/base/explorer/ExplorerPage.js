/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/explorer/ExplorerPage", 
	[	'dojo/_base/declare',	
		'dojo/topic',	"dojo/dom-style",
		"ddm/zui/page/base/explorer/_ExplorerPage",	
		'dijit/layout/ContentPane',	
		'dojo/text!./templates/ExplorerPage.html',
		'ddm/zui/page/base/explorer/PopupExplorerPage',
		"ddm/zui/page/base/explorer/ExplorerTab",
		"ddm/zui/page/base/explorer/FormContainer",
		"ddm/zui/layout/FlowLayoutContainer",
		"ddm/zui/page/base/explorer/TGridContainer",
		"ddm/fluwiz/chart/ChartContainer"
	],
	function( declare, topic,domStyle,widgetBase, ContentPane, template) {
	var base = [ ContentPane,widgetBase ];	
	return declare('ddm.zui.page.base.explorer.ExplorerPage', base, {
			templateString : template,
			setTitle:function(item){
				if(item.label){
					if(item && this.titleBar)
						this.titleBar.innerHTML = item.label;
				}else{
					domStyle.set(this.titleBar, "display", "none");
				}
			},			
			editForm: function(){
				this.inherited(arguments);
				this.saveBtn.set('disabled', false);
				this.editBtn.set('disabled', true);
			},
			onDelete:function(){
				var self = this;
				var message = "/summary/"+ self.page + "/refresh";
				topic.publish(message);
				self._closeForm();
			},
			preSave:function(){
				this.editBtn.set('disabled', false);
				this.saveBtn.set('disabled', true);
			},
			onSaveError:function(){
				this.editBtn.set('disabled', true);
				this.saveBtn.set('disabled', false);
			},
			onSave:function(result){
				var self = this;
				//var message = "/summary/"+ self.page + "/refresh";
				//topic.publish(message);
				self.editBtn.set('disabled', false);
				self.saveBtn.set('disabled', true);
				if(null == this.itemId || undefined == this.itemId){
					self.newItemId = result.ciId;
					setTimeout(function(){ self.closeForm(); }, 500);
				}else{
					self.newItemId = undefined;
				}
			},
			onSaveNew:function(result){
				var self = this;
				var message = "/form/new/"+ self.page;
				topic.publish(message);				
				self.saveBtn.set('disabled', true);
				if(null == this.itemId || undefined == this.itemId){
					self.newItemId = result.ciId;
					setTimeout(function(){ self.closeForm(); }, 500);
				}else{
					self.newItemId = undefined;
				}
			},
			onReset:function(){
				this.editBtn.set('disabled', false);
				this.saveBtn.set('disabled', true);
			},
			onClose:function(){
				if((null == this.itemId || undefined == this.itemId)){
					if(this.newItemId){
						this._closeNewForm();
					}else
						this._closeUnsavedForm();
				}
				else
					this._closeForm();
			},
			_closeUnsavedForm:function(){
				var url = window.location.href;
				url = url.substring(0, url.lastIndexOf('/'));
				window.location.replace(url + "/summary");
			},
			_closeNewForm:function(){				
				var url = window.location.href;
				url = url.substring(0, url.lastIndexOf('/'));
				window.location.replace(url + "/view/" + this.newItemId);				
			},
			_closeForm:function(){				
				var url = window.location.href;				
				url = url.substring(0, url.lastIndexOf('/', url.lastIndexOf('/')-1));
				window.location.replace(url + "/summary");				
			}, extractValue:function(/* string */ name, /* value Object */ value){
				var tabs = value.center;
				for(var _tab in tabs){
					var tab = tabs[_tab];
					for(var _cnt in tab.containers){
						var container = tab.containers[_cnt];
						if(container){
							var layout = container.formLayout;
							if(layout){
								for(var _fld in layout.fieldLayout){
									var field = layout.fieldLayout[_fld];
									if(field.code == name)
										return field.value;
								}
							}
						}
					}
				}
			}			
		});
	}
);