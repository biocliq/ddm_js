/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/page/base/Loader",	[
		'dojo/_base/declare',
		'dojo/_base/lang',
		'require'
	],
	function( declare, lang, require) {
	var base = null;
	
	return declare('ddm.zui.page.base.Loader', base, {				
			createWidget:function(widgetName, options, callback){
			   require([widgetName],
	               lang.hitch(this, function( widget ){
					   	try{
	                    	var _widget = new widget(options);
							if(undefined != callback)
	                    		callback.call(this, _widget);
						}catch(err){
							console.log(err);
							console.log(options);
						}						
	               })
		       );
			}
		});
	}
);