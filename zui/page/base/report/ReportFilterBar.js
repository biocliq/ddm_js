/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define([
        'dojo/_base/declare',
        'dojo/_base/lang',        
		'dojo/topic',
		"dijit/TitlePane",
		"dijit/layout/AccordionContainer",
		'gridx/modules/Filter',
		'dojo/text!./templates/ReportFilterBar.html',
		'ddm/gridx/modules/filter/_FilterBar',
		"ddm/zui/form/DropDownButton"
        ], function(declare, lang, topic,TitlePane,AccordionContainer,
        		F, template, _FilterBar){

	return declare([ _FilterBar], {
		templateString: template,		
					
		downloadXLS:function(){			
			this.grid.store.getXlsReport();
		}
	});
});

