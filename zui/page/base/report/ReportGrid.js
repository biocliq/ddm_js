/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/report/ReportGrid", [ 
	"dojo/_base/declare",
	'ddm/wire/store/grid/JsonGridStore', 
	'ddm/fluwiz/grid/ddmGrid', 
	'gridx/modules/SingleSort', 
	'gridx/modules/ColumnResizer',
	'gridx/modules/Filter', 
	"ddm/zui/page/base/report/ReportFilterBar",
	"gridx/modules/Pagination", 
	"gridx/modules/pagination/PaginationBar",
	"gridx/modules/Persist", 
	"gridx/modules/select/Row", 
	"gridx/modules/CellWidget" ,
	"dojo/topic"], function(
		declare, JsonGridStore, Grid, 
		Sort, ColumnResizer, Filter,ReportFilterBar,
		Pagination, PaginationBar,Persist, 
		SelectRow, CellWidget, topic) {

	var base = Grid;

	return declare("ddm.zui.page.base.report.ReportGrid", base, {		
		filterBarClass:"ddm/zui/page/base/report/ReportFilterBar",
		postMixInProperties : function() {
			// Creates the report store and attach with report Grid	
			this.store = new JsonGridStore({
				name:"reportGridStore",
				pageMenu : this.pageMenu,
				pageType : "report",
				idProperty:"_id",
				containerName : this.containerName
			});				      
			this.inherited(arguments);
		},
		gridModulesCustomize : function(){
			this.inherited(arguments);			
			if(undefined == this.moduleOptions){
				this.modules.push("gridx/modules/Pagination", "gridx/modules/pagination/PaginationBar");
			}
		},

		/**
		 * disable double click event
		 */
		evtCellDblClick:function(e){
			dojo.stopEvent(e);
		}
	});

});
