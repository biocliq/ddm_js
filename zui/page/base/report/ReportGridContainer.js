/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/report/ReportGridContainer", [
	"dojo/_base/declare", "dojo/_base/lang",
	"dijit/layout/ContentPane",
	"../GridContainer", 
	"ddm/zui/page/base/report/ReportGrid"], 
		function(declare,  lang,
				ContentPane,  GridContainer) {

	var base = [ContentPane, GridContainer];

	return declare("ddm.zui.page.base.report.ReportGridContainer", base, {
		grid: "ddm/zui/page/base/report/ReportGrid",
		padding: "0px",
        doLayout :"false",
		permission:{},
		startup:function()
		{
		//	this.permission = options.permission;
			this.inherited(arguments);
		},
		getGridOptions:function(){
			var options = this.inherited(arguments);			
			options.autoHeight = false;
            options.style="height:100%";
			options.permission=this.permission;	
			options = lang.mixin(options, this.grid_options);		
			return options;
		}
	});

});