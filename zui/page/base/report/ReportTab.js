/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/base/report/ReportTab", [ 
	"dojo/_base/declare",
	"dojo/_base/array", 
	"dijit/layout/ContentPane",  
    "./ReportGridContainer"], 
		function(declare, array,ContentPane,ReportGridContainer) {

	var base = ContentPane;

	return declare("ddm.zui.page.base.report.ReportTab", base, {
		page : null,
		resize : function(){
			this.inherited(arguments);
		},
		
		populate : function(){
			if(this.loaded)
				return;
			if(this.tabData){
				if(this.tabData.containers){
					this._populate(this.tabData.containers);
					this.loaded = true;
				}
				else{
					var _t = this;
					var cp = new ContentPane();
					this.addChild(cp);				
				}
			}
		}, 
		_populate : function(containers){
		    for(var _cont in containers){
				var container = containers[_cont];
				var cntWidget;
				if(!container.tableLayout)
					return;
				var tableData = container.tableLayout.tableData;
				switch(container.contentType){
				case 1:
					break;
				case 2: {
				
						var options = {
							pageMenu : this.page,
							style : "height:100%",
							autoHeight : true,
							data : container,
							permission:container.permission
						};

						// if(container.options){							
						// 	var _opt = JSON.parse(container.options);
						// 	if(_opt.toggleable != undefined)
						// 		tblWidget = "ddm/zui/page/base/explorer/RGridContainer";
						// 	if(_opt.open == false)
						// 		options.open = _opt.open;
						// 	options.grid_options = _opt;
						// }

						if(container.reference)
							options.grid = container.reference;
					
						cntWidget = new ReportGridContainer(options);
					break;
					}
				}
				
				if(cntWidget)
					this.addChild(cntWidget);				
		    }
		}, 
		refresh:function(){
			var children = this.getChildren();
			array.map(children, function(child){
				if(child.refresh){
					child.refresh();
				}
			});
		}
	});

});