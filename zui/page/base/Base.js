/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 
define("ddm/zui/page/base/Base",	[
		'dojo/_base/declare',
		'dijit/_TemplatedMixin',
	    'dijit/_WidgetsInTemplateMixin',
	    './FormBase', './Loader'
	],
	function( declare, 
	_TemplatedMixin, _WidgetsInTemplateMixin, FormBase, Loader) {
	var base = [_TemplatedMixin,_WidgetsInTemplateMixin, FormBase, Loader];
	/**
	 * Base class combining the functionalities of Dynamic Loader 
	 * and form functionlities
	 */
	return declare('ddm.zui.page.base.Base', base, {
			templateString:'<div></div>'
		});
	}
);