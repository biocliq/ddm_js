/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/user/ChangeFacility",
 [ 'dojo/_base/declare',
 "dojo/dom",
 'dojo/text!./templates/ChangeFacility.html', 
 "ddm/zui/page/base/Base",  
 "dijit/layout/ContentPane",
 "ddm/wire/store/base/JsonUserStore","dojo/store/Memory",
 "dijit/form/TextBox",
 "dojo/cookie",
 "dijit/form/Button"],
function(declare,dom,template,widgetBase, 
				ContentPane,JsonUserStore,Memory,TextBox,cookie,
				Button) {
	var base = [ContentPane, widgetBase]  

	return declare("ddm.zui.page.user.ChangeFacility", base, {
		templateString : template, 
		postCreate : function(){
			this.inherited(arguments);
			var _this=this;
			var options = {};
			var userStore = new JsonUserStore(options);
			userStore.getFacilities().then (function (data){
					var store = new Memory({
						data:data						
					}); 
					_this.facility.labelAttr= "name";
					_this.facility.set('store',store);
					var facility=dojo.cookie("facility");
					
					_this.facility.set("value",facility);
					_this = null;
					delete _this;
				});	
			
		}

		});

});