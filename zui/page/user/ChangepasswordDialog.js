/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define( [
	"dojo/_base/declare",
	"dojo/dom-class",
	"dojo/topic",
	"dijit/Dialog",
	"./ChangePassword",
	'dojox/encoding/digests/MD5',
	'ddm/util/config'
], function(declare, css,topic,Dialog, ChangePassword, MD5Enc,config){

	return declare(Dialog, {
		cssClass: 'gridxFilterDialog',
		autofocus: true,
		postCreate: function(){
			this.inherited(arguments);
			
			this.set('title','Change Password');
			this.addChild(new ChangePassword({				
				onSubmit: function(){
					var _this = this;
					var crntPsswrd = this.cPass.get('value');
					var newPsswrd = this.nPass.get('value');
					var rnewPsswrd = this.rPass.get('value');
					if((crntPsswrd!='')&&(newPsswrd!='')&&(rnewPsswrd!='')) {
					if(newPsswrd==rnewPsswrd) {
						//crntPsswrd = MD5Enc(crntPsswrd, 1);
						//rnewPsswrd = MD5Enc(rnewPsswrd, 1);
						var formData = {"currentPwd" : crntPsswrd,"newPwd" : rnewPsswrd};	
						var headers = {"Content-Type": "application/json"};				
						var xhrArgs = {
								url : config.url.base + config.url.passwdChange,
								postData:JSON.stringify(formData),
								preventCache: true,
								handleAs : "json",
								headers: headers,
								load : function(data){
									console.log("Pwd changed");
									_this.getParent().hide();
								},
								error:function(error){
									console.log(error);
									_this.getParent().hide();
									switch(error.status){
									case 401:{ 										
										topic.publish("/server/error/401");
										break;
									}
									case 403:{ 
										topic.publish("toaster", "Invalid Current password");
										break;
									}
									case 404:
									case 502:{
										console.log("Application server is down");
										topic.publish("toaster","Application server could be down. Please try again after sometime");
										break;
									}
									default:
									topic.publish("toaster","Unknown Error occured. Please try again after sometime ");
									}
								}
							};
							dojo.xhrPost(xhrArgs);
						}
						else {
							topic.publish("toaster","password is not matching");

						}
					} 
					else {
						topic.publish("toaster","All fields are mandatory");
					}					
				},
				
				onClear: function(){
					this.container.reset();
					this.container.hide();
				},
								
			}));
			
			css.add(this.domNode, 'gridxFilterDialog');
		},
	});
});
