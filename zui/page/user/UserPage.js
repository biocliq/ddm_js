/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/user/UserPage",
 [ 'dojo/_base/declare',
	"dojo/topic",
	'dojo/text!./templates/UserPage.html', 
	"ddm/zui/page/base/explorer/ExplorerPage",
	"ddm/zui/page/user/ResetPassword",
	"ddm/zui/widget/Dialog",
	'dojox/encoding/digests/MD5',
	'ddm/util/config'

		], 
		function(declare,topic,template,
			widgetBase, ResetPasswd, Dialog,
			MD5Enc, config) {

	var base = [widgetBase];

	return declare("ddm.zui.page.user.UserPage", base, {
		
		templateString : template, 
		showResetPasswordDialog : function(){			
			console.log(this);
			var val = this.get("value");
			var email = this.extractValue("email", val);
			var login = this.extractValue("loginName", val);
			var _this = this;
			var itemid = this.itemId;
			var rspwd = new ResetPasswd(
				{	
					userId:itemid,
					onSubmit:function(){
					var _this = this;					
					var newPsswrd = this.nPass.get('value');
					var rnewPsswrd = this.rPass.get('value');
					
					if((newPsswrd!='')&&(rnewPsswrd!='')) {
					if(newPsswrd==rnewPsswrd) {						
						//rnewPsswrd = MD5Enc(rnewPsswrd, 1);
						var formData = {"passwd" : rnewPsswrd, "email": email, "loginName" : login};
						var headers = {"Content-Type": "application/json"};				
						var xhrArgs = {
								url : config.url.base + config.url.adminResetPassword,
								postData:JSON.stringify(formData),
								preventCache: true,
								handleAs : "json",
								headers: headers,
								load : function(data){
									console.log("Pwd changed");
									_this.getParent().hide();
								},
								error:function(error){
									console.log(error);									
									switch(error.status){									
									case 404:{
										topic.publish("toaster.error", "User not found in the system");
										break;
									}
									case 502:{										
										topic.publish("toaster.error","Application server could be down. Please try again after sometime");
										break;
									}
									default:
										topic.publish("toaster.error","Unknown Error occured. Please try again after sometime ");
									}
								}
							};
							dojo.xhrPost(xhrArgs);
						}
						else {
							console.log("topic published");
							topic.publish("toaster.warn","password is not matching");
							
						}
					} 
					else {
						topic.publish("toaster","All fields are mandatory");
					}
				},
				onClear:function(){
					this.reset();
					this.getParent().hide();
				}
			}
			);
			var dg = new Dialog();
			dg.addChild(rspwd);
			dg.show();
		}	
	});

});