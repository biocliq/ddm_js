/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/page/usermgmt/Profile", 
	[
		'dojo/_base/declare',
		'dojo/_base/lang',
		'dojo/_base/array',
		'dojo/on',
		'require',
		'dojo/dom-style',
		"ddm/zui/page/base/Base", 
		'ddm/util/config',
		'dijit/layout/ContentPane',
	    'dojo/text!./templates/Profile.html', 
		"dijit/layout/TabContainer"
	],
	function( declare,lang,array,on, require, domStyle, widgetBase, config,
			ContentPane, template, TabContainer) {
	var base = [ContentPane, widgetBase ];	
	
	return declare('ddm.page.usermgmt.Profile', base, {
			templateString : template,
			customerDataUrl : config.url.customerList,
	
			createRecord:function(){
				console.log('Create Record called');
				if(this.isValid())
					console.log(this.get('value'));
			},
	
			_getValueAttr:function(){
				var data = array.map(this.fields.getChildren(), function(widget){
					return {id: widget.id, name : widget.name, value : widget.get('value')};
				});
				return data;
			}, 
			
			isValid:function(){
				return array.every(this.fields.getChildren(), function(widget){
					if(widget['isValid']){
						var status = widget.isValid();
						if(!status){
							widget.set("state", "Error");
						}
						return status;
					}
					return true;
				});
			}
	});
	}
);