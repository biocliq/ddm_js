/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/user/ResetPassword",
 [ 'dojo/_base/declare',
 'dojo/text!./templates/ResetPassword.html', 
 "ddm/zui/page/base/Base",  
 "dijit/layout/ContentPane",
 "dijit/form/TextBox",
 "dijit/form/ValidationTextBox",
 "dijit/form/Button",
 "dojox/layout/TableContainer"

		], 
		function(declare,template,widgetBase, 
				ContentPane,TextBox,ValidationTextBox,
				Button,TableContainer) {

	var base = [ContentPane, widgetBase];

	return declare("ddm.zui.page.user.ResetPassword", base, {
		
		templateString : template, 
	
		});

});