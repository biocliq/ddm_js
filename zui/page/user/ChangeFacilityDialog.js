/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define( "ddm/zui/page/user/ChangeFacilityDialog",
	["dojo/_base/declare",
	"dojo/dom-class",
	"dojo/dom",
	"ddm/zui/widget/Dialog",
	"ddm/zui/page/user/ChangeFacility",
	"ddm/wire/store/base/JsonUserStore",
	"dojo/store/Memory",
	//"dijit/form/Select",
	"dijit/form/FilteringSelect",
	"dojo/cookie",
	"ddm/util/config"
	
], function(declare,css,dom,Dialog, ChangeFacility,JsonUserStore,Memory,FilteringSelect,cookie,config){
	
	return declare(Dialog, {
		// cssClass: 'gridxFilterDialog',
		autofocus: true,
		postCreate: function(){
			this.inherited(arguments);
			this.set('title','Choose Facility');	
			this.addChild(new ChangeFacility({	
				onSubmit: function(){
					 var facility= this.facility;
					 cookie("facility",facility,{expires : 7});
					location.href = config.url.mainPage;  
				}
			}));
			
			css.add(this.domNode, 'gridxFilterDialog');
		},
		hide:function(){
			this.destroy(true);
		}
	});
});
