define("ddm/zui/page/role/RoleCI",
 [ 'dojo/_base/declare', "dojo/_base/lang","dojo/on",
 'dojo/text!./templates/RoleCI.html', 
 "ddm/zui/page/base/Base",  
 "dijit/layout/ContentPane",
 "dijit/form/TextBox",
 "dijit/form/ValidationTextBox",
 "dijit/form/Button",
 "ddm/zui/page/base/explorer/StaticColumnGrid",
  "ddm/wire/store/grid/ACLCitStore",
  "ddm/zui/page/role/RoleGridContainer",
  "dijit/form/CheckBox"
		], 
		function(declare,lang,on,template,widgetBase, 
				ContentPane,TextBox,ValidationTextBox,
				Button,StaticColumnGrid,ACLCIDevStore,RoleGridContainer) {
//ACLCIDevStore,
	var base = [ContentPane, widgetBase];

	return declare("ddm.zui.page.role.RoleCI", base, {
		
		templateString : template, 
       
        
	postCreate:function()
    {
        this.inherited(arguments);
        
           var str = [
           {id: 'cidevice', name: 'CI Device', field: 'citName',width: '30%'},
           {id: 'create', name: 'CREATE', field: 'create',width: '10%',
                editable:true, alwaysEditing: true, editor: "dijit.form.CheckBox",
                editorArgs: {
                    props: 'value: true'
                }
            },
            {id: 'read', name: 'Read', field: 'read',width: '10%',     
                editable:true, alwaysEditing: true, editor: "dijit.form.CheckBox",
                editorArgs: {props: 'value: true'}
            },
            {id: 'update', name: 'Update', field: 'update',width: '10%',     
                editable:true, alwaysEditing: true, editor: "dijit.form.CheckBox",
                editorArgs: {props: 'value: true'}
            },
            {id: 'delete', name: 'Delete', field: 'delete',width: '10%',     
                widgetsInCell :true,alwaysEditing: true,editor: "dijit.form.CheckBox",
                editorArgs: {props: 'value: true'}
            }
            ,{id: 'deny', name: 'Deny', field: 'deny',width: '10%',   
                widgetsInCell :true,editor: "dijit.form.CheckBox",editable: true,alwaysEditing: true,
                editorArgs: {props: 'value: true'},//,onApply: function(){console.log("onApply")}},
                 setCellValue: function(gridData, storeData, cellWidget){  // console.log("cellWidget");
                //  console.log(cellWidget);
                  
                },
                getCellWidgetConnects: function(cellWidget, cell){      	   
                    return [
                            [cellWidget.denyChkBox, 'onChange', function(e){

                        //cell.grid.model.set(cell.row.id, {deny: e});
                        }]
                    ];
                }
            }
       ];

            this.own(on(this.citDrpdwn, 'change', lang.hitch(this,function(value){
                     console.log("dropdown value");
                     console.log(value);
                    
                      var  aclCIDevstore = new ACLCIDevStore({itemId:1,aclType:'cidevice'});
                      var staticGrid =  new StaticColumnGrid({
                            structure :  str,
                            store:aclCIDevstore
                        });
                        console.log("staticGrid");
                        console.log(staticGrid);

                        var t =this;

                    var	options = {itemId: this.itemId,
					//	pageMenu :  this.page, 
                    //appId: container.id,
					//	title : container.label,
					//	tabId:this.tabData.id,
						citStore : aclCIDevstore,
						data:t.data, 
                        //mode:this.mode,
						//parent:this,
						permission:t.data.permission};
					
						this.createWidget(RoleGridContainer, options,
								function(widget){
							this.addChild(widget);
				         });
                       // this.addChild(staticGrid);

            /*      	var options = {
						pageMenu : "aclrole",
						style : "height:100%",
						autoHeight : true,
						 container : this.data,
						 permission:this.data.permission
					};
                    console.log(this);
                       options.grid = staticGrid;
                      var cntWidget = new RoleGridContainer(options);
                        if(cntWidget)
                        this.addChild(cntWidget);		*/


                        
	        	})));
        
            }
		});

});