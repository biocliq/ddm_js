define("ddm/zui/page/role/RoleCIField",[
"dojo/_base/declare",
"dijit/_WidgetBase",
"dijit/_OnDijitClickMixin",
"dijit/_TemplatedMixin",
"dijit/_WidgetsInTemplateMixin",
//"dojo/text!./templates/FileUpload.html",
"dijit/form/Button",
"dijit/layout/ContentPane",
"dojox/form/Uploader",
"dojox/form/uploader/FileList",
"ddm/wire/store/base/JsonRestStore",
'ddm/util/config',
'dojo/topic',
"dojo/on",
"gridx/core/model/cache/Sync",
"gridx/Grid",
"ddm/fluwiz/grid/ddmGrid",
"ddm/wire/store/grid/ACLCitStore",
"ddm/zui/page/base/explorer/StaticColumnGrid",
"gridx/modules/CellWidget",
"dijit/Fieldset",
"dijit/ConfirmDialog",
"gridx/modules/Pagination",
"gridx/modules/pagination/PaginationBar",
"dojo/dom",
"dojo/_base/lang",
 'dojo/text!./templates/RoleCIField.html' ,
 "dojo/data/ItemFileWriteStore"
], function(declare, _WidgetBase, _OnDijitClickMixin, _TemplatedMixin,
_WidgetsInTemplateMixin,Button,contentPane, Uploader, UploaderFileList,JsonRest,
                config,topic,on, Cache, Grid,ddmGrid,ACLCitStore,StaticColumnGrid,CellWidget,
                FieldSet,ConfirmDialog,Pagination,PaginationBar,dom,lang,template,ItemFileWriteStore) {
return declare("ddm.zui.page.role.RoleCIField", [_WidgetBase, _OnDijitClickMixin,
_TemplatedMixin, _WidgetsInTemplateMixin
], {

	templateString : template,
postCreate:function(){

        console.log("RoleCIField js");
    
          }
});

});
