define("ddm/zui/page/role/RoleCIT",[
      "dojo/_base/declare","ddm/zui/page/base/Base", 'dojo/_base/array',
      "dijit/layout/ContentPane","ddm/wire/store/grid/ACLCitStore",
      "ddm/zui/page/base/explorer/StaticColumnGrid",
      "ddm/zui/page/role/RoleCIFieldGridDialog",
      "dijit/form/CheckBox",
      "dijit/form/TextBox",
      "dijit/form/Button", "dojo/on"
   ], function(declare,widgetBase,array, contentPane, ACLCitStore,StaticColumnGrid,RoleCIFieldGridDialog) {

var base = [contentPane,widgetBase];

return declare("ddm.zui.page.role.RoleCIT", base, {
  
    staticGrid:null,
    ciFieldData:null,
 _getValueAttr:function()
			{
                var data = this.staticGrid.edit.model.getAllLazyData();
                console.log("testdata");
                  console.log(data);
           
                var jsonData = [];
                for(var id in data){
                    var cit = data[id];
                    jsonData.push({citId:id,create:cit.create,read:cit.read,update:cit.update,delete:cit.delete,deny:cit.deny,fields:cit.fields});
                }
           		return {customData : JSON.stringify(jsonData),processor : this.data.processor};
			},
        postCreate:function(){
        this.inherited(arguments);
        var thisGrid;
        var roleIdVal;
        var fieldData;
   	       var str = [
           {id: 'citype', name: 'CI Type', field: 'citName',width: '30%'},
           {id: 'create', name: 'CREATE', field: 'create',width: '10%',
                editable:true, alwaysEditing: true, editor: "dijit.form.CheckBox",
                editorArgs: {
                    props: 'value: true'
                }
            },
            {id: 'read', name: 'Read', field: 'read',width: '10%',     
                editable:true, alwaysEditing: true, editor: "dijit.form.CheckBox",
                editorArgs: {props: 'value: true'}
            },
            {id: 'update', name: 'Update', field: 'update',width: '10%',     
                editable:true, alwaysEditing: true, editor: "dijit.form.CheckBox",
                editorArgs: {props: 'value: true'}
            },
            {id: 'delete', name: 'Delete', field: 'delete',width: '10%',     
                widgetsInCell :true,alwaysEditing: true,editor: "dijit.form.CheckBox",
                editorArgs: {props: 'value: true'}
            }
            ,{id: 'deny', name: 'Deny', field: 'deny',width: '10%',   
                widgetsInCell :true,editor: "dijit.form.CheckBox",editable: true,alwaysEditing: true,
                editorArgs: {props: 'value: true'},//,onApply: function(){console.log("onApply")}},
                 setCellValue: function(gridData, storeData, cellWidget){  // console.log("cellWidget");
                //  console.log(cellWidget);
                  
                },
                getCellWidgetConnects: function(cellWidget, cell){      	   
                    return [
                            [cellWidget.denyChkBox, 'onChange', function(e){

                        //cell.grid.model.set(cell.row.id, {deny: e});
                        }]
                    ];
                }
            },{id: 'action', name: 'Action',field:'action',width: '10%',     
                   widgetsInCell :true,
                 decorator:function(value){
                       
                     return "<input type='button' data-dojo-props='showLabel: false,iconClass: &quot;editIcon&quot;,title: &quot;Edit&quot;' data-dojo-type='dijit.form.Button'  data-dojo-attach-point='editACLCIT'/>";
                 }
                 ,getCellWidgetConnects:function(cellWidget, cell){
                            
                        // return an array of connection arguments
                        return [
                        [cellWidget.editACLCIT, 'onClick', function(e){
                                console.log("edit CIT"+cell.row.id);//citId
                                console.log(arguments);
                                var gridDialog = new RoleCIFieldGridDialog({roleId:roleIdVal,citId:cell.row.id,parentGrid:thisGrid});
                                gridDialog.startup();
                                gridDialog.show();
                        }]
                        ];
                    }
                }
                ,{id: 'fields', name: 'fields',field:'fields',width:'0px'}     
                                    
            ];
           
                var t = this;
                var roleId = t.itemId ? t.itemId : 0;
                roleIdVal = roleId;
            var aclCITstore = new ACLCitStore({itemId:roleId,aclType:"cit"});
                
             this.staticGrid = thisGrid =  new StaticColumnGrid({
                    structure :  str,
                    store:aclCITstore
              });
               
            this.addChild(this.staticGrid);	
            var grid = this.staticGrid;
            grid.edit.connect(grid.edit, "onApply", function(cell, success) {
                  var item = cell.row.data()
    
                if(cell.column.id=='deny')
                {
                    if(item.deny)
                    grid.edit.model.set(cell.row.id,{create:false,read:false,update:false,delete:false});
                }  
                else if(item.deny)
                {
                    grid.edit.model.set(cell.row.id,{deny:false});
                }
            });
            grid.column('fields').sort(false);
           grid.hiddenColumns.add("fields");
          
          }
});

});
