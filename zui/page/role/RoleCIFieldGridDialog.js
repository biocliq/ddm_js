define("ddm/zui/page/role/RoleCIFieldGridDialog",
 [ 'dojo/_base/declare',
 "dojo/_base/lang",
 'dojo/_base/array',
 "dojo/topic",  
 "dojo/on",
 "dojo/dom-style",
 "dijit/layout/ContentPane",
 "ddm/zui/widget/Dialog",
 "dijit/form/Button", 
//'dojo/text!./templates/ExploreGridDialog.html',
"ddm/zui/page/base/explorer/StaticColumnGrid",
"ddm/zui/page/role/RoleCIFieldGridStore"
		], 
		function(declare,lang,array,topic,on,domStyle,
				ContentPane,Dialog,Button,
				//template,
				StaticColumnGrid,
				RoleCIFieldGridStore) {

	var base = [Dialog];

	return declare("ddm.zui.page.role.RoleCIFieldGridDialog", base, {
	    staticGrid:null,
		resizeAxis:'xy',
		postCreate:function(){
			this.inherited(arguments);
            //store//
            var citId = this.citId;
            
			var ciFieldGridStore = new RoleCIFieldGridStore({itemId:this.roleId,citId:citId});
            //structure//
             var str = [
            {id: 'cifield', name: 'CI Field', field: 'name',width: '100px'},
            {id: 'create', name: 'CREATE', field: 'create',width: '100px',
                editable:true, alwaysEditing: true, editor: "dijit.form.CheckBox",
                editorArgs: {
                    props: 'value: true'
                }
            },
            {id: 'read', name: 'Read', field: 'read',width: '100px',     
                editable:true, alwaysEditing: true, editor: "dijit.form.CheckBox",
                editorArgs: {props: 'value: true'}
            },
            {id: 'update', name: 'Update', field: 'update',width: '100px',     
                editable:true, alwaysEditing: true, editor: "dijit.form.CheckBox",
                editorArgs: {props: 'value: true'}
            },
            {id: 'delete', name: 'Delete', field: 'delete',width: '100px',     
                widgetsInCell :true,alwaysEditing: true,editor: "dijit.form.CheckBox",
                editorArgs: {props: 'value: true'}
            }
            ,{id: 'deny', name: 'Deny', field: 'deny',width: '100px',   
                widgetsInCell :true,editor: "dijit.form.CheckBox",editable: true,alwaysEditing: true,
                editorArgs: {props: 'value: true'},//,onApply: function(){console.log("onApply")}},
                 setCellValue: function(gridData, storeData, cellWidget){  // console.log("cellWidget");
                //  console.log(cellWidget);
                  
                },
                getCellWidgetConnects: function(cellWidget, cell){      	   
                    return [
                            [cellWidget.denyChkBox, 'onChange', function(e){

                        //cell.grid.model.set(cell.row.id, {deny: e});
                        }]
                    ];
                }
            }

                ];

         this.staticGrid =  this.popupGrid = new StaticColumnGrid({
                    structure :  str,
                    store:ciFieldGridStore
                });
              var grid = this.popupGrid;
                grid.edit.connect(grid.edit, "onApply", function(cell, success) {
                  var item = cell.row.data()
    
                    if(cell.column.id=='deny')
                    {
                        if(item.deny)
                        grid.edit.model.set(cell.row.id,{create:false,read:false,update:false,delete:false});
                    }  
                    else if(item.deny)
                    {
                        grid.edit.model.set(cell.row.id,{deny:false});
                    }
                });
	
			var toolPane = new ContentPane();
		
			var btn = new Button({label : 'Add', style:'align:right'});
			this.addChild(this.popupGrid);
			domStyle.set(toolPane.containerNode, 'float', 'right');
			this.addChild(toolPane);
			toolPane.addChild(btn);
			this.own(on(btn, "click", lang.hitch(this, 
						function(){this.addRecord(arguments)})));
		}, 
		addRecord:function(/* Click Event */ event){
         
            var data = this.staticGrid.edit.model.getAllLazyData();
            var fieldData = [];
            for(var id in data){
                var cit = data[id];
                fieldData.push({id:id,create:cit.create,read:cit.read,update:cit.update,delete:cit.delete,deny:cit.deny});
            }
          //  this.parentGrid.edit.model.set(this.citId,{fields:[{id:id,create:cit.create,read:cit.read,update:cit.update,delete:cit.delete,deny:cit.deny}]});
            this.parentGrid.edit.model.set(this.citId,{fields:fieldData});
            this.hide();
     	}
	});
});