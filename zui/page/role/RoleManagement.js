define("ddm/zui/page/role/RoleManagement", 
	[
		'dojo/_base/declare',
		'dojo/_base/lang',
		"dojo/_base/array",
		'dojo/on',
		'require',
		'dojo/dom-style',
		"ddm/zui/page/base/Base", 
		'dijit/layout/ContentPane',
		"dijit/Tree","dijit/form/Button","ddm/zui/layout/FieldSet",
		"dijit/form/TextBox","dijit/form/Select","dojox/layout/TableContainer",
		"dijit/TitlePane","dijit/form/DateTextBox","dojo/data/ItemFileReadStore", 
		"ddm/wire/store/base/JsonACLMenuStore","ddm/zui/tree/CBTree","ddm/zui/page/base/FormBase",
		 'dojo/text!./templates/RoleManagement.html' 
		],
	function(declare,lang,array,on, require, domStyle, widgetBase, 
			ContentPane, Tree,Button,Fieldset,TextBox,Select,TableContainer,TitlePane,
			DateTextBox,ItemFileReadStore,JsonACLMenuStore,CBTree,FormBase,template) {
	var base = [Fieldset,FormBase];	
	
	return declare('ddm.zui.page.role.RoleManagement', base, {
	//		templateString : template,
			//containerData : null,
				
			postCreate: function(){
		
			//	containerData=this.data;
			// var menuModel = new JsonACLMenuStore({
			// 		target : "../v2/menu", 
			// 		rootMenuCode : "root"
			// 	});
		
			var roleId = this.itemId ? this.itemId : 0;
				this.inherited(arguments);
	
            	var menuModel = new JsonACLMenuStore({
					target : "../v2/page/aclrole/menuRights", 
		            roleId:roleId
				});

				this.menuTree = new CBTree({
					model : menuModel,
					showRoot : false,
					betweenThreshold: "5",
					getIconClass : function(item, opened){						
						return "fa fa-" + item.icon;
					}
				});			
				this.addChild(this.menuTree);					
				
			},
            _getValueAttr:function()
			{
				console.log(this.menuTree.get('value'));
				return {customData : JSON.stringify(this.menuTree.get('value')),processor : this.data.processor};
			}
			
		});
	}
);