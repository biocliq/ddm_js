define("ddm/zui/page/role/RoleCIFieldGridStore", [ "dojo/_base/declare",
		 "ddm/wire/store/base/JsonRestStore" ], function( declare,
		 JsonRest) {

	var base = JsonRest;

	return declare("ddm.zui.page.role.RoleCIFieldGridStore", base, {		
		idProperty:"id",
		constructor:function(options){
			options.target = "../v2/page/aclrole/menuRights/"+options.itemId+"/cit/"+options.citId+"/ciField";
	    	this.inherited(arguments);
		},
	getACLCitypes:function()
		{
			return this.gget(this.target+"/"+this.itemId);
		}
	});
});