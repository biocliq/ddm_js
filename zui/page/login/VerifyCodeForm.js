/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/login/VerifyCodeForm", [
     "dojo/_base/declare",
    'dijit/_WidgetBase',
	"dijit/_TemplatedMixin",
    'dijit/_AttachMixin',
	"dijit/_OnDijitClickMixin",
    'dijit/_WidgetsInTemplateMixin',
    'dojox/encoding/digests/MD5',
    'ddm/util/config',
	'dijit/focus',
	"dojo/text!./templates/VerifyCodeForm.html"	, 
	"dijit/form/ValidationTextBox",
	"ddm/wire/store/base/JsonRestStore",
	"dijit/form/Select",
	"dijit/form/Button",
], function(declare, _WidgetBase, _TemplatedMixin, 
  _AttachMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin,MD5Enc,
 config,focusUtil,template,dom,domStyle,registry) {

 return declare("ddm.zui.page.login.VerifyCodeForm", [_WidgetBase, _OnDijitClickMixin, _TemplatedMixin,
 _AttachMixin,_WidgetsInTemplateMixin], {
		templateString: template,
		widgetsInTemplate: true, 
		isValid:function(){
            if(this.code.isValid()){
				return true;
			}else
				this.code.focus();
			return false;
		},
		startup:function(){
            this.inherited(arguments);
			this.code.focus();
		},
		focus:function(){
			this.code.focus();
		},
		verifyCode : function(event){
			dojo.stopEvent(event);
			if(!this.isValid())
                return;
            var code = this.code;
            var formData = {"code" : this.code.get('value')};
			var xhrArgs = {
				url : config.url.verifyPasswordCodeURL('default'),
				content : formData,
				preventCache: true,
				handleAs : "json",
				load : function(data){
                    console.log(data)
                    if("success" == data.data.status)
                    {
                        location.href = config.url.resetPasswordPage;
                    }
                    else if("failure" == data.data.status)
                    {
                        alert(data.data.message)
                    }
					
				},
				error:function(error){
					switch(error.status){
					case 401:{
						alert("Something went wrong. Please try again after sometime");
						break;
					}
					case 404:
					case 502:{
						alert("Application server could be down. Please try again after sometime");
						break;
					}
					default:
						console.log(error);
						alert("Server not reachable. Please try again after sometime ");
					}					
					code.focus();
				}
			};
				
			var deferred = dojo.xhrPost(xhrArgs);
		},
		
	});

});