/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/login/LoginForm", [
	 "dojo/_base/declare",
	 "dojo/dom-style",
	 "dojo/dom",
    'dijit/_WidgetBase',
	"dijit/_TemplatedMixin",
    'dijit/_AttachMixin',
	"dijit/_OnDijitClickMixin",
    'dijit/_WidgetsInTemplateMixin',
    'dojox/encoding/digests/MD5',
    'ddm/util/config',
	'dijit/focus',
	"dojo/text!./templates/LoginForm.html"	, 
	// "ddm/zui/page/login/GSignupPopup",
	"dijit/Dialog",
	"dijit/form/FilteringSelect",
	"ddm/zui/page/user/ChangeFacilityDialog",
	"dojo/cookie",
	"dijit/form/ValidationTextBox",
	"ddm/wire/store/base/JsonRestStore",
	"dijit/form/Button"
	
	
 ], function(declare,domStyle, dom,_WidgetBase, _TemplatedMixin, 
  _AttachMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin,MD5Enc,
 config,focusUtil,template,//  GSignupPopup,
 Dialog,FilteringSelect,ChangeFacilityDialog,cookie) {

 return declare("ddm.zui.page.login.LoginForm", [_WidgetBase, _OnDijitClickMixin, _TemplatedMixin,
 _AttachMixin,_WidgetsInTemplateMixin], {
		templateString: template,
		widgetsInTemplate: true, 
		isValid:function(){
			if(this.username.isValid()){
				if(this.passwd.isValid())
					return true;
				else
					this.passwd.focus();
			}else
				this.username.focus();
			return false;
		},
		startup:function(){
			this.inherited(arguments);
			this.username.focus();
			this.googleplusflag = 0;
			var okbtn = dom.byId("ok");
			
		},
		focus:function(){
			this.username.focus();
		},
		login : function(event){
			var _this = this;
			dojo.stopEvent(event);
			if(!this.isValid())
				return;
			var password = this.passwd.get('value');
			var userField = this.username.get('value');
		//	password = MD5Enc(password, 1);
			var formData = {"username" : this.username.get('value'),
							"password" : password};
						
			var xhrArgs = {
				url : config.url.loginURL(config.customer),
				content : formData,
				preventCache: true,
				handleAs : "json",
				load : function(data){
				//	location.href = config.url.mainPage;
					cookie("username",userField,{expires : 7});					
					if(data.facilities && data.facilities.length > 0){
						if(data.facilities.length == 1)
						{
							cookie("facility", data.facilities[0].id,{expires: 7});
							location.href = config.url.mainPage;  
						}
						else {
							var userName = dojo.cookie("username");							
							var password = dom.byId("pass");
							domStyle.set(password , "display", "none");						
							var loginbtn = dom.byId("login");
							domStyle.set(loginbtn , "display", "none");

							if(userName != userField ){
								cookie("username", "-1",{ expires: -1 });
								cookie("facility", "-1",{ expires: -1 });
							}

							_this.facilitydialog=new ChangeFacilityDialog();
							_this.facilitydialog.reset();
							_this.facilitydialog.show();							
						}
					}else{
						cookie("username", "-1",{ expires: -1 });
						cookie("facility", "-1",{ expires: -1 });
						location.href = config.url.mainPage;
					}
				},
				error:function(error){
					switch(error.status){
					case 401:{
						alert("Invalid username or password");
						break;
					}
					case 404:
					case 502:{
						alert("Application server could be down. Please try again after sometime");
						break;
					}
					default:
						console.log(error);
						alert("Server not reachable. Please try again after sometime ");
					}					
					//userField.focus();
				}
			};
				
			var deferred = dojo.xhrPost(xhrArgs);
		},
		googleSign : function(profile,id_token){
			this.id_token = id_token;
			var formData = {"idtoken" : id_token};
			var _this = this;	
			var xhrArgs = {
				url : config.url.googleloginURL('default'),
				content : formData,
				preventCache: true,
				handleAs : "json",
				load : function(data){
					if("success" == data.data.status)
                    {
                        location.href = config.url.mainPage;
                    }
                    else if("failure" == data.data.status)
                    {
						//open popup
						_this.openGSignupPopup();
                    }
					
				},
				error:function(error){
					console.log("error");
					switch(error.status){
					case 401:{
						alert("Invalid username or password");
						break;
					}
					case 203:{
						alert("Invalid Google Information");
						break;
					}
					case 404:
					case 502:{
						alert("Application server could be down. Please try again after sometime");
						break;
					}
					default:
						console.log(error);
						alert("Server not reachable. Please try again after sometime ");
					}					
				}
			};
				
			var deferred = dojo.xhrPost(xhrArgs);
		},
		openGSignupPopup:function()
		{
			this.gdialog.set('id',"gsignupdialog");
			this.node = dom.byId("gsignupdialog");
			domStyle.set(this.node, 'display', 'block');
		},
		onHide:function()
		{
			
		},
		popupFormValid : function()
		{
			if(this.phoneno.isValid()){
				if(this.dob.isValid())
					return true;
				else
					this.dob.focus();
			}else
				this.phoneno.focus();
			return false;
		},
		calculateAge : function()
		{
			var date = new Date(this.dob.get('value'));
			var today = new Date();
			var timeDiff = Math.abs(today.getTime() - date.getTime());
			var age = Math.floor(timeDiff / (1000 * 3600 * 24)) / 365;
			return age;
		},
		GSignUpAdditionalDetails : function()
		{
			this.popupFormValid();
			var age = Math.floor(this.calculateAge());
			if(age < 18)
			{
				alert("Age should not be less than 18.");
				return;
			}
			
			var formData = {"idtoken" : this.id_token,
							"dob" : this.dob.get('value').valueOf(),
							"phoneno":this.phoneno.get('value')};
			domStyle.set(this.node, 'display', 'none');
			var xhrArgs = {
				url : config.url.gSignUpURL('default'),
				content : formData,
				preventCache: true,
				handleAs : "json",
				load : function(data){
					location.href = config.url.mainPage;
				},
				error:function(error){
					switch(error.status){
					case 401:{
						alert("Invalid username or password");
						break;
					}
					case 404:
					case 502:{
						alert("Application server could be down. Please try again after sometime");
						break;
					}
					default:
						console.log(error);
						alert("Server not reachable. Please try again after sometime ");
					}					
					
				}
			};
				
			var deferred = dojo.xhrPost(xhrArgs);

		}

		

		
	});

});