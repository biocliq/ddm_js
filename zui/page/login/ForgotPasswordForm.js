/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/page/login/ForgotPasswordForm", [
     "dojo/_base/declare",
    'dijit/_WidgetBase',
	"dijit/_TemplatedMixin",
    'dijit/_AttachMixin',
	"dijit/_OnDijitClickMixin",
    'dijit/_WidgetsInTemplateMixin',
    'dojox/encoding/digests/MD5',
    'ddm/util/config',
	'dijit/focus',
	"dojo/text!./templates/ForgotPasswordForm.html"	, 
	"dijit/form/ValidationTextBox",
	"ddm/wire/store/base/JsonRestStore",
	"dijit/form/Select",
	"dijit/form/Button",
], function(declare, _WidgetBase, _TemplatedMixin, 
  _AttachMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin,MD5Enc,
 config,focusUtil,template,dom,domStyle,registry) {

 return declare("ddm.zui.page.login.ForgotPasswordForm", [_WidgetBase, _OnDijitClickMixin, _TemplatedMixin,
 _AttachMixin,_WidgetsInTemplateMixin], {
		templateString: template,
		widgetsInTemplate: true, 
		isValid:function(){
            if(this.username.isValid()){
				if(this.email.isValid())
					return true;
				else
					this.email.focus();
			}else
				this.username.focus();
			return false;
		},
		startup:function(){
            this.inherited(arguments);
			this.username.focus();
			this.resetButton()
		},
		focus:function(){
			this.username.focus();
		},
		resetButton : function(){
			this.submitBtn.set('label',"Send Code");
			this.submitBtn.disabled = false;
		},
		sendCode : function(event){
			dojo.stopEvent(event);
			if(!this.isValid())
                return;
            var email = this.email;
            var userField = this.username;
            var formData = {"email" : this.email.get('value'),
							"username" : this.username.get('value')};
			this.submitBtn.set('label',"Please wait...");
			this.submitBtn.disabled = true;
			var _this = this;
			var xhrArgs = {
				url : config.url.forgotPasswordURL('default'),
				content : formData,
				preventCache: true,
				handleAs : "json",
				load : function(data){
					
                    if("success" == data.data.status)
                    {
                        location.href = config.url.verifyCodePage;
                    }
                    else if("failure" == data.data.status)
                    {
						_this.resetButton();
                        alert(data.data.message)
                    }
					
				},
				error:function(error){
					switch(error.status){
					case 401:{
						alert("Something went wrong. Please try again after sometime");
						break;
					}
					case 404:
					case 502:{
						alert("Application server could be down. Please try again after sometime");
						break;
					}
					default:
						console.log(error);
						alert("Server not reachable. Please try again after sometime ");
					}					
					userField.focus();
					_this.resetButton();
				}
			};
				
			var deferred = dojo.xhrPost(xhrArgs);
		},
		
	});

});