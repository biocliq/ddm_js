define("ddm/zui/grid/acl/AclGroupsByUser", [ 
        "dojo/_base/declare",
        "dojo/topic",
        "dojo/_base/array",
        "dojo/_base/lang", 
        "dojo/on",
        'ddm/wire/store/grid/JsonGridStore', 
        'dojo/store/Cache',
        'ddm/fluwiz/grid/ddmGrid',
        'gridx/core/model/cache/Async',
        'gridx/modules/SingleSort', 
        'gridx/modules/ColumnResizer',
        'gridx/modules/Filter', 
        "ddm/gridx/modules/toolbar/ExplorerAclFilterBar",
        "gridx/modules/Pagination", 
        "gridx/modules/pagination/PaginationBar",
        "gridx/modules/select/Row",         
        "gridx/modules/IndirectSelect",
        "gridx/modules/extendedSelect/Row",
        "gridx/modules/RowHeader",
        "gridx/modules/HScroller",
        "dojo/store/Memory",
        "gridx/modules/Edit",
        "gridx/modules/Bar",
        "gridx/modules/CellWidget",
        "ddm/wire/store/base/JsonUserStore"
       ], function(
            declare,topic, array, lang, on,JsonGridStore, StoreCache, Grid,
            AsyncCache, Sort, ColumnResizer, Filter, FilterBar,
            Pagination, PaginationBar, SelectRow,
            IndirectSelect, ExSelectRow, RowHeader, HScroller,  Memory,Edit,Bar,CellWidget,JsonUserStore) {
    
        var base = [Grid];
    
        return declare("ddm.zui.grid.acl.AclGroupsByUser", base, {
    		filterBarClass:"ddm/gridx/modules/toolbar/ExplorerAclFilterBar",
            postMixInProperties : function() {
                this.inherited(arguments);
                this.createStructure();
                this.sourcePage = this.pageMenu;
                this.sourceId = this.itemId;
                this.destinationPage = "group";
                var restStore = new JsonGridStore({
                    pageMenu : this.pageMenu,
                    ciId : this.itemId,
                    idProperty : "id",
                    containerName : this.containerName
                });
                this.store = restStore;                
            },
            createStructure : function()
            {
                this.fields = [ 
                    {id: "sid", name: "sid", label: "Group",colspan:4,tabDisplay  :true},
                    {id: "description", name: "description", label: "Description",colspan:2,tabDisplay  :true},
                    {id: "action", name: "action", label: "Remove",colspan:2,tabDisplay  :true}
                    
                ]  
                this.setStructure();
            },
            setStructure : function()
            {
                var _grid = this;
                if(this.fields)
                {
                    var columns=[];
                        array.forEach(this.fields, lang.hitch(this, function(field){
                            if(field.tabDisplay){
                                if("action" == field.name)
                                {
                                    var column =  {id: field.name, field : field.name, name:field.label};
                                    column.editable = 0;
                                    column.width = "100px";
                                    column.widgetsInCell= true;
                                    column.decorator= function(){
                                        try {
                                            return "<div data-dojo-type='dijit/form/Button' " +
                                            "data-dojo-attach-point='removeGroup'"+
                                            "<span class='viewfieldsacl'>Remove</span></div>";
                                        } catch (e) {
                                            console.error('error decorating date: ' + e.toString());
                                        }
                                    }
                                    column.setCellValue= function(gridData,storeData,cellWidget){
                                        //cellWidget.aclmask.set('value', gridData);
                                        if(cellWidget.removeGroup._cnnt){
                                            // Remove previously connected events to avoid memory leak.
                                            cellWidget.removeGroup._cnnt.remove();
                                        }
                                        cellWidget.removeGroup._cnnt = dojo.connect(cellWidget.removeGroup, 'onClick', function(e){
                                            var rowData = cellWidget.cell.row.rawData();
                                            var options = {};
                                            var store = new JsonUserStore(options);
                                            store.deleteUserRelation(rowData.aclId,'group').then(function(){
                                                _grid.refresh();
                                            });
                                        });
                                    }
                                }
                                else{
                                    column =  {id: field.name, field : field.name, name:field.label};
                                    column.editable = 0;
                                }
                                columns.push(column);
                            }
                        }));				
                        this.structure = columns;
                }
                
            },
        });
    });
    