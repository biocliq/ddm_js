define("ddm/zui/grid/acl/AclUserByGroup", [ 
        "dojo/_base/declare",
        "dojo/topic",
        "dojo/_base/array",
        "dojo/_base/lang", 
        "dojo/on",
        'ddm/wire/store/grid/JsonGridStore', 
        'dojo/store/Cache',
        'ddm/fluwiz/grid/ddmGrid',
        'gridx/core/model/cache/Async',
        'gridx/modules/SingleSort', 
        'gridx/modules/ColumnResizer',
        'gridx/modules/Filter', 
        "ddm/gridx/modules/toolbar/ExplorerFilterBar",
        "gridx/modules/Pagination", 
        "gridx/modules/pagination/PaginationBar",
        "gridx/modules/select/Row",         
        "gridx/modules/IndirectSelect",
        "gridx/modules/extendedSelect/Row",
        "gridx/modules/RowHeader",
        "gridx/modules/HScroller",
        "dojo/store/Memory",
        "gridx/modules/Edit",
        "gridx/modules/Bar",
        "gridx/modules/CellWidget",
       ], function(
            declare,topic, array, lang, on,JsonGridStore, StoreCache, Grid,
            AsyncCache, Sort, ColumnResizer, Filter, FilterBar,
            Pagination, PaginationBar, SelectRow,
            IndirectSelect, ExSelectRow, RowHeader, HScroller,  Memory,Edit,Bar,CellWidget) {
    
        var base = [Grid];
    
        return declare("ddm.zui.grid.acl.AclUserByGroup", base, {
            filterBarClass:"ddm/gridx/modules/toolbar/ExplorerFilterBar",
            postMixInProperties : function() {
                this.inherited(arguments);
                this.createStructure();
                var restStore = new JsonGridStore({
                    pageMenu : this.pageMenu,
                    ciId : this.itemId,
                    idProperty : "id",
                    containerName : this.containerName
                });
                this.store = restStore;                
            },
            createStructure : function()
            {
                this.fields = [ 
                    {id: "loginName", name: "loginName", label: "Login Name",colspan:4,"searchable" : true,tabDisplay  :true},
                    {id: "name", name: "name", label: "Name",colspan:4,"searchable" : true,tabDisplay  :true},
                    {id: "firstName", name: "firstName", label: "First Name",colspan:2,tabDisplay  :true},
                    {id: "lastName", name: "lastName", label: "Last Name",colspan:2,tabDisplay  :true},
                    {id: "email", name: "email", label: "Email",colspan:2,tabDisplay  :true},
                ]  
                this.setStructure();
            },
            setStructure : function()
            {
                if(this.fields)
                {
                    var columns=[];
                        array.forEach(this.fields, lang.hitch(this, function(field){
                            if(field.tabDisplay){
                                column =  {id: field.name, field : field.name, name:field.label};
                                column.editable = 0;
                                columns.push(column);
                            }
                        }));				
                        this.structure = columns;
                }
                
            },
        });
    });
    
    