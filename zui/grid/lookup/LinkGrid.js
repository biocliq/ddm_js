define("ddm/zui/grid/lookup/LinkGrid", [ 
        "dojo/_base/declare",
        "dojo/topic",
        "dojo/_base/array",
        "dojo/_base/lang", 
        "dojo/on",
        "dijit/ConfirmDialog",
        'ddm/wire/store/grid/JsonGridStore', 
        'dojo/store/Cache',
        'ddm/zui/page/base/explorer/SimpleGrid',        
        'gridx/core/model/cache/Async',
        'gridx/modules/SingleSort', 
        'gridx/modules/ColumnResizer',
        'gridx/modules/Filter', 
        "gridx/modules/Pagination", 
        "gridx/modules/pagination/PaginationBar",
        "gridx/modules/select/Row",         
        "gridx/modules/IndirectSelect",
        "gridx/modules/extendedSelect/Row",
        "gridx/modules/RowHeader",
        "gridx/modules/HScroller",
        "dojo/store/Memory",
        "gridx/modules/Edit",
        "gridx/modules/Bar",        
        "gridx/modules/CellWidget",
        "ddm/wire/store/grid/JsonRelationStore",
        'ddm/zui/grid/lookup/LinkFilterBar',
        "dijit/form/Button",
       ], function(
            declare,topic, array, lang, on, Dialog, JsonGridStore, StoreCache, Grid,
            AsyncCache, Sort, ColumnResizer, Filter, 
            Pagination, PaginationBar, SelectRow,
            IndirectSelect, ExSelectRow, RowHeader, HScroller,  Memory,Edit,Bar,CellWidget,JsonRelationStore) {
    
        var base = [Grid];
    
        return declare("ddm.zui.grid.lookup.LinkGrid", base, {
    		filterBarClass:"ddm/zui/grid/lookup/LinkFilterBar",
            postMixInProperties : function() {
                this.inherited(arguments);
                this.store = new JsonGridStore({
                    pageMenu : this.pageMenu,
                    ciId : this.itemId,
                    containerName : this.containerName
                });
            },
            createRemoveColumn : function()
            {
                var _grid = this;
                var options = {pageMenu : _grid.pageMenu,
                    containerName : _grid.containerName,
                    ciId : _grid.itemId};

                var field = {id: "action", name: "action", label: "Remove",colspan:2,tabDisplay  :true, width : "100px", widgetsInCell:true, 
                    decorator: function(){
                        try {
                            return "<div data-dojo-type='dijit/form/Button' " +
                            "data-dojo-attach-point='removeGroup'"+
                            "<span class='viewfieldsacl'>Remove</span></div>";
                        } catch (e) {
                            console.error('error decorating date: ' + e.toString());
                        }
                    },
                    setCellValue: function(gridData,storeData,cellWidget){
                        if(cellWidget.removeGroup._cnnt){
                            cellWidget.removeGroup._cnnt.remove();
                        }

                        cellWidget.removeGroup._cnnt = dojo.connect(cellWidget.removeGroup, 'onClick', function(e){
                            var rowData = cellWidget.cell.row.rawData();
                            var store = new JsonRelationStore(options);
                            var message = _grid.confirmMessage || 'Are you sure, you want to delete this record';
                            var cont = '<div style="padding:30px;" >' + message + '</div>';
                            

                            var onOk = function(){
                                store.deleteRelation(rowData._id).then(
                                    function(result){
                                        _grid.refresh();
                                    }	
                                );
                            }

                            var dialogOptions = {content:cont, 
                                execute:onOk,
                                style:"width:200px;", 
                                onHide: function() {
                                    this.destroy();
                                }};

                            var dialog = new Dialog(dialogOptions);
                            dialog.set('buttonOk', 'Delete');
                            dialog.show();
                            // if (confirm(message)) {
                            //     store.deleteRelation(rowData._id).then(
                            //         function(result){
                            //             _grid.refresh();
                            //         }	
                            //     );
                            // }                            
                        });
                    }
                };

                return field;
            },
            setStructure : function(str)
            {   
                str.unshift(this.createRemoveColumn());                
                this.structure = str;
            },
        });
    });
    