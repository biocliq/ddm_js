define("ddm/zui/grid/facility/FacilitiesByUser", [ 
        "dojo/_base/declare",
        "dojo/topic",
        "dojo/_base/array",
        "dojo/_base/lang", 
        "dojo/on",
        'ddm/wire/store/grid/JsonGridStore', 
        'dojo/store/Cache',
        'ddm/fluwiz/grid/ddmGrid',
        'gridx/core/model/cache/Async',
        'gridx/modules/SingleSort', 
        'gridx/modules/ColumnResizer',
        'gridx/modules/Filter', 
        "ddm/gridx/modules/toolbar/ExplorerAclFilterBar",
        "gridx/modules/Pagination", 
        "gridx/modules/pagination/PaginationBar",
        "gridx/modules/select/Row",         
        "gridx/modules/IndirectSelect",
        "gridx/modules/extendedSelect/Row",
        "gridx/modules/RowHeader",
        "gridx/modules/HScroller",
        "dojo/store/Memory",
        "gridx/modules/Edit",
        "gridx/modules/Bar",
        "gridx/modules/CellWidget",
        "ddm/wire/store/base/JsonUserStore"
       ], function(
            declare,topic, array, lang, on,JsonGridStore, StoreCache, Grid,
            AsyncCache, Sort, ColumnResizer, Filter, FilterBar,
            Pagination, PaginationBar, SelectRow,
            IndirectSelect, ExSelectRow, RowHeader, HScroller,  Memory,Edit,Bar,CellWidget,JsonUserStore) {
    
        var base = [Grid];
    
        return declare("ddm.zui.grid.facility.FacilitiesByUser", base, {
    		filterBarClass:"ddm/gridx/modules/toolbar/ExplorerAclFilterBar",
            postMixInProperties : function() {
                this.inherited(arguments);
                this.createStructure();
                this.sourcePage = this.pageMenu;
                this.sourceId = this.itemId;
                this.destinationPage = "ZcmFacility";
                var restStore = new JsonGridStore({
                    pageMenu : this.pageMenu,
                    ciId : this.itemId,
                    containerName : this.containerName
                });
                this.store = restStore;                
            },
            createStructure : function()
            {
                this.fields = [ 
                    {id: "name", name: "name", label: "Name",colspan:4,tabDisplay  :true},
                    {id: "facilityCode", name: "facilityCode", label: "Facility Code",colspan:4,tabDisplay  :true},
                    {id: "facilityType", name: "facilityType", label: "Facility Type",colspan:4,tabDisplay  :true},
                    {id: "city", name: "city", label: "City",colspan:4,tabDisplay  :true},
                    {id: "stateName", name: "stateName", label: "State",colspan:4,tabDisplay  :true},
                    {id: "countryName", name: "countryName", label: "Country",colspan:4,tabDisplay  :true},
                    {id: "action", name: "action", label: "Action",colspan:4,tabDisplay  :true},
                ]  
                this.setStructure();
            },
            setStructure : function()
            {
                if(this.fields)
                {
                    var columns=[];
                        array.forEach(this.fields, lang.hitch(this, function(field){
                            if(field.tabDisplay){
                                if("action" == field.name)
                                {
                                    column =  {id: field.name, field : field.name, name:field.label};
                                    column.editable = 0;
                                    column.widgetsInCell= true;
                                    column.width = "100px";
                                    column.decorator= function(){
                                        try {
                                            return "<div data-dojo-type='dijit/form/Button' " +
                                            "data-dojo-attach-point='removeFacility'"+
                                            "<span class='viewfieldsacl'>X</span></div>";
                                        } catch (e) {
                                            console.error('error decorating date: ' + e.toString());
                                        }
                                    }
                                    column.setCellValue= function(gridData,storeData,cellWidget){
                                        //cellWidget.aclmask.set('value', gridData);
                                        if(cellWidget.removeFacility._cnnt){
                                            // Remove previously connected events to avoid memory leak.
                                            cellWidget.removeFacility._cnnt.remove();
                                        }
                                        cellWidget.removeFacility._cnnt = dojo.connect(cellWidget.removeFacility, 'onClick', function(e){
                                            var rowData = cellWidget.cell.row.rawData();
                                            var options = {};
                                            var store = new JsonUserStore(options);
                                            store.deleteUserRelation(rowData.id,'zcm_facility');
                                        });
                                    }
                                }
                                else{
                                    column =  {id: field.name, field : field.name, name:field.label};
                                    column.editable = 0;
                                }
                                columns.push(column);
                            }
                        }));				
                        this.structure = columns;
                }
                
            },
        });
    });
    