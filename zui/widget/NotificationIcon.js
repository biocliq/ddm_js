/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/widget/NotificationIcon", [
    "dojo/_base/declare",  
    "dojo/dom-class",
    "dijit/_WidgetBase",
    'dijit/_TemplatedMixin',
	'dijit/_WidgetsInTemplateMixin',  
    "dojo/text!./templates/NotificationIcon.html"
	], function(declare,domClass, _WidgetBase,_TemplatedMixin,_WidgetsInTemplateMixin, template){
    
    var base = [_WidgetBase,_TemplatedMixin,_WidgetsInTemplateMixin];
        
	return declare( "ddm.zui.widget.NotificationIcon", base,{
        templateString:template, 
        baseClass:'notificationIcon', 
        postCreate:function(){
            this.inherited(arguments);
            domClass.add(this.iconNode, this.iconClass);
        }
	});
});
