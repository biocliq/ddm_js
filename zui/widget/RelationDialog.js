define("ddm/zui/widget/RelationDialog",
        ['dojo/_base/declare',
        "ddm/zui/widget/Dialog",
        "ddm/zui/page/base/GridContainer",
        "ddm/zui/page/base/summary/SummaryGrid"], 
		function(declare,Dialog,GridContainer,SummaryGrid) {

	var base = [Dialog,GridContainer];

	return declare("ddm.zui.widget.RelationDialog", base, {
        grid: "ddm/zui/page/base/summary/SummaryGrid",
		padding: "0px",
        doLayout :"false",
		permission:{},
		startup:function()
		{
			this.inherited(arguments);
		},
		getGridOptions:function(){
			var options = this.inherited(arguments);			
			options.autoHeight = false;
            options.style="height:100%";
            options.permission=this.permission;
            options.defaultFilter = {__parentId : "234"};            
            options.evtCellDblClick = function(e){
                dojo.stopEvent(e);                
                this.getParent().onRowSelect(e.rowId);
                // topic.publish("/form/view", e.rowId, this.pageMenu);
            }
			return options;
        },
        onRowSelect:function(rowId){            
            this.callback.call(this, rowId);
            this.hide();
        }
		
	});
});
