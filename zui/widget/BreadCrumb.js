/** 
 * (C) Copyright 2018 {OWNER}.
 *
{LICENSE_TEXT}
 *
 */

define("ddm/zui/widget/BreadCrumb",
        ['dojo/_base/declare', 
        "dojo/_base/lang", "dojo/on",
        "dijit/TitlePane", 
        "dijit/form/Button",
        "ddm/zui/widget/Dialog",
        "ddm/chart/InterleaveBar",
        "dojo/text!./templates/BreadCrumb.html",], 
		function(declare,lang, on,TitlePane,Button,Dialog,Bar, template) {

	var base = [TitlePane];

	return declare("ddm.zui.widget.BreadCrumb", base, {
        templateString: template, 
        postCreate:function(){
            this.inherited(arguments);
            var btn = new Button({
                showLabel: false,iconClass: 'icon fa-list',title: 'History'
            });            
            this.own(on(btn.domNode, 'click', lang.hitch(this, "btnClick")));
            this.toolBar.appendChild(btn.domNode);


            var btn2 = new Button({
                showLabel: false,iconClass: 'icon fa-plus',title: 'Add'
            });
            this.own(on(btn2.domNode, 'click', lang.hitch(this, "btnClick")));
            this.toolBar.appendChild(btn2.domNode);
        },
        btnClick:function(evt){
            evt.stopPropagation();
            if(this.popup){
                this.popup.show();
            }
            if(this.title == "Prescription"){
                    var dialog = this.popup = new Dialog({title:"Prescription", style: 'width: 800px; height:500px'});
                    var bar = new Bar({style: 'width: 600px; height:400px', height:400, width:600});
                    dialog.addChild(bar);
                    console.log(bar);
                    // bar.initArea();
                        dialog.startup();
                        dialog.show();
                  
            }
        }
	});
});