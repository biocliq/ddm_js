/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/widget/ListNode",[
	'dojo/_base/declare',
	'dojo/_base/array',
	"dojo/dom-style","dojo/dom-class",
	"dojo/topic", 'dijit/Tree',
	 'dojo/text!./templates/ListNode.html'], 
		 function(declare, array, domStyle,domClass, topic,Tree,  template) {
		    return declare("ddm.zui.widget.ListNode", Tree._TreeNode, {
				templateString: template,
				baseClass:"ListNode",
				widgetsInTemplate: true,
		        _setIndentAttr: function(indent){
					var pixels = (Math.max(indent, 1) * this.tree._nodePixelIndent / 2) + "px";
					domStyle.set(this.domNode, "backgroundPosition", pixels + " 0px");
					domStyle.set(this.rowNode, this.isLeftToRight() ? "paddingLeft" : "paddingRight", pixels);
		
					array.forEach(this.getChildren(), function(child){
						child.set("indent", indent + 1);
					});
					this._set("indent", indent);
		        }
		    });
});