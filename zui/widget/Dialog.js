/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/zui/widget/Dialog",
        ['dojo/_base/declare', 
        "dojo/dom-construct",
        "dijit/Dialog"], 
		function(declare,domConstruct,Dialog) {

	var base = [Dialog];

	return declare("ddm.zui.widget.Dialog", base, {
		destroy:function(){
			domConstruct.destroy(this.domNode);
			this.inherited(arguments);
		}
	});
});