define("ddm/zui/widget/TimeLineSelector", 
	[
    'dojo/_base/declare','dojo/_base/lang','dojo/on',
    "dojo/dom-style","dojo/topic",'dojo/dom-class',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin', "dijit/layout/ContentPane",
    "dojo/text!./templates/TimeLineSelector.html", 
    "ddm/zui/form/DateWTimestampTextBox", 
    "ddm/zui/form/TimestampTextBox",
	],
    function( declare,lang, on,domStyle,topic,domClass,_TemplatedMixin, _WidgetsInTemplateMixin, ContentPane, template) {
        var base = [ContentPane,_TemplatedMixin, _WidgetsInTemplateMixin];	
        
        return declare('ddm.zui.widget.TimeLineSelector', base, {
            templateString:template,             
            baseClass: "dijitBorderTop",
            style:"align:left;",
            timeSelector:false,
            postCreate:function(){
                this.flag = true;
                if(this.options)
                {  
                    var obj = JSON.parse(this.options); 
                    if("day"==obj.timeselector){
                        this.timeSelector = true;
                        domStyle.set(this.currentMonth, 'display', 'none');
                        domStyle.set(this.lastMonth, 'display', 'none');
                    }
                
                    else if("month"==obj.timeselector){
                        domStyle.set(this.currentDay, 'display', 'none');
                        domStyle.set(this.YesterDay, 'display', 'none')
                        domStyle.set(this.lastTwoDays, 'display','none');
                        domStyle.set(this.lastWeek, 'display', 'none');
                    }
                }
                else{
                    domStyle.set(this.currentDay, 'display', 'none');
                    domStyle.set(this.YesterDay, 'display', 'none')
                    domStyle.set(this.lastTwoDays, 'display','none');
                    domStyle.set(this.lastWeek, 'display', 'none');
                }
                if(this.timeSelector){
                    domStyle.set(this.timeFromTextBox.domNode, 'display', "");
                    domStyle.set(this.timeToTextBox.domNode, 'display', "");
                }
                this.inherited(arguments);  
                var date = new Date();
                this.dateToTextBox.set('value', date);
                var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
                this.dateFromTextBox.set('value', firstDay);                
                // this.own(
                //     on(this.fields.addressCountry, 'change', lang.hitch(this, function(id) {
                //         this._onChangeCountry(id);
                //     } ))
                // );
                // this.own(
                //     on(this.fields.addressState, 'change', lang.hitch(this, function(id) {
                //         this._onChangeState(id);
                //     } ))
                // );  
            },
            startup:function(){
                var parent = this.getParent();
                if(parent){                   
                    domStyle.set(parent.domNode, {
                        'background-color': "unset",
                        'box-shadow': 'unset',
                        'text-align':"right"
                    });
                }
                this.inherited(arguments);
                if(this.options)
                {
                    var obj = JSON.parse(this.options);
                    if("day"==obj.timeselector){
                        this._setLastTwoDays();
                    }

                    else if("month"==obj.timeselector){
                         this._setCurrentMonth();
                    }
                }
                else{
                          this._setCurrentMonth();
                 }
            },
            _onChangeDateTime:function(){
               
            },
            _setCurrentMonth:function(){
                this.flag = false;
                var date = new Date();
                this.dateToTextBox.set('value', date);
                this.timeToTextBox.set('value', "T23:59:59");
                var firstDay = new Date(date.getFullYear(), date.getMonth(), 1, 0, 0, 0);
                this.dateFromTextBox.set('value', firstDay);
                this.timeFromTextBox.set('value', "T00:00:00");
                this._publish(firstDay.valueOf(),date.valueOf());
                this.flag = true;
            },
            _setLastMonth:function(){
                this.flag = false;
                var date = new Date();
                var toDay = new Date(date.getFullYear(), date.getMonth(), 0, 23, 59, 59);
                this.dateToTextBox.set('value', toDay);
                this.timeToTextBox.set('value', "T23:59:59");
                var fromDay = new Date(date.getFullYear(), date.getMonth()-1, 1, 0, 0, 0);
                this.dateFromTextBox.set('value', fromDay);
                this.timeFromTextBox.set('value', "T00:00:00");
                this._publish(fromDay.valueOf(), toDay.valueOf());
                this.flag = true;
            },
            _setToday:function(){
                this.flag = false;
                var todayDate = new Date();
                todayDate.setHours(0,0,0,0);
                var date = new Date();
                this.dateToTextBox.set('value', date);
                this.timeToTextBox.set('value', "T23:59:59");
                this.dateFromTextBox.set('value', date);
                this.timeFromTextBox.set('value', "T00:00:00");
                this._publish(todayDate.valueOf(),date.valueOf());
                this.flag = true;
            },
            _setYesterday:function(){
                this.flag = false;
                var yesterdayDate = new Date();
                yesterdayDate.setDate(yesterdayDate.getDate() - 1);
                yesterdayDate.setHours(0,0,0,0);
                this.dateToTextBox.set('value', yesterdayDate);
                this.timeToTextBox.set('value', "T23:59:59");
                this.dateFromTextBox.set('value', yesterdayDate);
                this.timeFromTextBox.set('value', "T00:00:00");
                var yesterdayEndDate = new Date(yesterdayDate);
                yesterdayEndDate.setHours(23,59,59,999)
                this._publish(yesterdayDate.valueOf(),yesterdayEndDate.valueOf());
                this.flag = true;
            },
            _setLastTwoDays:function(){
                this.flag = false;
                var date = new Date();
                this.dateToTextBox.set('value', date);
                this.timeToTextBox.set('value', "T23:59:59");
                var yesterdayDate = new Date();
                yesterdayDate.setDate(date.getDate() - 1);
                yesterdayDate.setHours(0,0,0,0)
                this.dateFromTextBox.set('value', yesterdayDate);
                this.timeFromTextBox.set('value', "T00:00:00");
                this._publish(yesterdayDate.valueOf(),date.valueOf());
                this.flag = true;
            },
            _setLastWeek:function(){
                var curr = new Date;
                curr.setDate(curr.getDate() - 7); // get current date
                var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                var last = first + 6; // last day is the first day + 6

                var firstday = new Date(curr.setDate(first));
                var lastday = new Date(curr.setDate(last));
                this.flag = false;
                var date = new Date();
                this.dateToTextBox.set('value', date);
                this.timeToTextBox.set('value', "T23:59:59");
                var firstDay = new Date(date.getFullYear(), date.getMonth(), 1, 0, 0, 0);
                this.dateFromTextBox.set('value', firstDay);
                this.timeFromTextBox.set('value', "T00:00:00");
                this._publish(firstDay.valueOf(),date.valueOf());
                this.flag = true;
            },
            _publish:function(from, to){
                topic.publish("chart/setFilter/" +this.page+ "/" + this.itemId, {fromDate:from, toDate:to} );
            }
		});
	}
);