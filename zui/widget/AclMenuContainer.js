define("ddm/zui/widget/AclMenuContainer", [ 
    "dojo/_base/declare","dijit/layout/ContentPane","ddm/util/config",
    "ddm/wire/store/grid/JsonAclStore","ddm/zui/widget/MCBTree"
    ], function(
        declare, ContentPane,config,JsonAclStore,MCBTree) {

    var base = [ContentPane];

	return declare("ddm.zui.widget.AclMenuContainer", base, {
        
		resize:function(){},
		postMixInProperties:function(){
            this.inherited(arguments);   
            
            if(!this.itemId){
                this._initialized = false;
                return;
            }else
                this._initialized = true;

            var model = new JsonAclStore({
                target : config.url.menu, 
                rootCode : "root",
                pageMenu : this.page,
                sid : this.itemId,
                aclType : this.appId,
            });

            this.mcbTree = new MCBTree({
                model : model,
                showRoot : false,
            });

        },
        isValid:function(){
            return true;
        },
        postCreate:function(){
            this.inherited(arguments);
            if(this.hasInitialized())
                this.addChild(this.mcbTree);
        },
        _getValueAttr:function(){
            if(this.hasInitialized()){
                var value = this.mcbTree._getValueAttr();
                return {id:this.containerId, name:this.appId, customValue : JSON.stringify(value)};
            }
        },
        reset : function()
        {
            
        },
        _setModeAttr:function(flag){
           if(this.hasInitialized())
           {
                var widget = this.mcbTree;
                if(flag == 1)
                    widget.set('disabled', true);
                else
                    widget.set('disabled', false);
            }
        },
        hasInitialized:function(){
            return this._initialized;
        },
        destroy:function(){
            this.inherited(arguments);
            delete this.mcbTree;
        }
	});

   
});
