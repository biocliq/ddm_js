/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/widget/Label",	[
    'dojo/_base/declare',
    'dojo/dom-class',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin', 
    "dijit/_HasDropDown",
    "dijit/form/TextBox", 
    "dijit/layout/ContentPane",   
    // "ddm/dchart/chartjs/MedicalTimeLine",
    "dojo/text!./templates/Label.html"
],
function( declare, domClass, _WidgetBase,
_TemplatedMixin, _WidgetsInTemplateMixin,_HasDropDown, TextBox,ContentPane,
//  LineChart,
 template) {
    var base = [_WidgetBase,_TemplatedMixin,_WidgetsInTemplateMixin,_HasDropDown];

    return declare('ddm.zui.widget.Label', base, {
            templateString:template,  
            set:function(name, value){

            } ,        
            openDropDown:function(){
                // var options = {
                //     data:this.tableData,
                //     xKey: this.timeLine,
                //     yKey:this.name,
                //     yLabel:this.label,
                //     xDimension:{
                //         h:200, w:500
                //     }
                // }
                // var line = new LineChart(options);
                // var c = new ContentPane({style:"background-color:white; height:160px; width:500px;"});
                // c.addChild(line);
                // this.dropDown = c;
                this.inherited(arguments);
            }, 
            // setValue(/*String*/ value, label){
            //     this.textbox.innerHTML = value;
            //     this.label = label;
            // }, 
            postCreate:function(){
                this.inherited(arguments);
                if(this.value)
                    this.textbox.innerHTML = this.value;
                else
                    return;
                if(this.value && null != this.value){
                    if(this.oldValue && null != this.oldValue){
                        if(this.value == this.oldValue){
                            domClass.add(this.variation, "fa-ellipsis-h");
                        }
                        else if(this.value > this.oldValue){
                            domClass.add(this.variation, "fa-arrow-up");
                            this.variation.innerHTML = (this.value - this.oldValue);
                        }else{
                            domClass.add(this.variation, "fa-arrow-down");
                            this.variation.innerHTML = (this.oldValue - this.value);
                        }
                    }
                }
            }
        });
    }
);