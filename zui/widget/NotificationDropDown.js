/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/widget/NotificationDropDown", [
    "dojo/_base/declare", "dojo/_base/lang", 
    "dojo/dom-class", "dojo/topic","dijit/TooltipDialog","dijit/layout/ContentPane",
    "dijit/_WidgetBase",'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin', "dijit/_HasDropDown",
    "ddm/fluwiz/widget/AccordionList","ddm/zui/page/base/GridContainer",
    "dojo/text!./templates/NotificationIcon.html"
    ], function(declare,lang,
        domClass, topic,TooltipDialog,ContentPane,
        _WidgetBase,_TemplatedMixin,
        _WidgetsInTemplateMixin,_HasDropDown, 
        AccordionList,GridContainer,
        template){
    
    var base = [_WidgetBase,_TemplatedMixin,_WidgetsInTemplateMixin, _HasDropDown,GridContainer];
        
	return declare( "ddm.zui.widget.NotificationDropDown", base,{
        templateString:template, 
        // baseClass: "dijitDropDownButton",
        grid:"ddm/fluwiz/widget/AccordionList",
        baseClass:'NotificationDropDown', 
        iconClass:"fa fa-generic",
        postCreate:function(){
            this.tgtPageMenu = this.data.tgtPage;
            this.inherited(arguments);
            if(this.data.options){
                var _opt = JSON.parse(this.data.options);
                if(_opt.iconClass)
                    this.iconClass = _opt.iconClass;
            }
            domClass.add(this.iconNode, this.iconClass);
            var tableLayout = this.data.tableLayout;
            
            if(tableLayout && tableLayout.tableData && tableLayout.tableData.result){                
                var size = tableLayout.tableData.result.length;
                this.contentNode.innerHTML = size;
                if(0 == size){
                    this.disableIcon();
                }
            }else{
                this.disableIcon();
            }
        }, 
        disableIcon:function(){
            domClass.add(this.iconNode, this.baseClass + "Disabled");
            domClass.add(this.contentNode, "notificationIconContentDisabled");
        },
        postGridCreate:function(widget){
            var dialog = new TooltipDialog({});
            var pane = new ContentPane();
            pane.addChild(widget);
            this.dropDown = dialog;
            dialog.set("content", pane);
            if(this.tgtPageMenu){
				this.topicHandle = topic.subscribe("/popup/summary/refresh/"+ this.itemId + "/" + this.tgtPageMenu, 
					lang.hitch(this, function(args){
						try {
							this.refresh();
						}catch(e){
							console.error(e);
						}
					})
				);
            }
        },
        refresh:function(){
            var child = this.dropDown;
            if(child){
                var model = child.model;
                if(model){
                    child.data = null;
                    child.refreshModel();
                    var root = child.rootNode.item;
                    if(root && root.children)
                    this.contentNode.innerHTML = root.children.length;
                }
            }
        },
        getGridOptions:function(){
            var options = this.inherited(arguments);
            options.itemId = this.itemId;	
            options.permission={};
            if(this.data.options){
                var _opt = JSON.parse(this.data.options);
                options = lang.mixin(options, _opt); 
            }
            options.tgtPageMenu = this.data.tgtPage;            
            return options;
        },
        loadDropDown: function(callback){
            var dropDown = this.dropDown;
            if(!dropDown){ return; }
            if(!this.isLoaded()){
                var handler = dropDown.on("load", this, function(){
                    handler.remove();
                    callback();
                });
                dropDown.refresh();
            }else{
                callback();
            }
        },
        isLoaded: function(){         
            var dropDown = this.dropDown;
            return (!!dropDown);
        },
		destroy:function(){
			if(this.topicHandle){
				this.topicHandle.remove();
			}
			this.inherited(arguments);
		}
	});
});
