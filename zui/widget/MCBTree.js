define("ddm/zui/widget/MCBTree", [
	"dojo/_base/declare", 
	"dojo/_base/lang",
	'dojo/_base/array', 
	"dijit/Tree", "ddm/zui/form/TriStateCheckBox"], 
	function(declare,lang,array,Tree, CheckBox) {

/*
	Extension of dijit/Tree module to provide  multi-tier checkbox.

	TODO:  rename checkBoxNode variable
*/

	var base = [Tree];
	var TreeNode = declare([Tree._TreeNode], {
		postCreate:function(){
			this.inherited(arguments);
			var checkBox = new CheckBox({id: this.id + this.item.id});
			checkBox.placeAt(this.labelNode, "first");
			this.checkBoxNode = checkBox;
			switch(this.item.mask){
				case 1:{				
					checkBox.set('checked', 'mixed');
					break;
				}
				case 2:{				
					checkBox.set('checked', true);
					break;
				}
				default:
					checkBox.set('checked', false);
			}
			this.set('disabled',true);
		},
		addChild:function(/*TreeNode*/ node){
			if(2 == this.item.state){
				node.checkBoxNode.set('checked',  true);
				node.item.state = 2;
			}
			if(-1 == this.item.state){
				node.checkBoxNode.set('checked',  false);
				node.item.state = -1;
			}
			node.checkBoxNode.set('disabled', this.checkBoxNode.get('disabled'));
			this.inherited(arguments);
		},
		_getValueAttr(){
			var checked = this.checkBoxNode.get('checked');
			var includeChildren = false;
			var _state = (checked == 'mixed') ? 1 : (checked ? 2 : -1);
			
			if(1 == _state || undefined == this.item.code){
				var _children = array.map(this.getChildren(),lang.hitch(this,function(child){
							return child.get('value');
						}));

				includeChildren = _children.length > 0;
			}

			if(includeChildren)
				return {id:this.item.id, mask : _state , children : _children, code:this.item.code} ;
			else
				return {id:this.item.id, mask : _state , code:this.item.code} ;
		},
		_setDisabledAttr:function(flag){
			var value = flag;
			array.forEach(this.getChildren(),lang.hitch(this,function(child){
				child._setDisabledAttr( value);
			}));
			this.checkBoxNode.set('disabled', value);
		}
    });

	
	return declare("ddm.zui.widget.MCBTree", Tree, {
		_createTreeNode: function(/*Object*/ options) {
			var tnode = new TreeNode(options);
			return tnode;
        },				
		_getValueAttr:function(){
			var node = this.rootNode;
			return node.get('value');
		},
		set:function(key, value){
			if('disabled' == key){
				var node = this.rootNode;
				if(node)
					node._setDisabledAttr( value);
			}
		},
		onClick : function(/*Menu*/ menuItem, /*TreeNode*/ node, /*clickEvent*/ event) {
			if(event.target.type == 'checkbox')
			{
				var newStatus = node.checkBoxNode.get('checked');
				node.item.state =  newStatus ? 2 : -1;
				this._updateChild(node, newStatus);
				this._updateParent(node, newStatus);
			}
		},
		_updateParent(/*TreeNode*/ node, /*Checked Status*/ newStatus){
			var parent = node.getParent();
			if(!parent || !parent.checkBoxNode)
				return;

			switch(newStatus){
				case 'mixed':{
						parent.checkBoxNode.set('checked', 'mixed');
						this._updateParent(parent, 'mixed');
						break;
				}
				//case 'false':
				case false:{
					var calcStatus = array.some(parent.getChildren(),lang.hitch(this,function(child){
						return child.checkBoxNode.get('checked') == true;
					}));
					var _newStatus = calcStatus ? 'mixed' : false;
					parent.checkBoxNode.set('checked', _newStatus);
					this._updateParent(parent, _newStatus);
					break;
				}
				//case 'true':
				case true:{
					var calcStatus = array.every(parent.getChildren(),lang.hitch(this,function(child){
						return child.checkBoxNode.get('checked') == true;
					}));
					var _newStatus = calcStatus ? true : 'mixed';
					parent.checkBoxNode.set('checked', _newStatus);
					this._updateParent(parent, _newStatus);
					break;				
				}
			}
		},
		
		_updateChild(/*TreeNode*/ node, /*Checked Status*/ newStatus){
			/*populate the newStatus to all the descendants*/
			array.forEach(node.getChildren(),lang.hitch(this,function(child){
				child.checkBoxNode.set('checked', newStatus);
				if(child.item.hasChild)
					this._updateChild(child, newStatus);
			}));
		}
    });
});