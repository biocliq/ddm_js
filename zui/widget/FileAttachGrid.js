/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/widget/FileAttachGrid", [ "dojo/_base/declare","dojo/_base/array",
		"dojo/date/locale",
		'ddm/fluwiz/grid/ddmGrid', 'gridx/core/model/cache/Async',
		'gridx/modules/SingleSort', 'gridx/modules/ColumnResizer',
		'gridx/modules/Filter', "ddm/gridx/modules/toolbar/ExplorerFilterBar",
		"gridx/modules/Pagination", "gridx/modules/pagination/PaginationBar",
		"gridx/modules/select/Row", 		
		"gridx/modules/CellWidget","gridx/modules/Bar",
		'gridx/modules/ColumnWidth',"gridx/modules/ColumnLock",
		"gridx/modules/HLayout", "gridx/modules/HScroller","gridx/modules/extendedSelect/Row",
		"dojo/store/Memory", "dijit/form/Button"], function(
		declare, array, locale,
		Grid, AsyncCache, 
		Sort, ColumnResizer, 
		Filter, FilterBar,
		Pagination, PaginationBar, 
		SelectRow,
		CellWidget,Bar,
		ColumnWidth,ColumnLock,
		HLayout, HScroller,ExSelectRow,
		Memory) {

	var base = Grid;

	var convertBytes = function(bytes){
		// summary:
		//		Converts bytes. Returns an object with all conversions. The "value" property is
		//		considered the most likely desired result.

		var kb = Math.round(bytes/1024*100000)/100000;
		var mb = Math.round(bytes/1048576*100000)/100000;
		var gb = Math.round(bytes/1073741824*100000)/100000;
		var value = bytes;
		if(kb>1) value = kb.toFixed(1)+" kb";
		if(mb>1) value = mb.toFixed(1)+" mb";
		if(gb>1) value = gb.toFixed(1)+" gb";
		return {
			kb:kb,
			mb:mb,
			gb:gb,
			bytes:bytes,
			value: value
		}; // Object
	};

	function openFile(path){
		var url = "v2/download/patient/4/" + path;
		var win = window.open(url, '_blank');
	}

	return declare("ddm.zui.widget.FileAttachGrid", base, {
		filterBarClass: "ddm/gridx/modules/toolbar/ExplorerFilterBar",
		paginationBarSizes: [10, 25, 50],
		pageSize: 50,
		paginationInitialPageSize: 10,
		autoHeight: true,		
		cacheClass: AsyncCache,				
		setStructure:function(str){			
			str.unshift(this.createRemoveColumn());
			this.structure = str;
		},
		
		formatBYTES:function(field, column){
			column.decorator = function(value){
				return convertBytes(value).value;
			}
		},
		gridModulesCustomize: function () {
			this.modules = [Sort, HLayout, HScroller, ColumnResizer, ExSelectRow, CellWidget, ColumnWidth, ColumnLock];			
			this.modules.push(Pagination);
			this.modules.push(PaginationBar);	
			
			this.modules.push(Filter, Bar);
			this.barTop = [{ pluginClass: this.filterBarClass, style: 'text-align: right;' }];
		},	
		createRemoveColumn : function(){			
			var field = {id: "sdf", name: " ", label: "Actions",colspan:2,tabDisplay  :true, width : "160px", widgetsInCell:true, 
				decorator: function(){
					try {
						var templateString =  "<div 'style=width:100%'>";

						templateString += "<div 'style=width:50%' data-dojo-type='dijit/form/Button' " +
						"data-dojo-attach-point='remove' "+
						"<span class='viewfieldsacl'>Remove</span></div>";
						// templateString += "<div 'style=width:50%' data-dojo-type='dijit/form/Button' " +
						// "data-dojo-attach-point='download'"+ 
						// "<span class='viewfieldsacl'>Download</span></div>";
						
						templateString += "</div>";
						return templateString;
					} catch (e) {
						console.error('error decorating date: ' + e.toString());
					}
				},
				setCellValue: function(gridData,storeData,cellWidget){
					
				},
				getCellWidgetConnects: function(cellWidget, cell){						
						var permission = cell.grid.permission;
						var result = [];
						// if(permission.delete){
							cellWidget.remove.set('disabled', false);
							result.push([cellWidget.remove, 'onClick', function(e){
								var store = cell.grid.store;
							var id =  cell.row.rawData()[store.idProperty];
							store.remove(id);
							}]);
						// }else{
						// 	cellWidget.remove.set('disabled', true);
						// }

						// if(permission.update){
						// 	cellWidget.download.set('disabled', false);
						// 	result.push([cellWidget.download, 'onClick', function(e){
						// 		alert('Downloading is currently disabled');
						// 	}]);
						// }else{
						// 	cellWidget.download.set('disabled', true);
						// }
						return result;
					}
			};

			return field;
		},
		
	});

});