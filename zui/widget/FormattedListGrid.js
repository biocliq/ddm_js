/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/zui/widget/FormattedListGrid", [ "dojo/_base/declare","dojo/_base/array",'dojo/_base/lang',"dojo/topic",
		'ddm/zui/page/base/explorer/SimpleGrid', 'gridx/core/model/cache/Async',
		'gridx/modules/SingleSort', 'gridx/modules/ColumnResizer',
		'gridx/modules/Filter', "ddm/gridx/modules/toolbar/ExplorerFilterBar",
		"gridx/modules/Pagination", "gridx/modules/pagination/PaginationBar",
		"gridx/modules/select/Row", 		
		"gridx/modules/CellWidget","gridx/modules/Bar",
		"dojo/store/Memory", "ddm/util/util"], function(
		declare, array, lang,topic,
		Grid, AsyncCache, 
		Sort, ColumnResizer, 
		Filter, FilterBar,
		Pagination, PaginationBar, 
		SelectRow,
		CellWidget,Bar, Memory, util) {

	var base = Grid;


	return declare("ddm.zui.widget.FormattedListGrid", base, {
		root_id : null,
		baseClass:"ListGrid",
		bodyEmptyInfo:"none",
		sys_page : "null",
		headerHidden: true,
		hideOptions:true,
		paginationInitialPageSize:50,
		paginationBarSizes: [10, 25, 50],
		autoHeight: true,
		// filterBarClass:"",
		columnWidthAutoResize: true,
		cacheClass: AsyncCache,
		selectRowTriggerOnCell: true,
		modules : [Sort, ColumnResizer, Bar, Filter, Pagination, SelectRow,CellWidget], 
		// barTop :  [{pluginClass: FilterBar, style: 'text-align: right;'}],
		structure : [
			{id:'id', name:'', field: 'id', 
				decorator:function(value, id1, id2, tmp){
					// var store = tmp.row.grid.store;
					var model = tmp.row.model;
					var id = tmp.row.id;
					var grid = tmp.row.grid
					var labelFormat = grid.displayFormat;
					if(labelFormat){
						var object = model.byId(id).item;
						var _object = {};
						if(object && labelFormat){
                            if(object.label)
                                return object.label;                    
                            for(idx in grid.fields){
								field = grid.fields[idx];
								if('DATE' == field.displayFormat 
								|| 'DATETIME' == field.displayFormat
								|| 'TIMESTAMP' == field.displayFormat){
                                    if(object[field.name]){
                                        _object[field.name] = util.calcAge(object[field.name], true);
                                    }
                                }else
                                    _object[field.name] = object[field.name];
							}
							var val = lang.replace(labelFormat, _object).replace('0 day ago', 'today');
							return val;
                        }
					}
					return value;
				}
			}			
		],
		
		postCreate:function(){
			this.inherited(arguments);			
			var self = this;
			if("encounter" == self.tgtPageMenu)		{
				this.connect(this.select.row, 'onSelected', function(row){
					console.log(row.id);
					topic.publish("/patient/" + self.itemId + "/past_encounter/", self.tgtPageMenu, row.id);
				});

				this.evtCellDblClick = function(){};
			}
		},
		processFields:function(){
		
		}
	});

});