define("ddm/zui/widget/AclGrid", [ 
    "dojo/_base/declare",
    "dojo/topic",
    "dojo/_base/array",
    "dojo/_base/lang", 
    "dojo/on",
    'ddm/wire/store/grid/JsonAclStore', 
    'dojo/store/Cache',
    'ddm/fluwiz/grid/ddmGrid',
    'gridx/core/model/cache/Async',
    'gridx/modules/SingleSort', 
    'gridx/modules/ColumnResizer',
    'gridx/modules/Filter', 
    "ddm/gridx/modules/toolbar/ExplorerFilterBar",
    "gridx/modules/Pagination", 
    "gridx/modules/pagination/PaginationBar",
    "gridx/modules/select/Row",         
    "gridx/modules/IndirectSelect",
    "gridx/modules/extendedSelect/Row",
    "gridx/modules/RowHeader",
    "gridx/modules/HScroller",
    "dojo/store/Memory",
    "gridx/modules/Edit",
    "gridx/modules/Bar",
    "gridx/modules/CellWidget",
   ], function(
        declare,topic, array, lang, on,JsonAclStore, StoreCache, Grid,
        AsyncCache, Sort, ColumnResizer, Filter, FilterBar,
        Pagination, PaginationBar, SelectRow,
        IndirectSelect, ExSelectRow, RowHeader, HScroller,  Memory,Edit,Bar,CellWidget) {

    var base = [Grid];

    return declare("ddm.zui.widget.AclGrid", base, {
        
        postMixInProperties : function() {
            
            this.inherited(arguments);
            this.pageSize = 10,
		    this.paginationInitialPageSize = 10,
			this.paginationBarSizes = [15, 25, 50],
            this.options = {};
            if("CiType" == this.title)
            {
                this.createStructure();
                this.createOptions(this.pageMenu,this.itemId,this.title,0);
            }
            else
                this.createOptions('group',this.pageMenu,this.itemId,this.mode);
            this.modules = [ Sort, Filter,Bar, ColumnResizer,Edit,Pagination, PaginationBar, SelectRow, CellWidget];
            this.barTop =  [{pluginClass: FilterBar, style: 'text-align: right;'}];
            console.log(this.options);
            this.store = new JsonAclStore(this.options);             
        },
        formatField : function(field,column)
        {
            this.inherited(arguments);
                    if("ciType" != field.name && "field" != field.name)
                    {
                        column.widgetsInCell= true;
                        column.decorator= function(){
                            try {
                                return "<div data-dojo-type='dijit/form/CheckBox' " +
                                "data-dojo-attach-point='aclmask' ></div>";
                            } catch (e) {
                                console.error('error decorating date: ' + e.toString());
                            }
                        },
                        column.setCellValue= function(gridData,storeData,cellWidget){
                            cellWidget.aclmask.set('value', gridData);
                            if(cellWidget.aclmask._cnnt){
                                // Remove previously connected events to avoid memory leak.
                                cellWidget.aclmask._cnnt.remove();
                            }
                            cellWidget.aclmask._cnnt = dojo.connect(cellWidget.aclmask, 'onClick', function(e){
                                var maskvalue = cellWidget.aclmask.get('value');
                                if(maskvalue != false)
                                    maskvalue = true;
                                var rowData = cellWidget.cell.row.rawData();
                                var columnName = cellWidget.cell.column.name().toLowerCase();
                                rowData[columnName] = maskvalue;
                                cellWidget.cell.grid.store.put(rowData);
                            });
                        }
                    }
                    if("field" == field.name)
                    {
                        column.widgetsInCell= true;
                        column.decorator= function(){
                            try {
                                return "<div data-dojo-type='dijit/form/Button' " +
                                "data-dojo-attach-point='viewFields'"+
                                "<span class='viewfieldsacl'>View Fields</span></div>";
                            } catch (e) {
                                console.error('error decorating date: ' + e.toString());
                            }
                        }
                        column.setCellValue= function(gridData,storeData,cellWidget){
                            //cellWidget.aclmask.set('value', gridData);
                            if(cellWidget.viewFields._cnnt){
                                // Remove previously connected events to avoid memory leak.
                                cellWidget.viewFields._cnnt.remove();
                            }
                            cellWidget.viewFields._cnnt = dojo.connect(cellWidget.viewFields, 'onClick', function(e){
                                var rowData = cellWidget.cell.row.rawData();
                                topic.publish("/popup/page/fieldacl/",rowData)
                            });
                        }
                    }
              
        },
        createStructure : function()
        {
            this.fields = [ 
                {id: "ciType", name: "ciType", label: "Ci Type",colspan:4,tabDisplay  :true},
                {id: "create", name: "create", label: "Create",colspan:2,tabDisplay  :true},
                {id: "read", name: "read", label: "Read",colspan:2,tabDisplay  :true},
                {id: "update", name: "update", label: "Update",colspan:2,tabDisplay  :true},
                {id: "delete", name: "delete", label: "Delete",colspan:2,tabDisplay  :true},
                {id: "field", name: "field", label: "Fields",colspan:2,tabDisplay  :true}
            ]  
            this.setStructure();
        },
        setStructure : function()
        {
            if(this.fields)
            {
                var columns=[];
					array.forEach(this.fields, lang.hitch(this, function(field){
						if(field.tabDisplay){
							column =  {id: field.name, field : field.name, name:field.label};
							this.formatField(field, column);
							column.editable = field.editable;
							columns.push(column);
						}
					}));				
					this.structure = columns;
            }
            
        },
        createOptions : function(pageMenu,itemId,title,ciId)
        {
            this.options.pageMenu = pageMenu;
            this.options.sid = itemId;
            this.options.aclType = title;
            this.options.ciId = ciId;
        },
        
    });
});

