define("ddm/zui/widget/Address", 
	[
        'dojo/_base/declare','dojo/_base/lang','dojo/on',
        "dojo/dom-class","dojo/dom-construct",
        "dijit/layout/ContentPane", "ddm/zui/page/base/explorer/FieldSetContainer"
	],
    function( declare,lang, on,domClass,domConstruct, 
        ContentPane, widgetBase) {
        var base = [widgetBase ];	
        
        return declare('ddm.zui.widget.Address', base, {
            postCreate:function(){
                this.flag = true;
                this.inherited(arguments);
                this.own(
                    on(this.widgets.addressCountry, 'change', lang.hitch(this, function(id) {
                        this._onChangeCountry(id);
                    } ))
                );
                this.own(
                    on(this.widgets.addressState, 'change', lang.hitch(this, function(id) {
                        this._onChangeState(id);
                    } ))
                );  
            },
            _onChangeCountry : function(/*drugId - int */ id){
                var state  = this.widgets.addressState;
                if(id)
                    state.query['country.id'] = id;
                else
                    delete state.query['country.id'];
                if(this.flag)
                    state.setValue('');
                else
                    this.flag = true;
            },
            _onChangeState:function(id, handler){
                var country = this.widgets.addressCountry;
                var state  = this.widgets.addressState;
                var data = state.store.get(id);
                if(data && data.referenceCIs){
                    var countryId = data.referenceCIs.country.id;
                    if(country.getValue() != countryId){
                        this.flag = false;
                        country.setValue(countryId);
                    }
                }
            }
		});
	}
);