define("ddm/zui/widget/SummaryDialog",
        ['dojo/_base/declare',
        "ddm/zui/widget/Dialog",
        "ddm/zui/page/base/GridContainer",
        "ddm/zui/page/base/summary/SummaryGrid"], 
		function(declare,Dialog,GridContainer,SummaryGrid) {

	var base = [Dialog,GridContainer];

	return declare("ddm.zui.widget.SummaryDialog", base, {
        grid: "ddm/zui/page/base/summary/SummaryGrid",
		padding: "0px",
        doLayout :"false",
		permission:{},
		startup:function()
		{
			this.inherited(arguments);
		},
		getGridOptions:function(){
			var options = this.inherited(arguments);			
			options.autoHeight = false;
            options.style="height:100%";
            options.permission=this.permission;            
            var defaultFilter = this.defaultFilter;            
            if(defaultFilter)
                options.defaultFilter = defaultFilter;
            
            if(this.parentSource){
                options.parentSource = this.parentSource;
            }

            options.evtCellDblClick = function(e){
                dojo.stopEvent(e);                
                this.getParent().onRowSelect(e.rowId);                
            }
			return options;
        }, 
        onRowSelect:function(rowId){            
            this.callback.call(this, rowId);
            this.hide();
        }
		
	});
});