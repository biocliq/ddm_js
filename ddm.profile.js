var profile = (function(){
	var	copyOnly = function(filename, mid){
			var list = {
				"ddm/ddm.profile":1,
				"ddm/package.json":1,
			};
			return (mid in list) ||
				/^dojo\/_base\/config\w+$/.test(mid) ||
				(/^dojo\/resources\//.test(mid) && !/\.css$/.test(filename)) ||
				/(png|jpg|jpeg|gif|tiff)$/.test(filename) ||
				/built\-i18n\-test\/152\-build/.test(mid);
		};

    return {
        resourceTags: {
            copyOnly: function(filename, mid){
				return copyOnly(filename, mid);
            },
            amd: function(filename, mid) {
                return /\.js$/.test(filename);
            }
        }
    };
})();