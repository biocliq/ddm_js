/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

require(["dojo/window","dojo/dom-style","dojo/on",
		"dijit/layout/BorderContainer", "dijit/layout/TabContainer",
		"dijit/layout/StackContainer","dijit/layout/TabController",
		"dojo/topic","dojo/router", 
		"dijit/layout/ContentPane", "dojox/calendar/Calendar",
		"ddm/zui/layout/HeaderContent","ddm/fluwiz/widget/AccordionMenu",
		"ddm/zui/page/base/explorer/ExplorerPage",		
		"dojo/domReady!","ddm/zui/widget/AclGrid"],
	function(dWindow, domStyle,on,
				BorderContainer, TabContainer,
				StackContainer,TabController,
				topic, router, 
				ContentPane, Calendar,
				HeaderContent, LeftMenu) {
	
		var work = new StackContainer({
			region : "center",
			id : "workArea",
			style:"border:0px; margin:0px;height: 100%;width:100%; float:left;"
		}, workArea);
		
		work.startup();
		var leftMenu = new LeftMenu({			
			style : "height:100%;"
		});
		var leftContent = new ContentPane({}, menuContainer);
		leftContent.addChild(leftMenu);
		leftContent.startup();

		var headerContentPane = new ContentPane({}, header);
		var headerPane = new HeaderContent({
			region : "center"
		});
		headerPane.startup();
		headerContentPane.addChild(headerPane);		
			
		work.own(dojo.connect(work,"_transition", function(newPage, oldPage){
			if(!newPage.loaded && newPage.populate){
				 newPage.populate();
			 }else if(newPage.resume)
				 newPage.resume();
			 
			 if(oldPage && oldPage.pause){
				 oldPage.pause();
			 }
			 if(newPage.calcHeight)
				   newPage.calcHeight();				    
		   }));

		topic.publish("/load/workArea", "completed");
	});
