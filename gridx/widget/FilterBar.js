/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define([
        'dojo/_base/declare',
        'dojo/_base/lang',
        'dojo/_base/array',
		'dojo/_base/xhr',
        'dojo/dom-class',
        'dojo/keys',
        'dojo/topic',
        'dijit/_WidgetBase',
        'dijit/_TemplatedMixin',
        'dijit/_WidgetsInTemplateMixin',
        'dijit/form/TextBox',
        'dijit/form/Button',
        'dijit/form/ComboButton',
        'dijit/Menu',
        'dijit/MenuItem',
        'gridx/modules/Filter',
        'dojo/text!../templates/FilterBar.html',
        'dojox/html/entities',
        'ddm/gridx/widget/FilterDialog'
        ], function(declare, lang, array, xhr,domClass, keys, topic,
        		_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
        		TextBox, Button, ComboButton, Menu, MenuItem,
        		F, template, entities, FilterDialog){

	return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
		templateString: template,
		dialog : null,
		constructor: function(args){
			var t = this;
			lang.mixin(t, args.grid.nls);
			t._hasFilterBar = args.grid.filterBar ? 'gridxQuickFilterHasFilterBar' : 'gridxQuickFilterNoFilterBar';
			if(t._hasFilterBar){
				t.connect(args.grid.filterBar, 'clearFilter', function(){
					if(t.textBox){
						t.textBox.set('value', '');
					}
				});
			}
			t.connect(args.grid.model, 'setStore', function(){
				t.textBox.set('value', '');
				domClass.remove(t.domNode, 'gridxQuickFilterActive');
			});
		},

		postCreate: function(){
			var grid = this.grid;
			
			grid.connect(grid.select.row,'onSelected', lang.hitch(this, function(){
				this.disableBtnDelete(false);
			}));

			this.dialog = new FilterDialog({grid: this.grid});
			this.dialog.hide();
			this.connect(this.textBox, 'onInput', '_onInput');
			this.disableBtnDelete(true);
		},

		grid: null,

		textBoxClass: 'dijit.form.TextBox',

		buttonClass: 'dijit.form.Button',

		comboButtonClass: 'dijit.form.ComboButton',

		menuClass: 'dijit.Menu',

		menuItemClass: 'dijit.MenuItem',

		autoApply: true,

		delay: 700,

		disableBtnDelete:function(flag){
		
			if(flag)
				this.btnDelete.setDisabled(true);
			else
				this.btnDelete.setDisabled(false);
		},

		disableBtnAdd:function(flag){
			if(flag)
				this.btnAdd.setDisabled(true);
			else
				this.btnAdd.setDisabled(false);
		},

		addRecord:function(){
			var t = this;
			console.log(t.grid.pageMenu);
			var page = t.grid.pageMenu;
			topic.publish("/form/new",page);
		},
		removeRecord:function(){
				var t = this;
				var selRowIdArray = t.grid.select.row.getSelected();
				var gridStore = t.grid.model.store;
			for(var i=0; i<selRowIdArray.length; i++){
				gridStore.remove(selRowIdArray[i]);
			}
		},
		//Private--------------------------------------------------------------------
		_onInput: function(evt){
			var t = this,
			dn = t.domNode,
			tb = t.textBox,
			key = evt.keyCode;
			setTimeout(function(){
				domClass.toggle(dn, 'gridxQuickFilterActive', tb.get('value'));
			}, 0);
			if(t.autoApply && key != keys.TAB){
				clearTimeout(t._handle);
				t._handle = setTimeout(function(){
					t._filter();
				}, key == keys.ENTER ? 0 : t.delay);
			}
		},

		_onKey: function(evt){
			if(evt.keyCode == keys.ENTER){
				this.grid.focus.stopEvent(evt);
				this._clear();
				this.textBox.focus();
			}
		},

		_clear: function(){
			this.textBox.set('value', '');
			domClass.remove(this.domNode, 'gridxQuickFilterActive');
			this._filter();
		},

		_filter: function(){
			var t = this,
			g = t.grid,
			v = t.textBox.get('value'),
			cols = array.filter(g.columns(), function(col){
				return col.filterable !== false;
			}),
			cs = t.grid.filter.arg('caseSensitive');
			clearTimeout(t._handle);
			if(g.filterBar){				
				console.log(v + " typed value");
				//TODO: is there a better way communicate with FilterBar?
				if(v === ''){
					g.filterBar.clearFilter(true);
				}else{
					g.filterBar.applyFilter({
						conditions: [{
							condition: 'contain',
							value: v
						}]
					});
				}
			}else{
				console.log(v + " typed else value");
				if(v === ''){
					g.filter.clearFilter(true);
				}else{
					var cols = ['quickFilter'];
					var _this = this;
					var sd = F.or.apply(0, array.map(cols, function(col){
						return F.contain(F.column(col, null, null, false), F.value(v, null, null, false));
					}));

					g.filter.setFilter(sd);
				}
			}
		},


		column: function(/* String|Number */name, /* String? */type, /* Function? */converter, caseSensitive){
			type = String(type || 'string').toLowerCase();

			return this.wrap(function(){
				return valueConvert( name, type, converter, caseSensitive);
			}, type, name, {isCol: true});
		},

		wrap:function(checker, op, operands, options){
			if(lang.isArray(operands)){
				operands = array.map(operands, function(operand){
					return operand.expr;
				});
			}
			return lang.mixin(checker, {
				expr: lang.mixin({
					op: op,
					data: operands
				}, options || {})
			});
		},

		_showFilterBar: function(){
			this.dialog.show();
			//console.log(this.grid);
		},
	
		downloadReport : function(){
			var t = this;
			g = t.grid;
			var filterData = g.filter.getFilter();
			if(filterData)
				{
				console.log(filterData);
				console.log(filterData.expr);
				var query = g.filterSetupQuery(filterData.expr);
					if(query){
						query = xhr.objectToQuery(query);
						console.log(query);
						window.location.href = "../v2/report/queryReport/"+this.grid.pageMenu + "?" + query;
					}
				}
			else{
				window.location.href = "../v2/report/queryReport/"+this.grid.pageMenu;
			}
		//	alert("download Report"+this.grid.pageMenu);
			
		//	window.location.href = "http://localhost:6060/ddmCore/v1/gepocv2/report/Aggregator";
			
		}
	});
});
