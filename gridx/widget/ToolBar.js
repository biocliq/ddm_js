/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/gridx/widget/ToolBar",[
        'dojo/_base/declare',
        'dojo/_base/lang',
        'dojo/_base/array',
		'dojo/_base/xhr',
        'dojo/dom-class',
        'dojo/topic',
        'dijit/_WidgetBase',
        'dijit/_TemplatedMixin',
		 'dijit/_AttachMixin',
        'dijit/_WidgetsInTemplateMixin',
        'dijit/form/TextBox',
        'dijit/form/Button',
        'dijit/form/ComboButton',
        'dijit/Menu',
        'dijit/MenuItem',
		"./FilterBar",
        'dojo/text!../templates/ToolBar.html'		
        ], function(declare, lang, array, xhr,domClass, topic,
        		_WidgetBase, _TemplatedMixin, _AttachMixin, _WidgetsInTemplateMixin,
        		TextBox, Button, ComboButton, Menu, MenuItem,FilterBar,
        		template
			){

	return declare("ddm.gridx.widget.ToolBar", [_WidgetBase, _TemplatedMixin,_AttachMixin,  _WidgetsInTemplateMixin], {
		templateString: template,
		dialog : null,
		addDialog : null,
		constructor:function(options){
			// this.grid = options.grid;			
		},
		postCreate: function(){
			// this.disableBtnDelete(true);		
			var filter = new FilterBar({grid:this.grid});
			this.filterContainer.addChild(filter, 0);
		},

		grid: null,

		disableBtnDelete:function(flag){		
			if(flag)
				this.btnDelete.setDisabled(true);
			else
			 this.btnDelete.setDisabled(false);
		},

		disableBtnAdd:function(flag){
			if(flag)
				this.btnAdd.setDisabled(true);
			else
				this.btnAdd.setDisabled(false);
		},

		showAddDialog:function(){
			var t = this.grid;
			
			var options = {containerId : t.containerId,
						itemId : t.itemId,
						structure : t.structure,
						store : t.store,
						fields : t.fields
					}
			var simpleGrid = new PopupGrid(options);
			var btn = new Button({label : 'Add',
								onClick: function(){
								//	console.log("btn click");
							//		console.log(this.grid.select.row.getSelected());
								},grid : simpleGrid});
			

			var dialog = new ExploreGridDialog({style: "width: 800px"});
			
		
			dialog.addChild(simpleGrid);
			dialog.addChild(btn);
			dialog.startup();
			simpleGrid.refresh();
			dialog.show();

		},

		removeRecord:function(){
				var t = this;
				var selRowIdArray = t.grid.select.row.getSelected();
				var gridStore = t.grid.model.store;
			for(var i=0; i<selRowIdArray.length; i++){
				gridStore.remove(selRowIdArray[i]);
			}
		},
		
	
		downloadReport : function(){
			var t = this;
			g = t.grid;
			var filterData = g.filter.getFilter();
			if(filterData){
				var query = g.filterSetupQuery(filterData.expr);
				if(query){
					query = xhr.objectToQuery(query);			
					window.location.href = "../v2/report/queryReport/"+this.grid.pageMenu + "?" + query;
				}
			}else{
				window.location.href = "../v2/report/queryReport/"+this.grid.pageMenu;
			}		
		}
	});
});
