/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/gridx/widget/FilterPane", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"dojo/_base/lang",
	"dojo/on",
	'dijit/_WidgetBase',
	"dijit/_TemplatedMixin",
	'dijit/_AttachMixin',
	"dijit/_OnDijitClickMixin",
	'dijit/_WidgetsInTemplateMixin',
	'dijit/layout/_LayoutWidget',
	'ddm/util/config',
	'dijit/focus',
	"dojox/layout/TableContainer",
	"dijit/form/TextBox",
	"ddm/zui/form/DateWTextBox", "dijit/form/TimeTextBox",
	"gridx/modules/Filter", "ddm/zui/form/DateWTimestampTextBox",
	"ddm/zui/form/BooleanFilteringSelect",
	"dojo/text!../templates/FilterPane.html",
	"dijit/Fieldset"
],
	function (declare, array, lang, on, _WidgetBase, _TemplatedMixin,
		_AttachMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin,
		_LayoutWidget, config, focusUtil, TableContainer, TextBox,
		DateTextBox, TimeTextBox, F, DateWTimestampTextBox,
		BooleanFilteringSelect, template) {

		return declare("ddm.gridx.widget.FilterPane", [_WidgetBase, _OnDijitClickMixin, _TemplatedMixin,
			_AttachMixin, _WidgetsInTemplateMixin], {
			templateString: template,
			widgetsInTemplate: true,

			applyFilter: function () {
				var _this = this;
				var fields = this.getData();
				
				if (0 == fields.length) {
					this.grid.filter.clearFilter();
					this.onClear();
				}
				else {
					var filterExpr = F.and.apply(0, array.map(fields, function (field) {
						return F.equal(_this.column(field.name, null, null, true), F.value(field.value, null, null, true));
					}));
					this.grid.filter.setFilter(filterExpr);
					this.onApply();
				}
			},

			column: function (/* String|Number */name, /* String? */type, /* Function? */converter, caseSensitive) {
				type = String(type || 'string').toLowerCase();

				return this.wrap(function () {
					return valueConvert(name, type, converter, caseSensitive);
				}, type, name, { isCol: true });
			},

			wrap: function (checker, op, operands, options) {
				if (lang.isArray(operands)) {
					operands = array.map(operands, function (operand) {
						return operand.expr;
					});
				}
				return lang.mixin(checker, {
					expr: lang.mixin({
						op: op,
						data: operands
					}, options || {})
				});
			},

			clearFilter: function () {
				var tableContainer = this.filter.getChildren()[0];
				var data = [];
				dojo.forEach(tableContainer.getChildren(), function (child) {
					var _value = child.set('value', '');
				});

				this.grid.filter.clearFilter();
				this.onClear();
			},

			getData: function () {
				var tableContainer = this.filter.getChildren()[0];
				var data = [];
				dojo.forEach(tableContainer.getChildren(), function (child) {
					var _value;

					//   				if(child.displayFormat == 'DATE'){
					//
					// 					_value = child.value;
					//
					//
					// 					if(_value != undefined && _value != '')
					// 					{
					// 						if(!isNaN(_value))
					// 						{
					// 							_value = _value.getTime();
					// 						}
					// 					}
					//
					// 				}else{
					_value = child.get('value');

					//}
					if (_value != undefined && _value != '' && _value.toString() != 'Invalid Date') {
						if ("searchFileDate" == child.name) {
							data.push({ name: child.name, value: child.dateFromTextBox.get('value') + " - " + child.dateToTextBox.get('value') });
						}
						else {
							data.push({ name: child.name, value: _value });
						}

					}
				});
				return data;
			},

			setData: function (data) {
				var tableContainer = this.filter.getChildren()[0];				
				dojo.forEach(tableContainer.getChildren(), function (child) {					
					if(data[child.name] || '' == data[child.name]) {						
						if(data[child.name])
							child.set('value', data[child.name])
						else{
							child.reset();
						}
					}					
				});
				var fields = this.getData();				
				this.applyFilter();
			},

			onApply: function () {
			},

			onClear: function () { },

			postCreate: function () {
				this.inherited(arguments);
				var fields = this.grid.fields;				
				this.grid.filterPane = this;
				var formContainer = new TableContainer({ cols: 2 });
				for (var _idx in fields) {
					var field = fields[_idx];
					if (field.searchable) {
						var djfield;
						field.style = "background-color : transparent;";
						field.appId = field.id;
						field.id = undefined;
						switch (field.displayFormat) {
							case "TEXTFIELD":
							case "TEXTAREA":
							case "DROPDOWN": {
								djfield = new TextBox(field);
								break;
							}
							case "DATE": {
								djfield = new DateTextBox(field);
								break;
							}
							case "TIMESTAMP": {
								djfield = new DateWTimestampTextBox(field);
								break;
							}
							case "BIT": {
								djfield = new BooleanFilteringSelect(field);
								break;
							}
							case "TIME": {
								djfield = new TimeTextBox(field);
								break;
							}
							default:
								break;
						}
						if (djfield)
							formContainer.addChild(djfield);

					}
				}

				this.filter.addChild(formContainer);
			}

		});
	});
