/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define([
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/_base/array",
	"dojo/dom-class",
	"dojo/string",
	"dojo/query",
	"dojo/keys",
	"dijit/registry",
	"ddm/zui/widget/Dialog",
	"dojox/html/metrics",
	"./FilterPane",
	"dijit/form/TextBox",
	"dijit/form/Select",
	"dijit/form/Button",
	"dijit/layout/AccordionContainer"
], function(declare, lang, array, css, string, query, keys, registry, Dialog, metrics, FilterPane, TextBox){

	return declare(Dialog, {
		cssClass: 'gridxFilterDialog',
		grid: null,
		
		autofocus: true,
		postCreate: function(){
			this.inherited(arguments);
			this.i18n = this.grid.nls;
			this.set('title', this.grid.nls.filterDefDialogTitle);
			this.addChild(new FilterPane({
				grid: this.grid,
				container:this,
				onApply: function(){
					this.container.hide();
				},
				onClear: function(){
					this.container.hide();
				}
			}));
			css.add(this.domNode, 'gridxFilterDialog');
		},
	});
});
