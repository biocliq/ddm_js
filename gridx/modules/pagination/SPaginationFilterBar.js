/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define([
	'dojo/_base/declare',
	'dojo/_base/lang',        
	'dojo/topic',
	"dijit/TitlePane",
	"dijit/layout/AccordionContainer",
	'gridx/modules/Filter',
	'dojo/text!././templates/SPaginationFilterBar.html',
	'ddm/gridx/modules/filter/_FilterBar',
	'gridx/support/LinkPager',
	"ddm/zui/form/DropDownButton"
	], function(declare, lang, topic,TitlePane,AccordionContainer,
			F, template, _FilterBar,LinkPager){

return declare([ _FilterBar,LinkPager], {
	templateString: template,		
	
	postCreate: function(){
		this.inherited(arguments);
		var grid = this.grid;
		// this.disableBtnDelete(true);
		if(true == grid.permission.delete)
		{
			grid.connect(grid.select.row,'onSelected', lang.hitch(this, function(){
				this.disableBtnDelete(false);
			}));
		}
		if(grid.permission.create)
		{
			this.disableBtnAdd(false);
		}
		else{
			this.disableBtnAdd(true);
		}
		if(this.titleBar)
			this.titleBar.innerHTML = grid.title;
	},
	
	// disableBtnDelete:function(flag){

	// 	if(flag)
	// 		this.btnDelete.setDisabled(true);
	// 	else
	// 		this.btnDelete.setDisabled(false);
	// },

	disableBtnAdd:function(flag){
		if(flag)
			this.btnAdd.setDisabled(true);
		else
			this.btnAdd.setDisabled(false);
	},		
	addRecord:function(){
		var t = this;
		var page = t.grid.pageMenu;
		topic.publish("/form/new",page);
	},
	removeRecord:function(){
			var t = this;
			var selRowIdArray = t.grid.select.row.getSelected();
			var gridStore = t.grid.model.store;
		for(var i=0; i<selRowIdArray.length; i++){
			gridStore.remove(selRowIdArray[i]);
		}
	}	
});
});
