define("ddm/gridx/modules/pagination/_PaginationBar", [ 
	'dojo/_base/declare',
	'dojo/dom-class',
	"gridx/modules/Pagination",
	"gridx/modules/pagination/PaginationBar",
	
	 ], function(declare,domClass,
		PaginationBar){

	
		return declare(PaginationBar, {
		
			
			position :  'bottom',
	
			description: false,
	
			stepper: false,

			visibleSteppers: 3,
	
			sizeSwitch: false,
	
			showRange: false,
	
			showTotal: false,

            gotoButton : false,
        
        
        });

    });