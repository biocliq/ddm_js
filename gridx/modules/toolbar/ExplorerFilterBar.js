/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

/**
 * This fitlerbar is used to define the actions in the Child tables in 
 * the Explorer page. 
 */

define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/_base/array',
	'dojo/dom-style',
	'dojo/topic',
	'gridx/modules/Filter',
	'dojo/text!../../templates/ExplorerFilterBar.html',
	'ddm/gridx/modules/filter/_FilterBar',
	"ddm/util/config"

], function (declare, lang, array, domStyle, topic,
	F, template, _FilterBar, config) {

	return declare([_FilterBar], {
		templateString: template,

		addDialog: null,

		newDialog: null,

		postCreate: function () {
			this.inherited(arguments);
			var grid = this.grid;
			this.disableBtnDelete(true);

			if (true == grid.permission.delete) {
				grid.connect(grid.select.row, 'onSelected', lang.hitch(this, function () {
					this.disableBtnDelete(false);
				}));
			}
			if (true == grid.permission.create) {
				this.disableBtnAdd(false);
			}
			else {
				this.disableBtnAdd(true);
			}
			if (this.titleBar)
				this.titleBar.innerHTML = grid.title;

			if (undefined === grid.tgtPageMenu) {
				this.disableBtnAdd(true);
			}

			if (grid.hideOptions) {
				domStyle.set(this.btnOptions.domNode, "display", "none");
			}
		},

		disableBtnDelete: function (flag) {
			if (flag)
				this.btnDelete.setDisabled(true);
			else
				this.btnDelete.setDisabled(false);
		},

		disableBtnAdd: function (flag) {
			if (flag)
				this.btnAdd.setDisabled(true);
			else
				this.btnAdd.setDisabled(false);
		},

		showAddDialog: function () {
			if (this.addDialog) {
				this.addDialog.show();
				return;
			}

			this.addDialog = new ExploreGridDialog({
				grid: this.grid,
				title: "Select	Record"
			});
			this.addDialog.startup();
			this.addDialog.show();
		},
		showNewRecord: function () {
			var self = this;
			var grid = self.grid;
			var pageMenu = self.grid.tgtPageMenu;
			var parentId = grid.itemId;
			var parentMenu = grid.pageMenu;

			if (pageMenu) {
				var options = {
					parentId: parentId, parentMenu: parentMenu,
					containerId: grid.containerId, containerName: grid.containerName
				};

				var callback = function (item, status) {
					self.grid.refresh();
				}

				topic.publish("/popup/page/new/", pageMenu, options, callback);
			}

		},
		destroy: function () {
			this.inherited(arguments);
			if (this.addDialog) {
				domConstruct.destroy(this.addDialog.domNode);
				this.addDialog.destroy();
				delete this.addDialog;
			}
		},

		removeRecord: function () {
			var t = this;
			var selRowIdArray = t.grid.select.row.getSelected();
			var gridStore = t.grid.model.store;

			if (selRowIdArray.length > 0) {
				if (confirm('Do you want to delete the selected records ')) {
					for (var i = 0; i < selRowIdArray.length; i++) {
						gridStore.remove(selRowIdArray[i]);
					}

				} else {
					return;
				}
			}else{
				alert("No records selected");
			}
		}
	});
});
