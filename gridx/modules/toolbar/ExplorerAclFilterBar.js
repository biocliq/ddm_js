/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define([
        'dojo/_base/declare',
        'dojo/_base/lang',
		'dojo/_base/array',
		'dojo/topic',
	    'gridx/modules/Filter',
		'dojo/text!../../templates/ExplorerAclFilterBar.html',
		'ddm/gridx/modules/filter/_FilterBar', 
		"ddm/util/config","ddm/zui/page/SummaryDialogContainer",
		"ddm/wire/store/base/JsonUserStore"
	], function(declare, lang, array, topic,
        		F, template, _FilterBar, config, SummaryDialogContainer,JsonUserStore){
	
	return declare([_FilterBar],{
		templateString: template,
		
		addDialog : null,

		newDialog : null,

		postCreate: function(){
			this.inherited(arguments);
			var grid = this.grid;
			console.log(grid)
			this.disableBtnDelete(true);
			if(true == grid.permission.delete)
			{
				grid.connect(grid.select.row,'onSelected', lang.hitch(this, function(){
					this.disableBtnDelete(false);
				}));
			}
			if(true == grid.permission.create)
			{			
				this.disableBtnAdd(false);
			}
			else{
				this.disableBtnAdd(true);
			}
			if(this.titleBar)
				this.titleBar.innerHTML = grid.title;
		},

		disableBtnDelete:function(flag){
		
		},

		disableBtnAdd:function(flag){
		},

		showAddDialog:function(){
			
		},
		showNewRecord:function(){
						
		},
		destroy:function(){
			this.inherited(arguments);		
			if(this.addDialog){
				domConstruct.destroy(this.addDialog.domNode);
				this.addDialog.destroy();
				delete this.addDialog;
			}
		},

		removeRecord:function(){
				var t = this;
				var selRowIdArray = t.grid.select.row.getSelected();
				var gridStore = t.grid.model.store;
				if(confirm('Do you want to delete the selected records ')){

				}else{
					return;
				}
			for(var i=0; i<selRowIdArray.length; i++){
				gridStore.remove(selRowIdArray[i]);
			}
		 },

		 searchRecord:function(){
            var menuCode = this.grid.destinationPage;//'Group';
              var _this = this;	
              var callback = function(value, status){
				_this.set('value', value);
				_this.saveRecord(value);

              }
			new SummaryDialogContainer(menuCode,callback)
		},
		
		saveRecord : function(id){
			var _grid = this.grid;
			_grid.destinationId = id;
			var options = {};

			var store = new JsonUserStore(options);
			store.addUserRelation(_grid).then(
				function(result){
					_grid.refresh();
				}	
			);
		}
    });
});
