/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

/**
 * This fitlerbar is used to define the actions in the Child tables in 
 * the Explorer page. 
 */

define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/_base/array',
    'dojo/dom-style',
    'dojo/topic',
    'gridx/modules/Filter',
    'dojo/text!../../templates/ReadOnlyFilterBar.html',
    'ddm/gridx/modules/filter/_FilterBar', 
    "ddm/util/config"

    ], function(declare, lang, array, domStyle,topic,
            F, template, _FilterBar, config){

return declare([_FilterBar],{
    templateString: template,
    

    postCreate: function(){
        this.inherited(arguments);
        var grid = this.grid;			
       
        if(this.titleBar)
            this.titleBar.innerHTML = grid.title;
        
    }	
});
});
