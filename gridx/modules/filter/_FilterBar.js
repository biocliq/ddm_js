/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/_base/array',    
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/keys',
    'dijit/_AttachMixin',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dijit/form/TextBox',
    'dijit/form/Button',
    'ddm/zui/form/Icon',
    'dijit/form/ComboButton',
    'dijit/Menu',
    'dijit/MenuItem',
    'gridx/modules/Filter',
    'dojox/html/entities',
    "ddm/zui/page/base/Base",
    'ddm/gridx/widget/FilterDialog'
    ], function(declare, lang, array, domClass, domConstruct, keys, _AttachMixin,
            _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
            TextBox, Button, Icon,ComboButton, Menu, MenuItem,
            F,entities,WidgetBase,FilterDialog){

return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,_AttachMixin],{
    templateString: null,

    dialog : null,

    grid: null,

    textBoxClass: 'dijit.form.TextBox',

    buttonClass: 'dijit.form.Button',

    comboButtonClass: 'dijit.form.ComboButton',

    menuClass: 'dijit.Menu',

    menuItemClass: 'dijit.MenuItem',

    autoApply: true,

    delay: 700,

    constructor: function(args){
        var t = this;
        lang.mixin(t, args.grid.nls);
        t._hasFilterBar = args.grid.filterBar ? 'gridxQuickFilterHasFilterBar' : 'gridxQuickFilterNoFilterBar';
        if(t._hasFilterBar){
            t.connect(args.grid.filterBar, 'clearFilter', function(){
                if(t.textBox){
                    t.textBox.set('value', '');
                }
            });
        }
        t.connect(args.grid.model, 'setStore', function(){
            t.textBox.set('value', '');
            domClass.remove(t.domNode, 'gridxQuickFilterActive');
        });
        args.grid.cfilterBar = this;
    },

    destroy:function(){        
        if(this.dialog){
            domConstruct.destroy(this.dialog.domNode);
            this.dialog.destroy();
            delete this.dialog;
        }
        this.inherited(arguments);
    },
    postCreate: function(){
        this.inherited(arguments);
        this.dialog = new FilterDialog({grid: this.grid});
        this.dialog.hide();
        this.connect(this.textBox, 'onInput', '_onInput');
    },
    //Private--------------------------------------------------------------------
    _onInput: function(evt){
        var t = this,
        dn = t.domNode,
        tb = t.textBox,
        key = evt.keyCode;
        setTimeout(function(){
            domClass.toggle(dn, 'gridxQuickFilterActive', tb.get('value'));
        }, 0);
        if(t.autoApply && key != keys.TAB){
            clearTimeout(t._handle);
            t._handle = setTimeout(function(){
                t._filter();
            }, key == keys.ENTER ? 0 : t.delay);
        }
    },

    _onKey: function(evt){
        if(evt.keyCode == keys.ENTER){
            this.grid.focus.stopEvent(evt);
            this._clear();
            this.textBox.focus();
        }
    },

    _clear: function(){
        this.textBox.set('value', '');
        domClass.remove(this.domNode, 'gridxQuickFilterActive');
        this._filter();
        // this.clearFilter();
    },

    clearFilter:function(){
        this.grid.filter.clearFilter();
    },

    refresh:function(){
        this.grid.refresh();
    },
    
    _filter: function(){
        var t = this;
        g = t.grid;
        if(t.textBox)
            v = t.textBox.get('value');
        else
            return;
        // cols = array.filter(g.columns(), function(col){
        //     return col.filterable !== false;
        // }),
        cs = t.grid.filter.arg('caseSensitive');
        clearTimeout(t._handle);
        if(g.filterBar){	
            if(v === ''){
                g.filterBar.clearFilter(true);
                this.clearFilter();
            }else{
                v = v + '*';
                g.filterBar.applyFilter({
                    conditions: [{
                        condition: 'contain',
                        value: v
                    }]
                });
            }
        }else{
            if(v === ''){
                g.filter.clearFilter(true);
                this.clearFilter();
            }else{
                v = v +'*';
                var cols = ['quickFilter'];
                var _this = this;
                var sd = F.or.apply(0, array.map(cols, function(col){
                    return F.contain(F.column(col, null, null, false), F.value(v, null, null, false));
                }));

                g.filter.setFilter(sd);
            }
        }
    },


    column: function(/* String|Number */name, /* String? */type, /* Function? */converter, caseSensitive){
        type = String(type || 'string').toLowerCase();

        return this.wrap(function(){
            return valueConvert( name, type, converter, caseSensitive);
        }, type, name, {isCol: true});
    },

    wrap:function(checker, op, operands, options){
        if(lang.isArray(operands)){
            operands = array.map(operands, function(operand){
                return operand.expr;
            });
        }
        return lang.mixin(checker, {
            expr: lang.mixin({
                op: op,
                data: operands
            }, options || {})
        });
    },

    _showFilterBar: function(){
        this.dialog.show();
    }
});
});
