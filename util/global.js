/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

/**
 * Global functions outside of dojo environment will be defined here. 
 */

function downloadFile(path){
    window.open("v2/download/" +  path, '_blank');
}
