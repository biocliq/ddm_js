/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

/**
 * Various utility methods to be used across the application.
 */
define("ddm/util/util", 
[ "dojo/_base/declare", "dojo/date/locale"], 
		function(declare, locale) {

	var util = {
		/**
		 * date and time format utilities.
		 * Primarily used by datetimetextbox  and 
		 * ddm/fluwiz/grid/ddmGrid.
		 * 		 
		 */
		datePattern : "dd-MMM-yyyy",
        timePattern : "hh:mm:ss",
		formatDateTime:function(data){
            return locale.format(data, 
				{timePattern:this.timePattern, 
				datePattern:this.datePattern});
		},
		formatDate:function(data){
			var dob;
			if(data instanceof Date)
				dob = data;
			else	
				dob = new Date(data);
            return locale.format(dob, 
				{datePattern:this.datePattern,selector: "date",});
		},
		calcAge : function( dateNumber, short ){
			var now = new Date();
			// var today = new Date(now.getYear(),now.getMonth(),now.getDate());
	
			var yearNow = now.getYear();
			var monthNow = now.getMonth();
			var dateNow = now.getDate();
			var dob;
			if(dateNumber instanceof Date)
				dob = dateNumber;
			else
				dob = new Date(dateNumber);
	
			var yearDob = dob.getYear();
			var monthDob = dob.getMonth();
			var dateDob = dob.getDate();
			var age = {};
			var result = "";
			var yearString = "";
			var monthString = "";
			var dayString = "";
	
	
			yearAge = yearNow - yearDob;
	
			if (monthNow >= monthDob)
				var monthAge = monthNow - monthDob;
			else {
				yearAge--;
				var monthAge = 12 + monthNow -monthDob;
			}
	
			if (dateNow >= dateDob)
				var dateAge = dateNow - dateDob;
			else {
				monthAge--;
				var dateAge = 31 + dateNow - dateDob;
	
				if (monthAge < 0) {
				monthAge = 11;
				yearAge--;
				}
			}
	
			age = {
				years: yearAge,
				months: monthAge,
				days: dateAge
				};
			
			yearString = short ? "y" : age.years > 1 ? "years" : "year";
			monthString = short ? "m" : age.months > 1 ? "months" : "month";
			dayString = short ? "d" : age.days > 1? "days" : "day";

			if(age.years > 0){
				result = age.years + yearString;
				if(age.months > 0 && age.years < 13){
					result += ' ' + age.months + monthString;
				}
				// else if(age.days > 0){
				// 	result += ' ' + age.days + dayString;
				// }
			}else{
				if(age.months > 0){
					result = age.months + monthString;
				}
				if(age.days > 0){
					result += ' ' + age.days + dayString;
				}
			}
			if ('' == result)
				result = '0 day';
			return result;
	
			// if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
			// 	ageString = age.years + yearString + " " + age.months + monthString;
			// else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
			// 	ageString = age.days + dayString ;
			// else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
			// 	ageString = age.years + yearString ;
			// else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
			// 	ageString = age.years + yearString + " " + age.months + monthString;
			// else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
			// 	ageString = age.months + monthString + " " + age.days + dayString;
			// else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
			// 	ageString = age.years + yearString 
			// else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
			// 	ageString = age.months + monthString;
			// else ageString = "Oops! Could not calculate age!";
			// return ageString;
		},
		forEach:function(fields, callback, thisObject){
			var field;
			if(thisObject){
				for(idx in fields){
					field = fields[idx];
					callback.call(thisObject, field, idx, fields);
				}
			}else
				for(idx in fields){
					field = fields[idx];
					callback(field, idx);
				}
		}, 
		map:function(fields, callback){
			var out = new Array();
			var field;
			var result;

			for(idx in fields){
				field = fields[idx];
				result = callback(field, idx);
				if(result)
					out.push(result);
			}
			return out;
		},
		every:function(fields, callback){
			var field;
			var result;
			for(idx in fields){
				field = fields[idx];
				result = callback(field, idx);
				if(!result)
					return false;
			}
			return true;
		},
		some:function(fields, callback){
			var field;
			var result;
			for(idx in fields){
				field = fields[idx];
				result = callback(field, idx);
				if(result)
					return true;
			}
			return false;
		},
		filterFirst:function(fields, callback){
			var field;
			var result;
			for(idx in fields){
				field = fields[idx];
				result = callback(field, idx);
				if(true == result)
					return field;
			}
		}
	};
	return util;
});
