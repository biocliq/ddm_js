/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

/**
 * Class to define the URLs and configurations to be used 
 * across the applications
 */
define("ddm/util/DateParser", ["dojo/_base/lang", 
    "momentjs/moment", "ddm/util/config"], 
  function(lang, momentjs, config){
	
	/**
	 * Configuration object to be exposed to the Global.
	 */
	var parser = {
        isValidDate(value){
            return value && value instanceof Date && !isNaN(value);
        },
		parseServerDate(value){
            var	datePattern = config.getDateFormat();
            return momentjs(value, datePattern).toDate();
		},
		parseServerTime(value){
			return value;
        },
        parseServerTimeStamp(value){
            var	timestampPattern = config.getTimestampFormat();
            return momentjs(value, timestampPattern).toDate();
        },
		formatServerDate(value){
            var	datePattern = config.getDateFormat();
            return momentjs(value).format(datePattern);
		},
		formatServerTime(value){
			return value;
        },
        formatServerTimeStamp(value){
            var	timestampPattern = config.getTimestampFormat();
            return momentjs(value).format(timestampPattern);
        }
	}
	return parser;
	
});