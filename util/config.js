/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

/**
 * Class to define the URLs and configurations to be used 
 * across the applications
 */
define("ddm/util/config", ["dojo/_base/lang"], 
  function(lang){
	
	/**
	 * Global variable for new Form Ids
	 */
	_newFormCounter = 10000;
	
	/**
	 * Common URL definitions used by XHR and store requests
	 */
	var url = {
		base :'v2/',
		mainPage : "fluwiz.html",
		//defPage:"/page/patient/summary",
		verifyCodePage : "verifycode.html",
		resetPasswordPage:"resetpassword.html",
		loginPage:"index.html",
		login : 'auth/{customer}/login',
		gsignup: 'auth/{customer}/gsignup',
		forgotPassword : 'auth/{customer}/forgot_password',
		resetPassword : 'auth/{customer}/reset_password',
		verifyPasswordCode : 'auth/{customer}/verifycode',
		googlelogin : 'auth/{customer}/glogin',
		logout : 'auth/logout', 
		userInfo : 'user/info',
		userFacilities : 'user/facilities',
		passwdChange : 'user/change_password',
		adminResetPassword:'user/reset_password',
		loginPage:'index.html',
		fieldLookup : 'v2/page/{page}/field/{fieldId}',
		uniqueLookup : 'v2/page/{page}/uniqueField/{fieldId}',
		menu : 'v2/menu',		
		basePage:'v2/page/',
		summaryGridLayout: "v2/page/{pageMenu}/summary/container/{container}/table",
		summaryGridData: "v2/page/{pageMenu}/summary/container/{container}/table/data",
		summaryReport: "v2/report/{pageMenu}/summary/container/{container}/{format}",
		explorerReport: "v2/report/{pageMenu}/explorer/{ciId}/container/{container}/{format}",
		chartReport: "v2/report/{format}/{pageMenu}/summary/{chartCode}",
		expChartReport: "v2/report/{format}/{pageMenu}/explorer/{ciId}/{chartCode}",
		dashChartReport: "v2/report/{format}/{pageMenu}/dashboard/{chartCode}",
		explorerGridLayout: "v2/page/{pageMenu}/explorer/{ciId}/container/{container}/table",
		explorerGridData: "v2/page/{pageMenu}/explorer/{ciId}/container/{container}/table/data",
		relationGridData: "v2/page/{pageMenu}/relation/{ciId}/container/{container}",
		customAction : "v2/action/{action}",
		chartData:"v2/chart/{pageMenu}/summary/{chartCode}",
		expChartData:"v2/chart/{pageMenu}/explorer/{ciId}/{chartCode}",
		// chartData:"v2/page/{pageMenu}/chart/{chartCode}",
		// expChartData:"v2/page/{pageMenu}/{ciId}/chart/{chartCode}",
		fileUpload : 'v2/tusupload/{pageMenu}/{ciid}/' ,
		fileDownload : 'v2/page/{pageMenu}/file/{ciid}/download/{fileid}' ,
		fileDelete : 'v2/page/{pageMenu}/file/{ciid}/delete/{fileid}' ,	
		loginURL(customer){
			return lang.replace(this.login, {customer : customer});
		},
		gSignUpURL(customer)
		{
			return lang.replace(this.gsignup, {customer : customer});
		},
		forgotPasswordURL(customer){
			return lang.replace(this.forgotPassword, {customer : customer});
		},
		verifyPasswordCodeURL(customer){
			return lang.replace(this.verifyPasswordCode, {customer : customer});
		},
		resetPasswordURL(customer)
		{
			return lang.replace(this.resetPassword, {customer : customer});
		},
		getActionURL(action){
			return lang.replace(this.customAction, {action : action});
		},
		googleloginURL(customer){
			return lang.replace(this.googlelogin, {customer : customer});
		},
		fileUploadURL(pagemenu,ciid)
		{
			return lang.replace(this.fileUpload, {pageMenu : pagemenu,ciid : ciid});
		},
		fileDownloadURL(pagemenu,ciid,fileid)
		{
			return lang.replace(this.fileDownload, {pageMenu : pagemenu,ciid : ciid,fileid : fileid});
		},
		fileDeleteURL(pagemenu,ciid,fileid)
		{
			return lang.replace(this.filedelete, {pageMenu : pagemenu,ciid : ciid,fileid : fileid});
		},
		getGridDataURL(pagemenu, container, ciId){
			var options = {pageMenu :pagemenu, container : container};
			if(ciId){
				options.ciId = ciId;
				return this._getURL(this.explorerGridData, options);
			}
			return this._getURL(this.summaryGridData, options);
		},
		getRelationDataURL(pagemenu, container, ciId){
			var options = {pageMenu :pagemenu, container : container, ciId : ciId};
			return this._getURL(this.relationGridData, options);			
		},
		getGridLayoutURL(pagemenu, container, ciId){
			var options = {pageMenu :pagemenu, container : container};
			if(ciId){
				options.ciId = ciId;
				return this._getURL(this.explorerGridLayout, options);
			}
			return this._getURL(this.summaryGridLayout, options);
		},
		getReportURL(pagemenu, container, ciId, format){
			var options = {pageMenu :pagemenu, container : container, format:format, ciId:ciId};
			if(ciId){
				options.ciId = ciId;
				return this._getURL(this.explorerReport, options);
			}
			return this._getURL(this.summaryReport, options);
		},
		getChartReportURL(pagemenu, chartCode, ciId, format){			
			var options = {pageMenu :pagemenu, chartCode : chartCode, format:format};
			if(ciId){
				options.ciId = ciId;
				return this._getURL(this.expChartReport, options);
			}
			return this._getURL(this.chartReport, options);
		},
		getDashChartReportURL(pagemenu, type, chartCode, format){
			var options = {pageMenu :pagemenu, chartCode : chartCode, format:format, type:type};			
			return this._getURL(this.dashChartReport, options);
		},
		_getURL(template, options){
			return lang.replace(template, options);
		}
	};
		
	/**
	 * Standard forms and widgets used
	 */
	var pageDef = {
		simplePage:"ddm/zui/page/base/page/Page",
		summary : "ddm/zui/page/base/summary/SummaryPage", 
		explorer : "ddm/zui/page/base/explorer/ExplorerPage",
		expTabWidget : "ddm/zui/page/base/explorer/ExplorerTab",
		expDashboardTab : "ddm/zui/page/base/explorer/DashboardTab",
		expSingleTabWidget : "ddm/zui/page/base/explorer/SingleTab",
		expFormWidget : "ddm/zui/page/base/explorer/FormContainer",
		expFormsWidget : "ddm/zui/page/base/explorer/FieldSetContainer",
		expTableWidget : "ddm/zui/page/base/explorer/TGridContainer",
		expChartContainer:"ddm/fluwiz/chart/ChartContainer",
		expFileContainer:"ddm/zui/page/base/explorer/FileContainer"
	}

	/**
	 * Configuration object to be exposed to the Global.
	 */
	var config = {
		url : url,
		page:pageDef,
		customer : 'default',
		replace : function(_string, _data){
			return lang.replace(_string, _data);
		},
		getNewFormId(){
			return 'a' + _newFormCounter++;
		}, 
		getCurrentRoute(){
			var url = window.location.href;
			url = url.substring(url.indexOf('#')+1);
			return url;
		}, 
		getDateFormat(){
			return "YYYY-MM-DD";
		},
		getTimestampFormat(){
			return "YYYY-MM-DDTHH:mm:ssZZ";
		}
	}
	return config;
	
});
