define("ddm/fluwiz/grid/BreadCrumbGrid", [ "dojo/_base/declare",
	"dojo/_base/array", 
	"dojo/_base/lang", 
	"dojo/on",
	"dojo/dom-class",
	"dojo/date/locale", 
	'dijit/_WidgetsInTemplateMixin',
	'dijit/_TemplatedMixin',
	'./ddmGrid', "dojo/text!./templates/BreadCrumbGrid.html",
	"dijit/layout/ContentPane"], 
	function(
		declare, array, lang, on,domClass, locale,_TemplatedMixin,_WidgetsInTemplateMixin, Grid, template, ContentPane) {

	var base = [_TemplatedMixin,_WidgetsInTemplateMixin, Grid];

	return declare("ddm.fluwiz.grid.BreadCrumbGrid", base, {		
		baseClass:"bread",
		templateString: template,
		pageSize:15,
		autoHeight:true,
		selectRowTriggerOnCell: true,
		paginationInitialPageSize:15,
		paginationBarShowRange : false,
		paginationBarSizeSeparator : ' | ',		
		filterServerMode : true,
			
		// postCreate:function(){
		// 	this.inherited(arguments);				
			
		// 	domClass.add(this.headerNode, this.baseClass + '-header');
		// 	domClass.add(this.mainNode, this.baseClass + '-main');
		// 	domClass.add(this.bodyNode, this.baseClass + '-body');
		// 	domClass.add(this.emptyNode, this.baseClass + '-empty');
		// 	domClass.add(this.footerNode, this.baseClass + '-footer');
		// },
	
		formatField:function(field, column){
			column.decorator= function(value){
				console.log(arguments);
				return value;
			}
		}
	});

});