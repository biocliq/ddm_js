/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/grid/ddmGrid", ["dojo/_base/declare",
	"dojo/_base/array", "dojo/_base/lang", "dojo/topic",
	"dojo/on", "dojo/dom-class", "dojo/date/locale", "dojo/store/Memory",
	"ddm/util/DateParser",
	'gridx/Grid', 'gridx/modules/SingleSort',
	"gridx/modules/HLayout", "gridx/modules/HScroller",
	'gridx/modules/ColumnResizer', 'gridx/modules/ColumnWidth', "gridx/modules/select/Row",
	"gridx/modules/RowHeader", "gridx/modules/IndirectSelect",
	"gridx/modules/extendedSelect/Row", "gridx/modules/ColumnLock",
	"gridx/modules/CellWidget", 'gridx/modules/Filter',
	"ddm/gridx/modules/toolbar/ExplorerFilterBar",
	"gridx/modules/Edit", "gridx/modules/GroupHeader",
	"gridx/modules/Bar", "gridx/modules/Pagination",
	"gridx/modules/pagination/PaginationBar", 'gridx/core/model/cache/Async'],
	function (declare, array, lang, topic,
		on, domClass, locale, Memory, DateParser,
		Grid, Sort,
		HLayout, HScroller, ColumnResizer, ColumnWidth, SelectRow, RowHeader, IndirectSelect, ExSelectRow,
		ColumnLock, CellWidget, Filter,
		FilterBar, Edit, GroupHeader, Bar, Pagination, PaginationBar, AsyncCache) {
		var base = [Grid];
		return declare("ddm.fluwiz.grid.ddmGrid", base, {
			cacheClass: AsyncCache,
			pageSize: 101,
			autoHeight: true,
			filterBarClass: "ddm/gridx/modules/toolbar/SummaryFilterBar",
			selectRowTriggerOnCell: false,
			paginationInitialPageSize: 25,
			paginationBarSizes: [25, 50, 100],
			paginationBarShowRange: true,
			paginationBarSizeSeparator: ' | ',
			paginationBarVisibleSteppers: 10,
			disableBtnDelete: false,
			filterServerMode: true,
			//	permission : {create : false,update : false,delete :false},
			/**
			 * Stub Methods for event handling
			 * 
			 * evtRowSelect:function(row)
			 * 
			 * evtCellClick:function(event)
			 * 
			 * evtCellDblClick:function(event)
			 * 
			 */
			filterSetupQuery: function (expr) {
				if (undefined == expr || undefined == expr.data)
					return;
				var data = expr.data
				var queryString = {};
				array.forEach(data, function (_expr) {
					queryString[_expr.data[0].data] = _expr.data[1].data;
				});
				return queryString;
			},
			postMixInProperties: function () {
				this.processFields();
				this.moduleOptions = this.grid_options;				
				this.gridModulesCustomize();
			},
			postCreate: function () {
				this.inherited(arguments);
				if (this.baseClass) {
					domClass.add(this.headerNode, this.baseClass + '-header');
					domClass.add(this.mainNode, this.baseClass + '-main');
					domClass.add(this.bodyNode, this.baseClass + '-body');
					domClass.add(this.emptyNode, this.baseClass + '-empty');
					domClass.add(this.footerNode, this.baseClass + '-footer');
				}
			},
			startup: function () {
				this.inherited(arguments);
				this._attachIOEvents();
			},
			_attachIOEvents: function () {
				var self = this;
				if (this.evtRowSelect) {
					try {
						self.connect(self.select.row, 'onSelected', lang.hitch(this, this.evtRowSelect));
					} catch (err) {
						console.error(err);
					}
				}
				if (this.evtCellClick) {
					this.own(on(this.domNode, 'rowClick', lang.hitch(this, this.evtCellClick)));
				}
				if (this.evtRowKeyUp) {
					this.own(on(this.domNode, 'rowKeyUp', lang.hitch(this, this.evtRowKeyUp)));
				}

				if (this.evtCellDblClick) {
					this.own(on(this.domNode, 'rowDblClick', lang.hitch(this, this.evtCellDblClick)));
				}
			},
			refresh: function () {
				this.filter.refresh(false);
			},
			processFields: function () {				
				if (this.fields) {
					var columns = [];
					array.forEach(this.fields, lang.hitch(this, function (field) {
						if (field.tabDisplay) {
							var column = { id: field.name, field: field.name, name: field.label };
							var cal_size = ((field.label.length + 1) * 8);
							if (field.width) {
								var width = field.width;
								var len = width.length;
								var givenSize;
								if (len > 1) {
									var suffix = width.substring(len - 2);
									if (suffix == 'px') {
										givenSize = width.substring(0, len - 2);
										column.width = ((givenSize < cal_size) ? cal_size : givenSize) + 'px';
									} else if ('%' == suffix.substring(1))
										column.width = width;
									else
										column.width = ((givenSize < cal_size) ? cal_size : width) + 'px';
								}
								else
									column.width = width + 'px';
							} else {
								if ('TIMESTAMP' == field.displayFormat || 'DATETIME' == field.displayFormat) {
									column.width = '135px';
								}
							}
							this.formatField(field, column);
							column.editable = field.editable;
							column.sortable = field.sortable;
							if (field.displayPattern) {
								try {
									column.displayPattern = JSON.parse(field.displayPattern, true);
								} catch (err) {

								}
							}
							columns.push(column);
						}
					}));
					this.setStructure(columns);
				}
			},
			setStructure: function (str) {
				this.structure = str;
			},
			gridModulesCustomize: function () {
				this.modules = [Sort, HLayout, HScroller, ColumnResizer, SelectRow, CellWidget, ColumnWidth, ColumnLock];
				//this.modules = [ Sort,SelectRow, CellWidget, ColumnWidth];				
				if (null != this.moduleOptions && undefined != this.moduleOptions) {
					if ("normal" == this.moduleOptions.pagination) {
						this.modules.push(Pagination);
						this.modules.push(PaginationBar);
					}
					if ("quickfilter" == this.moduleOptions.filterConfig || (!this.moduleOptions.filterConfig)) {
						this.modules.push(Filter, Bar);
						this.barTop = [{ pluginClass: this.filterBarClass, style: 'text-align: right;' }];
					}
				} else {
					this.modules.push(Filter, Bar);
					this.barTop = [{ pluginClass: this.filterBarClass, style: 'text-align: right;' }];
				}

				if(this.headerGroups){
					this.modules.push(GroupHeader);
				}
			},
			formatTIMESTAMP: function (field, column) {
				column.width = (field.width) ? field.width : "160px";

				column.decorator = function (value) {
					try {
						if (value) {
							var dt = new Date(value);
							var dateString = locale.format(dt, {
								selector: "date",
								datePattern: "dd-MMM-yyyy HH:mm:ss"
							});
							return dateString;
						}
					} catch (e) {
						console.error('error decorating date: ' + e.toString());
					}
				}
			},
			formatField: function (field, column) {
				var formatMethod = this['format' + field.displayFormat];
				if (undefined != formatMethod && typeof formatMethod == "function") {
					return formatMethod.call(this, field, column);
				}
			},
			formatDATETIME: function (field, column) {
				this.formatTIMESTAMP(field, column);
			},
			formatDATE: function (field, column) {
				var dateFormat = { selector: 'date', datePattern: 'dd-MMM-yyyy', locale: 'en-us' };
				if (field.displayPattern)
					dateFormat.datePattern = field.displayPattern;

				column.width = (field.width) ? field.width : "120px";
				column.decorator = function (value) {
					try {
						if (value) {
							var val = DateParser.parseServerDate(value);
							var dateString = locale.format(val, dateFormat);
							return dateString;
						}
					} catch (e) {
						console.error('error decorating date: ' + e.toString());
					}
				}
			},
			formatYEAR: function (field, column) {
				this._formatDateByPattern(field, column, 'yyyy');
			},
			formatMONTH: function (field, column) {
				this._formatDateByPattern(field, column, 'MMM-yyyy');
			},
			formatFYQ: function (field, column) {
				var yearFormat = { selector: 'date', datePattern: 'yy', locale: 'en-us' };
				var yearFormat = { selector: 'date', datePattern: 'yy', locale: 'en-us' };

				column.width = (field.width) ? field.width : "80px";
				column.decorator = function (value) {
					try {
						if (value) {
							var val = DateParser.parseServerDate(value);
							console.log(val.getMonth() +1 );
							var quarter = (Math.floor((val.getMonth()) / 3) + 1);
							var year = locale.format(val, yearFormat);
							console.log(year);
							var result =  "Q" + quarter + " FY" + year;
							return result;
						}
					} catch (e) {
						console.error('error decorating date: ' + e.toString());
					}
				}
			},
			_formatDateByPattern: function (field, column, datePattern) {
				var dateFormat = { selector: 'date', datePattern: datePattern, locale: 'en-us' };
				if (field.displayPattern)
					dateFormat.datePattern = field.displayPattern;

				column.width = (field.width) ? field.width : "120px";
				column.decorator = function (value) {
					try {
						if (value) {
							var val = DateParser.parseServerDate(value);
							var dateString = locale.format(val, dateFormat);
							return dateString;
						}
					} catch (e) {
						console.error('error decorating date: ' + e.toString());
					}
				}
			},
			formatBIT: function (field, column) {
				var pattern = field.displayPattern || [{"1" : "Yes"}, {"0" : "No"}];				
				var data = {};
				if(Array.isArray(pattern)){						
					array.forEach(pattern, function(item){
						data = {...data, ...item};
					});	
				}

				column.decorator = function (value) {					
					if (value){
						if(data[value])
							return data[value];
					}
					
					if(value)
						return data[1];
					else
						return data[0];
				}
			},
			formatBIGINT: function (field, column) {
				column.width = (field.width) ? field.width : "80px";
				column.style = "text-align: right;" + column.style;
			},
			formatINT: function (field, column) {
				column.width = (field.width) ? field.width : "80px";
				column.style = "text-align: right;" + column.style;
			},
			formatDOUBLE: function (field, column) {
				column.width = (field.width) ? field.width : "80px";
				column.style = "text-align: right;" + column.style;
			},
			formatDECIMAL: function (field, column) {
				column.width = (field.width) ? field.width : "80px";
				column.style = "text-align: right;" + column.style;
			}
		});
	});