/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/DualAxisLineChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs.DualAxisLineChart", base, {
       	getChartType:function(){
            return 'line';   
        },
        initChartOptions:function(){
            this.chartOptions =  {scales: {yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                },scaleLabel:{
                                    display:true, labelString: "sdfsdf"
                                }}]
                    }, xAxes:[{
                        scaleLabel:{
                            display:true, labelString: "sdfsdf"
                        }, ticks: {
                            display: true //this will remove only the label
                        }
                    }]};
        },
        getData:function(){
            var data = {
                labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                datasets: [
                    {
                        label: "Prime and Fibonacci",
                        borderColor: "rgba(151,249,190,0.7)",
                        backgroundColor: "rgba(151,249,190,0.2)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",                       
                        data: [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
                    },
                    {
                        label: "My Second dataset",
                        borderColor: "rgba(252,147,65,0.7)",
                        backgroundColor: "rgba(252,147,65,0.2)",
                        pointColor: "rgba(173,173,173,1)",
                        pointStrokeColor: "#fff",
                        data: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
                    }
                ]
            };

            return data;
        }
	});

});