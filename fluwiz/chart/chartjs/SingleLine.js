/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/SingleLine", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs.SingleLine", base, {
       	getChartType:function(){
            return 'line';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{
                                // boxWidth:'10',
                                ticks: {
                                    beginAtZero:true
                                },scaleLabel:{
                                    display:false, labelString: "na"
                                }}]
                        }, xAxes:[{
                            scaleLabel:{
                            display:false, labelString: "na"
                            }, ticks: { 
                                display: true //this will remove only the label
                                ,callback: function(value, index, values) {
                                    console.log(value);
                                    if (Math.floor(value) === value) {
                                        return value;
                                    }
                                }
                                }
                            }],
                legend:{
                    display: false,
                    labels: {
                        boxWidth:10,
                    },
                },
            };
        },
        convertRawData:function(rawData){
            console.log(rawData[0])
            var xName = 'name';
            var yName = 'count';            
            var data = [];
            var dataMin = [];
            var dataMax = [];
            var dataAvg = [];
            var label =  [];
            if(rawData)
                rawData.forEach((dt) => {
                    data.push(dt.minVal,dt.maxVal,dt.avgval);
                    label.push(dt.Date);
                    console.log(rawData);
                    dataMin.push(dt.minVal);
                    dataMax.push(dt.maxVal);
                    dataAvg.push(dt.avgval)
                });
            var datasets=[]
            var result = {
                labels:label,
                datasets: [
                    {
                        //label: 'MinimumData',
                        fill:false,
                        backgroundColor: "rgba(0, 153, 247,.5)",
                        borderColor: "rgba(0, 153, 247,1)",
                        pointBackgroundColor: "rgba(0, 153, 247,1)",
                        pointBorderColor: "rgba(0, 153, 247,1)",
                        pointHoverBackgroundColor: "rgba(0, 153, 247,1)",
                        pointHoverBorderColor: "rgba(0, 153, 247,1)",
                        data:data,
                        pointStyle: 'rectRounded',
                    },
                ]
            };console.log(datasets);
            return result;
        }
	});
});