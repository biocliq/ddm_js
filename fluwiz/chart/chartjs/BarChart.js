/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/BarChart", [
    "dojo/_base/declare",
    "dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase",
    "ddm/fluwiz/chart/base/_dataParser"],
    function (declare, array, _chartjsbase, _dataParser) {

        var base = [_chartjsbase, _dataParser];

        return declare("ddm.fluwiz.chart.chartjs.BarChart", base, {
            getChartType: function () {
                return 'bar';
            },
            initChartOptions: function () {
                this.chartOptions = {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (val, index) {
                                    return val % 1 === 0 ? val : '';
                                }
                            },
                            scaleLabel: {
                                display: false, labelString: "na"
                            }
                        }]
                        , xAxes: [{
                            calculateBaseWidth: function () {
                                // return (this.calculateX(1) - this.calculateX(0)) - (2*options.barValueSpacing);
                                var width = (this.calculateX(1) - this.calculateX(0)) - (2 * options.barValueSpacing);
                                return width > 50 ? 50 : width;
                            },
                            scaleLabel: {
                                display: false, labelString: "na"
                            }, ticks: {
                                display: true,
                                callback: function (value) {
                                    if (typeof value === 'string')
                                        return value.substr(0, 25);
                                }
                            },
                        }]
                    },
                    legend: {
                        display: false,
                        position: "left",
                        labels: {
                            boxWidth: 10,
                        }
                    },
                    tooltips: {
                        enabled: true
                    },
                    hover: {
                        animationDuration: 1
                    },
                    animation: {
                        easing: "easeInSine",
                        onComplete: function () {
                            var chartInstance = this.chart;
                            ctx = chartInstance.ctx;
                            ctx.textAlign = 'center';
                            ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                if (!meta.hidden) {
                                    meta.data.forEach(function (bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                    });
                                }
                            });
                        }
                    }
                };
                declare.safeMixin(this.chartOptions, this.options);
            },
            convertRawData: function (rawData) {
                var xName = this.options.xName || 'date';
                var yName = this.options.yName || ['total', 'used'];
                var data = [];
                for (var i = 0; i < yName.length; i++) {
                    data[i] = new Array();
                }
                var legend = this.options.yLabel || yName;

                var label = [];
                var bgColor = [];
                if (rawData) {
                    rawData.forEach((dt) => {
                        label.push(dt[xName]);
                        for (var i = 0; i < yName.length; i++) {
                            data[i].push(dt[yName[i]]);
                            bgColor.push(this.getRandomColor(dt[xName] + i));
                        }
                    });
                }
                if (this.options.colorArray) {
                    bgColor = this.getColorArray(this.options.colorArray, 0.6, bgColor.length);
                } else
                    bgColor = this.options.backgroundColor || bgColor;

                var dataSets = [];
                var finalLabels = [];
                for (var i = 0; i < data.length; i++) {
                    if (1 != data[i].length || undefined != data[i][0]) {
                        dataSets.push({
                            fillColor: "rgba(151,249,190,0.5)",
                            strokeColor: "rgba(255,255,255,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: data[i],
                            borderWidth: 2,
                            maxBarThickness: 60,
                            borderRadius: 10,
                            backgroundColor: bgColor[i],
                            borderSkipped: false,
                            label: legend[i]
                        });
                        finalLabels.push(label[i]);
                    }
                }

                var result = {
                    labels: label,
                    datasets: dataSets
                };
                return result;
            }
        });
    });