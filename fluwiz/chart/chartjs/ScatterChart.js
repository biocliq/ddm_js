/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/ScatterChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs.ScatterChart", base, {
        //xAxisLabel:null,
        // yAxisLabel:null,
       	getChartType:function(){
            return 'scatter';   
        },
        
        getChartOptions:function(){
            return{
                title: {
                    display: true,
                    text: 'Chart.js Scatter Chart'
                },
                showLines: false
            }
    }            
        
	});

});