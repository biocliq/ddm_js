/** 
* (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
* 
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential 
*
* Author k.raja@biocliq.com
*/


define("ddm/fluwiz/chart/chartjs/RadarStatic", [
   "dojo/_base/declare",
   "dojo/_base/array",
   "ddm/fluwiz/chart/base/_chartjsbase"], 
   function(declare,array, ContentPane) {

   var base = [ContentPane];

   return declare("ddm.fluwiz.chart.chartjs.RadarStatic", base, {
       rawData:[],
          getChartType:function(){
           return 'radar';   
       },
       initChartOptions:function(){
           this.chartOptions =  {
               scales: {yAxes: [{
                               // boxWidth:'10',
                               ticks: {
                                   beginAtZero:true
                               },
                               scaleLabel:{
                                   display:false, labelString: "na"
                               }}]
                       }, xAxes:[{
                           scaleLabel:{
                           display:false, labelString: "na"
                           }, ticks: { 
                               display: true //this will remove only the label
                               ,callback: function(value, index, values) {
                                   console.log(value);
                                   if (Math.floor(value) === value) {
                                       return value;
                                   }
                               }
                               },
                           }],
                           legend:{
                            display: true,
                            fontColor: "#fff",
                            position: 'left',
                            labels: {
                                fontColor: 'rgb(255, 99, 132)',
                                usePointStyle: false,
                                
                                boxWidth:15,
                            },
                        },
           };
       },
       convertRawData:function(rawData){
           var xName = 'name';
           var yName = 'count';            
           var data = [];
           var label =  [];
           console.log('convert');

           rawData.forEach((dt) => {
               label.push(dt.name);
               data.push(dt.count);
           });

           var result = {
               labels:["f0-10","f10-20","f20-30","f30-40","f40-50","f50-60","f60-70","f70-80"],
               datasets: [
                   {
                        label: "Asthma",
                        backgroundColor: "skyblue",
                        backgroundColor: "rgba(179,181,198,0.2)",
                        fill: true,
                        borderColor: "rgba(179,181,198,1)",
                        pointBorderColor: "#fff",
                        pointBackgroundColor: "rgba(179,181,198,1)",
                       data: [100,20,60,66,16,90]
                     },
                     {
                       label: "diabetes",
                        backgroundColor: "violet",
                        backgroundColor: "rgba(255,99,132,0.2)",
                        borderColor: "rgba(255,99,132,1)",
                        pointBorderColor: "#fff",
                        pointBackgroundColor: "rgba(255,99,132,1)",
                        pointBorderColor: "#fff",
                        data: [5,80,15,90,77,8]
                   },
                   {
                        label: "cancer",
                        backgroundColor: "rgba(255, 204, 102, 0.2)",
                        borderColor: "rgba(255, 204, 102, 1)",
                        pointBackgroundColor: "rgba(255, 204, 102, 1)",
                        pointBorderColor:"rgba(255, 204, 102, 1)",
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(255, 204, 102, 1)",
                        pointHoverBorderColor: "rgba(255, 204, 102, 1)",
                        data: [30,90,15,90,42,84]
                },{
                        label: "Hyper-tension",
                        backgroundColor: "rgba(0, 153, 247,.5)",
                        borderColor: "rgba(0, 153, 247,1)",
                        pointBackgroundColor: "rgba(0, 153, 247,1)",
                        pointBorderColor: "rgba(0, 153, 247,1)",
                        pointHoverBackgroundColor: "rgba(0, 153, 247,1)",
                        pointHoverBorderColor: "rgba(0, 153, 247,1)",
                        data: [44,6,97,13,85,92,45]
                },
               ]
           };
           return result;
       }
   });
});