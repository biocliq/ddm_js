/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/_RadarChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs._RadarChart", base, {
        rawData:[],
       	getChartType:function(){
            return 'radar';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{
                                // boxWidth:'10',
                                display:false,
                                ticks: {
                                    beginAtZero:true
                                },scaleLabel:{
                                    display:false, 
                                }}]
                        }, xAxes:[{
                            scaleLabel:{
                               display:false, 
                            }, ticks: { 
                                display: true //this will remove only the label
                                ,callback: function(value, index, values) {
                                    if (Math.floor(value) === value) {
                                        return value;
                                    }
                                }
                                }
                            }],
                legend:{
                    display: true,
                    position:"left",
                    labels: {
                        fontColor: "rgba(0, 153, 247,1)",
                        usePointStyle: true,
                        boxWidth:10,
                    },
                },
            };
        },
        _getDataIndexByGenderAge:function(gender, age){
            var offset = 'Male' == gender ? 4 : 0;
            var idx = age < 11 ? 0 : age < 21 ? 1 : age < 41 ?  2 :3;
            return idx + offset; 
        },
        convertRawData:function(rawData){
            rawData = [
                {name:"Cystic Fibrosis",count:1, gender : "Male", age:8},
                {name:"Cystic Fibrosis",count:1, gender : "Male", age:12},
                {name:"Cancer",count:1, gender : "Male", age:40},
                {name:"Diabetes",count:1, gender : "female", age:20},
                {name:"Diabetes",count:3, gender : "Male", age:50}
            ]
            var datas = {};
            var label =  ["f~10","f~20","f~40","f>40","m~10","m~20","m~40","m>40"];
            rawData.forEach((dt) => {
                data = datas[dt.name];
                if(undefined == data){
                    data = [0,0,0,0,0,0,0,0];
                    datas[dt.name] = data;
                };
                var age=dt.age;
                var gender = dt.gender;
                var idx = this._getDataIndexByGenderAge(gender, age);
                data[idx] += dt.count;
            }   
            );
            var datasets = [];
            for(idx in datas){
                var data = datas[idx];
                dataset = {
                        label:idx,
                        data:data,
                        fill:true,
                        backgroundColor: "rgba(0, 153, 247,.5)",
                        borderColor: "rgba(0, 153, 247,1)",
                        pointBackgroundColor: "rgba(0, 153, 247,1)",
                        pointBorderColor: "rgba(0, 153, 247,1)",
                        pointHoverBackgroundColor: "rgba(0, 153, 247,1)",
                        pointHoverBorderColor: "rgba(0, 153, 247,1)",
                        pointStyle: 'rectRot'
                }
                datasets.push(dataset);
            }
            for(i=0;i<datasets.length;i++){
               if(datasets[i].label=="Cystic Fibrosis")
               {        
                        datasets[i].fill=true,
                        datasets[i].backgroundColor= "rgba(0, 153, 247,.5)",
                        datasets[i].borderColor="rgba(0, 153, 247,1)",
                        datasets[i].pointBackgroundColor= "rgba(0, 153, 247,1)",
                        datasets[i].pointBorderColor= "rgba(0, 153, 247,1)",
                        datasets[i].pointHoverBackgroundColor= "rgba(0, 153, 247,1)",
                        datasets[i].pointHoverBorderColor= "rgba(0, 153, 247,1)",
                        datasets[i].pointStyle='rectRot'
                }
                else if(datasets[i].label == "Cancer")
                {
                    datasets[i].backgroundColor= "rgba(179,181,198,0.2)",
                    datasets[i].fill= true,
                    datasets[i].borderColor= "rgba(179,181,198,1)",
                    datasets[i].pointBorderColor= "#fff",
                    datasets[i].pointBackgroundColor= "rgba(179,181,198,1)";
                }
                else{
                    datasets[i].backgroundColor= "rgba(255,99,132,0.2)",
                    datasets[i].borderColor= "rgba(255,99,132,1)",
                    datasets[i].pointBorderColor= "#fff",
                    datasets[i].pointBackgroundColor= "rgba(255,99,132,1)",
                    datasets[i].pointBorderColor= "#fff";
                }
            }
            var result = {
                labels:label,
                datasets: datasets,
            };
            return result;
        }
	});
});