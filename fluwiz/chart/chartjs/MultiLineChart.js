/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/MultiLineChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase",
    "ddm/fluwiz/chart/base/_dataParser"], 
	function(declare,array, chartjsbase, dataParser) {

	var base = [chartjsbase, dataParser];

	return declare("ddm.fluwiz.chart.chartjs.MultiLineChart", base, {     
       	getChartType:function(){
            return 'line';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{                                
                                ticks: {
                                    beginAtZero:true
                                },scaleLabel:{
                                    display:false, labelString: "na"
                                }}]
                        , xAxes:[{
                            scaleLabel:{
                                display:false, labelString: "na"
                                }                                
                                }
                            ]},
                legend:{
                    display: true,
                    position:"left",
                    labels: {
                        boxWidth:10,
                        usePointStyle: true,
                    },
                },hover: {
                    animationDuration: 1
                },
                animation: {
                    easing : "easeInSine"
                }
            };

            if(this.options && this.options.showValue){
                this.chartOptions.animation.onComplete = function () {
                    var chartInstance = this.chart;
                    var ctx = this.chart.ctx;
                    ctx.font = this.ctx.font;
                    ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
                    ctx.textAlign = "center";
                    ctx.textBaseline = "bottom";
                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        if(! meta.hidden){
                            meta.data.forEach(function (point, index) {                            
                                var data = dataset.data[index];
                                ctx.fillText(data, point._model.x, point._model.y -5);
                            });
                        }
                    })
                };
            }
        },
        convertRawData:function(rawData){            
            var xName = this.options.xName || 'date';
            var yName = this.options.yName || ['total', 'used'];
            var labels = this.options.yLabel || ['Total', 'Used'];
            var data = [];
            for(var i =0; i < yName.length; i++){
                data[i] = new Array();
            }
            [];

            var xAxis =  [];
            var bgColor = [];
            if(rawData){
                rawData.forEach((dt) => {
                    xAxis.push(dt[xName]);
                    for(var i =0; i < yName.length; i++){
                        data[i].push(dt[yName[i]]);
                        data[i].yColumn = yName[i];
                        bgColor.push(this.getRandomColor(yName[i]));
                    }
                });
            }

            bgColor = this.options.backgroundColor || bgColor;

            var dataSets = [];

            for(var i =0; i < data.length; i++){
                dataSets.push({                    
                        label:labels[i],
                        fill:false,                       
                        pointBackgroundColor: bgColor[i],
                        pointBorderColor: bgColor[i],
                        yColumn :data[i].yColumn,
                        xColumn:xName,
                        pointHoverBackgroundColor: bgColor[i],
                        pointHoverBorderColor: bgColor[i],
                        data: data[i],
                        borderColor: bgColor[i]
                });
            }

            var result = {
                labels:xAxis,
                datasets: dataSets
            };
            return result;
        }
	});

});