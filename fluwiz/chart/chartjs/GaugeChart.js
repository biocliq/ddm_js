/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/GaugeChart", [
    "dojo/_base/declare",
    "dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase"],
    function (declare, array, ContentPane) {

        var base = [ContentPane];

        return declare("ddm.fluwiz.chart.chartjs.GaugeChart", base, {
            getChartType: function () {
                return 'gauge';
            },
            initChartOptions: function () {
                var _this = this;
                this.chartOptions = {
                    maintainAspectRatio: true,
                    cutoutPercentage: 70, // precent   
                    needle: {
                        radiusPercentage: 2,
                        widthPercentage: 3.2,
                        lengthPercentage: 85,
                        color: '#000000C0'
                    },
                    valueLabel: {
                        fontSize: '16',
                        backgroundColor:'#000000B0'
                    },
                    legend: {
                        display: false
                    },
                    layout: {
                        padding: '0'
                    },
                    events: [],
                    tooltips: {
                        enabled: false
                    }
                };
            },
            convertValue: function (chartData) {
                label = '';
                var progress = chartData[0];
                var remain = chartData[1];
                var value = 100 * progress / (progress + remain);

                if (!isNaN(value)) {
                    return value.toFixed(1);
                }
                else
                    return 0;
            },
            convertRawData: function (rawData) {
                var chartData = [];
                var label = [];
                var bgColor = [];

                var xName = this.options.xName || 'date';
                var yName = this.options.yName || ['total', 'used'];
                var legend = this.options.yLabel || yName;

                var dt = rawData[0];
                for (var i = 0; i < yName.length; i++) {
                    var value = dt[yName[i]];
                    label.push(legend[i]);
                    if (undefined == value)
                        value = 0;
                    chartData.push(value);
                    bgColor.push(this.getRandomColor(dt[xName] + i));
                }

                if (this.options.colorArray) {
                    bgColor = this.getColorArray(this.options.colorArray, 0.7, bgColor.length);
                } else
                    bgColor = this.options.backgroundColor || bgColor;

                var canvas = this.chartNode;
                var w = canvas.width * 7 / 10;
                var h = canvas.height * 8 / 10;

                if (chartData.length > 1) {
                    chartData[1] = chartData[1] - chartData[0];
                }

                var value = this.convertValue(chartData);


                var data = {
                    labels: legend,
                    datasets: [
                        {
                            label: "Progress",
                            data: [20, 40, 60, 80, 100],
                            value: value,
                            backgroundColor: [
                                '#FF0000A0',
                                '#FF000070',
                                '#FFA500A0',
                                '#6FB359A0',
                                '#008000B0'
                            ]
                        }
                    ]
                };
                return data;
            }
        });
    });