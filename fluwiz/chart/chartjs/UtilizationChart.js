/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/UtilizationChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase",
    "chroma/chroma"], 
	function(declare,array, ContentPane,chroma) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs.UtilizationChart", base, {
        getChartType:function(){
            return 'line';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{                                
                                    ticks: {
                                        beginAtZero:true
                                    },scaleLabel:{
                                        
                                    }}]
                    },
                        xAxes:[{
                            scaleLabel:{
                            display:true, labelString: "date"
                            }, ticks: { 
                                display: true //this will remove only the label
                                ,callback: function(value, index, values) {                                    
                                    if (Math.floor(value) === value) {
                                        return value;
                                    }
                                }
                                }
                            }],
                legend:{
                    display: true,
                    position:"left",
                    labels: {
                        usePointStyle: true,
                        boxWidth:10,
                    },
                },hover: {
                    animationDuration: 1
                },
                animation: {
                    easing : "easeInSine"
                }
            };

            if(this.options && this.options.showValue){
                this.chartOptions.animation.onComplete = function () {
                    var chartInstance = this.chart;
                    var ctx = this.chart.ctx;
                    ctx.font = this.ctx.font;
                    ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
                    ctx.textAlign = "center";
                    ctx.textBaseline = "bottom";
                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        if(! meta.hidden){
                            meta.data.forEach(function (point, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data, point._model.x, point._model.y -5);
                            });
                        }
                    })
                };
            }
        },
        convertRawData:function(rawData){            
            var xName = 'date';
            var totalName = 'total';
            var usedName = 'used';

            var dataTotal = [];
            var dataUsed = [];
            var label =  [];
            if(rawData)
                rawData.forEach((dt) => {
                    label.push(dt[xName]);
                    dataTotal.push(dt[totalName]);
                    dataUsed.push(dt[usedName]);
                });
            var result = {
                labels:label,
                datasets: [
                    {   
                            label: 'Total',
                            fill:false,
                            backgroundColor: "rgba(0, 153, 247,.5)",
                            borderColor: "rgba(0, 153, 247,1)",
                            pointBackgroundColor: "rgba(0, 153, 247,1)",
                            pointBorderColor: "rgba(0, 153, 247,1)",
                            pointHoverBackgroundColor: "rgba(0, 153, 247,1)",
                            pointHoverBorderColor: "rgba(0, 153, 247,1)",
                            data:dataTotal,
                            pointStyle: 'rectRounded',
                    },
                    { 
                            label: 'Used',
                            fill:false,
                            backgroundColor: "rgba(255, 204, 102, 0.2)",
                            borderColor: "rgba(255, 204, 102, 1)",
                            pointBackgroundColor: "rgba(255, 204, 102, 1)",
                            pointBorderColor:"rgba(255, 204, 102, 1)",
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "rgba(255, 204, 102, 1)",
                            pointHoverBorderColor: "rgba(255, 204, 102, 1)",
                            data:dataUsed,
                            pointStyle: 'rectRounded',
                        }
                ]
            };
            return result;
        }
	});
});