define("ddm/fluwiz/chart/chartjs/DiagBarChart", [
	"dojo/_base/declare",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,ContentPane) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs.BarChart", base, {
       	getChartType:function(){
            return 'bar';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{
                                ticks: {
                                    display: true, beginAtZero:true, min: 0,max:4, stepSize: 1
                                }
                            }]
                        }, 
                        xAxes:[{
                            ticks: { 
                                display: true, beginAtZero:true, min:0,max:4, stepSize: 1
                                }
                            }],
                legend:{
                    display: false,
                    labels: {
                        boxWidth:1,
                    },
                },
            };
        },
        convertRawData:function(rawData){
            console.log(rawData);
            var xName = 'name';
            var yName = 'count'; 
            // var value = 'gender';           
            var datasets = [{
                        label:'Male',
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        backgroundColor: "rgba(0,255,0,0.3)",
                        stack:true, data:[], }, 
                     {
                        label:'Female',
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        backgroundColor: "rgba(255,165,0,0.3)",
                        stack:true, 
                        data:[],
                    }, {label:'Transg',
                        fillColor: "rgba(51,49,90,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(22x0,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        backgroundColor: 'rgba(251, 85, 85, 0.4)',
                        stack:true, data:[]
                    }];
            var label =  [];
            rawData.forEach((dt) => {
                var dataset ;
                console.log(dt);
                var name = dt.name;
                var idx = label.indexOf(name);
                if(idx < 0){
                    idx = label.push(name) - 1;
                }
                switch(dt.gender){
                    case 'Male':{
                        dataset = datasets[0];
                        break;
                    }
                    case 'Female':{
                        dataset = datasets[1];
                        break;
                    }
                    default:{
                        dataset = datasets[2];
                    }
                }
                dataset.data[idx] = dt.count;            
            });
            var result = {
                labels:label,
                datasets: datasets
            }
            return result;
            
        }
	});
});