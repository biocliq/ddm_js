/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/StatusDoughnutChart", [
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom-construct",
    "ddm/fluwiz/chart/base/_chartjsbase"],
    function (declare, array, domConstruct, ContentPane) {

        var base = [ContentPane];


        return declare("ddm.fluwiz.chart.chartjs.DoughnutGaugeChart", base, {
            getChartType: function () {
                return 'doughnut';
            },
            textInCenter: function (value, label) {
                var chartNode = this.chartNode;
                var ctx = chartNode.getContext('2d');
                var chart = this.chart;
                var lines = [label, value];
                var centerConfig = {
                    text: label,
                    color: '#000000A0',
                    maxFontSize:3,
                    fontStyle: 'Arial', // Default is Arial
                    sidePadding: 20, // Default is 20 (as a percentage)
                    minFontSize: 1, // Default is 20 (in px), set to false and text will not wrap.
                    lineHeight: 25 // Default is 25 (in px), used for when text wraps
                };
                var fontStyle = centerConfig.fontStyle || 'Arial';
                var txt = centerConfig.text;
                var color = centerConfig.color || '#000';
                var maxFontSize = centerConfig.maxFontSize || 3;
                var sidePadding = centerConfig.sidePadding || 20;
                var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)

                ctx.font = "2vw " + fontStyle;

                // Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                var txtBoundingBox = ctx.measureText(txt);
                var valueBox = ctx.measureText(value);
                var stringWidth = Math.max(txtBoundingBox.width, valueBox.width);
                var stringHeight = txtBoundingBox.actualBoundingBoxAscent - txtBoundingBox.actualBoundingBoxDescent;
                var elementWidth = ((chart.innerRadius * 2) - sidePaddingCalculated - 10) * 0.95;
                // Find out how much the font can grow in width.
                var widthRatio = elementWidth / stringWidth;
                var newFontSize = 2 * widthRatio;
                var elementHeight = (chart.innerRadius * 2);

                // Pick a new font size so it will not be larger than the height of label.
                var fontSizeToUse = Math.min(newFontSize, elementHeight, maxFontSize);
                var minFontSize = centerConfig.minFontSize;
                var lineHeight = centerConfig.lineHeight || 25;
                var wrapText = false;

                if (minFontSize === undefined) {
                    minFontSize = 2;
                }

                if (minFontSize && fontSizeToUse < minFontSize) {
                    fontSizeToUse = minFontSize;
                    wrapText = true;
                }

                // Set font settings to draw it correctly.
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                ctx.fillStyle = '#AAAAAA';

                var labelFontSize = Math.max(fontSizeToUse * 0.7, minFontSize);
                var offset = ((fontSizeToUse) * 8.3);
                ctx.font = labelFontSize + "vw " + fontStyle;
                var y = centerY - offset;
                ctx.fillText(lines[0], centerX, y);
                ctx.font = fontSizeToUse + "vw " + fontStyle;
                ctx.fillStyle = color;
                var y = centerY + offset;
                ctx.fillText(lines[1], centerX, y);
            },
            initChartOptions: function () {
                var _this = this;
                _this.segmentHovered = false;
                this.chartOptions = {
                    maintainAspectRatio: true,
                    // circumference: Math.PI,
                    rotation: -Math.PI - 0.5,
                    cutoutPercentage: 45,
                    elements: {
                        arc: {
                            borderWidth: 1
                        },
                    },
                    legend: {
                        display: false
                    },
                    layout: {
                        padding: '0'
                    },
                    animation: {
                        onComplete: function (animation) {
                            if (!_this.segmentHovered) {
                                var chart = this.chart;
                                var datasets = chart.config.data.datasets;
                                if (datasets.length > 0) {
                                    var value = datasets[0].data.reduce(function (a, b) {
                                        return a + b;
                                    }, 0);
                                    label = 'Total';
                                    _this.textInCenter(value, label);
                                }
                            }
                        },
                    },
                    tooltips: {
                        enabled: true,
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var label = data.labels[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100), 1);
                                return [label, currentValue + ' (' + percentage + "%)"];
                            }
                        },
                        custom: function (tooltip) {
                            if (tooltip.body) {
                                _this.segmentHovered = true;
                            } else {
                                _this.segmentHovered = false;
                            }
                        }
                    }
                };
            },
            transposeData: function (rawData, xColumn, yColumn) {
                var result = {};
                for (idx in rawData) {
                    var data = rawData[idx];
                    result[data[xColumn]] = data[yColumn];
                }
                return [result];
            },
            convertRawData: function (rawData) {
                var xColumn = this.options.xColumn || 'status';
                var yColumn = this.options.yColumn || 'cnt';
                if (this.options.yColumn)
                    rawData = this.transposeData(rawData, xColumn, yColumn);

                var chartData = [];
                var label = [];
                var bgColor = [];

                var xName = this.options.xName || 'date';
                var yName = this.options.yName || ['total', 'used'];
                var legend = this.options.yLabel || yName;

                var dt = rawData[0];
                for (var i = 0; i < yName.length; i++) {
                    var value = dt[yName[i]];
                    label.push(legend[i]);
                    if (undefined == value)
                        value = 0;
                    chartData.push(value);
                    bgColor.push(this.getRandomColor(dt[xName] + i));
                }

                if (this.options.colorArray) {
                    bgColor = this.getColorArray(this.options.colorArray, 0.7, bgColor.length);
                } else
                    bgColor = this.options.backgroundColor || bgColor;


                var data = {
                    labels: legend,
                    datasets: [
                        {
                            label: "Progress",
                            data: chartData,
                            backgroundColor: bgColor
                        }
                    ]
                };
                return data;
            }
        });
    });