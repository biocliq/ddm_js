/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/StackBarChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase",
    "ddm/fluwiz/chart/base/_dataParser"], 
	function(declare,array, _chartjsbase, _dataParser) {

	var base = [_chartjsbase, _dataParser];

	return declare("ddm.fluwiz.chart.chartjs.StackBarChart", base, {
       	getChartType:function(){            
            return 'bar';
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{                                
                                ticks: {
                                    beginAtZero:true
                                },
                                scaleLabel:{
                                    display:false, labelString: "na"
                                }}]
                        , xAxes:[{
                            scaleLabel:{
                            display:false, labelString: "na"
                            }, ticks: { 
                                display: true,
                                callback: function(value) {
                                    return value.substr(0, 10);
                                }
                                },
                            }]},
                legend:{
                    display: false,
                    position:"left",
                    labels: {
                        boxWidth:10,
                    }
                },                
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function(tooltipItem, data) {                            
                            var stackName = data.datasets[tooltipItem.datasetIndex].stack;
                            var message = [];                            
                    
                            var total = 0;
                            for (var i = 0; i < data.datasets.length; i++){
                                var dataset = data.datasets[i];
                                var label = dataset.label;
                                var value = dataset.data[tooltipItem.index];
                                if(stackName == dataset.stack){
                                    if(dataset.data[tooltipItem.index])
                                        total += dataset.data[tooltipItem.index];
                                    if(value)
                                        message.push(label + ' ' + value);
                                }
                            }
                            if(message.length > 1)
                                message.push('Total' + ' ' + total);                            
                            return message;
                        }
                    }
                },
                hover: {
                    animationDuration: 1
                },
                animation: {
                    easing : "easeInSine",
                    onComplete: function () {
                        var chartInstance = this.chart;
                        ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
                        ctx.textBaseline = 'bottom';
                        var stack = {};
                        this.data.datasets.forEach(function(dataset, i){
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            var stackName = dataset.stack;
                            if(! meta.hidden){
                                dataset.data.forEach(function(data, i){
                                    if(data == undefined)
                                        data = 0;
                                    if(stack[stackName]){
                                        if(stack[stackName][i])
                                            stack[stackName][i] = stack[stackName][i] + data;
                                        else
                                            stack[stackName][i] = data;
                                    }else{
                                        stack[stackName] = {};
                                        stack[stackName][i] = data;
                                    };
                                });
                            }
                            
                        });                        

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            var stackName = dataset.stack;
                            if(! meta.hidden){
                                meta.data.forEach(function (bar, index) {                                
                                    var data = dataset.data[index];
                                    var offset = 20;
                                    localBar = bar;
                                    currentBar = undefined;
                                    ctx.fillText(data, bar._model.x, bar._view.y + offset);
                                    var total = stack[stackName][index];                                
                                    if(total)
                                        ctx.fillText(total, bar._model.x,
                                        bar._yScale.height - (bar._yScale.height / bar._yScale.end * total)+20);
                                    stack[stackName][index] = undefined;
                                });
                            }             
                        });
                    }
                }
            };
            declare.safeMixin(this.chartOptions, this.options);            
        },
        convertRawData:function(rawData){            
            var xName = this.options.xName || 'date';
            var yName = this.options.yName || ['total', 'used'];
            var stack = this.options.stack || [];
            var data = [];
            for(var i =0; i < yName.length; i++){
                data[i] = new Array();
            }            
            var legend = this.options.yLabel || yName;

            var label = [];
            var bgColor = [];
            if(rawData){
                rawData.forEach((dt) => {
                    label.push(dt[xName] + "");
            
                    for(var i =0; i < yName.length; i++){
                        if(0 != dt[yName[i]]){
                            data[i].push(dt[yName[i]]);
                            data[i].yColumn = yName[i];
                        }else
                            data[i].push(undefined);
                            
                        bgColor.push(this.getRandomColor(dt[xName] + "" + i));
                    }
                });
            }
            bgColor = this.options.backgroundColor || bgColor;
            var dataSets = [];
            
            for(var i =0; i < data.length; i++){
                var dataSet = {
                    fillColor: "rgba(151,249,190,0.5)",
                    strokeColor: "rgba(255,255,255,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    yColumn :data[i].yColumn,
                    xColumn:xName,
                    data: data[i],
                    borderWidth: 2,
                    borderRadius: 10,
                    backgroundColor: bgColor[i],
                    borderSkipped: false,
                    label:legend[i]
                };
                if(stack[i]){
                    dataSet.stack = stack[i];
                }
                dataSets.push(dataSet);
            }


            var result = {
                labels:label,
                datasets: dataSets
            };
            return result;
        }
	});
});
