/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/MultipleDataBarChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs.MultipleDataBarChart", base, {
        getChartType:function(){
            return 'bar';   
        },
        initChartOptions:function(){
            this.chartOptions =  {scales: {yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                },scaleLabel:{
                                    display:true, labelString: "sdfsdf"
                                }}]
                    }, xAxes:[{
                        scaleLabel:{
                            display:true, labelString: "sdfsdf"
                        }, ticks: {
                            display: false //this will remove only the label
                        }
                    }],
                    legend:{
                        display: true,
                        labels: {
                            boxWidth:10,
                    },
                },
                };
        },
        getData:function(){
            var data = {
                labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                datasets: [
                    {
                        label: "Prime and Fibonacci",
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [2, 3, 5, 7, 11, 13, 17, 19, 23, 29],
                        backgroundColor:[  'red',
                        'red','red','red','red','red','red','red','red','red'
                        ]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(252,147,65,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(173,173,173,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [2, 1, 1, 2, 3, 5, 8, 13, 21, 34],
                        backgroundColor:['green',
                        'green','green','green','green','green','green','green','green','green']
                    }
                ]
            };

            return data;
        }
	});

});