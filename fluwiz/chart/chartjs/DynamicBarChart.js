/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/DynamicBarChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase",
    "ddm/fluwiz/chart/base/_dataParser"], 
	function(declare,array, chartjsbase, dataParser) {

	var base = [chartjsbase, dataParser];

	return declare("ddm.fluwiz.chart.chartjs.DynamicBarChart", base, {     
       	getChartType:function(){
            return 'bar';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{                                
                                ticks: {
                                    beginAtZero:true
                                },scaleLabel:{
                                    display:false, labelString: "na"
                                }}]
                        , xAxes:[{
                            scaleLabel:{
                                display:false, labelString: "na"
                                }                                
                                }
                            ]},
                legend:{
                    display: true,
                    position:"left",
                    labels: {
                        boxWidth:10,
                        usePointStyle: true,
                    },
                },
                tooltips: {
                    enabled: true
                },
                hover: {
                    animationDuration: 0
                },
                animation: {
                    easing : "easeInSine",
                    onComplete: function () {
                        var chartInstance = this.chart;
                        ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            if(! meta.hidden){
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x, bar._model.y -5);
                                });
                            }
                        });
                    }
                }
            };
        },
        getIndex:function(/* value*/ value, /*list*/ list){            
            for(var i =0; i < list.length; i++){
                if(list[i] == value)
                    return i;
                if(value == undefined && list[i] == '')
                    return i;
            }
            return -1;
        },
        convertRawData:function(rawData){            
            var xName = this.options.xName || 'date';
            var yName = this.options.yName || ['total', 'used'];
            var yLabelColumn = this.options.yColumn || 'data';
            var yValueColumn = this.options.yValue || 'cnt';

            var data = [];
            for(var i =0; i < yName.length; i++){
                data[i] = new Array();
            }
            var labels = this.options.labels || [];

            var xAxis =  [];
            var bgColor = [];
            var oldXValue = '';
            var complete = -1;

            if(rawData){
                rawData.forEach((dt) => {
                    var xValue = dt[xName];

                    if(xValue != oldXValue){
                        if(complete != data.length && complete != -1){
                            for(var i = 0 ; i < data.length; i++){
                                if(data[i].length < xAxis.length){
                                    data[i].push(undefined);
                                }
                            }
                        }
                        xAxis.push(xValue);
                        oldXValue = xValue;
                        complete = 0;
                    }

                    var index = this.getIndex(dt[yLabelColumn], yName);
                    if(index > -1){
                        var yValue = dt[yValueColumn];                        
                        data[index].push(yValue);
                        bgColor.push(this.getRandomColor(yName[index]));
                        complete++;
                    }
                });
            }            
            bgColor = this.options.colors || bgColor;
            
            var dataSets = [];

            for(var i =0; i < data.length; i++){
                dataSets.push({                    
                        label:labels[i],                        
                        data: data[i],
                        backgroundColor: bgColor[i],
                        borderRadius:30
                });
            }

            var result = {
                labels:xAxis,
                datasets: dataSets
            };
            return result;
        }
	});

});