/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/MultipleDataBubbleChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {
	var base = [ContentPane];
	return declare("ddm.fluwiz.chart.chartjs.MultipleDataBubbleChart", base, {
        getChartType:function(){
            return 'bubble';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                responsive: true,
                title:{
                    text: "Deer Population- 2015"
                },
                axisX: {
                    title:"Life Expectancy (in Years)"
                },
                axisY: {
                    title:"Fertility Rate"
                },
                legend:{
                    display: true,
                    labels: {
                        usePointStyle: true,
                    },
                },
            }
        },
        getData:function(){
            var data = {
                datasets: [
                    {
                        label: ['Deer Population'],
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [{ x: 100,y: 0,r: 1 }, {x: 60,y: 30,r: 2}, {x: 40,y: 60,
                            r: 2}, {x: 80,y: 80,r: 5}, {x: 20,y: 30,r: 2}, {x: 0,y: 100,
                            r: 5}],
                        backgroundColor:[  'red','red','red','red','red','red','red','red','red','red'
                        ],
                    },
                    {
                        label: ['Deer Population'],
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data:  [{ x: 200,y: 2,r: 2 }, {x: 80,y: 50,r: 1}, {x: 40,y: 60,
                            r: 2}, {x: 80,y: 80,r: 5}, {x: 60,y: 40,r: 5}, {x: 75,y: 90,
                            r: 5}],
                        backgroundColor:[  'blue','blue','blue','blue','blue','blue','blue','blue','blue','blue'
                        ],   
                    },
                    {
                        label: ['Deer Population'],
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [{ x: 170,y: 25,r: 2 }, {x: 20,y: 40,r: 2}, {x: 80,y: 90,
                            r: 4}, {x: 40,y: 50,r: 9}, {x: 40,y: 40,r: 5}, {x: 15,y: 90,
                            r: 8}],
                        backgroundColor:[  'pink','pink','pink','pink','pink','pink','pink','pink','pink','pink'
                        ],   
                    },
                    {
                        label: ['Deer Population'],
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [{ x: 89,y: 66,r: 2 }, {x: 38,y: 80,r: 10}, {x: 60,y: 95,
                            r: 5}, {x: 30,y: 70,r: 3}, {x: 20,y: 40,r: 4}, {x: 15,y: 20,
                            r: 3}],
                        backgroundColor:[  'orange','orange','orange','orange','orange','orange','orange','orange','orange','orange'
                        ],   
                    }, 
                    {
                        label: ['Deer Population'],
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data:  [{ x: 75,y: 65,r: 5 }, {x: 30,y: 25,r: 15}, {x: 60,y: 95,
                            r: 5}, {x: 30,y: 70,r: 10}, {x: 20,y: 40,r: 5}, {x: 15,y: 20,
                            r: 5}],
                        backgroundColor:[  'purple','purple','purple','purple','purple','purple','purple','purple','purple','purple'
                        ],   
                    }, 
                ],
            };
            return data;
        }
	});
});