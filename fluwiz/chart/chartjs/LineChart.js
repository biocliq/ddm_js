/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/LineChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs.LineChart", base, {
       	getChartType:function(){
            return 'line';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{                                
                                ticks: {
                                    beginAtZero:true
                                },scaleLabel:{
                                    display:false, labelString: "na"
                                }}]
                        }, xAxes:[{
                            scaleLabel:{
                                display:false, labelString: "na"
                                }, 
                                ticks: { 
                                    display: true
                                    ,callback: function(value, index, values) {                                      
                                        if (Math.floor(value) === value) {
                                            return value;
                                        }
                                    }
                                },
                                type: 'time',
                                time: {
                                parser: 'MM/DD/YYYY HH:mm',
                                tooltipFormat: 'll HH:mm',
                                unit: 'day',
                                unitStepSize: 1,
                                displayFormats: {
                                    'day': 'MM/DD/YYYY'
                                }
                                }
                            }],
                legend:{
                    display: false
                },hover: {
                    animationDuration: 1
                },
                animation: {
                    easing : "easeInSine"
                }
            };

            if(this.options && this.options.showValue){
                this.chartOptions.animation.onComplete = function () {
                    var chartInstance = this.chart;
                    var ctx = this.chart.ctx;
                    ctx.font = this.ctx.font;
                    ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
                    ctx.textAlign = "center";
                    ctx.textBaseline = "bottom";
                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        if(! meta.hidden){
                            meta.data.forEach(function (point, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data, point._model.x, point._model.y -5);
                            });
                        }
                    })
                };
            }
        },
        convertRawData:function(rawData){
            var xName = this.options.xName || 'date';
            var yName = this.options.yName || 'total';
            var data = [];
            var data=[];
            var label =  [];
            
            if(rawData)
                rawData.forEach((dt) => {                    
                    label.push(dt[xName]);
                    data.push(dt[yName]);
                });

            var result = {
                labels:label,
                datasets: [
                    {
                        fill:false,
                        backgroundColor: "rgba(0, 153, 247,.5)",
                        borderColor: "rgba(0, 153, 247,1)",
                        pointBackgroundColor: "rgba(0, 153, 247,1)",
                        pointBorderColor: "rgba(0, 153, 247,1)",
                        pointHoverBackgroundColor: "rgba(0, 153, 247,1)",
                        pointHoverBorderColor: "rgba(0, 153, 247,1)",
                        data:data,
                        pointStyle: 'rectRounded',
                        datalabels: {
                          align: 'start',
                          anchor: 'start'
                        }
                    }
                ]
            };
            return result;
        }
	});
});