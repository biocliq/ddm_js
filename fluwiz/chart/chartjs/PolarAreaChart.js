/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/PolarAreaChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {
	var base = [ContentPane];
	return declare("ddm.fluwiz.chart.chartjs.PolarAreaChart", base, {
       	getChartType:function(){
            return 'polarArea';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                responsive: true,
				legend: {
					position: 'right',
				},
				title: {
					display: true,
					text: 'Chart.js Polar Area Chart'
				},
				scale: {
					ticks: {
						beginAtZero: true
					},
					reverse: false
				},
            }
        },
        getData:function(){
            var data = {
                labels: ["yellow","orange","lightblue","darkgreen","black"],
                datasets: [
                    {
                        label: "My DataSet",
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [   11, 16,7,3,14],
                        backgroundColor:[ 'yellow','orange','lightblue','darkgreen','black'
                        ],
                    },
                ]
            };
             return data;
        }
	}); 
});