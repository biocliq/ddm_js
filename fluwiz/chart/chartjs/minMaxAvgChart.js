/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/minMaxAvgChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase",
    "chroma/chroma"], 
	function(declare,array, ContentPane,chroma) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs.minMaxAvgChart", base, {
        getChartType:function(){
            return 'line';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{  
                                // stacked:false,
                                // boxWidth:'10',
                                    ticks: {
                                        beginAtZero:true
                                    },scaleLabel:{
                                        display:true, labelString: "min-max-avg"
                                    }}]
                    },
                        xAxes:[{
                            scaleLabel:{
                            display:true, labelString: "date"
                            }, ticks: { 
                                display: true //this will remove only the label
                                ,callback: function(value, index, values) {
                                    console.log(value);
                                    if (Math.floor(value) === value) {
                                        return value;
                                    }
                                }
                                }
                            }],
                legend:{
                    display: true,
                    position:"left",
                    labels: {
                        usePointStyle: true,
                        boxWidth:10,
                    },
                },
            };
        },
        convertRawData:function(rawData){
            console.log(rawData);
            var xName = 'date';
            var yName = 'minVal';            
            var dataMin = [];
            var dataMax = [];
            var dataAvg = [];
            var label =  [];
            if(rawData)
                rawData.forEach((dt) => {
                    label.push(dt[xName]);
                    dataMin.push(dt.minVal);
                    dataMax.push(dt.maxVal);
                    dataAvg.push(dt.avgval)
                });
            var result = {
                labels:label,
                datasets: [
                    {   
                            label: 'MinimumData',
                            fill:false,
                            backgroundColor: "rgba(0, 153, 247,.5)",
                            borderColor: "rgba(0, 153, 247,1)",
                            pointBackgroundColor: "rgba(0, 153, 247,1)",
                            pointBorderColor: "rgba(0, 153, 247,1)",
                            pointHoverBackgroundColor: "rgba(0, 153, 247,1)",
                            pointHoverBorderColor: "rgba(0, 153, 247,1)",
                            data:dataMin,
                            pointStyle: 'rectRounded',
                    },
                    { 
                            label: 'AverageData',
                            fill:false,
                            backgroundColor: "rgba(255, 204, 102, 0.2)",
                            borderColor: "rgba(255, 204, 102, 1)",
                            pointBackgroundColor: "rgba(255, 204, 102, 1)",
                            pointBorderColor:"rgba(255, 204, 102, 1)",
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "rgba(255, 204, 102, 1)",
                            pointHoverBorderColor: "rgba(255, 204, 102, 1)",
                            data:dataAvg,
                            pointStyle: 'rectRounded',
                        },
                        { 
                            label: 'MaximumData',
                            fill: false,
                            backgroundColor: "rgba(255,99,132,0.2)",
                            borderColor: "rgba(255,99,132,1)",
                            pointBorderColor: "#fff",
                            pointBackgroundColor: "rgba(255,99,132,1)",
                            pointBorderColor: "#fff",
                            data:dataMax,
                            pointStyle: 'rectRounded',
                        },
                ]
            };
            return result;
        }
	});
});