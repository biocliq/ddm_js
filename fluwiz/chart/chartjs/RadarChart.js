/** 
* (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
* 
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential 
*
* Author k.raja@biocliq.com
*/


define("ddm/fluwiz/chart/chartjs/RadarChart", [
   "dojo/_base/declare",
   "dojo/_base/array",
   "ddm/fluwiz/chart/base/_chartjsbase"], 
   function(declare,array, _chartjsbase) {

   var base = [_chartjsbase];

   return declare("ddm.fluwiz.chart.chartjs.RadarChart", base, {       
      getChartType:function(){
          return 'radar';   
      },      
      initChartOptions:function(){
        this.chartOptions =  {
          scales: {yAxes: [{
                          // boxWidth:'10',
                          ticks: {
                              beginAtZero:true
                          },scaleLabel:{
                              display:false, labelString: "na"
                          }}]
                  , xAxes:[{
                      scaleLabel:{
                          display:true, labelString: "na"
                          }, 
                          ticks: { 
                              display: true //this will remove only the label
                              ,callback: function(value, index, values) {                                      
                                  if (Math.floor(value) === value) {
                                      return value;
                                  }
                              }
                          },
                          type: 'time',
                          time: {
                          parser: 'MM/DD/YYYY HH:mm',
                          tooltipFormat: 'll HH:mm',
                          unit: 'day',
                          unitStepSize: 1,
                          displayFormats: {
                              'day': 'MM/DD/YYYY'
                          }
                          }
                      }]},
          legend:{
              display: true,
              position:"left",
              labels: {
                  boxWidth:10,
                  usePointStyle: true,
              },
          },
      };
      }
   });

});