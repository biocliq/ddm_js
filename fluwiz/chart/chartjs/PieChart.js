/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/PieChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, _chartjsbase) {
	var base = [_chartjsbase];
	return declare("ddm.fluwiz.chart.chartjs.PieChart", base, {        
       	getChartType:function(){
            return 'pie';   
        },
        initChartOptions:function(){
            this.chartOptions = {
                responsive: true,
                title: {
                    display: true,
                    position: "top",
                    text: "Pie Chart",
                    fontSize: 18,
                    fontColor: "#111"
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        fontColor: "#333",
                        fontSize: 12
                    }
                }
            }
        },
        convertRawData:function(rawData){
            var xName = this.options.xName || 'name';
            var yName = this.options.yName || 'total';

            var data = [];
            var label =  [];
            var bgColor = [];
            if(rawData)
                rawData.forEach((dt) => {                    
                    data.push(dt[yName]);
                    label.push(dt[xName]);
                    bgColor.push(this.getRandomColor(dt.name));
                });

            var result = {
                labels:label,
                datasets: [
                    {
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: data,
                        backgroundColor: bgColor // [ "rgba(255,165,0,0.3)","rgba(0,255,0,0.3)"],
                    },
                ]
            };
            return result;
        }
	});
});