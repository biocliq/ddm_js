/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/ThresholdBarChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase",
    "ddm/fluwiz/chart/base/_dataParser"], 
	function(declare,array, _chartjsbase, _dataParser) {

	var base = [_chartjsbase, _dataParser];

	return declare("ddm.fluwiz.chart.chartjs.ThresholdBarChart", base, {
       	getChartType:function(){            
            return 'bar';
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{                                
                        ticks: {
                            beginAtZero:true
                        },
                        scaleLabel:{
                            display:false, labelString: "na"
                        }}]
                , xAxes:[{
                    scaleLabel:{
                    display:false, labelString: "na"
                    }, ticks: { 
                        display: true,
                        callback: function(value) {
                            return value.substr(0, 10);
                        }
                        },
                    }]},
                legend:{
                    display: false,
                    position:"left",
                    labels: {
                        boxWidth:10,
                    }
                },
                borderColor: function(context, options) {
                    var color = options.color; // resolve the value of another scriptable option: 'red', 'blue' or 'green'
                    console.log(context);
                    return Chart.helpers.color(color).lighten(0.2);
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function(tooltipItem, data) {                            
                            var message = [];
                            
                            for (var i = 0; i < data.datasets.length; i++){
                                var dataset = data.datasets[i];
                                var label = dataset.label;
                                var value = dataset.data[tooltipItem.index];                            
                                if(value){
                                    message.push(label + ' ' + value);
                                    message.push("Threshold " + dataset.threshold[tooltipItem.index]);
                                }
                            }
                            return message;
                        }
                    }
                },
                hover: {
                    animationDuration: 1
                },
                animation: {
                    easing : "easeInSine",
                    onComplete: function () {
                        var chartInstance = this.chart;
                        ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            if(!meta.hidden){
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x, bar._model.y -5);
                                });
                            }
                        });
                    }
                }
            };
            declare.safeMixin(this.chartOptions, this.options);            
        },
        convertRawData:function(rawData){            
            var xName = this.options.xName || 'date';
            var yName = this.options.yName || ['total', 'used'];
            var tName = this.options.tName || [];
            var data = [];
            var threshold = [];

            for(var i =0; i < yName.length; i++){
                data[i] = new Array();
                threshold[i] = new Array();
            }            
            var legend = this.options.yLabel || yName;

            var label = [];
            var bgColor = [];
            var thresholdColor = [];
            if(rawData){
                rawData.forEach((dt) => {
                    label.push(dt[xName]);
                    for(var i =0; i < yName.length; i++){
                        var _value = dt[yName[i]];
                        var _threshold = dt[tName[i]];                        
                        data[i].push(_value);
                        threshold[i].push(_threshold);
                        bgColor.push(this.getRandomColor(dt[xName]+i));
                        thresholdColor.push(this.getRandomColor(dt[xName]+i));
                    }
                });
            }
            var colorfn = function(context){
                var dataIndex = context.dataIndex;
                var dsIndex = context.dataIndex;
                var dataSet = context.dataset;
                var dataSet = Array.isArray(dataSet) ? dataSet(dsIndex) : dataSet;

                var value = dataSet.data[dataIndex];
                var threshold = dataSet.threshold[dataIndex];
                console.log(value + '  ' + threshold);
                return (value <= threshold) ? dataSet.defaultColor : dataSet.thresholdColor;
            };

            console.log(threshold);

            bgColor = this.options.backgroundColor || bgColor;
            thresholdColor = this.options.thresholdColor || thresholdColor;
            var dataSets = [];
            
            for(var i =0; i < data.length; i++){
                dataSets.push({
                    fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: data[i],
                        borderWidth: 2,
                        borderRadius: 10,
                        threshold:threshold[i],
                        backgroundColor: colorfn,
                        defaultColor : bgColor[i],
                        thresholdColor : thresholdColor[i],
                        //backgroundColor: bgColor[i],
                        borderSkipped: false,
                        label:legend[i]
                });
            }


            var result = {
                labels:label,
                datasets: dataSets, 
                color:function(){
                    return "red";
                }
            };
            return result;
        }
	});
});