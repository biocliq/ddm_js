/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/Radar", [
	"dojo/_base/declare",
	"./BarChart"], 
	function(declare, BarChart) {

	var base = [BarChart];

	return declare("ddm.fluwiz.chart.chartjs.Radar", base, {
       	getChartType:function(){
            return 'radar';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{
                                ticks: {
                                    beginAtZero:true, min: 0,
                                    fontColor: 'red'
                                }
                            }]
                        }, 
                        xAxes:[{
                            ticks: { 
                                fontColor: 'green',
                                display: false, beginAtZero:true, min:0,
                                
                                }
                            }],
                legend:{
                    display: true,
                    fontColor: "#fff",
                    labels: {
                        fontColor: 'rgb(255, 99, 132)',
                        usePointStyle: false,
                        boxWidth:15,
                    },
                },
            };
        },
        convertRawData:function(rawData){
            console.log("rawdata",rawData.length)
            var xName = 'name';
            var yName = 'count';            
            var data = [];
            var label =  [];

            rawData.forEach((dt) => {                
                label.push(dt.name);
                data.push(dt.count);
            });

            var result = {
                labels:label,
                datasets: [
                    {   
                        label: "Diagnosis",
                        fill: true,
                        backgroundColor: "rgba(255,99,132,0.2)",
                        borderColor: "rgba(255,99,132,1)",
                        pointBorderColor: "#fff",
                        pointBackgroundColor: "rgba(255,99,132,1)",
                        pointBorderColor: "#fff",
                        data: data,
                        //backgroundColor:[ 'blue','green','orange','pink','violet', 'yellow'],
                    },
                ]
            };
            return result;
        }
	});
});