/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/ClusteredBarChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase",
    "ddm/fluwiz/chart/base/_dataParser"], 
	function(declare,array, chartjsbase, dataParser) {

	var base = [chartjsbase, dataParser];

	return declare("ddm.fluwiz.chart.chartjs.ClusteredBarChart", base, {
       	getChartType:function(){
            return 'bar';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{
                                // boxWidth:'10',
                                ticks: {
                                    beginAtZero:true
                                },
                                scaleLabel:{
                                    display:false, labelString: "na"
                                }}]
                        , xAxes:[{
                            scaleLabel:{
                            display:false, labelString: "na"
                            }, ticks: { 
                                display: true 
                                },
                            }]},
                legend:{
                    display: true,
                    labels: {
                        boxWidth:10,
                    }
                }
            };
        }
	});
});