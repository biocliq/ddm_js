/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/SingleBarChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase",
    "ddm/fluwiz/chart/base/_dataParser"], 
	function(declare,array, chartjsbase, dataParser) {

	var base = [chartjsbase, dataParser];

	return declare("ddm.fluwiz.chart.chartjs.SingleBarChart", base, {     
       	getChartType:function(){
            return 'bar';   
        },
        initChartOptions:function(){
            this.chartOptions =  {
                scales: {yAxes: [{                                
                                ticks: {
                                    beginAtZero:true
                                },scaleLabel:{
                                    display:false, labelString: "na"
                                }}]
                        , xAxes:[{
                            scaleLabel:{
                                display:false, labelString: "na"
                                }                                
                                }
                            ]},
                legend:{
                    display: false,
                },
                tooltips: {
                    enabled: true
                },
                hover: {
                    animationDuration: 0
                },
                animation: {
                    easing : "easeInSine",
                    onComplete: function () {
                        var chartInstance = this.chart;
                        ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            if(! meta.hidden){
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x, bar._model.y -5);
                                });
                            }
                        });
                    }
                }
            };
        },
        getIndex:function(/* value*/ value, /*list*/ list){            
            for(var i =0; i < list.length; i++){
                if(list[i] == value)
                    return i;
                if(value == undefined && list[i] == '')
                    return i;
            }
            return -1;
        },
        convertRawData:function(rawData){            
            var xName = this.options.xName || 'status';
            var xLabel = this.options.xLabel || this.options.xName || 'status';
            var yName = this.options.yName || 'cnt';

            var data = [];
            var labels = [];
            var values = [];
            
            var bgColor = [];

            if(rawData){
                rawData.forEach((dt) => {
                    var xValue = dt[xLabel];
                    labels.push(xValue);
                    values.push(dt[yName]);
                });
            }            
            bgColor = this.options.colors || bgColor;
            var dataSets = [];

            dataSets = [{                
                data: values,
                backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)',
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
                ],
                borderWidth: 1
              }];

            var result = {
                labels:labels,
                datasets: dataSets
            };
            return result;
        }
	});

});