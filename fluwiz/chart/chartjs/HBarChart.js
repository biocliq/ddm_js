/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/HBarChart", [
    "dojo/_base/declare",
    "ddm/fluwiz/chart/base/_chartjsbase"],
    function (declare, ContentPane) {

        var base = [ContentPane];

        return declare("ddm.fluwiz.chart.chartjs.HBarChart", base, {
            getChartType: function () {
                return 'horizontalBar';
            },
            initChartOptions: function () {
                this.chartOptions = {
                    indexAxis: 'y',
                    scales: {
                        yAxes: [{
                            ticks: {
                                display: true, beginAtZero: true,
                                callback: function (value) {
                                    if (value)
                                        return value.substr(0, 40);
                                    return value;
                                }
                            }
                        }]
                        ,
                        xAxes: [{
                            ticks: {
                                display: true, beginAtZero: true,
                                callback: function (val, index) {
                                    return val % 1 === 0 ? val : '';
                                }
                            }
                        }]
                    },
                    legend: {
                        display: false,
                        labels: {
                            boxWidth: 1,
                        },
                    },
                };

                this.chartOptions.legend = this.mix(this.options.legend, this.chartOptions.legend);

                if (this.options.scale) {
                    if (this.options.scale.hideYLabel) {
                        this.chartOptions.scales.yAxes[0].ticks.callback = function (value) {
                            return '';
                        };
                    }
                }
            },
            mix: function (newValue, oldValue) {
                if (newValue)
                    return { ...oldValue, ...newValue };
            },
            convertRawData: function (rawData) {
                console.log(rawData);
                var xName = this.options.xName || 'date';
                var yName = this.options.yName || ['total', 'used'];
                var data = [];
                for (var i = 0; i < yName.length; i++) {
                    data[i] = new Array();
                }
                var legend = this.options.yLabel || yName;

                var label = [];
                var bgColor = [];
                if (rawData) {
                    rawData.forEach((dt) => {
                        label.push(dt[xName]);
                        for (var i = 0; i < yName.length; i++) {
                            data[i].push(dt[yName[i]]);
                            bgColor.push(this.getRandomColor(dt[xName] + i));
                        }
                    });
                }

                if(this.options.colorArray){
                    bgColor = this.getColorArray(this.options.colorArray, 0.6, bgColor.length);
                }else
                    bgColor = this.options.backgroundColor || bgColor;

                var dataSets = [];

                for (var i = 0; i < data.length; i++) {
                    dataSets.push({
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: data[i],
                        borderWidth: 2,
                        maxBarThickness: 60,
                        borderRadius: 10,
                        backgroundColor: bgColor[i],
                        borderSkipped: false,
                        label: legend[i]
                    });
                }

                console.log(label);

                var result = {
                    labels: label,
                    datasets: dataSets
                };
                return result;
            }
            // convertRawData:function(rawData){
            //     var xName = this.options.xName || 'name';
            //     var yName = this.options.yName || 'total';

            //     var data = [];
            //     var label =  [];
            //     var bgColor = [];

            //     if(rawData)
            //         rawData.forEach((dt) => {                   
            //             label.push(dt[xName]);
            //             data.push(dt[yName]);
            //             bgColor.push(this.getRandomColor());
            //         });

            //     bgColor = this.options.backgroundColor || bgColor;                
            //     var result = {
            //         labels:label,
            //         datasets: [
            //             {
            //                 fillColor: "rgba(151,249,190,0.5)",
            //                 strokeColor: "rgba(255,255,255,1)",
            //                 pointColor: "rgba(220,220,220,1)",
            //                 pointStrokeColor: "#fff",
            //                 pointHighlightFill: "#fff",
            //                 pointHighlightStroke: "rgba(220,220,220,1)",
            //                 data: data,
            //                 backgroundColor: bgColor //[ "rgba(255,165,0,0.3)","rgba(0,255,0,0.7)", getRandomColor()],
            //             },
            //         ]
            //     };
            //     return result;

            // }
        });
    });