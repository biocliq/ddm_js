/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/DoughnutGaugeChart", [
    "dojo/_base/declare",
    "dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase"],
    function (declare, array, ContentPane) {

        var base = [ContentPane];

        return declare("ddm.fluwiz.chart.chartjs.DoughnutGaugeChart", base, {
            getChartType: function () {
                return 'doughnut';
            },
            textInCenter: function (value, label) {
                var tooltipCanvas = this.chartNode;
                var ctx = tooltipCanvas.getContext('2d');

                var w = tooltipCanvas.width / 2;
                var h = tooltipCanvas.height / 2;

                ctx.clearRect(w, h, tooltipCanvas.width / 8, tooltipCanvas.height / 4)

                ctx.restore();

                // Draw value
                ctx.fillStyle = '#333333';
                ctx.font = '24px sans-serif';
                ctx.textBaseline = 'middle';

                // Define text position
                var textMeasure = ctx.measureText(value);
                var textPosition = {
                    x: Math.round((tooltipCanvas.width - textMeasure.width) / 2),
                    y: Math.round(tooltipCanvas.height * 0.8 - (textMeasure.actualBoundingBoxAscent + textMeasure.actualBoundingBoxDescent))
                };

                ctx.fillText(value, textPosition.x, textPosition.y);

                // Draw label
                ctx.fillStyle = '#AAAAAA';
                ctx.font = '8px sans-serif';

                // Define text position
                var labelTextPosition = {
                    x: Math.round((tooltipCanvas.width - ctx.measureText(label).width) / 2),
                    y: tooltipCanvas.height / 2,
                };

                ctx.fillText(label, labelTextPosition.x, labelTextPosition.y - 20);
                ctx.save();
            },
            initChartOptions: function () {
                var _this = this;
                this.chartOptions = {
                    maintainAspectRatio: true,
                    circumference: Math.PI + 0.6,
                    rotation: -Math.PI - 0.3,
                    cutoutPercentage: 80, // precent                    
                    legend: {
                        display: false
                    },
                    layout: {
                        padding: '5'
                    },
                    animation: {
                        onComplete: function (animation) {
                            var chart = this.chart;
                            var datasets = chart.config.data.datasets;
                            if (datasets.length > 0) {
                                var value = datasets[0].data.reduce(function (a, b) {
                                    return a + b;
                                }, 0);
                                label = '';
                                var progress = datasets[0].data[0];
                                var remain = datasets[0].data[1];
                                var value = 100 * progress / (progress + remain);
                                if (!isNaN(value)) {                                    
                                    value = value.toFixed(1) + '%';
                                    _this.textInCenter(value, label);
                                }else{
                                    _this.textInCenter("No Data", label);
                                }
                            }
                        },
                    },
                    events: [],
                    tooltips: {
                        enabled: false
                    }
                };
            },
            convertRawData: function (rawData) {
                var chartData = [];
                var label = [];
                var bgColor = [];

                var xName = this.options.xName || 'date';
                var yName = this.options.yName || ['total', 'used'];
                var legend = this.options.yLabel || yName;

                var dt = rawData[0];
                for (var i = 0; i < yName.length; i++) {
                    var value = dt[yName[i]];
                    label.push(legend[i]);
                    if (undefined == value)
                        value = 0;
                    chartData.push(value);
                    bgColor.push(this.getRandomColor(dt[xName] + i));
                }

                if (this.options.colorArray) {
                    bgColor = this.getColorArray(this.options.colorArray, 0.7, bgColor.length);
                } else
                    bgColor = this.options.backgroundColor || bgColor;

                var canvas = this.chartNode;
                var w = canvas.width * 7 / 10;
                var h = canvas.height * 8 / 10;
                var gradientBlue = canvas.getContext('2d').createLinearGradient(10, 10, w, h);
                gradientBlue.addColorStop(0, '#F4A574');
                gradientBlue.addColorStop(0.6, '#9787FFdd');
                gradientBlue.addColorStop(1, '#2EaB27ee');

                var gradientGrey = canvas.getContext('2d').createLinearGradient(0, 0, 0, 150);
                gradientGrey.addColorStop(0, '#ffffffff');
                gradientGrey.addColorStop(1, '#efefefdd');

                if (chartData.length > 1) {
                    chartData[1] = chartData[1] - chartData[0];
                }

                var data = {
                    labels: legend,
                    datasets: [
                        {
                            label: "Progress",
                            data: chartData,
                            backgroundColor: [
                                gradientBlue,
                                gradientGrey
                            ]
                        }
                    ]
                };
                return data;
            }
        });
    });