/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/BubbleChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {
	var base = [ContentPane];
	return declare("ddm.fluwiz.chart.chartjs.BubbleChart", base, {        
       	getChartType:function(){
            return 'bubble';   
        },
        initChartOptions:function(){            
            this.chartOptions =  {
            responsive: true,
            title:{
                display: true,
                 text:  "Deer Population- 2015"
            },
            scales: {
             xAxes: [{
                scaleLabel:{
                    display:true,
                    labelString:"Life Expectancy (in Years)",
                    lineHeight:2,
                    fontColor:'black',
                    fontFamily:'Helvetica',
                    fontSize:'12',
                },
            }],
            },
            scales: {
            yAxes: [{
                scaleLabel:{
                    display:true,
                    labelString:"Fertility Rate",
                    lineHeight:2,
                    fontColor:'black',
                    fontFamily:'Helvetica',
                    fontSize:'12',
                },
            }],
            },
            legend:{
                display: true,
                labels: {
                    usePointStyle: true,
                }
            },
            }
        },
        convertRawData:function(rawData){
            console.log(rawData);
            var data = {
                datasets: [
                    {
                        label: ['Deer Population'],
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [{ x: 100,y: 0,r: 10 }, {x: 60,y: 30,r: 20}, {x: 40,y: 60,
                        r: 25}, {x: 80,y: 80,r: 50}, {x: 20,y: 30,r: 25}, {x: 0,y: 100,
                        r: 5}],
                        backgroundColor:[  'black','blue','pink','grey','purple'
                        ],
                    },
                ]
            };
             return data;
        }
	});
});