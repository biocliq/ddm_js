/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/DoughnutChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/base/_chartjsbase"], 
	function(declare,array, ContentPane) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.chartjs.DoughnutChart", base, {
        //xAxisLabel:null,
        // yAxisLabel:null,
       	getChartType:function(){
            return 'doughnut';   
        },
        getChartOptions:function(){
            // var options = {};
            // options.scales = {};
            // options.scales.xAxes = this.getXAxisOptions();
            return {responsive: true,
                title: {
                    display: true,
                    position: "bottom",
                    text: "Doughnut Chart",
                    fontSize: 18,
                    fontColor: "blue"
                },
        }
    },
        getData:function(){
            var data = {
                labels: ["match1", "match2", "match3", "match4", "match5"],
        datasets: [
            {
                label: "TeamA Score",
                data: [10, 50, 25, 70, 40],
                backgroundColor: [
                    "#DEB887",
                    "#A9A9A9",
                    "#DC143C",
                    "#F4A460",
                    "#2E8B57"
                ],
                borderColor: [
                    "#CDA776",
                    "#989898",
                    "#CB252B",
                    "#E39371",
                    "#1D7A46"
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
            };
                return data;
        }
	});
});