/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/chartjs/TimeLineChart", [
	"dojo/_base/declare",
	"dojo/_base/array",
    "ddm/fluwiz/chart/base/_chartjsbase",
    "ddm/fluwiz/chart/base/_dataParser"], 
	function(declare,array, _chartjsbase,_dataParser) {

	var base = [_chartjsbase,_dataParser];
    var timeFormat = 'MM/DD/YYYY HH:mm';

	return declare("ddm.fluwiz.chart.chartjs.TimeLineChart", base, {        
       	getChartType:function(){
            return 'line';   
        },      
        initChartOptions:function(){
            this.chartOptions = { legend:{
                        display: true,
                        labels: {
                            usePointStyle:true
                        },
                    },
                    scales: 
                        {xAxes: [{
                                    type: 'time',
                                    time: {
                                        parser: timeFormat,
                                        // round: 'day',
                                        tooltipFormat: 'll HH:mm'
                                    },
                                    scaleLabel: {
                                        display: false,
                                        labelString: ''
                                    }
                                }],
                        yAxes: [{
                                    stacked:true,
                                    scaleLabel: {
                                    display: false
                                },
                                ticks: {
                                    min: 0,
                                    callback: function(value, index, values) {
                                        if (Math.floor(value) === value) {
                                            return value;
                                        }
                                    }
                                }
                            }]
                        }
                    };
        },
        convertXvalue:function(data, key){
            return new Date(data);
        }
	});

});