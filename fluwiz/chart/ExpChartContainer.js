/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/ExpChartContainer", [
	"dojo/_base/declare",
    "dojo/_base/array",
    "dijit/layout/ContentPane",
	"./_ChartContainer"], 
	function(declare,array,ContentPane, _ChartContainer) {

	// var base = [_ChartContainer, ContentPane];
	var base = [ContentPane, _ChartContainer];
	return declare("ddm.fluwiz.chart.ExpChartContainer", base, {
		chartClass: "ddm/fluwiz/chart/chartjs/LineChart",
       		
	});

});