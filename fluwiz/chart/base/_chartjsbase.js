/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/base/_chartjsbase", [
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dijit/layout/ContentPane",
    "dijit/Fieldset",
    "color/randomColor",
    "chroma/chroma", "dojo/date/locale",
    "ddm/wire/store/widget/ChartStore"],
    function (declare, array, lang, domConstruct, ContentPane, _WidgetBase, randomColor, chroma, locale) {

        var base = [ContentPane];

        return declare("ddm.fluwiz.chart.base._chartjsbase", base, {
            style: "height:200px;",
            getChartType: function () {
                return 'bar';
            },
            getTitle: function () {
                if (this.label) {
                    return {
                        display: true,
                        text: this.label,
                        padding: 5
                    };
                } else {
                    return { display: false };
                }
            },
            setInitialFilter: function () {
                this._filter = {};
                this.initialFilter = {};
            },
            initChartOptions: function () { },
            customizeChart: function () { },
            getChartOptions: function () {
                return this.chartOptions;
            },
            postCreate: function () {
                this.inherited(arguments);
                this.setInitialFilter();
                this.chartOptions = {};
                this.initChartOptions();
                this.setConfigOptions();
                this.options = this.options || {};
                this.chartNode = domConstruct.create("canvas");
                this.domNode.appendChild(this.chartNode);
                var type = this.getChartType();
                var options = this.getChartOptions();

                options.title = this.getTitle();
                options.maintainAspectRatio = false;
                this._attachClickEvent(options);

                this.chart = new Chart(this.chartNode,
                    {
                        type: type,
                        options: options
                    });

                this.customizeChart();

                if (this.rawData) {
                    var data = this.convertRawData(this.rawData);
                    this.setData(data);
                }
                else
                    this.refresh();
            },

            resetZoom: function () {
                if (this.chart && this.chart.resetZoom) {
                    this.chart.resetZoom();
                }
            },

            _attachClickEvent: function (options) {
                if (this.config.drilldown) {
                    options.onClick = lang.hitch(this, function (evt, item) {
                        var elem = this.chart.getElementAtEvent(evt);
                        var options = { grid: this.config.drilldown };
                        if (elem[0]) {
                            let index = elem[0]["_index"];
                            var dsIndex = elem[0]["_datasetIndex"];
                            let xValue = elem[0]["_chart"].data.labels[index];
                            let yValue = elem[0]["_chart"].data.datasets[dsIndex].data[index];
                            let yColumn = elem[0]["_chart"].data.datasets[dsIndex].yColumn;
                            let xColumn = elem[0]["_chart"].data.datasets[dsIndex].xColumn;
                            let label = elem[0]["_chart"].data.datasets[dsIndex].label;

                            var genFilter = this.getFilterData(yColumn, yValue, xColumn, xValue, label);
                            genFilter = this.remapFilter(genFilter);
                            var drillFilter = declare.safeMixin(this._filter, genFilter);
                            options.filter = drillFilter;
                            options = this.configureDrilldownOptions(options, elem[0]);
                            this.handleOnClick(options);
                        }
                    });
                }
            },

            configureDrilldownOptions:function(){

            },

            remapFilter:function(drillFilter){
                var drilldown = this.config.drilldown;
                var options = this.options;                
                if(drilldown){
                    var filterRemap = options.filterRemap;
                    if(filterRemap){
                        if(drillFilter){
                            this.reassign(drillFilter, filterRemap, 'yColumn');
                            this.reassign(drillFilter, filterRemap, 'XColumn');
                            this.reassign(drillFilter, filterRemap, 'yValue');
                            this.reassign(drillFilter, filterRemap, 'xValue');
                            this.reassign(drillFilter, filterRemap, 'label');
                        }                        
                    }
                }
                return drillFilter;
            },

            reassign:function(value, lookup, attrib){
                var newAttrib = lookup[attrib]
                if(value[attrib] && newAttrib){
                    value[newAttrib] = value[attrib];
                    delete value[attrib];
                }
            },

            getFilterData: function (yColumn, yValue, xColumn, xValue, label) {
                var drillFilter = {};
                if (yColumn)
                    drillFilter.yColumn = yColumn;
                if (yValue)
                    drillFilter.yValue = yValue;
                if (xColumn)
                    drillFilter.xColumn = xColumn;
                if (xValue)
                    drillFilter.xValue = xValue;
                if (label)
                    drillFilter.label = label;
                return drillFilter;
            },

            // ** Will be called on mouse click
            handleOnClick: function (options) {
                console.log("Handle Click called");
                console.log(options);
            },

            refresh: function () {
                this.store.query(this._filter).then(lang.hitch(this, function (result) {
                    // this.rawData = result.data;
                    var data = this.convertRawData(result);
                    this.setData(data);
                }));
            },
            /**
             * To be implemented by all subclasses.
             * This function should take the raw data (from the server) and convert to 
             * chart specific data format.
             */
            convertRawData: function (rawData) { },
            setConfigOptions: function () { },
            resize: function () {
                this.inherited(arguments);
                this.chart.resize();
            },
            destroy: function () {
                this.chart.destroy();
                delete this.chart;
                domConstruct.destroy(this.chartNode);
                this.inherited(arguments);
            },
            setFilter: function (filter) {
                this._filter = lang.clone(this.initialFilter);
                declare.safeMixin(this._filter, filter);
                this.refresh();
            },
            _isSameArray: function (oldArray, newArray) {
                if (oldArray.length == newArray.length) {
                    return array.every(newArray, function (label, i) {
                        return label == oldArray[i++];
                    });
                }
                return false;
            },
            setData: function (data) {
                if (data) {
                    var chart = this.chart;
                    var i = 0;
                    var oldLabels = chart.data.labels;
                    var oldDataSets = chart.data.datasets;

                    var isDataRefreshOnly = this._isSameArray(oldLabels, data.labels) &&
                        this._isSameArray(oldDataSets, data.datasets);

                    if (isDataRefreshOnly) {
                        for (var i = 0; i < chart.data.datasets.length; i++)
                            chart.data.datasets[i].data = data.datasets[i].data;
                    } else {
                        if (chart.data.labels && data.labels)
                            chart.data.labels = data.labels;
                        if (chart.data.datasets && data) {
                            chart.data.datasets = data.datasets;
                        }
                    }
                    chart.update();
                } else {
                    this.removeData();
                }
            },
            addData: function (label, data) {
                var chart = this.chart;
                chart.data.labels.push(label);
                chart.data.datasets.forEach((dataset) => {
                    dataset.data.push(data);
                });
                chart.update();
            },
            removeData: function () {
                var chart = this.chart;
                if (chart.data.labels)
                    chart.data.labels.pop();

                chart.data.datasets.forEach((dataset) => {
                    dataset.data.pop();
                });
                chart.update();
            },
            getRandomColor: function (value) {
                var color = this._getColor(value);

                // var letters = '0123456789ABCDEF'.split('');
                // var color = '#';
                // for (var i = 0; i < 6; i++ ) {
                //     color += letters[Math.floor(Math.random() * 16)];
                // }
                color += "AF";
                return color;
            },
            getColorArray: function (range, opacity, noColors) {
                var result = chroma.scale(range).colors(noColors);
                for (i = 0; i < result.length; i++) {
                    result[i] = chroma(result[i]).alpha(opacity).hex('rgba');
                }
                return result;
            },
            _getColor: function (value) {
                if (value && typeof value === 'string') {
                    switch (value.toLowerCase()) {
                        case 'completed':
                        case 'success':
                            return "#7BE3B0";
                        case 'failed':
                        case 'fail':
                        case 'failure':
                            return "#D881CC";
                        case 'inprogress':
                        case 'progress':
                            return "#F4EB6B";
                        default:
                            return randomColor({
                                hue: 'random'
                            });
                    }
                }
                return randomColor({
                    hue: 'random'
                });
            },
            getRandomBgColor: function () {
                var letters = '0123456789ABCDEF'.split('');
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                color += "18";
                return color;
            }

        });

    });
