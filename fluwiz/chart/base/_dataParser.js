/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/base/_dataParser", [
	"dojo/_base/declare",
	"dojo/_base/array"], 
	function(declare,array) {

	var base = [];

	return declare("ddm.fluwiz.chart.chartjs.base._dataParser", base, {       	
        convertRawData:function(rawData){           
            var xAxisLabels =  this.parseXAxisLabels(rawData);
            var dataSets = this.parseMultiDataSets(rawData);
            
            var result = {
                labels:xAxisLabels,
                datasets: dataSets
            };
            return result;
        },

        setConfigOptions:function(){
            var options = this.options;
            try{
            if(options && options.xAxis && options.xAxis.scaleLabel) {
                var chartScaleLabel = this.chartOptions.scales.xAxes[0].scaleLabel;
                chartScaleLabel.display = true;
                chartScaleLabel.labelString = options.xAxis.scaleLabel;
            }
            if(options && options.xAxis && options.yAxis.scaleLabel) {
                var chartScaleLabel = this.chartOptions.scales.yAxes[0].scaleLabel;
                chartScaleLabel.display = true;
                chartScaleLabel.labelString = options.xAxis.scaleLabel;
            }

        }catch (error ){            
            console.log(error);
        }
        },

        parseXAxisLabels:function(rawData){
            var key;
            var options = this.options;
            if(options && options.xAxis && options.xAxis.dataKey) {
                key = options.xAxis.dataKey;
            }else
                key = "name";
            var xAxisLabels =  [];
            var value;
            rawData.forEach((dt) => {                
                value =  this.convertXvalue(dt[key], key);
                xAxisLabels.push(value);
            });  
            return xAxisLabels;
        },

        convertXvalue:function(data, key){
            return data;
        },

        convertYvalue:function(data, key){
            return data;
        },

        parseMultiDataSets:function(rawData){
            var yAxis;
            var data = [];
            var dataSets = [];
            var dataKey;
            var label;
            var colOptions;

            var colorOptions = {fillColor: "rgba(151,249,190,0.5)",
            strokeColor: "rgba(255,255,255,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)"}

            var options = this.options;

            if(options && options.yAxis && Array.isArray(options.yAxis)){
                yAxis = options.yAxis;
            }else{
                yAxis = [{"dataKey" : "count",
                        "backgroundColor" : this.getRandomColor("count"),//"skyblue",
                        "label" : "data"}]
            }

            yAxis.forEach((yCol) =>{
                data = [];
                colOptions = {};
                label = undefined;
                dataKey = yCol.dataKey;
                if(!yCol.backgroundColor)
                    yCol.backgroundColor = this.getRandomColor(dataKey);

                if(yCol.label)
                    label = yCol.label;
                
                if(label == undefined)
                    label = dataKey;

                var value;
                rawData.forEach((dt) => {                
                    value =  this.convertYvalue(dt[dataKey], dataKey);
                    data.push(value);
                });
                
                colOptions = yCol ;
                colOptions.data = data;
                colOptions.label = label;
                colOptions.stack = true;
                colOptions.borderWidth = 1;
                dataSets.push(colOptions);                  
            });

            return dataSets;
        }
	});
});