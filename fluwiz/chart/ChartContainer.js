/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


 define("ddm/fluwiz/chart/ChartContainer", [
	"dojo/_base/declare","dojo/_base/array","dojo/_base/lang",
	"dojo/topic","ddm/zui/page/base/Loader", 
	"ddm/zui/layout/FlowLayoutContainer","ddm/wire/store/widget/ChartStore"], 
	function(declare,array,lang,topic, Loader, 
		FlowLayoutContainer, ChartStore) {

	var base = [Loader,FlowLayoutContainer];

	return declare("ddm.fluwiz.chart.ChartContainer", base, {	
		style:"height:200px",
		autoHeight:true,
		startup:function(){			
			this.wChildren = [];
			var chartLayout = this.data.chartLayout;
			var charts = chartLayout.charts;
			
			if(this._initialized)
				return;
			this._initialized = true;

			this.inherited(arguments);

			for(var i in charts){
				var chart = charts[i];
				this.createChart(chart);
			}

			this.own(
				topic.subscribe("chart/setFilter/" + this.page + "/" + this.itemId,
					lang.hitch(this, function(filter ){
						return array.forEach(this.wChildren, function(widget){							
							if(widget.setFilter)
								widget.setFilter(filter);			
						});
					})
				)
			);
		}, 
		
		createChart:function(chart){			
			var renderer = chart.renderer || chart.reference;
			var storeOptions = {page: this.page, chartCode:chart.code, itemId:this.itemId, dataSource:chart.dataSource};			
			var store = new ChartStore(storeOptions);
			var options = {store:store, label:chart.label, options : chart.options};
			if(chart.height) options.heightPercent = chart.height;
			if(chart.width) options.widthPercent = chart.width;
			var childList = this.wChildren;
			options.config = chart;

			if(chart.drilldown){
				options.handleOnClick = lang.hitch(this, function(options){
					options.page = this.page;
					options.itemId= this.itemId;
					options.container = this.name;
					options.renderer = "ddm/fluwiz/chart/GridContainer";
					console.log(options);
					topic.publish("/chart/popup/grid", options);
				});
			}
			var _this = this;
			this.createWidget(renderer, options,
				function(widget){
					childList.push(widget);					
					widget.startup();
					_this.addChild(widget);
					_this.layout();
				});
		},
		resume:function(){
			array.forEach(this.wChildren, function(widget){
				try{
					if(widget.resume)
						widget.resume();
				}catch(err){console.error(err);}
			});
		},
		pause:function(){
			array.forEach(this.wChildren, function(widget){
				try{
					if(widget.pause)
						widget.pause();
				}catch(err){console.error(err);}
			});
		},
		refresh:function(){
			array.forEach(this.wChildren, function(widget){
				try{
					if(widget.refresh)
						widget.refresh();
				}catch(err){console.error(err);}
			});
		},
		layoutChildren: function(/*DomNode*/ container, /*Object*/ dim, /*Widget[]*/ children){
			var sdim = this.marginBox2contentBox(container);			
			var _this = this;						
			array.forEach(children, function(child){				
				var width = child.widthPercent ? child.widthPercent : 100;
				var height = child.heightPercent ? child.heightPercent : 100;

				if(isNaN(height)){					
					height = height.replace(/\D+/g, "");						
				}
				
				width = (sdim.w) * width / 100;				
				height = sdim.h * height / 100;					
				_this.size(child, {w: (width - 3), h:height});	
			});
        }
	});

});
