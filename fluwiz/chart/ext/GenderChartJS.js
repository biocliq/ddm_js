/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/ext/GenderChartJS", [
	"dojo/_base/declare",
	"dojo/_base/array",
	"ddm/fluwiz/chart/chartjs/BarChart"], 
	function(declare,array, ContentPane) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.chart.ext.GenderChartJS", base, {       	
        convertRawData:function(rawData){   
            var data = [0,0,0];
            var label =  ['Male','Female','Transg'];
            var idx = 0;
            if(rawData)
                rawData.forEach((dt) => {
                    idx = label.indexOf(dt.name);
                    data[idx] += dt.count;
                });

            var result = {
                labels:label,
                datasets: [
                    {
                        fillColor: "rgba(151,249,190,0.5)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: data,
                        backgroundColor:["rgba(0,255,0,0.3)", "rgba(251, 85, 85, 0.4)","rgba(255,165,0,0.3)"],
                    },
                ]
            };
            return result;
        }
	});
});