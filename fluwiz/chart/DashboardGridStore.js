/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/fluwiz/chart/DashboardGridStore", [ "dojo/_base/declare","dojo/_base/lang",
		"dojo/_base/xhr",
		 "ddm/wire/store/base/JsonRestStore", "ddm/util/config" ], function( declare,lang,
		 xhr, JsonRest, config) {

	var base = JsonRest;

	return declare("ddm.fluwiz.chart.DashboardGridStore", base, {		
		constructor:function(options){			
			this.defaultFilter = options.defaultFilter || {};			
			
			if(options.idProperty)
				this.idProperty = options.idProperty;
			console.log(options);
			var targetUrl;
			if(options.itemId)
				targetUrl = config.url.expChartData
			else
				targetUrl = config.url.chartData;

			options.target = config.replace(targetUrl,
				{pageMenu : options.pageMenu, ciId:options.itemId,
				chartCode:options.dataSource}
			);
			this.inherited(arguments);
		},
		getTableLayout:function()		{
			
		},
		hasChildren:function(id, object){
			return false;
		},
		query:function(){
			arguments[0] = lang.mixin(arguments[0], this.defaultFilter);
			this.lastFilter = arguments[0];
			this.lastOptions = arguments[1];
			console.log(arguments);
			return this.inherited(arguments)
		},
		getXlsReport(){
			this.getReport('excel');
		},
		getReport:function(format){
			var url = config.url.getChartReportURL(this.pageMenu, this.dataSource, this.ciId, format);

			var hasQuestionMark = url.indexOf("?") > -1;
			query = xhr.objectToQuery(this.lastFilter);
			query = query ? (hasQuestionMark ? "&" : "?") + query: "";
			
			var options = this.lastOptions;
			if(options && options.sort){
				var sortParam = this.sortParam;
				query += (query || hasQuestionMark ? "&" : "?") + (sortParam ? sortParam + '=' : "sort(");
				for(var i = 0; i<options.sort.length; i++){
					var sort = options.sort[i];
					query += (i > 0 ? "," : "") + (sort.descending ? this.descendingPrefix : this.ascendingPrefix) + encodeURIComponent(sort.attribute);
				}
				if(!sortParam){
					query += ")";
				}
			}
			url = url + query;			
			window.open(url, '_blank');
		}	
	});
});
