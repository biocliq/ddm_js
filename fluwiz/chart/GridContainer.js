/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


 define("ddm/fluwiz/chart/GridContainer", [
	"dojo/_base/declare","dojo/_base/array","dojo/_base/lang",
	"dojo/topic","ddm/zui/page/base/Loader", 
	"ddm/zui/layout/FlowLayoutContainer","ddm/wire/store/widget/ChartStore"], 
	function(declare,array,lang,topic, Loader, 
		FlowLayoutContainer, ChartStore) {

	var base = [Loader,FlowLayoutContainer];

	return declare("ddm.fluwiz.chart.GridContainer", base, {		
		autoHeight:false,
		style:{height:"98%"},
		startup:function(){
			this.wChildren = [];			
			if(this._initialized)
				return;

			this._initialized = true;
			this.inherited(arguments);

			this.createGrid({});
		}, 
		
		createGrid:function(grid){
			var grid = this.grid;
			var renderer = grid.renderer || "ddm/fluwiz/chart/DashboardGrid";
			var childList = this.wChildren;			
			var _this = this;
			var options = grid;
			options.defaultFilter = this.filter;
			if(this.itemId)
				options.defaultFilter.itemId = this.itemId;
			options.itemId = this.itemId;
			options.page = this.page;

			this.createWidget(renderer, options,
				function(widget){
					childList.push(widget);
					widget.startup();
					_this.addChild(widget);
					_this.layout();
				});
		},
		layoutChildren: function(/*DomNode*/ container, /*Object*/ dim, /*Widget[]*/ children){
			var sdim = this.marginBox2contentBox(container);			
			var _this = this;						
			array.forEach(children, function(child){				
				var width = child.widthPercent ? child.widthPercent : 100;
				var height = child.heightPercent ? child.heightPercent : 100;

				if(isNaN(height)){					
					height = height.replace(/\D+/g, "");						
				}
				
				width = (sdim.w) * width / 100;				
				height = sdim.h * height / 100;					
				_this.size(child, {w: (width - 3), h:height});	
			});
        }
	});

});
