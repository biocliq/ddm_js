/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define([
        'dojo/_base/declare',
        'dojo/_base/lang',        
		'dojo/topic',
		"dijit/TitlePane",
		"dijit/layout/AccordionContainer",
		'gridx/modules/Filter',		
		'ddm/gridx/modules/filter/_FilterBar',
		"ddm/zui/form/DropDownButton"
        ], function(declare, lang, topic,TitlePane,AccordionContainer,
        		F, _FilterBar){

	return declare([ _FilterBar], {
		templateString: "<div></div>",
		
		postCreate: function(){
			this.inherited(arguments);
			var grid = this.grid;
			var _this = this;
			
			grid.connect(grid.select.row,'onSelected', function(){
				console.log("selection fired");			
			});

			grid.select.row.onSelected = function(){
			
			}
			
			if(this.titleBar)
				this.titleBar.innerHTML = grid.title;
		}		
	});
});
