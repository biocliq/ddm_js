/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

 /**
  * The simplegrid is mostly used in Explorerpage to display 
  * records with parent / child relationship in grid format
  * (foriegn key relation)
  */
 
  define("ddm/fluwiz/chart/DashboardGrid", [
	"dojo/_base/declare",
	"dojo/topic",
	"dojo/_base/lang",
	'ddm/fluwiz/chart/DashboardGridStore',
	'dojo/store/Cache',
	'ddm/fluwiz/grid/ddmGrid', 
	'gridx/core/model/cache/Async',
	'gridx/modules/SingleSort', 
	'gridx/modules/ColumnResizer',
	'gridx/modules/ColumnWidth',
	'gridx/modules/Filter', 
	"./HiddenFilterBar",
	"gridx/modules/Pagination", 
	"gridx/modules/pagination/PaginationBar",
	"gridx/modules/select/Row", 		
	"gridx/modules/IndirectSelect",
	"gridx/modules/extendedSelect/Row",
	"gridx/modules/RowHeader",
	"dojo/store/Memory",
	"gridx/modules/Edit",
	"gridx/modules/Bar",
	"gridx/modules/CellWidget", 
	"gridx/modules/Filter",
	"ddm/zui/page/base/report/ReportBar",
	"gridx/modules/filter/QuickFilter"], function(
		declare, topic, lang, JsonGridStore, StoreCache, Grid, 
		AsyncCache, Sort, ColumnResizer, ColumnWidth,Filter, HiddenFilterBar,
		Pagination, PaginationBar, SelectRow,
		IndirectSelect, ExSelectRow, RowHeader, Memory,Edit,Bar,CellWidget) {

	var base = Grid;

	return declare("ddm.fluwiz.chart.DashboardGrid", base, {
		root_id : null,
		style:{height:"99%"},
		sys_page : "null",
		filterBarClass:"ddm/zui/page/base/report/ReportBar",
		paginationInitialPageSize:15,
		paginationBarSizes: [15, 25, 50],
		autoHeight:false,
		postMixInProperties : function() {
			var data = this.data;			
			if(null != data && undefined != data)
				isPagination = data.totalCount > 10;

			this.cacheClass = AsyncCache;
			var restStore = new JsonGridStore({
				pageMenu : this.page,
				ciId : this.itemId,
				chartCode: this.chartCode,
				defaultFilter:this.defaultFilter,
				idProperty:this.id,
				dataSource:this.dataSource
			});
			
			this.store = restStore;
			this.inherited(arguments);			
		},
		gridModulesCustomize : function(){
			this.inherited(arguments);			
			if(undefined == this.moduleOptions){
				this.modules.push("gridx/modules/Pagination", "gridx/modules/pagination/PaginationBar");
			}
		},
		focus:function(){
			console.log("focus called");
		},
		evtCellDblClick : function(e){
			dojo.stopEvent(e);
		}, 
		postCreate:function(){
			this.inherited(arguments);
		},
		destroy:function(){
			if(this.topicHandle){
				this.topicHandle.remove();
			}
			this.inherited(arguments);
		}
	});

});
