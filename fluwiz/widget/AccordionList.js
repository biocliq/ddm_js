/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/fluwiz/widget/AccordionList", 
        ["dojo/_base/declare", "dojo/topic", "dojo/router",
        'dojo/_base/lang',"dijit/layout/ContentPane", "dijit/Tree",
        "ddm/util/config","ddm/util/util",
        "ddm/wire/store/grid/JsonGridTreeStore", "ddm/zui/widget/ListNode"], 
        function(declare,topic,router, 
            lang,ContentPane,Tree,
            config,util,
            JsonGridTreeStore, Node) {

	var base = [Tree];

	return declare("ddm.fluwiz.widget.AccordionList", base, {
        resize:function(){},
        displayFormat : "tbd - {id}",
        title : "root",
        baseClass:"AccordionList",
        showRoot : false,
        betweenThreshold: "5",
        _createTreeNode: function(args) {
            return new Node(args);
        },
        getIconClass : function(item, opened){                  
            return item.icon;
        },	
        getRowClass : function(item, opened){
            if(item.rootNode)
                return "listRootNode";
        },
        onClick : function(item, node, event) {
            if(item.hasChild)
                this._onExpandoClick({node:node});
            dojo.stopEvent(event);
            var self = this;
            var callback = function(item, status){				
                topic.publish("/popup/summary/refresh/"+ self.itemId + "/" + self.tgtPageMenu);
            }			
            topic.publish("/popup/page/edit/",this.tgtPageMenu, item.id, {}, callback);
        },
        refreshModel: function () {
			this._itemNodesMap = {};
			this.model.root = null;
			if (this.rootNode) {
				this.rootNode.destroyRecursive();
			}
			this._load();
		},
		postMixInProperties:function(){
            this.inherited(arguments);
            var labelFormat = this.displayFormat;
            var rootLabel = this.title;
            if(undefined == this.model){
                var model = new JsonGridTreeStore({
                    fields:this.fields,
                    pageMenu : this.pageMenu,
                    ciId : this.itemId,
                    containerId : this.containerId,
                    getLabel:function(object){
                        var _object = {};                    
                        var idx;
                        var field;
                        if(object && labelFormat){
                            if(object.label)
                                return object.label;                    
                            for(idx in this.fields){
                                field = this.fields[idx];
                                if('DATE' == field.displayFormat){
                                    if(object[field.name]){
                                        _object[field.name] = util.calcAge(object[field.name]);
                                    }
                                }else
                                    _object[field.name] = object[field.name];
                            }
                            return lang.replace(labelFormat, _object);
                        }
                        else
                            return object['id'];
                    }, 
                    destroy:function(){
                        delete this.fields;
                        this.inherited(arguments);
                    }
                });
                this.model = model;
            }            
		}
	});
});