define('ddm/fluwiz/widget/Address/Address',[
    "dojo/_base/xhr",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/Deferred",
    "dojo/topic",
    "dojo/on",
    "dijit/Destroyable",
    // "ddm/zui/page/base/explorer/_FormContainer",
    "ddm/zui/page/base/explorer/FieldSetContainer",
],function(xhr,declare,lang,array,Deferred,topic,on,Destroyable,FieldSetContainer){
    var base =FieldSetContainer ;
    return declare("ddm.fluwiz.widget.Address.Address",base, {
        postCreate: function() {
            this.flag=1;
            this.inherited(arguments);
            this.own(
                on(this.widgets.countryCode,'change',lang.hitch(this,function(id){
                    this._onChangeCountry(id);
                } ))
                );
            this.own(
                on(this.widgets.state,'Change',lang.hitch(this,function(id){
                    this._onChangeState(id);
                } )) 
                );
            this.own(
                on(this.widgets.district,'change',lang.hitch(this,function(id){
                    this._onChangeDistrict(id);
                } ))
                );   
        },
        _onChangeCountry:function(id){
            var state= this.widgets.state;
            if(id){
                 if(1==this.flag||2==this.flag){
                    this.widgets.district.setValue('null');
                    state.query['country.id']=id;
                    this.widgets.state.setValue(''); 
                    this.flag=2; 
                 }
                  if(5==this.flag){
                     this.flag = 1;
                 }
                 if(4==this.flag){
                     this.flag=3;
                 }
            }
            else{
                this.widgets.state.setValue('');
                this.widgets.district.setValue('');
            }
         },
        _onChangeState:function(id){
            if(id){
               if (2==this.flag){
                    this.widgets.district.setValue('');
                    var district=this.widgets.district;
                    district.query['state.id']=id;
                    this.flag=2;
               }
               if(1==this.flag){
                    var state= this.widgets.state;
                    var countryId = state.item.referenceCIs.country.id;
                    this.widgets.countryCode.setValue(countryId);
                    this.widgets.district.setValue('');
                    var district=this.widgets.district;
                    district.query['state.id']=id;
                    this.flag=4;
                }
                if(3==this.flag){
                    var state= this.widgets.state;
                    var countryId = state.item.referenceCIs.country.id;
                    this.widgets.countryCode.setValue(countryId); 
                    var district=this.widgets.district;
                    district.query['state.id']=id;
                    this.widgets.district.setValue('');   
                }
                if(5==this.flag){
                    var state= this.widgets.state;
                    var countryId = state.item.referenceCIs.country.id;
                    this.widgets.countryCode.setValue(countryId)
                    this.flag=5;
                }
            }
            else{
                this.widgets.district.setValue('');
            }
        },
         _onChangeDistrict:function(id){
             if(id){
                 if(this.flag==2){
                     this.flag=2;
                 }
                if(this.flag==1){
                    var district= this.widgets.district;
                    var stateId = district.item.referenceCIs.state.id;
                    this.flag=5;
                    this.widgets.state.setValue(stateId);
                }
                if(this.flag==3){
                    this.flag=3;
                }
            }
         }
    });
});
