/** 
 * (C) Copyright 2018 {OWNER}.
 *
{LICENSE_TEXT}
 *
 */

define("ddm/fluwiz/widget/List",	[
    'dojo/_base/declare',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin'],
function( declare, _TemplatedMixin, _WidgetsInTemplateMixin) {
    var base = [_TemplatedMixin,_WidgetsInTemplateMixin];

    var ListItem = declare("fluwiz.widget._ListItem", [_Widget, _TemplatedMixin, _CssStateMixin], {
		templateString: template,
		label: "",
		_setLabelAttr: {node: "titleTextNode", type: "innerHTML" },		
		title: "",
		_setTitleAttr: {node: "titleTextNode", type: "attribute", attribute: "title"},
		
		baseClass: "fluwizList",

		getParent: function(){
			return this.parent;
		},

		buildRendering: function(){
			this.inherited(arguments);
			var titleTextNodeId = this.id.replace(' ', '_');
			domAttr.set(this.titleTextNode, "id", titleTextNodeId + "_title");
			this.focusNode.setAttribute("aria-labelledby", domAttr.get(this.titleTextNode, "id"));
			dom.setSelectable(this.domNode, false);
		},

		getTitleHeight: function(){		
			return domGeometry.getMarginSize(this.domNode).h;	// Integer
		},
		
		_onItemClick: function(){			
			var parent = this.getParent();
			parent.selectChild(this.contentWidget, true);
			focus.focus(this.focusNode);
		},

		_setSelectedAttr: function(/*Boolean*/ isSelected){
			this._set("selected", isSelected);
			this.focusNode.setAttribute("aria-expanded", isSelected ? "true" : "false");
			this.focusNode.setAttribute("aria-selected", isSelected ? "true" : "false");
			this.focusNode.setAttribute("tabIndex", isSelected ? "0" : "-1");
		}
	});


    return declare('ddm.fluwiz.widget.List', base, {
        templateString:'<div></div>'

    });
}
);