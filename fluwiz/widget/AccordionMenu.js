/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/fluwiz/widget/AccordionMenu", 
        ["dojo/_base/declare", "dojo/topic", "dojo/router",
        "dijit/layout/ContentPane", "dijit/Tree","ddm/util/config",
        "ddm/wire/store/base/JsonTreeStore", "ddm/zui/widget/MenuNode"], 
        function(declare,topic,router, 
            ContentPane,Tree,config,
            JsonTreeStore, MenuNode) {

	var base = [ContentPane];

	return declare("ddm.fluwiz.widget.AccordionMenu", base, {
		resize:function(){},
		startup:function(){
			this.inherited(arguments);
            var model = new JsonTreeStore({
                target : config.url.menu, 
                rootCode : "root"
            });
				
            var menu = new Tree({
                model : model,
                persist:true,
                cookieName:"cookie_menu_flzw",
                id: "left_Menu",
                showRoot : false,
                betweenThreshold: "5",
                _createTreeNode: function(args) {
                    return new MenuNode(args);
                },
                getIconClass : function(item, opened){                  
                    return item.icon;
                },				
                onClick : function(menuItem, node, event) {                    
                    if (menuItem.action && !menuItem.hasChild){                        
                        router.go("/page/" + menuItem.pageName + "/" + menuItem.action);
                    }
                    if(menuItem.hasChild)
                        this._onExpandoClick({node:node});
                }
            });
			
            this.addChild(menu);
		}
	});
});