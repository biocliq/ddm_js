/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


 define("ddm/wire/store/widget/FieldUniqueStore", 
	[ "dojo/_base/xhr", "dojo/_base/declare",
	"dojo/_base/lang","dojo/_base/array",
	"dojo/Deferred", "../base/JsonRestStore", 'ddm/util/config' ], 
	
	function(xhr, declare,lang,array, 
		Deferred, JsonRest, config) {

	var base = JsonRest;

	return declare("ddm.wire.store.widget.FieldUniqueStore", base, {		
		constructor:function(options){
			if(options.page && options.fieldId)
				options.target = config.replace(config.url.uniqueLookup, 
					{page : options.page, 
					fieldId : options.fieldId}
				);
			this.inherited(arguments);
		}, 
		query:function(query, options, callee){
			if(this.itemId)
				query.__itemId =  this.itemId;
			if(this.parentId)
				query.__parentId = this.parentId;
			return this.inherited(arguments);
		}
	});
});