/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/wire/store/widget/ChartStore", 
[ "dojo/_base/xhr", "dojo/_base/declare",
"dojo/_base/lang","dojo/_base/array",
"dojo/Deferred", "../base/JsonRestStore", 'ddm/util/config' ], 

function(xhr, declare,lang,array, 
    Deferred, JsonRest, config) {

var base = JsonRest;

return declare("ddm.wire.store.widget.ChartStore", base, {		
    constructor:function(options){
        if(options.page){
            var targetUrl;
            if(options.itemId)
                targetUrl = config.url.expChartData
            else
                targetUrl = config.url.chartData;

            options.target = config.replace(targetUrl,
                {pageMenu : options.page, ciId:options.itemId,
                chartCode:options.dataSource}
            );
        }
        this.inherited(arguments);
    }
});
});