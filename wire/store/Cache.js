//ddm/wire/store/Cache

define(["dojo/_base/lang","dojo/when", "ddm/util/util" /*=====, "../_base/declare", "./api/Store" =====*/],
function(lang, when, util /*=====, declare, Store =====*/){

// module:
//		dojo/store/Cache

var Cache = function(masterStore, cachingStore, options){
	options = options || {};
	return lang.delegate(masterStore, {
		deleteQueue:[],
		updateQueue:[],
		query: function(query, directives){
			var result = cachingStore.query(query, directives);
			if(result.total > 0)
				return result;

			var results = masterStore.query(query, directives);
			results.forEach(function(object){
				if(!options.isLoaded || options.isLoaded(object)){
					cachingStore.put(object);
				}
			});
			return results;
		},
		// look for a queryEngine in either store
		queryEngine: masterStore.queryEngine || cachingStore.queryEngine,
		get: function(id, directives){
			console.log('querying for ' + id);
			return when(cachingStore.get(id), function(result){
				return result || when(masterStore.get(id, directives), function(result){
					if(result){
						cachingStore.put(result, {id: id});
					}
					return result;
				});
			});
		},
		add: function(object, directives, temp){
			object.action=true;
			cachingStore.put(object, directives);
			return object;
		},
		put: function(object, directives){		
			object.action=true;
			cachingStore.put(result && typeof result == "object" ? result : object, directives);
			return object;
			// first remove from the cache, so it is empty until we get a response from the master store
			// cachingStore.remove((directives && directives.id) || this.getIdentity(object));
			// return when(masterStore.put(object, directives), function(result){
			// 	// now put result in cache
			// 	cachingStore.put(result && typeof result == "object" ? result : object, directives);
			// 	return result; // the result from the put should be dictated by the masterStore and be unaffected by the cachingStore
			// });
		},
		remove: function(id, directives){
			if(!(typeof id === 'string' && id.startsWith('id_')))
				this.deleteQueue.push(id);
			return cachingStore.remove(id, directives);

			// return when(masterStore.remove(id, directives), function(result){
			// 	return cachingStore.remove(id, directives);
			// });
		},
		evict: function(id){
			return cachingStore.remove(id);
		}, 
		getChanges:function(){
			var result = [];
			util.forEach(this.deleteQueue, function(id){
				result.push({id:id, action:'delete'});
			})
			if(cachingStore.data){
				util.forEach(cachingStore.data, function(data){
					if(data.action){
						var d = lang.clone(data);
						if(d.id && typeof d.id === 'string'){
							delete d.id;
							d.action="create";
						}else
							d.action="update";
						result.push(d);
					}
				})
			}
			console.log(result);
			return result;
		}
	});
};
lang.setObject("ddm.wire.store.Cache", Cache);

return Cache;
});
