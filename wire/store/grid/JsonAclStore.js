/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/wire/store/grid/JsonAclStore", [ "dojo/_base/declare",
		 "../base/JsonRestStore", "ddm/util/config" ], function( declare,
		 JsonRest, config) {

	var base = JsonRest;

	return declare("ddm/wire/store/grid/JsonAclStore", base, {		
		constructor:function(options){
            this.pageMenu = options.pageMenu;
			this.aclType = options.aclType;
			this.ciId = options.ciId;
			if(undefined != options.ciId && null != options.ciId)
			{
				options.target = "v2/page/" + options.pageMenu + "/explorer/container/"+options.sid+"/"+  
					options.aclType+"/"+options.ciId+"/table/data";
			}
			else
			{
				options.target = "v2/page/" + options.pageMenu + "/explorer/container/"+options.sid+"/"+  
					options.aclType+"/";
			}
		this.inherited(arguments);
		},
		put: function(object, options) {
			return dojo.store.JsonRest.prototype.put.apply(this, arguments);
		},
		// getTableLayout:function()
		// {
		// 	var url = config.url.getGridLayoutURL(this.pageMenu, this.containerId, this.ciId);
		// 	return this.gget(url);
		// },
		hasChildren:function(id, object){
			return false;
		},
		_getChildren: function(object) {
			if(object.code)
				return this.get(object.code + "/children");
			else
				return this.get("children");
		},
		getRoot: function(onItem, onError) {
			var _t = this;
			this.get().then(function(items) {
				
				var root = {label:"others"};
                            root.children = items;	
                            root.hasChild = true;
                            root.rootNode = true;
                onItem(root);
            }, onError);
		},
		getLabel: function(object) {
			return object.name;
		},
		getChildren: function(object, onComplete, onError) {
			this._getChildren(object).then(function(fullObject) {
				object.children = fullObject.children;
				object.hasChild = true;
				onComplete(object.children);
			}, function(error) {
				object.children = [];
				onComplete([]);
			});
		},
		mayHaveChildren: function(object) {
			return object.hasChild;
		},
		refreshChildren: function(object) {
			var _t = this;
			this._getChildren(object).then(function(fullObject) {
				object.children = fullObject;				
				_t.onChildrenChange(object, object.children);
			});
		},
		reload: function () {
			// delete the tree's itemNodes
			this._itemNodesMap = {};
			
			// reset the state of the rootNode
			//this.rootNode = "UNCHECKED";
			console.log(this.rootCode)
			// unset the tree.model's root-children
			//this.model.root.children = null;
			
			// unset the JsonRestStore's cache 
			// storeTarget = this.model.store.target;
			// for (var idx in dojox.rpc.Rest._index) {
			// 	if (idx.match("^" + storeTarget)) {
			// 		delete dojox.rpc.Rest._index[idx];
			// 	}
			// }
			
			// remove the rootNode 
			if (this.rootCode) {
				//this.rootCode.destroyRecursive();
			}
			
			// rebuild the tree
			this._load();
		} 
	});
});