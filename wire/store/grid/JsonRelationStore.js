/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/wire/store/grid/JsonRelationStore", [ "dojo/_base/declare","dojo/_base/lang",
		"dojo/_base/xhr",
		 "../base/JsonRestStore", "ddm/util/config" ], function( declare,lang,
		 xhr, JsonRest, config) {

	var base = JsonRest;

	return declare("ddm/wire/store/grid/JsonRelationStore", base, {
		constructor:function(options){
			this.pageType = options.pageType;
			this.pageMenu = options.pageMenu;			
			this.defaultFilter = options.defaultFilter || {};			
			this.containerName = options.containerName;
			if(options.idProperty)
				this.idProperty = options.idProperty;				
			this.ciId = options.ciId;
			
			if(!options.target){				
				this.pageType = "relation";
				options.target = config.url.getRelationDataURL(this.pageMenu, this.containerName, this.ciId);				
			}
			this.inherited(arguments);
		},
		addRelation:function(id){
			var url = this.target + "/" + id;
			return this.gput(url);
		}, 
		deleteRelation:function(id){
			var url = this.target + "/" + id;
			return this.gdelete(url);
		},
		query:function(){
			arguments[0] = lang.mixin(arguments[0], this.defaultFilter);
			this.lastFilter = arguments[0];
			this.lastOptions = arguments[1];			
			return this.inherited(arguments)
		}
	});
});