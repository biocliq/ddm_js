define("ddm/wire/store/grid/ACLCitStore", [ "dojo/_base/declare",
		 "../base/JsonRestStore" ], function( declare,
		 JsonRest) {

	var base = JsonRest;

	return declare("ddm.wire.store.grid.ACLCitStore", base, {		
		idProperty:"citId",
		constructor:function(options){
			var _target = "../v2/page/aclrole/menuRights/cit"+"/"+options.itemId+"/aclType/"+options.aclType;
			// if(options.itemId)
			// 	_target += "/"+options.itemId;
			// else
			// 	_target += "/0";
			options.target = _target;

			console.log("target");
			console.log(options.target);
	    	this.inherited(arguments);
		},
	getACLCitypes:function()
		{
			return this.gget(this.target+"/"+this.itemId);
		}
	});
});