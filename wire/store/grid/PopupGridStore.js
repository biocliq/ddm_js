/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/wire/store/grid/PopupGridStore", [ "dojo/_base/declare",
		 "../base/JsonRestStore" ], function( declare,
		 JsonRest) {

	var base = JsonRest;

	return declare("ddm.wire.store.grid.PopupGridStore", base, {		
		constructor:function(options){
			if(!options.target){
			    	options.target = "../v2/page/" + options.pageMenu + "/"+options.itemId+"/explorer/container/" 
					+ options.containerId + "/relation";
			}
			this.inherited(arguments);
		},
	
	});
});