/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/wire/store/grid/JsonGridStore", [ "dojo/_base/declare","dojo/_base/lang",
		"dojo/_base/xhr",
		 "../base/JsonRestStore", "ddm/util/config" ], function( declare,lang,
		 xhr, JsonRest, config) {

	var base = JsonRest;

	return declare("ddm/wire/store/grid/JsonGridStore", base, {		
		constructor:function(options){
			this.pageType = options.pageType;
			this.pageMenu = options.pageMenu;			
			this.defaultFilter = options.defaultFilter || {};			
			this.containerName = options.containerName;
			if(options.idProperty)
				this.idProperty = options.idProperty;
			else
				this.idProperty = "_id";
			this.ciId = options.ciId;
			if(!options.target){
				if(!options.pageType)
					this.pageType = options.pageType = "explorer";
				options.target = config.url.getGridDataURL(this.pageMenu, this.containerName, this.ciId);				
			}
			this.inherited(arguments);
		},
		getTableLayout:function()
		{
			var url = config.url.getGridLayoutURL(this.pageMenu, this.containerName, this.ciId);
			return this.gget(url);
		},
		hasChildren:function(id, object){
			return false;
		}, 
		query:function(){
			arguments[0] = lang.mixin(arguments[0], this.defaultFilter);
			this.lastFilter = arguments[0];
			this.lastOptions = arguments[1];			
			return this.inherited(arguments)
		},
		getXlsReport(){			
			this.getReport('excel');
		},
		getReport:function(format){
			var url = config.url.getReportURL(this.pageMenu, this.containerName, this.ciId, format);
			var hasQuestionMark = url.indexOf("?") > -1;
			query = xhr.objectToQuery(this.lastFilter);
			query = query ? (hasQuestionMark ? "&" : "?") + query: "";
			
			var options = this.lastOptions;
			if(options && options.sort){
				var sortParam = this.sortParam;
				query += (query || hasQuestionMark ? "&" : "?") + (sortParam ? sortParam + '=' : "sort(");
				for(var i = 0; i<options.sort.length; i++){
					var sort = options.sort[i];
					query += (i > 0 ? "," : "") + (sort.descending ? this.descendingPrefix : this.ascendingPrefix) + encodeURIComponent(sort.attribute);
				}
				if(!sortParam){
					query += ")";
				}
			}
			url = url + query;			
			window.open(url, '_blank');
		}	
	});
});