/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/wire/store/grid/JsonGridTreeStore", [ "dojo/_base/declare",
		 "../base/JsonRestStore", "ddm/util/config" ], function( declare,
		 JsonRest, config) {

	var base = JsonRest;

	return declare("ddm/wire/store/grid/JsonGridTreeStore", base, {		
		constructor:function(options){
			this.pageType = options.pageType;
			this.pageMenu = options.pageMenu;
			this.containerId = options.containerId;
			this.ciId = options.ciId;
			if(!options.target){
				if(!options.pageType)
					this.pageType = options.pageType = "explorer";
				options.target = config.url.getGridDataURL(this.pageMenu, this.containerId, this.ciId);
				// if("explorer" == options.pageType)
				// 	options.target = "../v2/page/" + options.pageMenu + "/explorer/" + options.ciId 
				// 		+ "/container/" + options.containerId + "/table/data";
				// else
				// 	options.target = "../v2/page/" + options.pageMenu + "/summary/container/" 
				// 	+ options.containerId + "/table/data";
			}
			this.inherited(arguments);
		},
		getTableLayout:function()
		{
			var url = config.url.getGridLayoutURL(this.pageMenu, this.containerId, this.ciId);
			return this.gget(url);
		},
		getRoot: function(onItem, onError) {
			var _t = this;                    
			this.get().then(function(items) {
				var root = {label:this.rootLabel};
					root.children = items;	
					root.hasChild = true;
					root.rootNode = true;
					onItem(root);
				}, onError);
		},
		getLabel:function(object){
			return object.label;
			/** Override this function as required */
		}, 
		mayHaveChildren:function(object){
			// return true;
			return true == object.hasChild;
		}, 
		hasChildren:function(object){
			// return true;
			return true == object.hasChild;
		}, 
		getChildren:function(object, onComplete){
			onComplete(object.children);
		}
	});
});