/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


 define("ddm/wire/store/grid/AttachmentGridStore", [ "dojo/_base/declare",
		 "../base/JsonRestStore" ], function( declare,
		 JsonRest) {

	var base = JsonRest;

	return declare("ddm.wire.store.grid.AttachmentGridStore", base, {		
		constructor:function(options){			
			console.log(options);
			options.target = "v2/page/" + options.pageMenu + "/file/" + options.itemId  + "/table/data";	
			this.inherited(arguments);			
		},hasChildren:function(id, object){
			return false;
		}	
	});
});