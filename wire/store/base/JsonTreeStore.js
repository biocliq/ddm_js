/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/wire/store/base/JsonTreeStore", [ "dojo/_base/xhr", 
		"dojo/_base/declare","dojo/_base/lang",
		"dojo/_base/array","dojo/Deferred", "./JsonRestStore" ], 
		function(xhr, declare,lang,
		array, Deferred, JsonRest) {

	var base = JsonRest;

	return declare("ddm.wire.store.base.JsonTreeStore", base, {
		rootCode :null,
				
		mayHaveChildren: function(object) {
			return object.hasChild;
		},
		hasChildren:function(id, object){
			return object.hasChild;
		},

		getChildren: function(object, onComplete, onError) {
			this._getChildren(object).then(function(fullObject) {
				object.children = fullObject.children;
				object.hasChild = true;
				onComplete(object.children);
			}, function(error) {
				object.children = [];
				onComplete([]);
			});
		},
		_getChildren: function(object) {
			return this.get(object.code + "/children");
		},
		getRoot: function(onItem, onError) {
			var _t = this;
			this.get(this.rootCode).then(function(root) {					
				onItem(root);
				// _t._getChildren(root).then(function(test) {
				// 	root.children = test.children;	
				// 	root.hasChild = true;
				// 
				// }, onError);
			}, onError);
		},
		getObject:function(/*String*/ code, onItem, onError){
			var _t = this;
			this.get(code).then(function(result) {					
					onItem(result);
				}, onError);
		},
		getLabel: function(object) {
			return object.name;
		},
	
		refreshChildren: function(object) {
			var _t = this;
			this._getChildren(object).then(function(fullObject) {
				object.children = fullObject;				
				_t.onChildrenChange(object, object.children);
			});
		},

		onChange: function(/* dojo/data/Item *//* ===== item ===== */) {
		},

		onChildrenChange: function(/* ===== parent, newChildrenList ===== */) {
		},

		pasteItem: function(/*Item*/ childItem, /*Item*/ oldParentItem, /*Item*/ newParentItem,
				/*Boolean*/ bCopy, /*int?*/ insertIndex, /*Item*/ before) {
			var store = this;
			var d = new Deferred();
			console.log("pasteItem called " + bCopy + oldParentItem.menu_name + " " + newParentItem.menu_name + " " + insertIndex);
			if(oldParentItem.id === newParentItem.id && !bCopy){
				var oldChildren = [];
				var _start = false;
				var cnt = 0;
				if(Number.isInteger(insertIndex)){
					if(0 == insertIndex){
						childItem.referenceCIs.parent = newParentItem;
						childItem.display_order = cnt;
						oldChildren.push(childItem);
						var data = {
								   "id" : childItem.id,
								   ciType : childItem.ciType,
								   "referenceCIs" : {
								      "parent" : {
								         "id" : newParentItem.id
								      }
								   },
								   "display_order" : cnt
								};								
						store.put(data);
						console.log("pushed moved child " + data.id + childItem.menu_name);
						cnt++;
					}
					
					array.forEach(oldParentItem.children, function(child){	
						console.log(child.menu_name)
						if(child.id != childItem.id){
							oldChildren.push(child)
							if(child.display_order != cnt){
								var data = {id : child.id, ciType :child.ciType, display_order:cnt};
								store.put(data);
								child.display_order = cnt;
							}
							cnt++;
						}						
						if(cnt == insertIndex){
							childItem.referenceCIs.parent = newParentItem;
							childItem.display_order = cnt;
							oldChildren.push(childItem);
							var data = {
									   "id" : childItem.id,
									   ciType : childItem.ciType,
									   "referenceCIs" : {
									      "parent" : {
									         "id" : newParentItem.id
									      }
									   },
									   "display_order" : cnt
									};								
							store.put(data);
							console.log("pushed moved child " + data.id + childItem.menu_name);
						}	
					});
					oldParentItem.children = oldChildren;
					this.onChildrenChange(oldParentItem, oldChildren);
				}
				
				d.resolve(true);
				return d;
			}
			else if(oldParentItem && !bCopy){
					this.getChildren(oldParentItem, lang.hitch(this, function(oldParentChildren){
						oldParentItem.children = array.filter(oldParentChildren, function(element) {
									return element.id != childItem.id;
								});
						this.onChildrenChange(oldParentItem, oldParentItem.children);
					}));
						//d.resolve(this.put(childItem));
						var cnt = -1;
						var newChildren = [];
						
						if(!Number.isInteger(insertIndex)){
							insertIndex = 0;
							newChildren.push(childItem);
							var data = {
									   "id" : childItem.id,
									   ciType : childItem.ciType,
									   "referenceCIs" : {
									      "parent" : {
									         "id" : newParentItem.id
									      }
									   },
									   "display_order" : cnt
									};		
							console.log("pushing data" + childItem.menu_name);
							store.put(data);
						}else
						array.forEach(newParentItem.children, function(child){	
							console.log(child);
							cnt++;
							if(cnt == insertIndex){
								childItem.referenceCIs.parent = newParentItem;
								childItem.display_order = cnt;
								newChildren.push(childItem);
								var data = {
										   "id" : childItem.id,
										   ciType : childItem.ciType,
										   "referenceCIs" : {
										      "parent" : {
										         "id" : newParentItem.id
										      }
										   },
										   "display_order" : cnt
										};		
								console.log("pushing data" + childItem.menu_name);
								store.put(data);
							}else if(cnt > insertIndex){
								child.display_order = cnt;
								var data = {id : child.id, ciType :child.ciType, display_order:cnt};
								console.log("pushing data" + child.menu_name);
								store.put(data);
							}
							newChildren.push(child);
						
						});
//						newParentItem.children = newChildren;
//						this.onChildrenChange(newParentItem, newChildren);
						this.refreshChildren(newParentItem);
				
			}else{
				d.resolve(this.put(childItem, {
					overwrite: true,
					parent: newParentItem,
					oldParent: oldParentItem,
					before: before
				}));
			}

			return d;

		},
		put: function(object, options) {
			return dojo.store.JsonRest.prototype.put.apply(this, arguments);
		}
	});

});