/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


define("ddm/wire/store/base/JsonUserStore", [ 
	"dojo/_base/declare",
	"./JsonRestStore", "ddm/util/config" ], 
	function(declare, JsonRest, config) {
/**
 * This class will be used by Explorer Page to retrieve the page tabs and other associated contents
 */
	var baseClass = JsonRest;

	return declare("ddm.wire.store.base.JsonUserStore", baseClass, {
				
		constructor:function(options){			
				options.target = config.url.base ;
			this.inherited(arguments);
		},
		getUserItem : function(){
    		return this.gget(this.target + config.url.userInfo);
    	},
    	changePwd : function(){
    		return this.gget(this.target + config.url.passwdChange);
		},
		addUserRelation : function(values){
			return this.gget(this.target+ "user/" + values.sourcePage+"/"+values.sourceId+"/"+values.destinationPage+"/"+values.destinationId)
		},
		deleteUserRelation : function(id,page){
			return this.gget(this.target + "user/" +page+"/"+id);
		},
		getFacilities : function(){
			return this.gget(this.target + config.url.userFacilities);
		}
	});
});
