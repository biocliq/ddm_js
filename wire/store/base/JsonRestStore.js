/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/wire/store/base/JsonRestStore", [ 
	"dojo/_base/xhr", "dojo/_base/declare",
	"dojo/_base/lang",
	 "dojo/topic", "dojo/store/JsonRest" ], function(xhr, declare,lang,
		 topic, JsonRest) {

	var base = JsonRest;

	return declare("ddm/wire/store/base/JsonRestStore", base, {

		constructor: function(options){
			this.headers = {preventCache: true, 'Content-Type' : "application/json"};
			declare.safeMixin(this, options);
		}, 
		add:function(){
			var result = this.inherited(arguments);
			this.addErrBack(result)
			return result;
		},
		put:function(){
			var result = this.inherited(arguments);
			this.addErrBack(result)
			return result;
		},
		get:function(){
			var result = this.inherited(arguments);
			this.addErrBack(result)
			return result;
		},
		remove:function(){
			var result = this.inherited(arguments);
			this.addErrBack(result)
			return result;
		},
		query: function(){
			var result = this.inherited(arguments);
			this.addErrBack(result)
			return result;
		},
		gget: function(url, options){
			var options = options || {};
			var headers = lang.mixin({ Accept: this.accepts }, this.headers, options.headers || options);
			var result =  xhr("GET", {
				url: url,
				handleAs: "json",
				preventCache : true,
				headers: headers
			});
			this.addErrBack(result);
			return result;
		},
		
		gaction: function(url, options,postData, action){
			var options = options || {};
			var headers = lang.mixin({ "X-Action": action, Accept: this.accepts, "Content-Type": "application/json" }
									, this.headers
									, options.headers || options);

			var result =  xhr(action, {
				url: url,
				postData:JSON.stringify(postData),
				handleAs: "json",
				preventCache : true,
				headers: headers
			});
			this.addErrBack(result);
			return result;
		},
		gpost: function(url, options, postData){
			return this._gsend(url, options, postData, "POST");
		},
		gdelete: function(url, options, postData){
			return this._gsend(url, options, postData, "DELETE");
		},
		gput: function(url, options, postData){
			return this._gsend(url, options, postData, "PUT");
		},
		addErrBack(result){
			result.addErrback(function(error){
				var httpErrorCode = error.status;
				topic.publish("/server/error/" + httpErrorCode, error);
			});
		},
		_gsend: function(url, options, postData, action){
			var options = options || {};
			var headers = lang.mixin({"Content-Type": "application/json", Accept: this.accepts }, 
							this.headers, options.headers || options);
			console.log(headers);
			var options = {
				url: url,			
				handleAs: "json",
				preventCache : true,
				headers: headers
			};
			if("PUT" == action){
				options.putData = JSON.stringify(postData);
			}else{
				options.postData = JSON.stringify(postData);
			}
			var result =  xhr(action, options);
			this.addErrBack(result);
			return result;
		}
	});
});