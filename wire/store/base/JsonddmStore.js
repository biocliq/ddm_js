/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/wire/store/base/JsonddmStore", [ 
	"dojo/_base/declare", "./JsonRestStore", "ddm/util/config" ], 
	function(declare, JsonRest, config) {
/**
 * This class will be used by Explorer Page to retrieve the page tabs and other associated contents 
 */
	var base = JsonRest;

	return declare("ddm.wire.store.base.JsonddmStore", base, {
		_explorer : "explorer",
		_summary : "summary",
		_dashboard:"dashboard",
		_tableContainer : "/container/",
		
		constructor:function(options){
			if(!options.target && options.page)
				options.target = config.url.basePage + options.page + "/";	
				
			this.headers = {preventCache: true};
			declare.safeMixin(this, options);		
			// this.inherited(arguments);
		},			
		getExplorerTab : function(/** CI Id **/ ciId, /** page tab Id **/ tabId, options){
			return this._getTab(this._explorer, ciId, tabId, options);
		},
		getDashboardTab : function(/** CI Id **/ ciId, /** page tab Id **/ tabId, options){
			return this._getTab(this._dashboard, ciId, tabId, options);
		},
		get: function(/** CI Id **/ ciId, options){
			return this._getTab(this._explorer, ciId, options);
		},
		validateField:function(pageMenu, /** CI Id **/ ciId, fieldId, value, options){
			var _target = config.url.basePage+pageMenu+"/explorer";
			if(ciId){
				_target +=  "/" + ciId;
			}
			_target += "/field/" + fieldId + "?value=" + encodeURIComponent(value);		
			return this.gget(_target, options);
		},
		getAclData : function(sid,acltype,typeId){
			if(undefined == typeId && null == typeId)
				typeId= 0;
			this._explorer = this._explorer+"/container/"+sid+"/"+acltype+"/"+typeId;
			return this._getTab(this._explorer,undefined, undefined, undefined);
		},
		getSummaryTab : function( /** page tab Id **/ tabId, options){
			return this._getTab(this._summary,undefined, tabId, options);
		},
		_getTab : function(/**explore/summary **/ base, /** CI Id **/ ciId, /** page tab Id **/ tabId, options) {			
			var _target = this.target + base;
			if(ciId){
				if(tabId)
					_target += "/" + ciId + "/tab/" + tabId;
				else
					_target +=  "/" + ciId;
			}else{
				if(tabId)
					_target += "/tab/" + tabId;
			}			
			return this.gget(_target, options);
		}
	});

});