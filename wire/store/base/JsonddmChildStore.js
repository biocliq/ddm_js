/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

define("ddm/wire/store/base/JsonddmChildStore", [ 
	"dojo/_base/declare", "./JsonddmStore", "ddm/util/config" ], 
	function(declare, DdmStore, config) {
/**
 * This class will be used by Explorer Page to retrieve the page tabs and other associated contents 
 */
	var base = DdmStore;

	return declare("ddm.wire.store.base.JsonddmChildStore", base, {
        parentId: null,
        parentMenu: null,
        constructor:function(options){            
			// if(!options.target && options.page)
            options.target = config.url.basePage + options.parentMenu + "/" + options.parentId 
                + "/" + options.containerName + "/child/" + options.page +"/";	
				
			this.headers = {preventCache: true};
            declare.safeMixin(this, options);            
			// this.inherited(arguments);
		},
		_getTab : function(/**explore/summary **/ base, /** CI Id **/ ciId, /** page tab Id **/ tabId, options) {			
            var _target = this.target + base;
            console.log(_target);
			if(ciId){
				if(tabId)
					_target += "/" + ciId + "/tab/" + tabId;
				else
					_target +=  "/" + ciId;
			}else{
				if(tabId)
					_target += "/tab/" + tabId;
			}			
			return this.gget(_target, options);
		}
	});

});