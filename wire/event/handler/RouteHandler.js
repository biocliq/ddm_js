/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


require([ "dojo/_base/declare", "dojo/_base/lang","dojo/topic",
    "dojo/router", "dojo/on",  "dojo/store/Observable",
    "dojo/store/Memory", "dijit/registry","dijit/layout/ContentPane" ,
    "dijit/layout/StackContainer", "ddm/wire/store/base/JsonTreeStore", 
    "ddm/wire/store/base/JsonddmStore",	"ddm/util/config", 
    "ddm/zui/widget/Dialog", "ddm/zui/page/base/Base", "dojo/domReady!"],
        
    function(declare, lang, topic,
        router, on,Observable,
        Memory, registry, ContentPane,
        StackContainer, JsonTreeStore,
        ddmStore, config, 
        Dialog,  WidgetBase) {
        
        var widgetFactory = new WidgetBase();
        
        var work = registry.byId("workArea");

        dojo.connect(work,"_transition", function(newPage, oldPage){
            var title = newPage.get('title');            
            if(title)
                document.title = title;
            else
                document.title = newPage.title;
            if(newPage.resize)
                newPage.resize();
        });
        
        function createWidget(/*String*/ widgetName, /*Object*/options){                
            var workArea = registry.byId("workArea");
            if(!workArea){
                console.error('workArea cannot be loaded');
                return;
            }
            var widget = registry.byId(options.id);
            if(!widget){
                widgetFactory.createWidget(widgetName, options, function(widget){
                    workArea.addChild(widget);						
                    workArea.selectChild(widget);
                })
            }else
                workArea.selectChild(widget);
        }

        function createReplaceWidget(/*String*/ widgetName, /*Object*/options){                
            var workArea = registry.byId("workArea");
            if(!workArea){
                console.error('workArea cannot be loaded');
                return;
            }            
            var widget = registry.byId(options.id);
            if(widget){
                workArea.removeChild(widget);
                widget.destroy();
                delete widget;
            }
            widgetFactory.createWidget(widgetName, options, function(widget){
                workArea.addChild(widget);						
                workArea.selectChild(widget);
            });
        }
                
        function widgetExists(/* String*/ id){
            var widget = registry.byId(id);
            if(!widget){
                return false;
            }else{
                var workArea = registry.byId("workArea");
                workArea.selectChild(widget);
            }
            return true;
        }

        function getWidgetName(/*Page Object*/page, defaultRenderer){            
            if(page.renderer){
                /*get the custom renderer for the page*/
                return page.renderer;
            }
            return defaultRenderer;
        }
            
        function createExplorerPage(/*String*/pageMenu,/*int*/ mode, /*int*/ itemId, /*Object */ args){
            var uq ;    
            var page = config.page.explorer;
            if(!itemId){
                uq = 'exp_' +pageMenu + config.getNewFormId();
                page += "New";
            }else
                uq = pageMenu + itemId;

            var citStore = new ddmStore({
                page : pageMenu 
            });

            citStore.get(itemId).then(function(item){
                var options = {page : pageMenu, itemId : itemId, item:item,pageType:"explorer", citStore:citStore,
                        id : uq,closable : false, widgetId : uq, mode:mode, args:args, title: 'test'};                
                    var explorerPage = getWidgetName(item, page);
                    createReplaceWidget(explorerPage, options);	
                });
        }

        function createDashboardPage(/*String*/pageMenu,/*int*/ mode, /*int*/ itemId, /*Object */ args){            
            if(!itemId){
                return;
            }
            var uq = 'dashboard_' + pageMenu + itemId;            
            if(widgetExists(uq))
                return;
                
            var defaultRenderer = "ddm/zui/page/base/dashboard/DashboardPage";
            var citStore = new ddmStore({
                page : pageMenu 
            });            
            citStore.getDashboardTab(itemId).then(function(item){
                var options = {page : pageMenu, itemId : itemId, item:item,pageType:"explorer", citStore:citStore,
                        id : uq,closable : false, widgetId : uq, mode:mode, args:args, title: 'test'};                
                    var dashboardPage = getWidgetName(item, defaultRenderer);
                    
                    createWidget(dashboardPage, options);	
                });
        }

        function createSummaryDashboard(/*String*/pageMenu, /*Object */ args){                        
            var uq = 'dashboard_' + pageMenu;
            if(widgetExists(uq))
                return;

            var citStore = new ddmStore({
                page : pageMenu 
            });
            var defaultRenderer = "ddm/zui/page/base/dashboard/DashboardPage";
            
            citStore.getDashboardTab().then(function(item){
                var options = {page : pageMenu, item:item,pageType:"explorer", citStore:citStore,
                        id : uq,closable : false, widgetId : uq, args:args, title: 'test'};                
                    var dashboardPage;
                    var dashboardPage = getWidgetName(item, defaultRenderer);
                    
                    createWidget(dashboardPage, options);	
                });
        }
            
        function createSummaryPage(/*String*/menuCode, path){
            var uq = 'summary_' + menuCode;
            if(! widgetExists(uq)){
                var citStore = new ddmStore({
                    page : menuCode
                });
                citStore.getSummaryTab().then(function(item){                    
                    var options = { pageType:"summary", id : uq,closable : true,
                        doLayout:false, data:item, title:item.label, page : menuCode,tgtPage: item.tgtMenuCode,
                        citStore:citStore,url:path};
                    createWidget(getWidgetName(item, config.page.summary), options);
                });
            }
        }

        function createScheduler(){	
            var key = "scheduler";
            var curTS = new Date().getTime();
            var diff = 15;
            var someData = [{
                                id: 0,
                                patient:1,
                                summary: "Kumaran",
                                editable:false,
                                startTime: new Date(curTS - 1415 * 60000),
                                endTime: new Date(curTS  - 1400*60000), 
                            }, 
                            {
                                id: 1,
                                patient:1,
                                summary: "Kumaran",
                                startTime: new Date(curTS),
                                endTime: new Date(curTS + diff*60000), 
                            }, 
                            {
                                id: 2,
                                patient:2,
                                summary: "Selvi",
                                startTime: new Date(curTS + 60*60000),
                                endTime: new Date(curTS + (60+diff)*60000), 
                            }, 
                            {
                                id: 3,
                                patient:3,
                                summary: "Sukumari - diagnostics",
                                startTime: new Date(curTS),
                                endTime: new Date(curTS + diff*60000), 
                            }, 
                            {
                                id: 4,
                                patient:3,
                                summary: "Sukumari",
                                startTime: new Date(curTS + 1400 * 60000),
                                endTime: new Date(curTS + 1415*60000), 
                            }];

            if(! widgetExists(key)){				
                var curTime = new Date();
                var options = {id: key, title:"Schedule", doLayout:false,
                    style:"position:relative;width:400px;height:300px",
                    store: new Observable(new Memory({data: someData})),
                    closable : true, dateInterval: "day", 
                    timeSlotDuration:5,					
                    columnViewProps:{minHours:0, maxHours:24, hourSize:150,timeSlotDuration:5,
                        startTimeOfDay:{hours: (curTime.getHours()), duration:4000}								
                    }, 
                    onItemClick:function(evt){
                        var item = evt.item;
                        router.go("/page/patients/view/" + item.patient);
                    }, isItemEditable: function(item, renderer){
                        return item.editable != false;
                    },
                };
                createWidget("dojox/calendar/Calendar", options);
            }
        }

        router.register("/page/:pageMenu/summary", function(evt){            
            createSummaryPage(evt.params.pageMenu, evt.newPath);
        });

        router.register("/page/schedule/:action", function(evt){					
            createScheduler();
        });

        router.register("/page/:pageMenu/view/:itemId", function(evt){					
            createExplorerPage(evt.params.pageMenu,1, evt.params.itemId);
        });

        router.register("/page/:pageMenu/dashboard", function(evt){					
            createSummaryDashboard(evt.params.pageMenu);
        });

        router.register("/page/:pageMenu/dashboard/:itemId", function(evt){					
            createDashboardPage(evt.params.pageMenu,1, evt.params.itemId);
        });
        router.register("/page/:pageMenu/edit/:itemId", function(evt){					
            createExplorerPage(evt.params.pageMenu,2, evt.params.itemId);
        });
        router.register("/page/:pageMenu/new", function(evt){					
            createExplorerPage(evt.params.pageMenu,3);
        });
        //router.startup();
        var start = topic.subscribe("/load/workArea", function(/*String*/status){            
            router.startup();
            start.remove();
            delete start;
        });
});