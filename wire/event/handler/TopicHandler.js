/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

require(["dojo/_base/declare", "dojo/_base/lang", "dojo/router",
    "dojo/topic", "dojo/on", "dojo/store/Observable", "dojo/store/Memory",
    "dijit/registry", "dijit/layout/ContentPane",
    "ddm/wire/store/base/JsonTreeStore", "ddm/wire/store/base/JsonddmStore",
    "ddm/wire/store/base/JsonddmChildStore",
    "ddm/util/config", "ddm/zui/widget/Dialog", "ddm/zui/page/base/Base",
    "dojo/domReady!"],
    function (declare, lang, router,
        topic, on, Observable, Memory,
        registry, ContentPane,
        JsonTreeStore, ddmStore,
        ddmChildStore,
        config, Dialog, WidgetBase) {
        var widgetFactory = new WidgetBase();

        function createDialog(/*String*/ widgetName, /*Object*/options) {
            widgetFactory.createWidget(widgetName, options, function (widget) {
                var dialog = new Dialog({ style: 'width: 75%; height:75%;' });
                dialog.own(on(dialog, 'hide',
                    lang.hitch(dialog, function (value) {
                        dialog.destroyRecursive();
                    })
                ));
                if (options.title)
                    dialog.set('title', options.title);

                dialog.addChild(widget);
                dialog.show();
                if (widget.focus) {
                    setTimeout(function () { widget.focus(); }, 300);
                }
            });
        }

        function createFormDialog(/*String*/ widgetName, /*Object*/options, style) {
            widgetFactory.createWidget(widgetName, options, function (widget) {
                var _dialogStyle = style || { style: 'width: 95%; height:95%;' };
                var dialog = new Dialog(_dialogStyle);
                dialog.own(on(dialog, 'hide',
                    lang.hitch(dialog, function (value) {
                        dialog.destroyRecursive();
                    })
                ));
                if (options.title)
                    dialog.set('title', options.title);

                dialog.addChild(widget);
                dialog.show();
                if (widget.focus) {
                    setTimeout(function () { widget.focus(); }, 300);
                }
            });
        }

        function getSummaryWidgetName(/*Menu Object*/menuItem) {
            if (menuItem.summary) {
                /*get the custom pages for menu if any*/
                return menuItem.summary;
            }
            return config.page.summary;
        }

        function createExplorerDialog(/*String*/pageMenu,/*int*/ mode, /*int*/ itemId,
                /*Object */ args, /*function*/ callback) {
            var uq;
            if (!itemId) {
                uq = pageMenu + config.getNewFormId();
            } else
                uq = "ch" + pageMenu + itemId;

            var citStore;
            if (undefined == args || undefined == args.parentId) {
                citStore = new ddmStore({
                    page: pageMenu
                });
            } else {
                args.page = pageMenu;
                citStore = new ddmChildStore(args);
            }

            // Pass the parentId - for combobox selection limitation.
            var parentId = args.parentId;

            citStore.get(itemId).then(function (item) {
                var title;
                if (itemId) {
                    title = item.label;
                }
                else
                    title = 'Add ' + item.label;

                var options = {
                    page: pageMenu, itemId: itemId, item: item, pageType: "explorer", citStore: citStore,
                    id: uq, closable: true, widgetId: uq, mode: mode, args: args, title: title, parentId: parentId,
                    onSave: function () {
                        if (callback)
                            callback(arguments);
                        this.getParent().hide();
                    }
                };

                var menuModel = new JsonTreeStore({
                    target: config.url.menu
                });

                menuModel.getObject(pageMenu, function (item) {
                    var explorerPage;
                    if (item.renderer && item.renderer != "") {
                        //custom 
                        explorerPage = item.renderer;
                    } else
                        explorerPage = "ddm/zui/page/base/explorer/PopupExplorerPage";
                    createDialog(explorerPage, options);
                });
            });
        }

        function createExplorerAclDialog(sid, aclType, ciTypeData) {
            var uq;
            if (!aclType) {
                uq = sid + config.getNewFormId();
            } else
                uq = sid + aclType;

            var citStore = new ddmStore({
                page: sid
            });
            citStore.getAclData(sid, aclType, ciTypeData.id).then(function (item) {

                var options = {
                    page: sid, itemId: aclType, item: item, pageType: "explorer", citStore: citStore,
                    id: uq, closable: true, widgetId: uq, mode: ciTypeData.id, args: ciTypeData, title: item.label
                };
                explorerPage = "ddm/zui/page/base/explorer/PopupExplorerPage";
                createDialog(explorerPage, options);
            });
        }

        function createNavigatableDialog(/*String*/pageMenu,/*int*/ mode, /*int*/ itemId,
            /*Object */ args, /*function*/ callback) {
            var uq;
            if (!itemId) {
                uq = pageMenu + config.getNewFormId();
            } else
                uq = "ch" + pageMenu + itemId;

            var citStore;
            if (undefined == args || undefined == args.parentId) {
                citStore = new ddmStore({
                    page: pageMenu
                });
            } else {
                args.page = pageMenu;
                citStore = new ddmChildStore(args);
            }

            // Pass the parentId - for combobox selection limitation.
            var parentId = args.parentId;

            citStore.get(itemId).then(function (item) {
                var title;
                if (item.label)
                    title = item.label;

                var options = {
                    page: pageMenu, itemId: itemId, item: item, pageType: "explorer", citStore: citStore,
                    id: uq, closable: true, widgetId: uq, mode: mode, args: args, title: title, parentId: parentId
                };

                var menuModel = new JsonTreeStore({
                    target: config.url.menu
                });

                var explorerPage;
                if (item.renderer && item.renderer != "") {
                    //custom
                    explorerPage = item.renderer;
                } else
                    explorerPage = "ddm/zui/page/base/explorer/NavigatablePage";

                createFormDialog(explorerPage, options);

                // menuModel.getObject(pageMenu, function(item){
                //     var explorerPage;
                //     console.log(item);
                //     if(item.renderer && item.renderer != ""){
                //         //custom 
                //         explorerPage = item.renderer;
                //     }else
                //         explorerPage = "ddm/zui/page/base/explorer/NavigatablePage";                    
                //         createFormDialog(explorerPage, options);	
                // });	
            });
        }

        /** Subscribe to various events */
        topic.subscribe("/form/view", function (/*int*/ itemId, /*String*/pageMenu) {
            if (itemId && itemId != 'undefined')
                router.go("/page/" + pageMenu + "/view/" + itemId);
            else
                console.log("Id not defined for the record ");
        });

        topic.subscribe("/form/edit", function (/*int*/ itemId, /*String*/pageMenu) {
            if (itemId && itemId != 'undefined')
                router.go("/page/" + pageMenu + "/edit/" + itemId);
            else
                console.log("Id not defined for the record ");
        });

        topic.subscribe("/form/new", function (/*String*/pageMenu, /*Object*/ args) {
            router.go("/page/" + pageMenu + "/new");
        });

        topic.subscribe("/popup/page/new/", function (/*String pageMenu */ pageMenu, options, callback) {
            createExplorerDialog(pageMenu, 3, undefined, options, callback);
        });

        topic.subscribe("/popup/form/view/", function (/*String pageMenu */ tgtPageMenu, ciId, options, callback) {
            if (ciId && ciId != 'undefined') {
                if(options.navigatable == false)
                    createExplorerDialog(tgtPageMenu, 1, ciId, options, callback);
                else
                    createNavigatableDialog(tgtPageMenu, 1, ciId, options, callback);                
            }
            else {
                console.log("Id not defined for the record ");
            }
        });

        topic.subscribe("/popup/page/edit/", function (/*String pageMenu */ tgtPageMenu, ciId, options, callback) {
            if (ciId && ciId != 'undefined')
                createExplorerDialog(tgtPageMenu, 1, ciId, options, callback);
            else
                console.log("Id not defined for the record ");
        });

        topic.subscribe("/popup/page/fieldacl/", function (ciTypeData) {
            createExplorerAclDialog(ciTypeData.sid, 'field', ciTypeData);
        });


        topic.subscribe("/chart/popup/grid", function (options, callback) {
            if (options.renderer) {
                createFormDialog(options.renderer, options, { style: 'width: 80%; height:80%;' });
            }
        });
    });
