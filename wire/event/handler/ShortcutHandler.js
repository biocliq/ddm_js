/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */


require([ "dojo/_base/declare", "dojo/_base/lang","dojo/topic",
    "dojo/router", "dojo/on",  "dojo/store/Observable","dojo/hash",
    "dojo/store/Memory", "dijit/registry","dijit/layout/ContentPane" ,
    "dijit/layout/StackContainer", "ddm/wire/store/base/JsonTreeStore", 
    "ddm/wire/store/base/JsonddmStore",	"ddm/util/config", 
    "ddm/zui/widget/Dialog", "ddm/zui/page/base/Base", "dojo/domReady!"],
        
    function(declare, lang, topic,
        router, on,Observable,hash,
        Memory, registry, ContentPane,
        StackContainer, JsonTreeStore,
        ddmStore, config, 
        Dialog,  WidgetBase) {

			function openNewRecord(){
				var currentRoute = hash();
				if(currentRoute.startsWith("/page/")){
					currentRoute = currentRoute.replace("/page/", "");
					var page = currentRoute.substring(0, currentRoute.indexOf("/"));
					topic.publish("/form/new", page);
				}
			}

			function openRecord(){
				var currentRoute = hash();
				var workarea = dijit.byId("workArea");
				if(currentRoute.startsWith("/page/")){
					currentRoute = currentRoute.replace("/page/", "");
					var page = currentRoute.substring(0, currentRoute.indexOf("/"));
					var summary = workarea.selectedChildWidget;
					console.log(summary);
					topic.publish("/form/view", 'sdf', page);
				}
			}

			function saveRecord(){
				var workarea = dijit.byId("workArea");
				var form = workarea.selectedChildWidget;				
				if(form && form.canSave && form.saveForm){
					if(form.canSave()){
						form.saveForm({});
					}
				}
			}

			function editRecord(){
				var workarea = dijit.byId("workArea");
				var form = workarea.selectedChildWidget;				
				if(form && form.canEdit && form.editForm){
					if(form.canEdit()){
						form.editForm();
						form.focus();
					}
				}
			}

			function closeRecord(){
				var workarea = dijit.byId("workArea");
				var form = workarea.selectedChildWidget;				
				if(form && form.closeForm){
					form.closeForm();
				}
			}

			shortcut.add("Alt+s", function(){
				saveRecord();
			})

			shortcut.add("Alt+n", function(){
				openNewRecord();
			})

			shortcut.add("Alt+a", function(){
				openNewRecord();
			})

			shortcut.add("Alt+e", function(){
				editRecord();
			})

			shortcut.add("Alt+c", function(){
				closeRecord();
			})

			// shortcut.add("Alt+enter", function(){
			// 	openRecord();
			// })
});