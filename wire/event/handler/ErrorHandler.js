/** 
 * (C) Copyright 2018 BioCliq Technologies Pvt. Ltd India. All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 *
 * Author k.raja@biocliq.com
 */

//	Module to handle all the error events.

require([ "dojo/topic","dijit/Dialog",
	"dijit/form/Button", "ddm/util/config", "notifier/Notifier"],
	function(topic,Dialog,
		Button, config, Notifier) {
		var sessionDialog = new Dialog({
			title: "Session Time-Out",
			content: "<div>Your session may have expired. Please Login to continue</div>",
			style: "width: 300px;padding-bottom:10px;",
			onHide: function() {
				location.href = config.url.loginPage;
			 }
		});
		var notifier = new Notifier();
		var relogin=new Button({
			style:"float:right;",
			label: "Login",
				onClick: function(){
						location.href = config.url.loginPage;
				}
			});
			sessionDialog.addChild(relogin);


		var notify = function(mode, error){
			console.log(error.responseText);
			var response = JSON.parse(error.responseText);
			var message = response.errorCode + ":" + response.message;

			var notification = notifier.notify(mode, message);
			notification.push();
		}

		topic.subscribe("/server/error/502", function(){
			alert("Application server is down.  please try again after some time");
		});

		
		topic.subscribe(["/server/error/403"], function(error){
			console.log(error);
			var notification = notifier.notify("warning", "The record you are trying to reach is not accessible");
			notification.push();
		});

		topic.subscribe([ "/server/error/401"], function(error){
			sessionDialog.show();
		});

		topic.subscribe(["/server/error/403", "/server/error/404"], function(error){
			var notification = notifier.notify("error", "The record you are trying to read is not found. Please check the configuration");
			notification.push();
		});

		topic.subscribe(["/server/error/500","/server/error/409"], function(error){			
			notify("error", error);
			// var notification = notifier.notify("error", "A server error has occured, Please try again later."
			// +" If the problem persists, contact the site administrator");
			// notification.push();
		});

		topic.subscribe(["/client/script"], function(error){
			var notification = notifier.notify("error", error);
			notification.push();
		});

		topic.subscribe(["/client/info"], function(info){
			var notification = notifier.notify("info", info);
			notification.push();
		});

		topic.subscribe(["/client/warn"], function(info){
			var notification = notifier.notify("warning", info);
			notification.push();
		});

		topic.subscribe(["/server/error/400"], function(error){
			notify("error", error);			
		});
	}
);
